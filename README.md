# [ORACLE-ADF.RU SOURCE CODES](https://oracle-adf.ru) source codes

<br/>

### Запустить как сервис

    # vi /etc/systemd/system/oracle-adf.ru.service

вставить содержимое файла oracle-adf.ru.service

    # systemctl enable oracle-adf.ru.service
    # systemctl start oracle-adf.ru.service
    # systemctl status oracle-adf.ru.service

http://localhost:4006
