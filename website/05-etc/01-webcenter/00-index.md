---
layout: page
title: WebCenter
description: WebCenter
keywords: Oracle ADF, WebCenter
permalink: /webcenter/
---

<br/>

# WebCenter

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/zyC0_VjbXXY" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/xpaZ1r8S-_8" frameborder="0" allowfullscreen></iframe>
</div>

<div align="center">
	<iframe width="853" height="480" src="http://www.youtube.com/watch?v=YYvrImP6va8" frameborder="0" allowfullscreen></iframe>
</div>
