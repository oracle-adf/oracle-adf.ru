---
layout: page
title: Вопросы без решения
description: Вопросы без решения
keywords: Oracle ADF, Вопросы без решения
permalink: /issue/not-solved/
---

# Вопросы без решения

<br/>

UPD: Вышла новая версия Jdeveloper 12.2.1.1.0. После установки в Tools --> Preferences --> Environment --> Encoding --> UTF8. И перезагрузки проекта из subversion, в консоли стали печататься русские буквы.

<br/>

**Возможное решение, которое я уже не могу попробовать**

Скорее всего нужно было менять в настройках операционной системы язык по умолчанию для non Unicode или как-то так. Потом удалять каталог с файлами Jdeveloper (${MyUserName}\AppData\Roaming\JDeveloper\) и сам Jdeveloper.

<br/>

### HELP!!! Как Embedded Weblogic заставить в консоль писать русские буквы?

Имеем: Windows 10 (ENG), Jdeveloper 12.2.1

Я подключился к проекту, где уже люди много чего создали.
Все было настроено не так как обычно делаю я. Т.е. и в настройках Jdeveloper и компиляторах проекта были заданы значения по умолчанию.

Вообмщем имеем следующее.

Не удается в Embedded Weblogic заставить в консоль писать русские буквы и сообщения об ошибках на английском.

@#$%^& wtf. На английской Windows с английским интерфейсом, jdeveloper пытается писать мне ошибки на русском и вместо русских букв ставит вопросы.

Ну какого **\*** , я в 2016 году должен еще **\*** с кодировками?

В настройках Windows установлен язык для non unicode - русский.
В настройках Jdeveloper - установлена кодировка - UTF-8
В каждом из субпроектов указано, что компиляция в UTF8

В конфиге jdev.conf

    AddVMOption -Duser.language=en
    AddVMOption -Duser.country=US

В конфиге jdk.conf

     AddVMOption -Dfile.encoding=UTF-8

И всеравно при выполнении, в консоли печатаются символы в cp1251.

Приходится перекодировать текст следующей командой.

    new String(myvar.getBytes("UTF8")

Это не особо удобно.  
Будут какие-нибудь рекомендации?

UPD.

Далее, мне подсказали, что нужно копать локальный weblogic. Он хоть и встроенный, но лежик в каталогах, где его можно найти и поковырять конфиги.

C:\Users\Username\AppData\Roaming\JDeveloper\system12.2.1.0.42.151011.0031\DefaultDomain\bin\startWebLogic.cmd

    SETLOCAL
    chcp 1251

<br/>

    set JAVA_OPTIONS=%JAVA_OPTIONS% -Duser.country=US -Duser.language=en
    set SAVE_JAVA_OPTIONS=%JAVA_OPTIONS%

    echo ***************************************************
    echo ** JAVA_OPTIONS=%JAVA_OPTIONS%
    echo ***************************************************

В JAVA_OPTIONS имеются переданные параметры.

И пока всеравно не работает.

Вот, что получаю:

    WARNING: DVT-23004 �� ������ ����� ��������� ��� ��������.

И тоже самое при распечатке данных из VO с данными в российской кодировке.

<br/><br/>

Делаю следующее:

    import java.io.ByteArrayOutputStream;
    import java.io.OutputStreamWriter;

    import java.nio.charset.Charset;

<br/>

    System.out.println(String.format("========================="));
    System.out.println(String.format("========================="));

    System.out.println("Default Charset=" + Charset.defaultCharset());
    // System.setProperty("file.encoding", "UTF8");
    System.out.println("file.encoding=" + System.getProperty("file.encoding"));
    System.out.println("Default Charset=" + Charset.defaultCharset());
    System.out.println("Default Charset in Use=" + getDefaultCharSet());

    System.out.println(String.format("========================="));
    System.out.println(String.format("Привет!"));
    System.out.println(String.format("========================="));

    System.out.println(String.format("========================="));
    try {
    	System.out.println(String.format(new String("Привет".getBytes("UTF8"))));
    } catch (UnsupportedEncodingException e) {
    }
    System.out.println(String.format("========================="));

<br/>

    private static String getDefaultCharSet() {
    	OutputStreamWriter writer = new OutputStreamWriter(new ByteArrayOutputStream());
    	String enc = writer.getEncoding();
    	return enc;
    }

На выходе:

    =========================
    =========================
    Default Charset=windows-1251
    file.encoding=Cp1251
    Default Charset=windows-1251
    Default Charset in Use=Cp1251
    =========================
    ������!
    =========================
    =========================
    Привет
    =========================

<br/>

Вот так у меня выводятся VO в консоли (по умолчанию).

    ==================
    === Result Sets=====
    2	����� �����	2016-01-21	3
    9	����������� ���������	2015-09-09	1
    10	����������� ������	2016-01-20	1
    2	����� �����	2016-01-19	1
    6	�� ��������� ������ � �������� � ��������	2016-01-18	2
    8	���������� � ��������	2016-01-20	2
    9	����������� ���������	2016-01-18	1
    2	����� �����	2016-01-19 15:33:19.0	1
    2	����� �����	2016-01-01	1
    5	���������� FTP	2016-01-21	1
    6	�� ��������� ������ � �������� � ��������	2016-01-21	1
    9	����������� ���������	2016-01-21	1
    2	����� �����	2016-01-20	1
    1	����� �������	2016-01-01	1
    3	����� ��	2015-12-10	1
    4	����� ���������	2016-01-18	2
    2	����� �����	2016-01-19 11:57:49.0	1
    7	�������������������� �������� ��������	2016-01-18	1
    2	����� �����	2016-01-19 15:43:44.0	2
    2	����� �����	2016-01-19 12:57:31.0	2
    7	�������������������� �������� ��������	2016-01-21	1
    8	���������� � ��������	2016-01-21	1
    8	���������� � ��������	2015-09-09	1
    9	����������� ���������	2015-11-25	1
    7	�������������������� �������� ��������	2015-09-10	1
    10	����������� ������	2016-01-18	2
    === Result Sets END =====
