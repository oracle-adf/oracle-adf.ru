---
layout: page
title: Вопросы решенные
description: Вопросы решенные
keywords: Oracle ADF, Вопросы решенные
permalink: /issue/solved/
---

# Вопросы решенные

<br/>

### ViewCriteria возвращает Null вместо даты

Если к VO выполняется ExecuteWithParams, далее если я пытаюсь работать с этим VO с помощь VC, получаю в качестве значений VC null.

Вызываю тот же самый метод.

После вызова ExecuteWithParams.

    ---Variables:
    ***
      --- Name: firstDayOfMonth Value: null
      --- Name: lastDayOfMonth Value: null
      --- Name: firstDayOfQuarter Value: null
      --- Name: lastDayOfQuarter Value: null
      --- Name: firstDayOfWeek Value: null
      --- Name: lastDayOfWeek Value: null
    ***
    === ViewObject DATA =====

И без вызова ExecuteWithParams:

    ---Variables:
     ***
      --- Name: firstDayOfMonth Value: 2016-02-01 00:00:00.0
      --- Name: lastDayOfMonth Value: 2016-02-29 00:00:00.0
      --- Name: firstDayOfQuarter Value: 2016-01-01 00:00:00.0
      --- Name: lastDayOfQuarter Value: 2016-03-31 00:00:00.0
      --- Name: firstDayOfWeek Value: 2016-02-08 00:00:00.0
      --- Name: lastDayOfWeek Value: 2016-02-14 00:00:00.0
      ***
    === ViewObject DATA =====

<br/>
<br/>

<strong>Решение:</strong>

Оказалось, что нужно зайти в исходники \*\*\*PageDef.xml и удалить перечистенные записи. С помощью GUI удалить их не получалось. Кнопки добавления/удаления в Jdeveloper были неактивны.

В моем случае это были записи вида:

    <NamedData NDName="today" NDType="java.sql.Timestamp"/>
    <NamedData NDName="firstDayOfWeek" NDType="java.sql.Timestamp"/>
    <NamedData NDName="firstDayOfMonth" NDType="java.sql.Timestamp"/>
    <NamedData NDName="firstDayOfQuarter" NDType="java.sql.Timestamp"/>
    <NamedData NDName="yesterday" NDType="java.sql.Timestamp"/>
