---
layout: page
title: JDeveloper тестирование бизнес компонентов с помщью junit плагина
description: JDeveloper тестирование бизнес компонентов с помщью junit плагина
keywords: Oracle ADF, Junit
permalink: /test/junit/
---

<br/>

# JDeveloper тестирование бизнес компонентов с помщью junit плагина

Плагин сам генерирует тесты.

http://www.oracle.com/webfolder/technetwork/jdeveloper/downloads/1213center.xml

Дополнительный плагины для Jdeveloper:

Для 12.2.1 пока не особо чего сделали. Зато Полно вариантов для 12.1.3

Качаем:

    Details for BC4J JUnit Integration
    12.2.1.0.42.151011.0031

    Details for JUnit Integration
    12.2.1.0.42.151011.0031

    Details for JUnit Integration for Java EE
    12.2.1.0.42.151011.0031

Ставим в слудующем порядке:

Help --> Check For Updates --> junit.zip, junit-j2ee.zip, bc4j-junit.zip

После каждого шага restart

Создаем каталог в Model, например test.

New --> From Gallery --> Unit Tests --> AdF Business Component Test Suite.

После создания RUN.

<!--

private AppModuleAMFixture() {
    AuthenticationService authService =
                               AuthenticationServiceUtil.getAuthenticationService();
    authService.login("test_user", "test_user");
    _am = Configuration.createRootApplicationModule("ru.javadev.xxx.model.AppModule", "AppModuleLocal");
}

-->

<!--

Делаем тест, чтобы было:

@Test
public void testAccess() {
    ViewObject view = fixture1.getApplicationModule().findViewObject("StoreViewSQL");
    assertNotNull(view);
    view.executeQuery();
}

-->
