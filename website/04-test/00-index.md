---
layout: page
title: Oracle ADF Testing
description: Oracle ADF Testing
keywords: Oracle ADF, Testing
permalink: /testing/
---

# ADF Testing:

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/cudvV12KGiQ?list=PLkuq7GqfJia3r8t7kb8_hoU61VaYjgVkS" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### Task Flow Tester

    Jdeveloper 12.2.1.0.0

    Help --> Checks For Udates

    Open Source and Partners Extensions

    next --> ADF EMG Task Flow Tester

    ViewController --> Properties

    Libraries and Classpath --> Add Library --> OK

    Run --> TaskFlow Tester

<br/>

### JUnit

<ul>
    <li><a href="/test/junit/"><strong>JDeveloper тестирование бизнес компонентов с помщью junit плагина</strong></a></li>
</ul>

<br/>

### SELENIUM:

<ul>
    <li><a href="https://www.google.de/url?sa=t&source=web&rct=j&url=http://www.oracle.com/technetwork/developer-tools/adf/learnmore/adfloadstresstesting-354067.pdf&ved=0ahUKEwj3kZ6HzfvJAhXEzRQKHSBpBu8QFggsMAA&usg=AFQjCNHZb_rWaOM3J5xClP5EwQ3p4VsTSg&sig2=kOTyQt83RyZ2yqtiok1F7A">Techniques for Testing Performance/Scalability and StressTesting ADF Applications</a> (PDF)</li>

    <li><a href="https://github.com/wvanderdeijl/adf-selenium">Just a quick example to demonstrate how easy it is to test an ADF application with these tools</a></li>

</ul>

<br/>

### Stress и Performance тесты:

<ul>
    <li><a href="http://andrejusb.blogspot.de/2011/11/stress-testing-oracle-adf-bc.html">Andrejus Baranovskis Blog: Stress Testing Oracle ADF BC Applications - Passivation and Activation</a></li>

    <li><a href="http://andrejusb.blogspot.de/2012/01/adf-performance-marathon-22-hours.html">Andrejus Baranovskis Blog: ADF Performance Marathon - 22 Hours Stress Test</a></li>

    <li>http://www.oracle.com/technetwork/developer-tools/adf/learnmore/adfloadstresstesting-354067.pdf</li>
    <li>http://www.oracle.com/technetwork/oem/app-test/ds-ats-adf-testing-accelerator-200156.pdf</li>
    <li>http://www.oracle.com/technetwork/articles/adf/part6-094560.html</li>
    <li>https://www.youtube.com/watch?v=cudvV12KGiQ</li>

</ul>
