---
layout: page
title: Oracle ADF Security Авторизация с помощью Weblogic (Пользователи и их роли на weblogic)
permalink: /dev/sec/authentication-and-authorization-on-weblogic/
---

# [Oracle ADF Security] Авторизация с помощью Weblogic (Пользователи и их роли на weblogic)

<br/>

Weblogiс Web Console:

Application's Security Settings > Security Realms > myRealm > Users and Groups

У меня уже есть пользователи с Provider (DefaultAuthenticator (WebLogic Authentication Provider))


Появились они скорее всего при старте какого-то учебного приложения.  
Можно попробовать создать руками.

Можно даже залогиниться пользователем weblogic с паролем от weblogic console.

Само приложение:  
https://bitbucket.org/oracle-adf/oramag-adf-security

В нем уже есть login и др. методы.


Нужно, чтобы билиотека

Weblogic 12.1 Remote Client

присутствовала в проекте.

---

1) Application Resourse -> ADF-META-INF --> adf-config.xml здесь указывается, используется или авторизация и аутентификация.

2) Application --> Secure --> Configure ADF Security

ADF Authentication and Authorization

Form-Based Authentication. Login/Logout page, No Automatic Grants, Redirect to Home page.

3) Application --> Secure --> Intitment Grants

Entitments --> entitment1

Resourse Add All

Grants: authenticated-role


---

Если нужно реализовать свои методы для добавления/удаления пользователей из приложения, следует почитать:


http://docs.oracle.com/middleware/1213/wls/JMXCU/editsecurity.htm#JMXCU206

Там еще в скрипте ошибка. (Чтобы карась не дремал)

    if (isEditor = true) break;

<br/>



**Нужно теперь посмотреть повнимательнее:**

Understanding Security for Oracle WebLogic Server  
http://docs.oracle.com/middleware/1213/wls/SCOVR/toc.htm

Configuring Security Providers  
http://docs.oracle.com/middleware/1213/wls/SECMG/providers.htm#SECMG131

Configuring Authorization and Role Mapping Providers  
http://docs.oracle.com/middleware/1213/wls/SECMG/atz_rm.htm#SECMG687
