---
layout: page
title: Oracle ADF Row Level Security using VPD and ADF
permalink: /dev/sec/row-level-security-using-vpd-and-adf/my/
---


# [Oracle ADF] Row Level Security using VPD and ADF

<br/>

<ul>
    <li><a href="/dev/sec/row-level-security-using-vpd-and-adf/my/1/">Шаг 1 - Настраиваем контексты, процедуры, подготавливаем appmodule</a></li>
    <li><a href="/dev/sec/row-level-security-using-vpd-and-adf/my/2/">Шаг 2 - подготавливаем и включаем VPD</a></li>
</ul>
