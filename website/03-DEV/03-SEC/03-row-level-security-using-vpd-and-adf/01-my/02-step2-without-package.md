---
layout: page
title: Oracle ADF Row Level Security using VPD and ADF - Шаг 2 - подготавливаем и включаем VPD
permalink: /dev/sec/row-level-security-using-vpd-and-adf/my/2/without-package/
---


# [Oracle ADF] Row Level Security using VPD and ADF - Шаг 2 - подготавливаем и включаем VPD

<br/>

Функцию нужно создавать под sysdba или под учетной с достаточным количеством прав!  
Я создавал под пользователем 2 часа не мог понять где ошибка!

<br/>

На сервере где расположена база. (Разумеется это linux).

<br/>

    $ cd /tmp

<br/>

    $ vi function.sql

<br/>

    CREATE OR REPLACE FUNCTION STORES_RESTRICT_ACCESS (
        p_schema in varchar2,
        p_object in varchar2)
        RETURN VARCHAR2
        AS

    BEGIN
    RETURN 'STORE in (580, 423)';
    END;
    /


<br/>

Создать:

<br/>

    $ vi policy-enable.sql


<br/>

    begin
    dbms_rls.add_policy
    (object_schema => 'MY_SCHEMA',
    object_name => 'STORE',
    policy_name => 'STORE_VPD1',
    policy_function => 'STORES_RESTRICT_ACCESS');
    end;
    /


<br/>

    $ sqlplus / as sysdba

    @function.sql
    @policy-enable.sql


<br/>

Удалить

$ vi policy-disable.sql

<br/>

    begin
    DBMS_RLS.DROP_POLICY('MY_SCHEMA', 'STORE', 'STORE_VPD1');
    end;
    /


<br/>

Можно также выключить:

<br/>

    begin
    DBMS_RLS.ENABLE_POLICY('MY_SCHEMA', 'STORE', 'STORE_VPD1', FALSE);
    end;
    /
