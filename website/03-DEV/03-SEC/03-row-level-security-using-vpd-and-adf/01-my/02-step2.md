---
layout: page
title: Oracle ADF Row Level Security using VPD and ADF - Шаг 2 - подготавливаем и включаем VPD
permalink: /dev/sec/row-level-security-using-vpd-and-adf/my/2/
---


# [Oracle ADF] Row Level Security using VPD and ADF - Шаг 2 - подготавливаем и включаем VPD


<br/>

ШАГ 2: Что же со всем этим добром теперь делать?

Смотрим пример:

<ul>
    <li><a href="https://www.youtube.com/watch?v=rs1Of-DoeF8" rel="nofollow">How to create a basic VPD policy</a></li>
</ul>


<br/>

И здесь у меня 2 варианта:

<br/>

Заработало у меня не сразу. Пришлось делать по шагам. Сначала фукнцию создавал вне пакета (как делается в видео), потом заработало и в в пакете.

<br/>

<ul>
    <li><a href="/dev/sec/row-level-security-using-vpd-and-adf/my/2/without-package/">Без пакета (не рекомендуется. Пусть лучше в пакете валяется)</a></li>
</ul>

<br/>

### Продолжаем дальше рассматривать вариант с пакетом


При создании policy я использовал 3 параметра как в видео и у меня вроде не работало. Может просто был не день Бекхама.
Из этого вопроса, пришел к выводу, что можно использовать большее количество параметров.

https://stackoverflow.com/questions/36413922/implementing-vpd-predicate-function-ora-28110-policy-function-or-package-has

<br/>

После того, как стал использовать такой способ. Стало работать.

Функцию нужно создавать под sysdba или под учеткой с достаточным количеством прав!  
Я создавал под пользователем 2 часа не мог понять где ошибка!

<br/>

На сервере где расположена база. (Разумеется это linux), делаю следующее:

<br/>

    $ cd /tmp

<br/>

Создаю Enable и Disable скрипты. Сразу лучше написать disable, чтобы если что-то пошло не так, можно было бы быстро отключить.

<br/>

**Enable**

    $ vi policy-enable.sql

<br/>

    begin
    dbms_rls.add_policy
    (object_schema => 'MY_SCHEMA',
    object_name => 'STORE',
    policy_name => 'STORE_TABLE_VPD1',
    function_schema => 'MY_SCHEMA',
    policy_function => 'SEC_CTX_PKG.WHITE_ACESS_LIST',
    statement_types => 'SELECT');
    end;
    /

<br/>

    $ sqlplus / as sysdba

<br/>

    @policy-enable.sql


<br/>

**Disable**

<br/>

    $ vi policy-disable.sql

<br/>

    begin
    DBMS_RLS.DROP_POLICY('MY_SCHEMA', 'STORE', 'STORE_TABLE_VPD1');
    end;
    /

<br/>

    $ sqlplus / as sysdba

<br/>

    @policy-disable.sql


<br/>
<br/>


Определение в голове пакета:

<br/>

FUNCTION WHITE_ACESS_LIST (p_schema VARCHAR2, p_object VARCHAR2) RETURN VARCHAR2;


<br/><br/>

Функция в теле пакета:

    FUNCTION WHITE_ACESS_LIST (
        p_schema in varchar2,
        p_object in varchar2)
        RETURN VARCHAR2
        AS

    L_LOGGED_IN_USER varchar2(250);
    L_RES           VARCHAR2(255);

    BEGIN


           -- Удалять не стал. L_LOGGED_IN_USER подставляю в реальный запрос. Который не буду здесь писать.
           -- Но если путаешься с кавычками как и я, смотри сюда: plsql.ru/docs/other/variables-concat/
           -- Лично я на этих кавычках потерям много времени

           -- SELECT SYS_CONTEXT ('SEC_CTX', 'user_id') INTO L_LOGGED_IN_USER FROM DUAL;

           L_RES := 'STORE in (100, 101)';

            RETURN L_RES;

    END WHITE_ACESS_LIST;


<br/>

    Ну и собственно все.

<br/>


Можно подключиться пользователем к схеме указанной в policy и сделать SELECT. В результате будет выведено всего 2 записи с указанными в policy ID.

Ну а чтобы ограничить данных разрешенных для пользователя, можно создать таблицу с разрешенными для пользователя ID. И делать что-то похожее на

    SELECT * FROM ACCESS_LIST WHERE USER = _LOGGED_IN_USER;

<br/>

При необходимости добавить decode по вкусу.
