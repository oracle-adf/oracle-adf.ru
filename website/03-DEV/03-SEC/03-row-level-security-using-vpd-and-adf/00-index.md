---
layout: page
title: Oracle ADF - Ограничение выводимых данных VO в зависимости от залогиненного пользователя на уровне базы данных
permalink: /dev/sec/row-level-security-using-vpd-and-adf/
---


# [Oracle ADF] Ограничение выводимых данных VO в зависимости от залогиненного пользователя на уровне базы данных

<br/>

### Реализация настроенная мной. Я в ней разобрался, значит она единственно правильная! Версия 1 Beta

<ul>
    <li><a href="/dev/sec/row-level-security-using-vpd-and-adf/my/">Поехали</a></li>
</ul>

<br/>

### Может что полезное осталось:

<br/>


<ul>
    <li><a href="http://www.dba-oracle.com/t_adv_plsql_vpd_application_contexts.htm" rel="nofollow">VPD Policy Creation using an Application Context</a></li>
    <!-- <li><a href="https://blogs.oracle.com/jheadstart/entry/row_level_security_using_vpd_a" rel="nofollow">Row Level Security using VPD and ADF</a></li> -->
    <li><a href="http://adfexplorer.blogspot.ru/2012/11/row-level-security.html" rel="nofollow">ADF Row Level Security</a></li>
</ul>

<!-- <br/><br/>

Пример не ADF:

<br/><br/>

<ul>
    <li><a href="https://www.youtube.com/watch?v=g06xtm7BEjs" rel="nofollow">Column-Level Virtual Private Database (VPD)</a></li>
</ul>


<br/><br/>

Пока мое предпочтение (а ничего друго я и не умею) - получить залогиненного пользователя с помощью groovy скрипта во ViewObject и подставить этого пользователя в запрос. -->
