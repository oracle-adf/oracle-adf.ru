---
layout: page
title: Oracle ADF Security Form-Based Authentication Login Bean example
permalink: /dev/sec/form-based-authentication/login-bean-example/
---

# [Oracle ADF Security] Form-Based Authentication Login Bean example

<br/>

    package adf.sample.managed.login;

    import javax.faces.application.FacesMessage;
    import javax.faces.context.FacesContext;

    import javax.security.auth.Subject;
    import javax.security.auth.callback.CallbackHandler;
    import javax.security.auth.login.FailedLoginException;
    import javax.security.auth.login.LoginException;

    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    import weblogic.security.services.Authentication;

    import weblogic.servlet.security.ServletAuthentication;

    import javax.servlet.RequestDispatcher;
    import javax.servlet.ServletException;

    import java.io.IOException;

    import javax.faces.context.ExternalContext;

    import weblogic.security.URLCallbackHandler;

    public class LoginBean {

        String _username = null;
        String _password = null;

        public LoginBean() {
            super();
        }

        public void setUsername(String _username) {
            this._username = _username;
        }

        public String getUsername() {
            return _username;
        }

        public void setPassword(String _password) {
            this._password = _password;
        }

        public String getPassword() {
            return _password;
        }

        public String doLogin() {
            String un = _username;
            byte[] pw = _password.getBytes();
            FacesContext ctx = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
            try {
                CallbackHandler handler = new URLCallbackHandler(un, pw);
                Subject mySubject = Authentication.login(handler);
                ServletAuthentication.runAs(mySubject, request);
                ServletAuthentication.generateNewSessionID(request);
                String loginUrl = "/adfAuthentication?success_url=/faces" + ctx.getViewRoot().getViewId();
                HttpServletResponse response = (HttpServletResponse)ctx.getExternalContext().getResponse();
                sendForward(request, response, loginUrl);
            } catch (FailedLoginException fle) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Incorrect Username or Password", "An incorrect Username or Password" +
                                     " was specified");
                ctx.addMessage(null, msg);
            } catch (LoginException le) {
                reportUnexpectedLoginError("LoginException", le);
            }
            return null;
        }

        private void sendForward(HttpServletRequest request, HttpServletResponse response, String forwardUrl) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            RequestDispatcher dispatcher = request.getRequestDispatcher(forwardUrl);
            try {
                dispatcher.forward(request, response);
            } catch (ServletException se) {
                reportUnexpectedLoginError("ServletException", se);
            } catch (IOException ie) {
                reportUnexpectedLoginError("IOException", ie);
            }
            ctx.responseComplete();
        }

        private void reportUnexpectedLoginError(String errType, Exception e) {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error during login", "Unexpected error during login (" +
                                 errType + "), please consult logs for detail");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            e.printStackTrace();
        }

        public String logout() {
            FacesContext ctx = FacesContext.getCurrentInstance();  
            ExternalContext ectx = ctx.getExternalContext();
            String logoutUrl = "faces" + ctx.getViewRoot().getViewId();
            ((HttpServletRequest)ectx.getRequest()).getSession().invalidate();
            try {
                System.out.println("Logout URL: "+logoutUrl);
                ectx.redirect(logoutUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


Библиотека с доп классами
Library and Classpath --> Add Library --> Weblogic 12.1 Remote Client



<!--

Было так

package org.javadev.xxx.view.common;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import oracle.adf.share.logging.ADFLogger;

import ru.javadev.xxx.utils.ADFUtils;

public class LoginBean {

    public static final ADFLogger _logger = ADFLogger.createADFLogger(LoginBean.class);

    private String _username;
    private String _password;

    public void setUsername(String username) {
        this._username = username;
    }

    public String getUsername() {
        return _username;
    }

    public void setPassword(String password) {
        this._password = password;
    }

    public String getPassword() {
        return _password;
    }

    public String doLogin() {

         _logger.info("!!! doLogin !!!");

        FacesContext ctx = FacesContext.getCurrentInstance();

        if (_username == null || _password == null) {
            showError("Invalid credentials", "An incorrect username or password was specified.", null);
        } else {
            ExternalContext ectx = ctx.getExternalContext();
            HttpServletRequest request = (HttpServletRequest) ctx.getExternalContext().getRequest();
            try {
                request.logout();
                request.login(_username, _password); // Servlet 3.0 login
                _username = null;
                _password = null;
                HttpSession session = request.getSession();
                session.setAttribute("success_url", "/faces/home");
                redirect(ectx.getRequestContextPath() + "/adfAuthentication");
            } catch (ServletException fle) {
                    _logger.info("!!! ServletException !!! " + fle.toString());
                showError("ServletException", "Login failed. Please verify the username and password and try again.",
                          null);
                } catch (Exception ex){
                    _logger.info("!!! Exception !!! " + ex.toString());
                }
        }
        return null;
    }

    private void redirect(String forwardUrl) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        try {
            ectx.redirect(forwardUrl);
        } catch (IOException ie) {
            showError("IOException", "An error occurred during redirecting. Please consult logs for more information.",
                      ie);
        }
    }

    private void showError(String errType, String message, Exception e) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, errType, message);
        FacesContext.getCurrentInstance().addMessage("d2:it35", msg);
        if (e != null) {
            e.printStackTrace();
        }
    }

    public String doLogout() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        HttpServletRequest httpRequest = (HttpServletRequest) ectx.getRequest();
        try {
            httpRequest.logout(); // Servlet 3.0 logout
            HttpSession session = httpRequest.getSession(false);
            if (session != null) {
                session.invalidate();
            }
            String logoutUrl = ectx.getRequestContextPath() + "/faces/login.jsf";
            redirect(logoutUrl);
        } catch (ServletException e) {
            showError("ServletException", "An error occurred during logout. Please consult logs for more information.",
                      e);
        }
        return null;
    }

    public void goHome(ActionEvent actionEvent) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        String homeUrl = ectx.getRequestContextPath() + "/faces/home";
        redirect(homeUrl);
    }
}


-->
