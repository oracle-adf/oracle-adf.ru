---
layout: page
title: Oracle ADF - Oracle Internet Directory Auhenticator (LDAP)
permalink: /dev/sec/ldap/
---


# [Oracle ADF] Oracle Internet Directory Auhenticator (LDAP)

<br/>

Все что связано с LDAP, Microsoft Active Directory (AD) будет добавляться сюда.

<br/>


Используется вот это <a href="https://bitbucket.org/oracle-adf/oramag-adf-security">приложение</a>.

<ul>
    <li><a href="https://www.youtube.com/watch?v=NfFhsZCAgAo">[YouTube] [Dmitry Nefedkin] Ldap [ENG]</a></li>
    <li><a href="https://www.youtube.com/watch?v=tjBSLZBBZYY">[YouTube] [Dmitry Nefedkin] Setting up SSO in ADF application using OAM 11g [ENG]</a></li>
</ul>

<br/>

**Возможно полезные ссылки:**

<br/>

<ul>
    <li><a href="http://andrejusb.blogspot.ru/2010/07/managing-adf-security-using-oracle.html" rel="nofollow">Managing ADF Security Using Oracle Enterprise Manager 11g</a></li>
    <li><a href="http://jtyreman.blogspot.ru/2014/06/adf-show-display-name-of-logged-in-user.html" rel="nofollow">ADF show Display Name of logged in user</a></li>
    <li><a href="/dev/sec/ldap/get-parameters-from-ad/" rel="nofollow">Получение параметров из AD</a></li>
</ul>
