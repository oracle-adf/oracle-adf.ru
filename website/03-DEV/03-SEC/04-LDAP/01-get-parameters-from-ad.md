---
layout: page
title: Oracle ADF - Получение параметров из AD
permalink: /dev/sec/ldap/get-parameters-from-ad/
---

# [Oracle ADF] Получение параметров из AD

<br/>

{% highlight java linenos %}

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.Hashtable;

public class TestAD {

    public static void main(String[] args) throws  NamingException{
        Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
        ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        ldapEnv.put(Context.PROVIDER_URL,  "ldap://mysite.ru:389");
        ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
        ldapEnv.put(Context.SECURITY_PRINCIPAL, "тут был логин");
        ldapEnv.put(Context.SECURITY_CREDENTIALS, "тут был пароль");
        InitialDirContext ldapContext = new InitialDirContext(ldapEnv);
        // Create the search controls
        SearchControls searchCtls = new SearchControls();
        // Specify the attributes to return
        String returnedAtts[] = {"sAMAccountName", "extensionAttribute1"};
        searchCtls.setReturningAttributes(returnedAtts);
        // Specify the search scope
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        // specify the LDAP search filter
        String searchFilter = "(&(sAMAccountName=n_dunda)(objectclass=user))";
        // Specify the Base for the search
        String searchBase = "OU=Группа компаний Холидей,DC=hclass,DC=ru";
        // Search for objects using the filter
        NamingEnumeration<javax.naming.directory.SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls);
        // Loop through the search results
        while (answer.hasMoreElements()) {
            javax.naming.directory.SearchResult sr = answer.next();
            totalResults++;
            System.out.println(">>>" + sr.getAttributes());

        }
    }
}

{% endhighlight %}
