---
layout: page
title: Oracle ADF - How to Create Test Users in JDeveloper
permalink: /dev/sec/how-to-create-test-users/
---

# [Oracle ADF] > How to Create Test Users in JDeveloper


For development purposes, you can use the OPSS jazn-data.xml flat file to create users and roles for testing.

Example 49-25 shows an example of the identity store in the flat file used by JDeveloper.

Example 49-25 Oracle Platform Security Services (OPSS) XML Policy Store

    <?xml version="1.0" encoding="windows-1252" standalone="yes" ?>
    <jazn-data xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="http://xmlns.oracle.com/oraclass/schema/jazn-data-11_0.xsd">
    <jazn-realm default="jazn.com">
      <realm>
        <name>jazn.com</name>
        <users>
          <user>
            <name>admin</name>
            <credentials>{903}Qh3td3z2HHlun9ROrbvE6bYJDnNaoGir</credentials>
          </user>
          <user>
            <name>joeUser</name>
            <credentials>{903}FarN3p4C9IU9PZODN5RLmrpHf45eCw+W</credentials>
          </user>
        </users>
        <roles>
          <role>
            <name>adminRole</name>
            <members>
              <member>
                <type>user</type>
                <name>admin</name>
              </member>
            </members>
          </role>
          <role>
            <name>regularUserRole</name>
            <members>
              <member>
                <type>user</type>
                <name>admin</name>
              </member>
              <member>
                <type>user</type>
                <name>joeUser</name>
              </member>
            </members>
          </role>
        </roles>
      </realm>
    </jazn-realm>
    </jazn-data>



http://docs.oracle.com/cd/E25178_01/fusionapps.1111/e15524/sec_fndgrants.htm#CIHFCBBA
