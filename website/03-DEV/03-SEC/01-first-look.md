---
layout: page
title: Oracle ADF Знакомство с Oracle ADF Security
permalink: /dev/sec/first-look/
---


# [Oracle ADF] Знакомство с Oracle ADF Security

<br/>

Реализация adf security для ранее не работавшего с ADF человека трудна и запутанна.
Здесь собираются, как мне кажется, полезные материалы чтобы при необходимости можно было выбрать нужный вариант применения security.


<br/>

### Form-Based Authentication

<ul>
    <li>[YouTube] Firebox Training <a href="https://www.youtube.com/watch?v=mAWBezngA1s">video 1</a> <a href="https://www.youtube.com/watch?v=TyDJ2cdj4I4">video 2</a> (Добавляют библиотеки: C:\Oracle\Middleware\Oracle_Home\wlserver\modules\com.bea.core.weblogic.security.auth.jar,  C:\Oracle\Middleware\Oracle_Home\wlserver\modules\com.bea.core.weblogic.security.identity.jar (не нашел у себя), C:\Oracle\Middleware\Oracle_Home\wlserver\server\lib\wls-api.jar Не заработало у меня на Jdeveloper 12)</li>
    <li>[YouTube] Raj Venugopal <a href="https://www.youtube.com/watch?v=ofUvs4q6QuI">Create Login/Logout using ADF security framework</a></li>
    <li><a href="/dev/sec/form-based-authentication/login-bean-example/">Вариант LoginBean.java</a></li>
</ul>


<br/>

### HTTP Basic Authentication

<ul>
    <li><a href="http://www.oracle.com/technetwork/issue-archive/2012/12-jan/o12adf-1364748.html">Security for Everyone (By Frank Nimphius)</a></li>
    <li><a href="https://bitbucket.org/oracle-adf/oramag-adf-security">Исходные коды</a></li>
</ul>


<br/>

### SQL authentication provider (Логины, пароли, группы - все хранится в базе и они должны отобразиться на Weblogic)

Остались сомнения по поводу этого метода. Пользователи хранятся в базе но всеравно подгружаются на Weblogic. При замене сервера Weblogic, придется дополнительно настраивать пользователей руками или скриптами.

Пароли (вроде) хранятся в открытом виде.  Можно залогититься одним пользователем в несколько приложений на сервере.


<ul>
    <li><a href="https://www.youtube.com/watch?v=t0Y2dhCkBPw">Weblogic SQL Authenticator</a></li>
    <li><a href="http://biemond.blogspot.ru/2008/12/using-database-tables-as-authentication.html" rel="nofollow">Using database tables as authentication provider in WebLogic</a></li>
</ul>

<br/><br/>

Файл с конфигом на embedded weblogic в windows:  
C:\Users\Username\AppData\Roaming\JDeveloper\system12.2.1.0.42.151011.0031\DefaultDomain\config\config.xml

Файл с конфигом datasource windows:  
C:\Users\Username\AppData\Roaming\JDeveloper\system12.2.1.0.42.151011.0031\DefaultDomain\config\jdbc\my_ds-jdbc.xml


How To Encrypt Clear Text Passwords With WebLogic Server  
http://weblogicserver.blogspot.ru/2008/10/how-to-encrypt-clear-text-password-with.html

<br/>

### Oracle Internet Directory Auhenticator (LDAP)

<a href="/dev/sec/ldap/">Все что связано с LDAP, Microsoft Active Directory (AD) и т.д.</a>

<br/>


### Авторизация с помощью Weblogic (Пользователи и их роли на weblogic).


Пришел более опытный разработчик. Показал нам, как правильно крутить гайки.  
Но мне такой способ не нравится. Ничего с собой поделать не могу. 
<br/>

Подробности <a href="/dev/sec/authentication-and-authorization-on-weblogic/">здесь</a>
