---
layout: page
title: Oracle ADF - Получить информацию из Security Context с помощью Expression Language
permalink: /dev/sec/get-info-from-security-context-by-expression-language/
---

# [Oracle ADF] > Получить информацию из Security Context с помощью Expression Language

<br/>

| Expression  | Description  |
|---|---|
| #{securityContext.authenticated}  |  Returns true if the user is authenticated, false otherwise. |
| #{securityContext.userName}  | Returns the authenticated username or anonymous for unauthenticated users.  |
| #{securityContext.userInRole['roleList']} | Checks if user is member of at least one of the listed enterprise or application roles. The role names are comma-separated.  |
| #{securityContext.userInAllRoles['roleList']}  | Checks if the user is a member in all of the listed roles. Role names are commaseparated.  |
| #{securityContext.taskflowViewable['target']}  | Checks if the user has permission to access the specified task flow. Returns true or false.  |
| #{securityContext.regionViewable['target']}  | Checks if the user has permission to access the specified PageDef file to evaluate the user access to an ADF bound page.  |
| #{securityContext.userGrantedPermission['permission']}  | Checks if the user has access to a specific permission defined in the policy store. This can be a custom Permission class.  |
| #{securityContext.userGrantedResource['permission']}  | Checks access to a named resource and action. Uses ResourcePermission class in OPSS, a multipurpose Permission class that saves developers from writing their own.  |
