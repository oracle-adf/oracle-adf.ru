---
layout: page
title: Oracle ADF Security
permalink: /dev/sec/
---

# [Oracle ADF] Security

<br/>

### Начало

<ul>
    <li><a href="http://adfindepth.blogspot.ru/2017/02/login-with-database-users-dynamically.html" rel="nofollow">How you can perform login by using DB level user name and password</a></li>
    <li><a href="/dev/sec/first-look/">Знакомство с Oracle ADF Security</a></li>
</ul>



<br/>

### Какие-то полезные примеры:

<ul>
    <li><a href="/dev/sec/how-to-create-test-users/">How to Create Test Users in JDeveloper</a></li>
    <li>Получить программно информацию о залогиненом пользователе непосредственно в AppModuel. (Будет время добавлю)</li>
    <li><a href="/dev/sec/get-info-from-security-context-programmatically/">Получить программно информацию из Security Context</a></li>
    <li><a href="/dev/sec/get-info-from-security-context-by-expression-language/">Получить информацию из Security Context с помощью Expression Language</a></li>
    <li><a href="/dev/sec/get-info-from-security-context-by-groovy-language/">Получить информацию из Security Context с помощью Groovy</a></li>

</ul>

<br/>

### Ограничение выводимых данных VO в зависимости от залогиненного пользователя (Row Level Security и Virtual Private Database) на уровне базы данных

<ul>
    <li><a href="/dev/sec/row-level-security-using-vpd-and-adf/">Ограничение выводимых данных VO в зависимости от залогиненного пользователя на уровне базы данных</a></li>
</ul>



<br/>

### Доки

<ul>

<li><a href="http://docs.oracle.com/cd/E14571_01/web.1111/b31974/adding_security.htm#ADFFD877" rel="nofollow">Enabling ADF Security in a Fusion Web Application</a></li>

<li><a href="http://docs.oracle.com/middleware/1221/adf/adf-secure.htm" rel="nofollow">Secure</a></li>

<li><a href="http://docs.oracle.com/middleware/1221/adf/develop/GUID-6450362C-4F09-4C60-87E3-FF5A3D5E20EF.htm#ADFFD877" rel="nofollow">Enabling ADF Security in a Fusion Web Application</a></li>

</ul>

<br/>

### Статьи

<ul>
    <li><a href="http://suneesh-adf.blogspot.ru/2013/08/adf-security-basics-part-8-expression.html" rel="nofollow">ADF Security Basics part 8 : Expression Language (EL) with ADF security</a></li>

    <li><a href="http://docs.oracle.com/cd/E25178_01/fusionapps.1111/e15524/sec_fndgrants.htm#CIHFCBBA" rel="nofollow">How to Validate Data Security Configuration with Diagnostic Scripts</a></li>
</ul>
