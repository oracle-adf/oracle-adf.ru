---
layout: page
title: Oracle ADF - Получить информацию из Security Context с помощью Groovy
permalink: /dev/sec/get-info-from-security-context-by-groovy-language/
---

#  [Oracle ADF] Получить информацию из Security Context с помощью Groovy

<br/>

    adf.context.securityContext.userName

<br/>

Лучше создать переменную в инициализационном бине и уже подставлять данные из нее:

    adf.context.sessionScope.loggedInUser
