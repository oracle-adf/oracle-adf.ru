---
layout: page
title: Oracle ADF - Получить программно информацию из Security Context
permalink: /dev/sec/get-info-from-security-context-programmatically/
---

# [Oracle ADF] > Получить программно информацию из Security Context

<br/>

    public static Boolean IsSecurityTurnedOn(){
        return ADFContext.getCurrent().getSecurityContext().isAuthorizationEnabled();
    }

    public static Boolean IsAuthenticated(){
        return ADFContext.getCurrent().getSecurityContext().isAuthenticated();
    }

    public static String getCurrentUser(){
        return ADFContext.getCurrent().getSecurityContext().getUserName();
    }

    public static Boolean isUserInROle(String role){
        return ADFContext.getCurrent().getSecurityContext().isUserInRole(role);
    }  


<!--

    import oracle.adf.share.ADFContext;
    import oracle.adf.share.logging.ADFLogger;
    import oracle.adf.share.security.SecurityContext;


    ADFContext adfCtx = ADFContext.getCurrent();  
    SecurityContext secCntx = adfCtx.getSecurityContext();  
    String user = secCntx.getUserPrincipal().getName();  
    String _user = secCntx.getUserName();  

    _logger.info("user " + user);
    _logger.info("_user " + _user);


http://mahmoudoracle.blogspot.ru/2012/06/adf-get-current-logged-user-name.html

-->
