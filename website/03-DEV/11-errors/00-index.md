---
layout: page
title: Ошибки при программировании на Oracle ADF
description: Ошибки при программировании на Oracle ADF
keywords: Oracle ADF, Ошибки при программировании на Oracle ADF
permalink: /dev/errors/
---

# Ошибки при программировании на Oracle ADF

### java.lang.NumberFormatException: For input string: "NaN"

Решается изменением свойства scrollPolicy у таблицы на scrollPolicy="page"
