---
layout: page
title: Разработка на Oracle ADF
description: Разработка на Oracle ADF
keywords: Oracle ADF, Разработка на Oracle ADF
permalink: /dev/
---

# Разработка на Oracle ADF

<br/>

### Набор рекомендаций, как программировать на Oracle ADF (обновляется с накоплением опыта):

По возможности рекомендую использовать Oracle JET. От Oracle ADF использовать только бизнес компоненты, бины, task flow.
Правда для этого нужно будет еще разобраться как работать с REST.

<ul>
    <li><a href="/dev/how-to-programming/"><strong>Как программировать (может быть уже старело)</strong></a></li>
</ul>

<br/>

### View-Controller Layer (UI Components, Bindings etc):

<ul>
    <li><a href="/dev/view/"><strong>View-Controller Layer</strong></a></li>
</ul>

<br/><br/>

### Model Layer (Data, DataContorls) + SQL, PL/SQL:

<ul>
    <li><a href="/dev/model/">Model</a></li>
</ul>

<br/>
<hr/>
<br/>

### Security:

<ul>
    <li><a href="/dev/sec/">ADF Security</a></li>
</ul>

<br/>

### Печать отчетов:

<ul>
    <li><a href="/dev/reports/"><strong>Печать отчетов</strong></a></li>
</ul>

<br/>

### Personalization and MDS:

<ul>
    <li><a href="/personalization-and-mds/">Personalization and MDS</a></li>
</ul>

<br/><br/>

### Utils:

<ul>
    <li><a href="http://storage1.static.itmages.ru/i/15/1230/h_1451469198_6708500_5a31f81d7d.png">Adding the ADF Library JAR manually</a></li>
</ul>

<br/>

### Работа с датами:

<ul>
    <li><a href="/dates/">Работа с датами</a></li>
</ul>

<br/>

### Последние шаги при разработке:

<ul>
    <li><a href="http://docs.oracle.com/cd/E14571_01/web.1111/b31974/partpage5.htm">Completing Your Application</a></li>
</ul>

<br/>

### Супер трюки, офигенски понравившиеся:

<ul>
    <li><a href="https://oracle-adf.com/my/implementing-search-for-multiple-attributes-of-a-view-object/">Реализация поиска по всем полям таблицы в одном поле для поиска</a></li>
</ul>

<br/><br/>

<hr/>
<br/><br/>

### Web Services:

<ul>
    <li><a href="/dev/ws/rest/">Rest</a></li>
</ul>

<br/>

### Разработка с использованием Oracle JET:

<ul>
    <li><a href="/dev/jet/">Разработка с использованием Oracle JET</a></li>
</ul>
