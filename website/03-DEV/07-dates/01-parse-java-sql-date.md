---
layout: page
title: Парсим java.sql.Date
permalink: /dates/parse-java-sql-date/
---


# Парсим java.sql.Date

Нужно было установить по умолчанию данные с текущим годом и предыдущим месяцем.
Сделал так:



{% highlight java linenos %}


@PostConstruct
public void init() {

    java.sql.Date firstDayOfPreviousMonth = Dates.getFirstDayOfPreviousMonth();


    System.out.println("");
    System.out.println("firstDayOfPreviousMonth " + firstDayOfPreviousMonth.toString());
    System.out.println("");

    Calendar cal = Calendar.getInstance();
    cal.setTime(firstDayOfPreviousMonth);

    int month = cal.get(Calendar.MONTH);

    month++;

    int day = cal.get(Calendar.DAY_OF_MONTH);
    int year = cal.get(Calendar.YEAR);

    this.yearDefaultValue = year;
    this.monthDefaultValue = Integer.toString(month);
}

{% endhighlight %}

<br/>

Отсутствующие функции можно взять в нашей библиотеке на bitbucket.
