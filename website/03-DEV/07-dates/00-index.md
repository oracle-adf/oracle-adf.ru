---
layout: page
title: Oracle ADF - Примеры работы с датами
permalink: /dates/
---


# [Oracle ADF] Примеры работы с датами

<ul>
    <li><a href="/dates/parse-java-sql-date/">Парсим java.sql.Date</a></li>
    <li><a href="/dev/groovy/dates-for-work-with-oracle-adf-framework/">Вычисление дат в Groovy для удобной работы с фреймворком Oracle ADF</a></li>
</ul>


### Конвертация дат:

http://mahmoudoracle.blogspot.ru/2012/03/date-classes-conversion-in-adf.html  
http://johnytips.blogspot.ru/2015/05/oafjava-playing-with-dates-in-oracle.html
