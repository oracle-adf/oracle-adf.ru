---
layout: page
title: Вычисление дат в Groovy для удобной работы с фреймворком Oracle ADF
permalink: /dev/groovy/dates-for-work-with-oracle-adf-framework/
---

# Вычисление дат в Groovy для удобной работы с фреймворком Oracle ADF

Имеем ViewCriteria.  
У него есть Bind Variables

ViewCriteria должна быть применена к ViewObject по умолчанию.

Галочку в appmodule не составит труда поставить.

<br/>
<br/>

![ViewObject ViewCriteria Bind Variables](/img/questions/groovy/dates/Groovy-Dates-1.png "ViewObject ViewCriteria Bind Variables"){: .center-image }

<br/>
<br/>

![ViewObject ViewCriteria Bind Variables](/img/questions/groovy/dates/Groovy-Dates-2.png "ViewObject ViewCriteria Bind Variables"){: .center-image }

<br/>
<br/>

![ViewObject ViewCriteria Bind Variables](/img/questions/groovy/dates/Groovy-Dates-3.png "ViewObject ViewCriteria Bind Variables"){: .center-image }

<br/>
<br/>

Создавал тикет в комьюнити.  
https://community.oracle.com/message/13591622#13591622

Нужно зайти в исходники ViewObject

```xml
<Variable
    Name="firstDayOfWeek"
    Kind="viewcriteria"
    Type="java.sql.Timestamp">
    <TransientExpression
    Name="VariableScript"
    trustMode="trusted"
    CodeSourceName="PromoStoreViewSQL"/>
</Variable>
```

<br/>

И указать, trustMode="trusted".

Иначе возникает ошибка: "Caused by: oracle.jbo.ExprSecurityException: JBO-29114 ADFContext is not setup to process messages for this exception. Use the exception stack trace and error code to investigate the root cause of this exception. Root cause error code is JBO-25151. Error message parameters are {0=java.util.Date, 1=calendarDate}"

<br/>
<br/>

    import oracle.jbo.script.annotation.VariableExpression;
    import java.util.Date;
    import java.util.Calendar;
    import java.text.SimpleDateFormat;
    import java.text.DateFormat;

<br/>
<br/>

### Первый день недели

    @VariableExpression(path="firstDayOfWeek")
    def firstDayOfWeek_VariableScript_Variable()
    {
    def date = new Date();
    return today() - date.calendarDate.dayOfWeek -2;
    }

<br/>
<br/>

### Последний день недели

    @VariableExpression(path="lastDayOfWeek")
    def lastDayOfWeek_VariableScript_Variable()
    {
    def date = new Date();
    return today() + (7 - (date.calendarDate.dayOfWeek - 1));
    }

<br/>
<br/>

### Первый день месяца

    @VariableExpression(path="firstDayOfMonth")
    def firstDayOfMonth_VariableScript_Variable()
    {
    def date = new Date();
    return today() - (date.calendarDate.dayOfMonth -1);
    }

Вычисление дат, предлагаю взять в Utils в нашем репо на github. В заголовке ссылка на него.

<br/>
<br/>

В общем решили, что ну нах этот груви для такого рода задач.
Создали отдельную java библиотеку с логикой, которая возвращает дату.
Получается, что return ourlib.getLastMonth

Получилось вот так.
Может где и ошибка закралась.

Можно взять в Utils в нашем репо на github. В заголовке ссылка на него.

<br/>

**Ну и сам Groovy файл.**

```groovy

import oracle.jbo.script.annotation.VariableExpression;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.GregorianCalendar;


@VariableExpression(path="firstDayOfWeek")
def firstDayOfWeek_VariableScript_Variable()
{
    return org.javadev.libs.Dates.getFirstDayOfWeek();
}


@VariableExpression(path="lastDayOfWeek")
def lastDayOfWeek_VariableScript_Variable()
{
    return org.javadev.libs.Dates.getLastDayOfWeek();
}


@VariableExpression(path="firstDayOfMonth")
def firstDayOfMonth_VariableScript_Variable()
{
    return org.javadev.libs.Dates.getFirstDayOfMonth();
}


@VariableExpression(path="lastDayOfMonth")
def lastDayOfMonth_VariableScript_Variable()
{
        return org.javadev.libs.Dates.getLastDayOfMonth();
}


@VariableExpression(path="firstDayOfQuarter")
def firstDayOfQuarter_VariableScript_Variable()
{
    return org.javadev.libs.Dates.getFirstDayOfQuarter();
}


@VariableExpression(path="lastDayOfQuarter")
def lastDayOfQuarter_VariableScript_Variable()
{
    return org.javadev.libs.Dates.getLastDayOfQuarter();
}

```

<br/>

### Другие варианты использования groovy

    adf.context.current.sessionScope.get('someVar');

или даже вот так:

    return Dates.getOffsetDays(adf.context.current.pageFlowScope.get('p_someDate'), -30);

<!--

// System.out.println("Date Test " + Dates.getOffsetDays((java.sql.Date)ScopeUtils.getObjectFromPageFlowScope("p_writtenDate"), -30));

-->
