---
layout: page
title: Oracle ADF Localization
permalink: /dev/localization/
---

<br/>

# [Oracle ADF] Локализация

<br/>

Локализация и Интенационализация здесь одна и таже тема.


<br/>

### Локаль по-умолчанию можно установить в faces-config:

<br/>


{% highlight xml linenos %}


<faces-config version="2.1" xmlns="http://java.sun.com/xml/ns/javaee">

***

    <locale-config>
      <default-locale>en</default-locale>
      <supported-locale>en</supported-locale>
      <supported-locale>ru</supported-locale>
    </locale-config>

***

</faces-config>

{% endhighlight %}


<br/>



<br/>

### Формат даты вида dd-MM-yyy установить по умолчанию, например для английской локали можно следующим образом:

<br/>

UPD! Мдя. Оказалось что данный способ ломает компонент <af:chooseDate id="cd1"/>. И как назло мы его используем!
При заходе на страницу с этим компонентом, он не отображается и кнопки на этой форме перестают работать.

<br/>

Создал обсуждение:  
https://community.oracle.com/thread/4099055


<br/>


{% highlight xml linenos %}

<trinidad-config xmlns="http://myfaces.apache.org/trinidad/config">
    ***
    <formatting-locale>en-GB</formatting-locale>

    ***
</trinidad-config>


{% endhighlight %}

<br/>



Подробнее на сайте:  <br/>

http://soadev.blogspot.ru/2010/03/change-global-date-format-of-your.html



<br/>

### Установить локаль программно:

<br/>

{% highlight java linenos %}

// String language = "en";
String language = "ru";

Locale locale = new Locale(language);  
FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);

{% endhighlight %}


<br/>

### Получить текущую локаль программно:


{% highlight java linenos %}

Locale locale = (Locale) FacesContext.getCurrentInstance().getViewRoot().getLocale();
System.out.println("LOCALE  " + locale.toString());

{% endhighlight %}

<br/>

### Создать SOC с локалями:


{% highlight xml linenos %}

<af:selectOneChoice value="#{bindings.Language.inputValue}"
                   label="#{usersBundle['users.user.language']}"
                   id="soc3"
                   required="true">
   <f:selectItems value="#{pageFlowScope.MyBean.supportedLocales}" id="si6"/>
</af:selectOneChoice>

{% endhighlight %}



<br/>


{% highlight java linenos %}

public List<SelectItem> getSupportedLocales() {
    Iterator<Locale> iteratorSupportedLocales = FacesContext.getCurrentInstance()
                                                            .getApplication()
                                                            .getSupportedLocales();
    supportedLocales = new ArrayList<SelectItem>();
    SelectItem itemLocale = null;
    while (iteratorSupportedLocales.hasNext()) {
        Locale locale = iteratorSupportedLocales.next();
        itemLocale = new SelectItem(locale.getLanguage(), locale.getDisplayName(), locale.getDisplayName());
        supportedLocales.add(itemLocale);
    }

    return supportedLocales;
}

{% endhighlight %}


<br/>

### Получить текущую локаль программно:


{% highlight java linenos %}

Locale locale = (Locale) FacesContext.getCurrentInstance().getViewRoot().getLocale();
System.out.println("LOCALE  " + locale.toString());

{% endhighlight %}



<br/>

### Основные наработки:

<ul>
    <li><a href="/dev/localization/view/">Локализация для VIEW</a></li>
    <li><a href="/dev/localization/model/">Локализация для Модели</a></li>
</ul>


<br/>

### Дополнительные наработки:

<ul>
    <li><a href="/dev/localization/groovy/">Получить locale с помощью groovy скриптов</a></li>
    <li><a href="/dev/localization/refresh/">Сброс локали при обновлении страницы по F5 или по кнопке Обновить</a></li>
</ul>


<br/>

### Еще куча всевозможных примеров в том числе на уровне БД.

https://technology.amis.nl/2012/08/10/implement-resource-bundles-for-adf-applications-in-a-database-table/
