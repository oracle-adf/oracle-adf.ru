---
layout: page
title: Oracle ADF Получить locale с помощью groovy скриптов
permalink: /dev/localization/groovy/
---

<br/>

# [Oracle ADF] Получить locale с помощью groovy скриптов


{% highlight groovy %}

@VariableExpression(path="p_locale")
def p_locale_VariableScript_Variable()
{
return adf.context.locale.language;
}

{% endhighlight %}
