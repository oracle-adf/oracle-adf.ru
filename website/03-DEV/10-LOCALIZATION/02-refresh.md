---
layout: page
title: Oracle ADF Сброс локали при обновлении страницы по F5 или по кнопке Обновить
permalink: /dev/localization/refresh/
---

<br/>

# [Oracle ADF] Oracle ADF Сброс локали при обновлении страницы по F5 или по кнопке Обновить


Была проблема.  
Создал тикет.  
https://community.oracle.com/message/14592622#14592622


Мы при логине устанавливали программно локаль пользователя в зависимости от настроек учетной записи пользователя в базе данных.

Устанавливали приблизительно так:


{% highlight groovy %}

String language = "en";  
Locale locale = new Locale(language);   
FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);

{% endhighlight %}



Тимо Хан написал, что по F5 viewRoot пересоздается, а мы при этом не вызываем повторо установку нужной локали.


Наш тим лид победил проблему следующим образом:


src/META-INF/adf-settings.xml

{% highlight xml %}

<?xml version="1.0" encoding="UTF-8" ?>
<adf-settings xmlns="http://xmlns.oracle.com/adf/settings">
    <adfc-controller-config xmlns="http://xmlns.oracle.com/adf/controller/config">
        <lifecycle>
            <phase-listener>
                <listener-id>LocalizationPhaseListener</listener-id>
                <class>beans.filters.LocalizationFilter</class>
            </phase-listener>
        </lifecycle>
    </adfc-controller-config>
    <dvt-faces-settings xmlns="http://xmlns.oracle.com/dss/adf/faces/settings">
        <geomap id="mapConfig1" mapViewerUrl="map" geocoderUrl="mapgeocoder"/>
    </dvt-faces-settings>
</adf-settings>

{% endhighlight %}


<br/>
<br/>


{% highlight java %}

package org.javadev.rim.beans.filters;

import java.util.Locale;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;

import oracle.adf.controller.v2.lifecycle.Lifecycle;
import oracle.adf.controller.v2.lifecycle.PagePhaseEvent;
import oracle.adf.controller.v2.lifecycle.PagePhaseListener;


public class LocalizationFilter implements PagePhaseListener {
    @SuppressWarnings("compatibility:6704449372756164727")
    private static final long serialVersionUID = 1685662412939062645L;

    @Override
    public void afterPhase(PagePhaseEvent pagePhaseEvent) {
        if (pagePhaseEvent.getPhaseId() == Lifecycle.PREPARE_RENDER_ID) {
            ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request = (HttpServletRequest) ectx.getRequest();

            String locale = "en";

                Locale locale = new Locale(locale);
                FacesContext.getCurrentInstance()
                            .getViewRoot()
                            .setLocale(locale);
        }
    }

    @Override
    public void beforePhase(PagePhaseEvent pagePhaseEvent) {
    }
}

{% endhighlight %}
