---
layout: page
title: Oracle ADF Локализация для View
permalink: /dev/localization/view/jsf/
---

<br/>

# [Oracle ADF] Локализация для View


<br/>

### Создание бандлов:

a. Создаем property file для существующих языков и для default локали, где текущий путь:
my.project.view.localization

	Название файла (Default): <название, связанное с областью>Bundle.properties (оставляю пустым)
    Название файла (Localized): <название, связанное с областью>Bundle_ru.properties (заполняю данными)
    Название файла (Localized): <название, связанное с областью>Bundle_en.properties (заполняю данными)

b. Добавляем созданный бандл в faces-config.xml

    Application - > Resource Bundle

    Base Name --> путь к bundle ru.myPackage.localization.MyBundle.properties
    Var Name --> myBundle

    Locale config: Default Locale --> ru

    Supported Locale: en, ru

c. Добавляем во ViewController созданный бандл.

    ViewController -> свойства.

    ResourceBundle -> Bundle Search  -> Добавить наш bundle.


d. Прописываем в property file необходимый текст в локализации

Если текст, общий для всего приложения, добавляем текст в CommonBundle.

	Пример:

		common.add = Добавить
    	month.june.short = Июн

	В каждом отдельном проекте:

	myBundle.periodic = Периодическое расписание
	myBundle.delivery.type = Тип доставки

где последнее слово определяет смысл текста для локализации, а предыдущие слова определяют таск флоу.

e. Далее добавляем на страницу этот текст. Формат:

	#{название бандла [‘свойство из бандла’]}

	Пример:

		label="#{myBundle['myBundle.endDate']}"


<br/>

### Получить текст в бине

	Если текст нужно получить в бине с обращением к bundle по умолчанию:

		JSFUtils.getStringFromBundle(key);

	Пример:

		JSFUtils.getStringFromBundle("myBundle.months");

	Если текст нужно получить с бина с обращением к конкретному bundle:

		JSFUtils.getStringFromBundle(bundleName, key);

	Пример:

		JSFUtils.getStringFromBundle(“myBundle”, “myBundle.months")


<br/>

В нашей общей библиотеке ADF-UTILS есть ResourceBundleUtils.  
Наверное, методы из него, следует заменить на JSFUtils.getStringFromBundle


<br/>
<br/>

### Еще есть разные варианты, которые я бы не рекомендовал использовать

В faces-config.xml указываем Message Bundle: myPackage.localization.ViewControllerBundle

И на странице указываем:

    <c:set var="viewcontrollerBundle" value="#{adfBundle['myPackage.localization.ViewControllerBundle']}"/>


И далее работаем как обычно

    label="#{viewcontrollerBundle['myBundle.endDate']}"


<br/>
<br/>

## Дополнительно:

### Локализация для View-Controller

<ul>
    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/ADF_Localization.pdf" rel="nofollow">Localization</a> (не рекомендую так делать)</li>
    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/ADF_JQueryUI_Datepicker.pdf" rel="nofollow">Локализация af:inputDate при помощи Datepicker из JQuery UI</a></li>
</ul>

<br/>

### Reading train stop display names from a resource bundle

Можно вставлять как обчно, т.е. что-то похожее на #{commonBundle['common.dates.month']} вставлять.

Да подвсечивает красным, но собирается и вроде как работает.

<ul>
    <li><a href="https://blogs.oracle.com/jdevotnharvest/reading-train-stop-display-names-from-a-resource-bundle" rel="nofollow">Reading train stop display names from a resource bundle</a></li>
</ul>
