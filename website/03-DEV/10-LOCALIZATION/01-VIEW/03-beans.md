---
layout: page
title: Oracle ADF Локализация текста в бинах
permalink: /dev/localization/view/beans/
---

<br/>

# [Oracle ADF] Локализация текста в бинах


Все уже написано, остается только пользоваться методом getResourceBundleKey в ResourceBundleUtils.


https://bitbucket.org/oracle-adf/adf-utils/src/43e4d7d6d409efb54afd543b3fca0680d60feb87/src/main/java/org/javadev/adf/utils/ResourceBundleUtils.java?at=master&fileviewer=file-view-default


    ResourceBundleUtils.getResourceBundleKey(ConstantsResourceBundle.COMMON_BUNDLE, myKey);


В классе ConstantsResourceBundle перечислены какие бандлы используются и где их найти.

По сути остается передать ключь.  Рекомендуется создать приватную переменную с ключом и далее уже передавать в метод эту переменную.
