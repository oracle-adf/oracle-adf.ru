---
layout: page
title: Oracle ADF Localization > Локализация Основные наработки по VIEW
permalink: /dev/localization/view/
---

<br/>

# [Oracle ADF] Локализация Основные наработки по VIEW:

<br/>

<ul>
    <li><a href="/dev/localization/view/jsf/">Локализация для JSF страниц</a></li>
    <li><a href="/dev/localization/view/jet/">Локализация для JET</a></li>
    <li><a href="/dev/localization/view/beans/">Локализация текста в бинах</a></li>
</ul>
