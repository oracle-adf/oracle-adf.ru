---
layout: page
title: Oracle ADF Создание и вызов javascript в бине
description: Oracle ADF Создание и вызов javascript в бине
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/run-js-from-bean/
---

# [Oracle ADF] Создание и вызов javascript в бине

<br/>

### Hello World Example

{% highlight java linenos %}

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

---

public void onFirstButtonPressed(ActionEvent actionEvent) {

    System.out.println("Buton 1 Begin");

    String buttonId = "pt1:analysis_fragment:pt1:some_other_region:dc_ctb2";

    FacesContext context = FacesContext.getCurrentInstance();

    StringBuilder script = new StringBuilder();

    script.append("alert('HELLO!'); ");

    System.out.println("Result " + script.toString());

    ExtendedRenderKitService erks = Service.getService(context.getRenderKit(), ExtendedRenderKitService.class);
    erks.addScript(context, script.toString());

    System.out.println("Buton 2 END");

}

{% endhighlight %}

<br/>

### Next Step

{% highlight java linenos %}

public void onFirstButtonPressed(ActionEvent actionEvent) {

    System.out.println("Buton 1 Begin");

    String buttonId = "pt1:analysis_fragment:pt1:some_other_region:dc_ctb2";

    FacesContext context = FacesContext.getCurrentInstance();

    StringBuilder script = new StringBuilder();


    script.append("var comp1 = AdfPage.PAGE.findComponentByAbsoluteId('").append(buttonId).append("'); ");

    script.append("alert(comp1); ");

    System.out.println("Result " + script.toString());

    ExtendedRenderKitService erks =
                            Service.getService(context.getRenderKit(), ExtendedRenderKitService.class);
                        erks.addScript(context, script.toString());


    System.out.println("Buton 2 END");

}

{% endhighlight %}

Добиваемся, чтобы на экране появилось сообщение вида:

    AdfRichButton [oracle.adf.RichButton] id = <some_id>

<br/>

### Next Step

{% highlight java linenos %}

public void onFirstButtonPressed(ActionEvent actionEvent) {

    System.out.println("Buton 1 Begin");

    String buttonId = "pt1:analysis_fragment:pt1:some_other_region:dc_ctb2";

    FacesContext context = FacesContext.getCurrentInstance();

    StringBuilder script = new StringBuilder();

    script.append("var comp1 = AdfPage.PAGE.findComponentByAbsoluteId('").append(buttonId).append("'); ");

    script.append("var actionEvent = new AdfActionEvent(comp1); ");
    script.append("actionEvent.forceFullSubmit(); ");
    script.append("actionEvent.noResponseExpected(); ");
    script.append("actionEvent.queue(); ");

    System.out.println("Result " + script.toString());

    ExtendedRenderKitService erks =
                            Service.getService(context.getRenderKit(), ExtendedRenderKitService.class);
                        erks.addScript(context, script.toString());


    System.out.println("Buton 2 END");

}

{% endhighlight %}

Что конкретно делают, forceFullSubmit(), noResponseExpected() не знаю.
Потому, что быдлокодер.

<br/>

### Помогло:

https://community.oracle.com/thread/2326406?tstart=0

<br/>

### Еще примеры:

<ul>
<li><a href="http://www.awasthiashish.com/2012/11/popup-component-in-oracle-adf.html" rel="nofollow">PopUp component in Oracle ADF, Use showPopUpBehavior/JavaScript to show PopUp</a></li>
</ul>
