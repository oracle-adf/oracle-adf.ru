---
layout: page
title: Oracle ADF Таймер обратного отсчета
description: Oracle ADF Таймер обратного отсчета
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/count-down-timer-console/
---

# Oracle ADF Таймер обратного отсчета в консоль браузера (здесь скорее всего неправильная логика вычисления секунд)

<br/>

Печатает в консоль браузера!

    <af:outputText value="" id="timerText" clientComponent="true">
      <af:resource type="javascript" source="resources/js/timer.js?1"/>
      <af:clientListener method="countDown()" type="load"/>
    </af:outputText>

<br/>

timer.js

    function countDown() {

    var fiveMinutes = 60 * 5;
        startTimer(fiveMinutes);
    }


    function startTimer(duration) {
        console.log("START TIMER");

        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            console.log(minutes + ":" + seconds);

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }


    window.onload = countDown;

<br/>

### Oracle ADF Таймер обратного отсчета во View

<br/>

Печатает во view!

Тоже самое, но только после

    console.log(minutes + ":" + seconds);

Добавил:

    var comp = AdfPage.PAGE.findComponentByAbsoluteId('pt1:timerText');
    comp.setValue(minutes + ":" + seconds);

<br/>

### Тоже самое, но еще и с учетом часов:

    hours = parseInt(timer / 3600, 10);
    minutes = parseInt((timer - hours * 3600) / 60, 10);
    seconds = parseInt((timer - hours * 3600) % 60, 10);

    hours   = hours   < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
