---
layout: page
title: Oracle ADF Вызов метода бина с помощью JavaScript
description: Oracle ADF Вызов метода бина с помощью JavaScript
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/run-method-from-bean-by-javascript/
---

# [Oracler ADF] Вызов метода бина с помощью JavaScript

В данном случае на примере panelSplitter. По изменению бегунка (или как он там называется), должна программно обновляться panelSplitter.
Для этого вызываем метод, описанный в бине.

    <af:resource type="javascript">
          function disclosePanelSplitter(event) {
               var inputComp = event.getSource();
               AdfCustomEvent.queue(inputComp, "myDiscloseEvent",{}, true);
          }
    </af:resource>

<br/>

    ***

        <af:clientListener method="disclosePanelSplitter" type="propertyChange"/>
        <af:serverListener type="myDiscloseEvent"  method="#{MerchHier.doCustomEvent}"/>

    </af:panelSplitter>

 <br/>

    // Геттеры и Сеттеры

    private RichPanelSplitter panelSplitter;

    public void setPanelSplitter(RichPanelSplitter panelSplitter) {
    this.panelSplitter = panelSplitter;
    }

    public RichPanelSplitter getPanelSplitter() {
        return panelSplitter;
    }


    // Метод который обновляет panelSplitter

     public void doCustomEvent(ClientEvent event){
        AdfFacesContext.getCurrentInstance().addPartialTarget(panelSplitter);
    }

Можно посмотреть:

http://docs.oracle.com/cd/E15523_01/apirefs.1111/e12419/tagdoc/af_serverListener.html

У Френка синтаксическая ошибка в коде:

https://blogs.oracle.com/jdevotnharvest/entry/how-to_call_server_side_java_from_javascript

А вообще делал по примеру, но здесь забыли добавить serverListener

https://community.oracle.com/thread/2532146
