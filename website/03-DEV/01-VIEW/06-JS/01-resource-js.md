---
layout: page
title: Oracle ADF где писать JavaScript
description: Oracle ADF где писать JavaScript
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/resource/
---

# Oracle ADF где писать JavaScript

Так лучше не делать.
А лучше вынести в отдельный файл.

    <af:resource type="javascript">

        function handleTableDoubleClick(evt) {
            var myVar = evt.getSource();
            console.log("Step1 Completed");
            evt.cancel();
        }


       function printInvoked(event) {
           var inputComp = event.getSource();
           console.log("Step1 Completed");
           var comp = AdfPage.PAGE.findComponentByAbsoluteId('myComponentId');

           alert(comp);

              if(comp != null) {
                   ActionEvent.queue(comp, true);
              }

           event.cancel();
       }

    </af:resource>

<br/>
<br/>

**Лучше вот так:**

    <af:resource type="javascript" source="/js/myCompanyName.js"/>
