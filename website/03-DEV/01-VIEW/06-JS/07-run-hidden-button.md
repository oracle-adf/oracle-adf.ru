---
layout: page
title: Oracle ADF Нажать на скрытую кнопку с помощью JavaScript (ComponentReference)
description: Oracle ADF Нажать на скрытую кнопку с помощью JavaScript (ComponentReference)
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/run-hidden-button/
---

# [Oracle ADF] Нажать на скрытую кнопку с помощью JavaScript (ComponentReference)

Итак, есть 2 кнопки.  
Одна для печати pdf, а другая, должна выполнять какую-нибудь логику предшествующую нажатию на эту кнопку.

Сделано т.к. никакой логики добавить в событие печати не получилось.

Вообще обузить можно везде, где нужно вызвать кнопку на форме, когда непонятно какой у нее id.

А непонятен он т.к. этот id может быть динамическим.

<br/>

<br/>

{% highlight xml linenos %}

<af:button id="b6"
**_
actionListener="#{pageFlowScope.OrdersSchedulePDFBean.onPrintCliked}">
</af:button>
<af:button id="b7"
_**
binding="#{pageFlowScope.MyBean.hiddenPrintButton}" visible="false">
<af:fileDownloadActionListener method="#{pageFlowScope.MyBean.exportToPDF}" contentType="application/pdf" filename="myPDF.pdf"/>
</af:button>

{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

// ---------------------------------------

private ComponentReference hiddenPrintButton;

public void setHiddenPrintButton(RichButton hiddenPrintButton) {
this.hiddenPrintButton = ComponentReference.newUIComponentReference(hiddenPrintButton);
}

public RichButton getHiddenPrintButton() {
if (hiddenPrintButton != null) {
return (RichButton) hiddenPrintButton.getComponent();
} else {
return null;
}
}

// ---------------------------------------

private void onPrintCliked() {

        if (// something true) {
            // вызвать печать
            doRun(this.getHiddenPrintButton().getClientId(FacesContext.getCurrentInstance()));

        } else {
            ShowMessage.onScreen("MY MESSAGE");
        }

}

public void exportToPDF(FacesContext facesContext, OutputStream outputStream) {

    // логика по печати pdf

}

{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

public static void doRun(String buttonId) {
FacesContext context = FacesContext.getCurrentInstance();

    StringBuilder script = new StringBuilder();

    script.append("var comp1 = AdfPage.PAGE.findComponentByAbsoluteId('")
          .append(buttonId)
          .append("'); ");

    script.append("var actionEvent = new AdfActionEvent(comp1); ");
    script.append("actionEvent.forceFullSubmit(); ");
    script.append("actionEvent.noResponseExpected(); ");
    script.append("actionEvent.queue(); ");

    ExtendedRenderKitService erks = Service.getService(context.getRenderKit(), ExtendedRenderKitService.class);
    erks.addScript(context, script.toString());

}

{% endhighlight %}
