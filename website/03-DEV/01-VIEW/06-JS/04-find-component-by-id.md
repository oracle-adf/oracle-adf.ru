---
layout: page
title: Oracle ADF findComponentByAbsoluteId
description: Oracle ADF findComponentByAbsoluteId
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/findComponentByAbsoluteId/
---

# [Oracle ADF] findComponentByAbsoluteId

      function disclosePanelSplitter(event) {

      console.log("Step1");
           var comp = AdfPage.PAGE.findComponentByAbsoluteId('d3');
          event.cancel();

      }

<br/>

Смотри еще примеры:

https://community.oracle.com/thread/2326406?tstart=0
