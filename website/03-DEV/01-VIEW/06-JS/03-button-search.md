---
layout: page
title: Oracle ADF Выполнение поиска по нажатию ENTER в поле Input
description: Oracle ADF Выполнение поиска по нажатию ENTER в поле Input
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/button-pressed/
---

# [Oracle ADF] Выполнение поиска по нажатию ENTER в поле Input

По нажатию на Enter в поле поиска выполняется поиск, без необходимости дополнительно мышкой нажимать на кнопку поиска.

    <af:resource type="javascript">
      function handleEnterEvent(evt) {
          var _keyCode = evt.getKeyCode();
          if (_keyCode == AdfKeyStroke.ENTER_KEY) {
              var comp = evt.getSource();
              var button = comp.findComponent('searchLink');
              AdfActionEvent.queue(button, button.getPartialSubmit());
              evt.cancel();
          }
      }
    </af:resource>

И собственно в Input добавляю clientListener

    <af:inputText value="#{bindings.inputParameter.inputValue}" label=""
                  required="#{bindings.inputParameter.hints.mandatory}" columns="40"
                  maximumLength="#{bindings.inputParameter.hints.precision}"
                  shortDesc="#{bindings.inputParameter.hints.tooltip}" id="it1"
                  styleClass="searchIT">
        <f:validator binding="#{bindings.inputParameter.validator}"/>
        <af:clientListener method="handleEnterEvent" type="keyPress"/>
    </af:inputText>
    <af:commandButton id="searchLink"
             actionListener="#{bindings.applySearchSuppliers.execute}"
             disabled="#{!bindings.applySearchSuppliers.enabled}"
                      styleClass="submitButton"/>
    </af:panelGroupLayout>

Обратить внимание на clientListener
