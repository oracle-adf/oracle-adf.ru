---
layout: page
title: JavaScript в Oracle ADF
description: JavaScript в Oracle ADF
keywords: Oracle ADF, view, javascript
permalink: /dev/view/js/
---

# JavaScript в Oracle ADF

<ul>
    <li><a href="/dev/view/js/resource/">Oracle ADF где писать JavaScript</a></li>
    <li><a href="/dev/view/js/run-method-from-bean-by-javascript/">Вызов метода бина с помощью JavaScript</a></li>
    <li><a href="/dev/view/js/button-pressed/">Выполнение поиска по нажатию ENTER в поле Input</a></li>
    <li><a href="https://bitbucket.org/oracle-adf/adf-sample-applications/src/master/PopupSample/" rel="nofollow">Приложение с фокусировкой на нужное поле с использованием javascript</a></li>
    <li><a href="/dev/view/js/findComponentByAbsoluteId/">findComponentByAbsoluteId</a></li>
    <li><a href="https://community.oracle.com/thread/2495237?tstart=0" rel="nofollow">ADF showPrintablePageBehavior with action</a></li>

    <li><a href="http://www.awasthiashish.com/2015/08/set-adf-faces-component-properties.html" rel="nofollow">Set ADF Faces Component properties using custom javascript</a></li>
    <li><a href="http://sameh-nassar.blogspot.ru/2010/02/javascript-functions-sample.html" rel="nofollow">JavaScript With ADF Faces Samples</a></li>

</ul>

<br/>

### Распечатать в консоли все элементы на странице

{% highlight xml linenos %}

StringBuilder script = new StringBuilder();

script.append(" var all = document.getElementsByTagName(\"\*\"); ");

script.append(" for (var i=0, max=all.length; i < max; i++) { ");

script.append(" console.log(all[i]); ");

script.append(" } ");

JsUtils.runJS(script);

{% endhighlight %}

<br/>

### Примеры

<ul>
    <li><a href="/dev/view/js/count-down-timer-console/">Таймер обратного отсчета</a></li>
</ul>

<br/>

### JavaScript в бине

<ul>
    <li><a href="/dev/view/js/run-js-from-bean/">Нажать на скрытую кнопку с помощью JavaScript (ComponentReference)</a></li>
    <li><a href="/dev/view/js/run-js-from-bean/">Создание и вызов javascript в бине (По кнопке)</a></li>
</ul>

<!-- <br/>

Если нужно найти например, кнопку, возможно, что нужно, чтобы у нее было выставлено значение clientComponent="true". -->

<br/>

### ADF TabIndex:

<ul>
    <li><a href="https://blog-raphaufrj.rhcloud.com/how-to-specify-a-form-tab-index-in-adf/" rel="nofollow">TabIndex</a> <a href="http://archive.li/RS4jx" rel="nofollow">(копия)</a></li>
</ul>

<br/>

### JavaScript API Reference for Oracle ADF Faces:

http://docs.oracle.com/html/E12046_03/oracle/adfinternal/view/js/laf/dhtml/rich/AdfFocusUtils.html

### ADF Faces Rich Client Development

http://biemond.blogspot.ru/2010/11/adf-faces-rich-client-development.html

копия навсяк:  
http://archive.li/S4c7R

src:  
https://bitbucket.org/oracle-adf/adf-faces-rich-client-development
