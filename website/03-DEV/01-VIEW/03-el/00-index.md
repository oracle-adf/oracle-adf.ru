---
layout: page
title: Oracle ADF - Expression Language (EL)
description: Oracle ADF - Expression Language (EL)
keywords: Oracle ADF, view, Expression Language (EL)
permalink: /dev/view/el/
---

<br/>

# [Oracle ADF] Expression Language (EL)

<br/>

<ul>
    <li><a href="/dev/view/el/end-or-not/">And OR в Not в EL</a></li>
    <li><a href="/dev/view/el/null-checks/">Проверка на null на страницах с помощью EL</a></li>
</ul>
