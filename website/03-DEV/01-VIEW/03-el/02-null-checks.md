---
layout: page
title: Oracle ADF Проверка на null во View с помощью EL
description: Oracle ADF Проверка на null во View с помощью EL
keywords: Oracle ADF, view, Expression Language (EL)
permalink: /dev/view/el/null-checks/
---

# [Oracle ADF] Проверка на null во View с помощью EL

<br/>

Пример 1:

    <af:outputFormatted value=" #{item.bindings.Phone.inputValue == null ? '' : item.bindings.Phone.inputValue }" id="of5"/>

<br/>

Пример 2:

    #{empty sessionScope.OBJECT_FROM_SESSION.getSomething() ? '!!! ОШИБКА !!!' : sessionScope.OBJECT_FROM_SESSION.getSomething()}

<br/>

Пример 3:

    visible="#{!empty bindings.PromoId.inputValue}"
    visible="#{pageFlowScope.p_edit == 'Y'}}"
