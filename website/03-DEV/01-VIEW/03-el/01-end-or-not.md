---
layout: page
title: Oracle ADF And OR в Not в EL
description: Oracle ADF And OR в Not в EL
keywords: Oracle ADF, view, Expression Language (EL)
permalink: /dev/view/el/end-or-not/
---

# [Oracle ADF] And OR в Not в EL

<br/>

Лучше всегда скобки ставить !!!

    Операторы && || не работают !!!

<br/>

Пример 1:

    readOnly="#{(!empty bindings.PromoId.inputValue) or (pageFlowScope.p_edit == 'Y')}"
