---
layout: page
title: Oracle ADF > Жизненный цикл
description: Oracle ADF > Жизненный цикл
keywords: Oracle ADF, view, phases
permalink: /dev/view/phases/
---

# [Oracle ADF] Жизненный цикл:

<br/>

    PhaseId.RESTORE_VIEW
    PhaseId.APPLY_REQUEST_VALUES
    PhaseId.PROCESS_VALIDATIONS
    PhaseId.UPDATE_MODEL_VALUES
    PhaseId.INVOKE_APPLICATION
    PhaseId.UPDATE_MODEL_VALUES
    PhaseId.RENDER_RESPONSE

    PhaseId.ANY_PHASE

<br/>

    if (phaseId.equals(PhaseId.APPLY_REQUEST_VALUES)){

    // some logic

    }

<br/>

Если не будет работать пример выше, можно попробовать...

if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE) {
// Do here your job which should run right before the RENDER_RESPONSE.
}

<br/>

To get access to the current PhaseId in a managed bean, you use a local phase listener
method configured on the f:view element:

    private PhaseId phaseId = null;
    […]
    public void beforePhaseListener(PhaseEvent phaseEvent) {
    phaseId = phaseEvent.getPhaseId();
    }

### Еще один пример

    if (pagePhaseEvent.getPhaseId() == Lifecycle.Lifecycle.INIT_CONTEXT_ID){
    //...
    }

<br/>

https://blogs.oracle.com/jdevotnharvest/entry/how_to_configure_an_adf_phase_listener_and_where_to_put_the_file
