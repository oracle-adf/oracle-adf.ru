---
layout: page
title: Oracle ADF - Вызов методов до и после построения формы
description: Oracle ADF - Вызов методов до и после построения формы
keywords: Oracle ADF, view, phases
permalink: /dev/view/phases/before-and-after-phase/
---

# [Oracle ADF] Вызов методов до и после построения формы:

<br/>

    <?xml version='1.0' encoding='UTF-8'?>
    <jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.1" xmlns:f="http://java.sun.com/jsf/core"
              xmlns:af="http://xmlns.oracle.com/adf/faces/rich" xmlns:h="http://java.sun.com/jsf/html">
        <jsp:directive.page contentType="text/html;charset=UTF-8"/>

        <f:view beforePhase="#{mainbean.beforePhaseLoad}" afterPhase="#{mainbean.afterPhaseLoad}">

            <af:document title="Название документа" id="d1">


    ****

<br/>

     public void beforePhaseLoad(PhaseEvent phaseEvent) {
            System.out.println("BeforePhaseLoad");
        }


     public void afterPhaseLoad(PhaseEvent phaseEvent) {
            System.out.println("AfterPhaseLoad");
        }
