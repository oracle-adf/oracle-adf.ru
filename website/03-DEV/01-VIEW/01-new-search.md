---
layout: page
title: Oracle ADF Новый поиск
description: Oracle ADF Новый поиск
keywords: Oracle ADF, view, search
permalink: /dev/view/new-search/
---

# [Oracle ADF] Новый поиск

<br/>

1. Добавляю во ViewObject поля для поиска

Название + код

    , j.loc_name || h.to_loc loc_search
    , r.sup_name || h.from_loc sup_search

2. Создаю VC с CONSTANTS
   all, 50, removable

3. Датаконтрол --> Named Criteria --> первый вариант
   В resultComponentId указать id Таблицы
