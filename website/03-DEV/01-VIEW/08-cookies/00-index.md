---
layout: page
title: Oracle ADF Cookies
description: Oracle ADF Cookies
keywords: Oracle ADF, view, cookies
permalink: /dev/view/cookies/
---

<br/>

# [Oracle ADF] Cookies

ВНИМАНИЕ! СЛЕДУЮЩИМ СПОСОБОМ ОНИ СТАВЯТСЯ БЕЗ КАКОГО ЛИБО ПРИМЕНЕНИЯ СЕКЬЮРИТИ.
Т.Е. Т.Е. КАК ЕСТЬ В ЯВНОМ ВИДЕ!

**Установить Cookies**

    FacesContext facesContext = FacesContext.getCurrentInstance();
    ExternalContext externalContext = facesContext.getExternalContext();
    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
    HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

    if (this.rememberMeButton.isSelected()){

        int cookieAge = 14 * 24 * 60 * 60; // Two weeks

        Cookie cookieLogin = new Cookie("cookieLogin.cookie", this._username);
        cookieLogin.setMaxAge(cookieAge);
        response.addCookie(cookieLogin);

        Cookie cookiePass = new Cookie("cookiePass.cookie", this._password);
        cookiePass.setMaxAge(cookieAge);
        response.addCookie(cookiePass);

    }

**Считать**

    FacesContext facesContext = FacesContext.getCurrentInstance();
    ExternalContext externalContext = facesContext.getExternalContext();
    Map<String, Object> cookies = externalContext.getRequestCookieMap();

    Cookie cookieLogin = (Cookie)cookies.get("cookieLogin.cookie");
    Cookie cookiePass = (Cookie)cookies.get("cookiePass.cookie");

    String cookieLogin1 = cookieLogin.getValue();
    String cookiePass1 = cookiePass.getValue();

<br/>
<br/>

my.cookieLogin.cookie и my.cookiePass.cookie - следует вынести в файл с константами!

<br/>

Чтобы работало cookieLogin.setSecure(true); - нужно дополнительно произвести настройки на сервере. Внизу ссылка.

<br/>
<br/>

Examples:

<ul>

<li><a href="http://learnfrommike.blogspot.ru/2013/12/adding-remember-me-functionality-to.html" rel="nofollow">Adding “remember me” functionality to Oracle ADF login</a></li>

<li><a href="http://redmavericks.com/blog/2015/10/02/adf-application-event-listeners-querystring-parameters-reinjection-between-sessions-part-iii/" rel="nofollow">ADF Application Event Listeners, QueryString Parameters reinjection between sessions – Part III</a></li>

<li><a href="https://github.com/vjaeng/FusionOrderDemo_R1PS6/blob/master/SupplierModule/ViewController/src/oracle/fodemo/security/Login.java" rel="nofollow">FusionOrderDemo_R1PS6</a></li>

<li><a href="https://docs.oracle.com/cd/E21764_01/doc.1111/e14308/securecookies.htm#OMADM4243" rel="nofollow">Enabling Secure Cookies</a></li>

</ul>
