---
layout: page
title: Oracle ADF - View-Controller Layer
description: Oracle ADF - View-Controller Layer
keywords: Oracle ADF, view,  View-Controller Layer
permalink: /dev/view/
---

# [Oracle ADF] View-Controller Layer

<br/>

### Компоненты и примеры работы с ними:

<ul>
    <li><a href="/dev/view/uic/">Основные компоненты форм</a> (будут добавляться по мере необходимости работы с ними)</li>
</ul>

<br/>

### Бины (Beans):

<ul>
    <li><a href="/dev/view/beans/">Бины</a></li>
</ul>

<br/>

### Bindings:

<br/>

![Oracle adf Bindings](/img/dev/view/bindings/bindings.gif 'Oracle adf Bindingsindings'){: .center-image }

<br/>
<br/>

<ul>
    <li><a href="/dev/view/bindings/">Bindings</a></li>
</ul>

<br/>

### События (Events):

<ul>
    <li><a href="/dev/view/events/">События (Events)</a></li>
</ul>

<br/>

### Expression Language (EL)

<ul>
    <li><a href="/dev/view/el/">Expression Language (EL)</a></li>
</ul>

<br/>

### Retrieve GET parameters from URL in Managed Bean in Oracle ADF:

<ul>
    <li><a href="http://techutils.in/2015/09/10/retrieve-get-parameters-from-url-in-managed-bean-in-oracle-adf/" rel="nofollow">Retrieve GET parameters from URL in Managed Bean in Oracle ADF</a></li>
</ul>

 <br/>

### Работа с сессионными параметрами

<ul>
    <li><a href="/dev/view/session/">Работа с сессионными параметрами</a></li>
</ul>

<br/>

### Cookies:

<ul>
    <li><a href="/dev/view/cookies/">Cookies</a></li>
</ul>

<br/>

### Примеры связанные с жизненным циклом:

<ul>
    <li><a href="/dev/view/phases/">PhaseId</a></li>
    <li><a href="/dev/view/phases/before-and-after-phase/">Вызов методов до и после построения формы</a></li>
    <li><a href="https://blogs.oracle.com/aramamoo/an-epic-question-how-to-call-a-method-when-the-page-loads-v2" rel="nofollow">How to call a method when the page loads</a></li>
</ul>

<br/>

### Localization:

<ul>
    <li><a href="/dev/localization/">Localization</a></li>
</ul>

<br/>

### ADF_Logger:

<ul>
    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/ADF_Logger.pdf" rel="nofollow">ADF_Logger</a></li>
</ul>

<br/>

### Примеры использования JavaScript на форме:

<ul>
    <li><a href="/dev/view/js/">Примеры использования JavaScript</a></li>
</ul>

<br/>

### How to Use CAPTCHA in ADF application:

<ul>
    <li><a href="http://prabhat-java.blogspot.ru/search?updated-min=2015-01-01T00:00:00-08:00&updated-max=2015-07-13T10:23:00-07:00&max-results=19&start=11&by-date=false" rel="nofollow">How to Use CAPTCHA in ADF application</a></li>
</ul>

<br/>

### Upload & Download файлов:

<ul>
    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/UploadDownloadInADF.pdf" rel="nofollow">How to Use CAPTCHA in ADF application</a></li>
</ul>

<br/>

### Работаем с Web-камерой через Flash плагин jpegcam:

<ul>
    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/ADFWebCamWithFlashPlugin.pdf" rel="nofollow">Работаем с Web-камерой через Flash плагин jpegcam</a></li>
</ul>

<br/>

### Повторный выбор selectOneChoice (Сделать так, чтобы событие срабатывало повторно, когда это же событие выполнялось последний раз):

    RowKeySet rks = new RowKeySetImpl();
    this.lv1.setSelectedRowKeys(rks);
