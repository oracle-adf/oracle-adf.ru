---
layout: page
title: Oracle ADF > Bindings Layer > action
description: Oracle ADF > Bindings Layer > action
keywords: Oracle ADF, view, bindings, action
permalink: /dev/view/bindings/bindings/action/
---

# [Oracle ADF] Bindings Layer > action

<br/>

### Примеры вызова OperationBinding

В bindigs создаем action типа Commit, Rollback, Execute с одноименными названиями

{% highlight java linenos %}

private String commit = "commit";
private String rollback = "Rollback";
private String execute = "Execute";

private String executeWithParams = "ExecuteWithParams";
private String myParam1 = "myParam1";
private String myParam2 = "myParam2";

private void commit() {
DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
OperationBinding operationBinding = bindings.getOperationBinding(commit);
operationBinding.execute();

    // Наверное, лучше сразу попробовать такое
    // ADFUtils.findOperation(commit).execute();

}

private void rollback() {
DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
OperationBinding operationBinding = bindings.getOperationBinding("rollback");
operationBinding.execute();
}

private void execute() {
DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
OperationBinding operationBinding = bindings.getOperationBinding(execute);
operationBinding.execute();
}

private void executeWithParams() {
DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
OperationBinding operationBinding = bindings.getOperationBinding(executeWithParams);

    operationBinding.getParamsMap().put("my_param1", myParam1);
    operationBinding.getParamsMap().put("my_param2", myParam2);


    // Если нужно передать null. Можно попробовать выполнить это следующей командой.
    // Я пытался передавать null, "null", вообще не выполнять эту команду, но получилось только следующим образом.

    // operationBinding.getParamsMap().put("my_param2", "");


    Object result1 = operationBinding.execute();

    if(!operationBinding.getErrors().isEmpty()){
            System.out.println("Error");
        }  else {
            System.out.println("Success");
    }

}

this.commit();
this.rollback();
this.execute();
this.executeWithParams();

{% endhighlight %}

<br/>

## Еще пример с использованием ADFUtils:

<br/>

{% highlight java linenos %}

OperationBinding getTaskoperation = ADFUtils.findOperation("ExecuteWithParams");
getTaskoperation.getParamsMap().put("myParam1", "myValue1");
getTaskoperation.getParamsMap().put("myParam2", "myValue2");
Object result = getTaskoperation.execute();

{% endhighlight %}

<br/>

## И еще у меня осталось в закладках:

http://matteoformica.blogspot.ru/2014/02/executing-operation-binding.html
