---
layout: page
title: Oracle ADF > Bindings Layer > tree
description: Oracle ADF > Bindings Layer > tree
keywords: Oracle ADF, view, bindings, tree
permalink: /dev/view/bindings/bindings/tree/
---

# [Oracle ADF] Oracle ADF > Bindings Layer > tree

<br/>

{% highlight java %}

BindingContext bctx = BindingContext.getCurrent();
BindingContainer bindings = bctx.getCurrentBindingsEntry();

// Tree
JUCtrlHierBinding res2 = (JUCtrlHierBinding) bindings.get("bindingName");

for (Row r : res2.getAllRowsInRange()) {

    System.out.println(r.getAttribute("attributeId") + " " +  r.getAttribute("attributeName"));

}

{% endhighlight %}

<br/>

{% highlight java %}

//the data in the suggest list is queried by a tree binding.
JUCtrlHierBinding hierBinding = (JUCtrlHierBinding) bindings.get("MY_TREE");
//requery the list based on the new bind variable values
hierBinding.executeQuery();

//The rangeSet, the list of queries entries, is of type
//JUCtrlValueBndingRef.
List<JUCtrlValueBindingRef> displayDataList = hierBinding.getRangeSet();

ArrayList<SelectItem> selectItems = new ArrayList<SelectItem>();
for (JUCtrlValueBindingRef displayData : displayDataList) {
Row rw = displayData.getRow();
//populate the SelectItem list
SelectItem si = new SelectItem((String) rw.getAttribute("MY_DESCRIPTION"), (String) rw.getAttribute("MY_DESCRIPTION"));
String item = (String) rw.getAttribute("MY_NAME");

    selectItems.add(si);

}

{% endhighlight %}
