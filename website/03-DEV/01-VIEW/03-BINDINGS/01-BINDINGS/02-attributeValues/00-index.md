---
layout: page
title: Oracle ADF > Bindings Layer > attributeValues
description: Oracle ADF > Bindings Layer > attributeValues
keywords: Oracle ADF, view, bindings, attributeValues
permalink: /dev/view/bindings/bindings/attributeValues/
---

# [Oracle ADF] Oracle ADF > Bindings Layer > attributeValues

<br/>

### GET VALUE FROM ATTRIBUTE BINDING IN BEAN

{% highlight java %}

((oracle.binding.AttributeBinding)bindings.getControlBinding("bindingName")).getInputValue();

{% endhighlight %}

<br/>
<br/>

Я делаю вот так.

{% highlight java %}

BindingContext bctx = BindingContext.getCurrent();
BindingContainer bindings = bctx.getCurrentBindingsEntry();

// Attribute Values
AttributeBinding res1 = (AttributeBinding) bindings.get("bindingName");

System.out.println(" res1 " + res1);

{% endhighlight %}

<br/>

### Доступ к параметрам файла PageDef.

    DCBindingContainer dcBindings = (DCBindingContainer)getBindings();
    DCParameter param = dbBindings().findParameter("deptno");
    String paramValue = (String)param.getValue();

<br/>

# [Oracle ADF] Получение и присвоение значений атрибутов с помощью bindings

<br/>

Получение:

    AttributeBinding deptBinding = (AttributeBinding)getBindings().getControlBinding("DepartmentName");
    String departmentName = (String)deptBinding.getInputValue();

<br/>

Присвоение:

    AttributeBinding deptBinding = (AttributeBinding)getBindings().getControlBinding("DepartmentName");
    deptBinding.setInputValue("Special Projects");

### Пример:

Имеем

    value="#{bindings.p_sup_name.inputValue}"
    actionListener="#{SearchTest.searchInputChanged}"

В bindings при наведении на нее пишет attributeValues

Нужно получить ее значение в бине.
Сделал так. Наверняка можно и попроще.

<br/>

    import java.io.UnsupportedEncodingException;

    import javax.faces.event.ActionEvent;

    import oracle.binding.BindingContainer;
    import oracle.binding.AttributeBinding;
    import oracle.adf.model.BindingContext;

    ***

    public void searchInputChanged(ActionEvent actionEvent) {

        BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();

        AttributeBinding attr = (AttributeBinding)bindings.getControlBinding("p_sup_name");
        String val = (String) attr.getInputValue();

    }

<br/>

Можно еще так:

<br/>

    BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
    AttributeBinding attr =  ADFUtils.findControlBinding(bindings, "p_sup_name");

<br/>

Еще что-то есть:

<br/>

    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    OperationBinding ob = bindings.getOperationBinding("Commit");

    AttributeBinding byHour = (AttributeBinding) bindings.get("MY_ATTR");

<br/>

### Получить и установить программно данные attributeValues типа Boolean из bindings

Имеем Attribute ссылающийся на переменную типа Boolean. Со значением по умолчанию true.

Нужно по нажатию на кнопку поменять значение на противоположено.

<br/>

### Вариант 1

    <af:button text="ON | OFF" id="b1" partialSubmit="true">
        <af:setPropertyListener from="#{!bindings.MySwitchAttr.inputValue}" to="#{bindings.MySwitchAttr.inputValue}" type="action"/>
    </af:button>

<br/>

### Вариант 2

    public void onButtonClick(ActionEvent actionEvent) {
        boolean result = (Boolean)ADFUtils.getBoundAttributeValue("MySwitchAttr");
        ADFUtils.setBoundAttributeValue("MySwitchAttr", result ? false : true);
    }

Примеры:  
https://bitbucket.org/oracle-adf/attribute-bindings-example
