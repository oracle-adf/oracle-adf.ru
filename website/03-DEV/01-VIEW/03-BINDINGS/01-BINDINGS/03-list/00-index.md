---
layout: page
title: Oracle ADF > Bindings Layer > list
description: Oracle ADF > Bindings Layer > list
keywords: Oracle ADF, view, bindings, list
permalink: /dev/view/bindings/bindings/list/
---

# [Oracle ADF] Oracle ADF > Bindings Layer > list

<br/>

### GET VALUE FROM LIST BINDING IN BEAN

{% highlight java %}

((JUCtrlListBinding)bindings.getControlBinding("bindingName")) .attributeValue();

{% endhighlight %}
