---
layout: page
title: Oracle ADF > Bindings Layer
description: Oracle ADF > Bindings Layer
keywords: Oracle ADF, view, bindings
permalink: /dev/view/bindings/bindings/
---

# [Oracle ADF] Bindings Layer - Bindings

<br/>

BindingsUtils.java

https://bitbucket.org/oracle-adf/adf-utils/src/087f677cba287be7b1c3cebf038a20ed2cbff7e4/src/main/java/org/javadev/adf/utils/BindingsUtils.java

<br/>

### Получить программно bindings текущей страницы:

{% highlight java linenos %}

DCBindingContainer myBindingContainer = ADFUtils.getBindingContainer();
DCBindingContainer myDCBindingContainer = ADFUtils.getDCBindingContainer();

{% endhighlight %}

<br/>

### Еще вариант

{% highlight java linenos %}

import oracle.adf.model.BindingContext;
import oracle.binding.BindingContainer;

/**\***Generic Method to get BindingContainer of current page, fragment or region\*\*/
public BindingContainer getBindingsCont() {
return BindingContext.getCurrent().getCurrentBindingsEntry();
}

{% endhighlight %}

<br/>

### Получить программно bindings произвольной страницы (по PageDef):

<br/>

Перехожу на страницу, bindings которой хочу получить.  
Далее в Bindings добираюсь до DataBindings.cpx

Захожу в исходники DataBindings.cpx и ищу по названию траницы UsageId.

<br/>

{% highlight java linenos %}

// -- findBindingContainer взято из ADFUtils - там приватный метод, я не стал менять оригинальные тексты.
BindingContainer bindings = BindingsUtils.findBindingContainer(ConstantsPegeDefs.MY\_\_PAGE_DEF);

{% endhighlight %}

<br/>

### Еще вариант

<br/>

Получаю UsageId способом описанным выше.

<br/>

{% highlight java linenos %}

    BindingContainer myBindingContainer = BindingsUtils.getBindingsContainerByPageDef(pageUsageId);

{% endhighlight %}

<br/>

Можно почитать:  
http://www.awasthiashish.com/2014/08/access-bindingcontainer-page-bindings.html

<br/>

### Доступ к данным и методам.

    FacesContext ctx = FacesContext.getCurrentInstance();
    Application app = ctx.getApplication();
    ValueBinding bind = app.createValueBinding("#{bindings.DepartmentName.inputValue}");
    String departmentName = (String)bind.getValue(ctx);

<br/>

### Примеры реализации:

<ul>
    <li><a href="/dev/view/bindings/bindings/action/">action</a></li>
    <li><a href="/dev/view/bindings/bindings/attributeValues/">attributeValues</a></li>
    <li>button</li>
    <li>eventBinding</li>
    <li>graph</li>
    <li><a href="/dev/view/bindings/bindings/list/">list</a></li>
    <li>listOfValues</li>
    <li><a href="/dev/view/bindings/bindings/methodAction/">methodAction</a></li>
    <li>navigationlist</li>
    <li>table</li>
    <li><a href="/dev/view/bindings/bindings/tree/">tree</a></li>
    <li>treetable</li>
</ul>

<br/>

### Еще какие-то примеры:

<ul>
    <li><a href="http://www.awasthiashish.com/2015/02/working-with-afiterator-and-afforeach.html" rel="nofollow">Working with af:iterator and af:forEach programmatically (Add and delete records using List)</a></li>
    <li><a href="http://www.awasthiashish.com/2015/02/populate-afiterator-and-afforeach.html" rel="nofollow">Working with af:iterator and af:forEach programmatically (Populate values using POJO from Managed Bean)</a></li>
</ul>
