---
layout: page
title: Oracle ADF > Bindings Layer > methodAction
description: Oracle ADF > Bindings Layer > methodAction
keywords: Oracle ADF, view, bindings, methodAction
permalink: /dev/view/bindings/bindings/methodAction/
---

# [Oracle ADF] Bindings Layer > methodAction

<br/>

Нужно чтобы вызывался какой-нибудь метод.
(Или хранимая процедура / функция)

<br/>

Идем в AppModuleImpl.java

<br/>

Создаем какой-нибудь метод:

<br/>

{% highlight java linenos %}

    public void my_method_action(String param1, String param2, String param3) {

        String err_msg;
        long rslt;

        CallableStatement st = null;
        try {
            st =
                getDBTransaction()
                .createCallableStatement("begin ? := sys.myutil.bool_to_int(  " +
                                         "     myPackage.myMethod(?,?,?,? )); end;", 0);
            //
            st.registerOutParameter(1,
                                    Types.NUMERIC); //result - Boolean parameters are translated from/to integers:  0/1/null <--> false/true/null

            st.setString(2, param1);
            st.setString(3, param2);
            st.setString(4, param3);


            st.registerOutParameter(5, Types.VARCHAR); //err_msg

            st.executeUpdate();

            rslt = st.getLong(1);
            err_msg = st.getString(5);

        } catch (SQLException e) {
            throw new JboException(e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                }
            }
        }
    }

{% endhighlight %}

<br/>

Создаем интерфейс в файле AppModule.java:

<br/>

    void my_method_action(String param1, String param2, String param3);

<br/>

Далее в списке DataControl появляется наш метод.

<br/>

Далее в на странице bindigs добавляем methodAction.

<br/>

{% highlight java linenos %}

    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    ob = bindings.getOperationBinding("my_method_action");
    ob.getParamsMap().put("param1", "CALC_REGULAR_ORDERS");

    AttributeBinding param2 = (AttributeBinding) bindings.get("param2");
    ob.getParamsMap().put("param2", (String) param2.getInputValue());
    AttributeBinding param3 = (AttributeBinding) bindings.get("param3");
    ob.getParamsMap().put("param3", (String) param3.getInputValue());

    ob.execute();

{% endhighlight %}

<br/>

### Еще притмеры:

<ul>
    <li><a href="http://storage1.static.itmages.ru/i/15/1207/h_1449480523_1192947_f9e00c8087.png">Programmatically execute a method binding</a></li>
</ul>

<br/>

### Пример с комьюнити

{% highlight java linenos %}

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

String methodToCall= "sufficientRolesExist" ;
String param1Name ="businessLine";
String param2Name ="appName" ;

// the businessLine and appName are passed as pageflowScope parameters.

        String param1Value = (String) ErpScopeUtils.getPageFlowScope("businessLine");
        String param21Value = (String) ErpScopeUtils.getPageFlowScope("appName");

        BindingContext bctx = BindingContext.getCurrent();
        DCBindingContainer bindings =    (DCBindingContainer) bctx.getCurrentBindingsEntry();
        OperationBinding methodCall =    bindings.getOperationBinding( methodToCall );

        methodCall.getParamsMap().put(  param1Name  ,   param1Value   );
        methodCall.getParamsMap().put(  param2Name  ,   param2Value   );

        Object result = methodCall .execute();
        Boolean boolResult = (Boolean) result;

{% endhighlight %}

https://community.oracle.com/thread/4042676
