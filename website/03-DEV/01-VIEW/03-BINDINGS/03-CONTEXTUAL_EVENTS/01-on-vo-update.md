---
layout: page
title: Oracle ADF > Contextual Events > Обновление компонентов на форме при выполнении ViewObject
description: Oracle ADF > Contextual Events > Обновление компонентов на форме при выполнении ViewObject
keywords: Oracle ADF, view, bindings, contextual-events
permalink: /dev/view/bindings/contextual-events/on-vo-update/
---

# Oracle ADF > Contextual Events > Обновление компонентов на форме при выполнении ViewObject

<br/>

Абузим то, что при событии, фреймворк сам обновляет компонент.

<br/>

Происходит событие.

Во ViewObject передаются данные и он изменяется или он просто вызывается.  
Появляется необходимость рефрешить форму.

Вариант с использованием partialPageRendering не подходит, т.к. используются разные регионы.

В общем всем рекомендую попробовать. И каждый раз, кгода будет выполняться execute VO. Результаты будут обновляться в приложении.

<br/>

### Декларативный вариант

Смотрим у компоенента в property inspector поле "Contextual Events".

Если нет, отправляемся к варианту 2.

Если есть, просто добавляем с каким-нибудь именем.  
При этом в bindings должен создаться eventBinding --> events --> myEvent

Далее, если событие должно происходить, делаем следующий финт.
Меняем созданный jdeveloper actionListener, тем который был у нас.

Добавляем в метод updateComponetnByEventBinding(); который приводится в шаге 2.

====

Далее

Заходим в Bindings страницы где находится компонент который нужно рефрешить, создаем Action с ссылкой на этот VO (Например Execute).

Переходим в Contextual Events.

Subscribers --> и добавляем созданное.

<br/>

### Вариант 2

Есть компонент, в котором происходит событие:

Например такой

    <af:commandMenuItem text="SOME TEXT" id="cmi7"
        actionListener="#{MyBean.onSelect}">

<br/>

### Шаг 1

Нахожу этот компонент.
Правой конокой мыши, Go To Page Definition.

Если скажет, что ее нет, то создать.

Далее в bindings создать eventBinding

В структуре кликнуть на созданный eventBinding --> Insert Inside eventBinding. Выбрать events.

В Левом нижнем углу появилась структура в которой есть eventBinding.

Правой кнопкой мыши по events --> Insert Inside events. Выбрать event и задать ему какое-нибудь удачное названиет. (Например, "eventBinding")

<br/>

Добавляю к стандартному событию еще вот это:

<br/>

    import oracle.adf.model.BindingContext;
    import oracle.jbo.uicli.binding.JUEventBinding;
    import oracle.adf.model.binding.DCBindingContainer;
    import oracle.adf.model.events.EventDispatcher;

<br/>

    public void onSelect (ActionEvent actionEvent) {

        updateComponetnByEventBinding();

        // SOME OTHER LOGIC

    }

<br/>

eventBinding - это то как вы назвали event

<br/>

    private void updateComponetnByEventBinding(){
        JUEventBinding eventBinding =
           (JUEventBinding) BindingContext.getCurrent().getCurrentBindingsEntry().get("eventBinding");
        System.out.println("eventBinding = " + eventBinding);

        EventDispatcher eventDispatcher =
           ((DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry()).getEventDispatcher();
        System.out.println("eventDispatcher = " + eventDispatcher);
        eventDispatcher.queueEvent(eventBinding, null);
    }

<br/>

### Шаг 2

В Bindings страницы где привязан наш VO.

Должен быть создан Action со ссылкой на VO. Если нет, создать.

(Выбираю Execute моего VO)

Перешхожу на вкладку Contextual Events.

Далее вкладка Subscribers.

Добавить:

Нахожу наш event, добавляю Publisher и Handler.

Сохраняю.

Все.
