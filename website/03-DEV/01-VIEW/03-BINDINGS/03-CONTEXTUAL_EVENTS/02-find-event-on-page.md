---
layout: page
title: Oracle ADF > Contextual Events > Найти Contextual Event не на текущей странице
description: Oracle ADF > Contextual Events > Найти Contextual Event не на текущей странице
keywords: Oracle ADF, view, bindings, contextual-events
permalink: /dev/view/bindings/contextual-events/find-event-on-page/
---

# Oracle ADF > Contextual Events > Найти Contextual Event не на текущей странице

ХЗ, работае или нет. Но ошибок не пишет.  
Мдя, не работает. Хотя и ошибок не возвращает.  
Разбираюсь.

Буду благодарен за помощь.

<br/>

Перехожу на страницу, bindings которой хочу получить.  
Далее в Bindings добираюсь до DataBindings.cpx

Захожу в исходники DataBindings.cpx и ищу по названию траницы UsageId.

Вставляю вместо my_longStringPageName_PageDef.

eventBinding - название event.

<br/>

    import oracle.jbo.uicli.binding.JUEventBinding;
    import oracle.adf.model.events.EventDispatcher;
    import oracle.adf.model.binding.DCBindingContainer;
    import oracle.adf.model.BindingContext;

<br/>

    private void test(){
        System.out.println();
        System.out.println("TEST BEGIN");

        JUEventBinding eventBinding =
            (JUEventBinding) getBindingsContOfOtherPage("my_longStringPageName_PageDef").get("eventBinding");
         System.out.println("eventBinding = " + eventBinding);

        EventDispatcher eventDispatcher =
           ((DCBindingContainer) getBindingsContOfOtherPage("my_longStringPageName_PageDef")).getEventDispatcher();


        System.out.println("eventDispatcher = " + eventDispatcher);

        eventDispatcher.queueEvent(eventBinding, null);

        System.out.println("TEST END");
        System.out.println();
    }

<br/>

    public Object resolvEl(String data) {
           FacesContext fc = FacesContext.getCurrentInstance();
           Application app = fc.getApplication();
           ExpressionFactory elFactory = app.getExpressionFactory();
           ELContext elContext = fc.getELContext();
           ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
           Object Message = valueExp.getValue(elContext);
           return Message;
       }

<br/>

    public BindingContainer getBindingsContOfOtherPage(String pageUsageId) {
       return (BindingContainer) resolvEl("#{data." + pageUsageId + "}");
    }
