---
layout: page
title: Oracle ADF > Bindings Layer > Contextual Events
description: Oracle ADF > Bindings Layer > Contextual Events
keywords: Oracle ADF, view, bindings, contextual-events
permalink: /dev/view/bindings/contextual-events/
---

# [Oracle ADF] Bindings Layer / Contextual Events

Contextual events have two parts:

-   A publisher (or producer), such as a button that raises a named event, with or without a custom payload
-   A handler (or consumer) that listens for a specifically named event or a wildcard event, to process that event

Because Contextual Events is a feature of the binding layer, all event handlers must be exposed as action or method bindings in the page definition file of the view that is supposed to change in response to an event.

<br/>

Мдя. Все так сложно. Может потом станет полегче.

<br/>

### Пример из видео (просто познакомиться с евентами):

https://www.youtube.com/watch?v=XIVom59Kq2U

Имеется главная страница и 2 региона.  
При изменении в одном регионе, другой должен обновляться.

<br/>

    Шаг1. Поля привязываются к бину. Значение полей также задаются в бине.

    Шаг2. Создается на странице на поле LastName LastNameChangeEvent с помощью Property Inspector.
    В contextual Events появился LastNameChangeEvent.

    Шаг3. Создается класс MyEventConsumer.

<br/>

    import javax.faces.context.FacesContext;

    import oracle.adf.model.binding.DCBindingContainerValueChangeEvent;
    import oracle.adf.view.rich.context.AdfFacesContext;

<!-- public class EventHandler {

    public EventHandler() {
        super();
    }

    public void handleEventObjectPayload(DCBindingContainerValueChangeEvent customPayLoad) {
        String changedDepartmentName =
            (String)customPayLoad.getNewValue();
        handleEventStringPayload(changedDepartmentName);
    }

    public void handleEventStringPayload(String customPayLoad) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elctx = fctx.getELContext();
        ExpressionFactory exprFactory =
            fctx.getApplication().getExpressionFactory();
        ValueExpression ve =
            exprFactory.createValueExpression(elctx, "#{backingBeanScope.mirrorPageBean}",
                                              Object.class);
        MirrorPageBean mirrorPageBean = (MirrorPageBean)ve.getValue(elctx);
        mirrorPageBean.setMirrorValue(customPayLoad);
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.getCurrentInstance().addPartialTarget(mirrorPageBean.getMirrorTextField());
    }
} -->

<br/>

        public void handleEvent(DCBindingContainerValueChangeEvent payload){

            Получает созданный ранее бин.

            String newValue = (String) payload.getNewValue();

            Устанавливает новое значение для поля из бина.

            Обновление поля.

        }

    Шаг4. По созданному классу правой кнопкой мыши --> Create Data Control


    Шаг5. _________PageDef.xml

    bindings --> insert inside bindings --> Generic Bindings --> methodAction

    Выбирается созданный класс.


    Шаг 6. На главной странице в bindings.

    в структуре правой кнопкой мыши по mainPageDef --> Edit event map.

    Producer:  empFormPagePageDef.LastName
    Event Name:   LastNmaeChangeEvent
    Consumer:  **empNamePageDef.handeEvent

<br/>

    Consumer Params:

    Param Name: payload
    Param Value: #{payLoad}

<br/>
<br/>

### Еще пример от Френка Нимфуса:

https://www.youtube.com/watch?v=IFl-1RQm_so

https://bitbucket.org/oracle-adf/implement-contextual-events/

<br/>
<br/>

### Примеры с которыми мне пришлось иметь дело:

<br/>

<ul>
    <li><a href="/dev/view/bindings/contextual-events/on-vo-update/">Contextual Events > Обновление компонентов на форме при выполнении ViewObject</a></li>
    <li><a href="/dev/view/bindings/contextual-events/find-event-on-page/">Contextual Events > Найти Contextual Event не на текущей странице</a></li>
</ul>

<br/>

### Еще примеры:

<br/>

<ul>
    <li><a href="https://middlewarebylink.wordpress.com/2014/06/05/how-to-pass-complex-payload-in-programmatic-contextual-event-launch/" rel="nofollow">How to pass complex payload in programmatic Contextual Event launch</a></li>
</ul>
