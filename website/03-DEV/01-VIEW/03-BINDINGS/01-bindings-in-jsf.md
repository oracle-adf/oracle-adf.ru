---
layout: page
title: Oracle ADF > Bindings Layer > Bindings на JSF странице
description: Oracle ADF > Bindings Layer > Bindings на JSF странице
keywords: Oracle ADF, view, bindings
permalink: /dev/view/bindings/bindings-in-jsf/
---

# [Oracle ADF] Bindings на JSF странице

Кнопка отключена, если в bindings какие-то условия:

    disabled="#{bindings.MyLIST.estimatedRowCount == 0 or bindings.MyLIST.inputValue == null}"
