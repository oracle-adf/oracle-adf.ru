---
layout: page
title: Oracle ADF > Bindings Layer
description: Oracle ADF > Bindings Layer
keywords: Oracle ADF, view, bindings
permalink: /dev/view/bindings/
---

# [Oracle ADF] Bindings Layer

<br/>

Все, что нужно, скорее всего можно найти в ADFUtils. <br/>
Следует сначала посмотреть реализацию в нем, иначе можно начать городить свой огород, как например сделал я.

<br/>

### Bindings and Executables на вкладке bindings страницы jsf:

<br/>

![BindingContainer vs DCBindingContainer](/img/dev/view/bindings/bingingcontainer-and-dcbingingcontainer.png 'BindingContainer vs DCBindingContainer'){: .center-image }

<br/>

<ul>
    <li><a href="/dev/view/bindings/bindings/">Bindings / Bindings</a> (первая колонка)</li>
    <li><a href="/dev/view/bindings/executables/">Bindings / Executables</a> (вторая колонка)</li>
</ul>

<br/>

### Contextual Events:

<br/>

<ul>
    <li><a href="/dev/view/bindings/contextual-events/">Contextual Events</a></li>
</ul>

<br/>

### Bindings на JSF странице с прямым обращением к bindings (примеры без использования бинов):

<br/>

<ul>
    <li><a href="/dev/view/bindings/bindings-in-jsf/">Bindings на JSF странице</a></li>
</ul>
