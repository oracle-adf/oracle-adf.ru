---
layout: page
title: Oracle ADF - Iterator
description: Oracle ADF - Iterator
keywords: Oracle ADF, view, bindings, Iterator, executables
permalink: /dev/view/bindings/executables/iterator/
---

# [Oracle ADF] Iterator

{% highlight java linenos %}

String myIteratorName = "myIteratorName";
String myAttributeName = "myAttributeName";

{% endhighlight %}

<br/>

{% highlight java linenos %}

ADFUtils.findIterator(myIteratorName).executeQuery();

{% endhighlight %}

<br/>

{% highlight java linenos %}

ViewObject vo = ADFUtils.findIterator(myIteratorName).getViewObject();

{% endhighlight %}

<br/>

{% highlight java linenos %}

Object myValue = ADFUtils.findIterator(myIteratorName).getCurrentRow().getAttribute(myAttributeName);

{% endhighlight %}

<br/>

Еще 1 вариант получить текущую строку (если нужно будет, например пройтись по VO для каких-нибудь целей)

{% highlight java linenos %}

MyVOViewImpl myVo =
(MyVOViewImpl) ADFUtils.findIterator("OrderSheduleDeliveriesView1Iterator").getViewObject();

MyVOViewRowImpl currRow = myVo.getCurrentRow();

RowSetIterator rs = myVo.createRowSetIterator(null);

{% endhighlight %}

<br/>

В MyVOViewImpl к VO добавляем:

<br/>

{% highlight java linenos %}

@Override
public MyVOViewRowImpl getCurrentRow() {
return (MyVOViewRowImpl) super.getCurrentRow();
}

{% endhighlight %}

<br/>

### Утановить программно итератор по ключу:

{% highlight java linenos %}

DCIteratorBinding myBindings = ADFUtils.findIterator("OrderSheduleDeliveriesView1Iterator");
myBindings.setCurrentRowWithKeyValue("4444");

{% endhighlight %}

<br/>

https://docs.oracle.com/middleware/1213/adf/api-reference-model/oracle/adf/model/binding/DCIteratorBinding.html

<br/>

### Создать SOC с локалями:

{% highlight xml linenos %}

<af:selectOneChoice value="#{bindings.Language.inputValue}"
label="#{usersBundle['users.user.language']}"
id="soc3"
required="true">
<f:selectItems value="#{pageFlowScope.MyBean.supportedLocales}" id="si6"/>
</af:selectOneChoice>

{% endhighlight %}

<br/>

{% highlight java linenos %}

public List<SelectItem> getSupportedLocales() {
Iterator<Locale> iteratorSupportedLocales = FacesContext.getCurrentInstance()
.getApplication()
.getSupportedLocales();
supportedLocales = new ArrayList<SelectItem>();
SelectItem itemLocale = null;
while (iteratorSupportedLocales.hasNext()) {
Locale locale = iteratorSupportedLocales.next();
itemLocale = new SelectItem(locale.getLanguage(), locale.getDisplayName(), locale.getDisplayName());
supportedLocales.add(itemLocale);
}

    return supportedLocales;

}

{% endhighlight %}

<br/>

<ul>
    <li><a href="/dev/view/bindings/executables/iterator/print_values_from_vo_by_iterator/">Распечатать в консоли viewobject с помощью итератора</a></li>
</ul>
