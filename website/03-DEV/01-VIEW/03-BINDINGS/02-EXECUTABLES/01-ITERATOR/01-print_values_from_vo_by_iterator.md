---
layout: page
title: Oracle ADF - Распечатать в консоли viewobject с помощью итератора
description: Oracle ADF - Распечатать в консоли viewobject с помощью итератора
keywords: Oracle ADF, view, bindings, Iterator, executables
permalink: /dev/view/bindings/executables/iterator/print_values_from_vo_by_iterator/
---

# Oracle ADF - Распечатать в консоли viewobject с помощью итератора

<br/>

Создаю viewObject с именем voEmp.<br/>

    select * from emp

Добавляю в ApplicationModule этот voEmp получаю voEmp1.

<br/>

DataBindings.cpx --> <br/><br/>

В Page Definition Usage кликаю по ru.javadev.view.pageDefs.viewPageDef<br/>

В executables добавляю элемент "iterator" и выбираю voEmp1. В результате создается итератор с именем "voEmp1Iterator"<br/><br/>

     <af:commandButton text="PushMe" id="cb1" action="#{mainBean.buttonPressed}"/>

<br/>

    package ru.javadev.view;

    import java.util.Date;

    import oracle.adf.model.BindingContext;
    import oracle.adf.model.binding.DCBindingContainer;
    import oracle.adf.model.binding.DCIteratorBinding;

    import oracle.jbo.Row;
    import oracle.jbo.RowSetIterator;

    public class mainBean {

        public String buttonPressed() {
            System.out.println("");
            System.out.println("----------------------------");
            System.out.println("-- Method buttonPressed() BEGIN -------!!!");
            System.out.println("---" + (new Date()).toString() + "------");


            BindingContext ctx = BindingContext.getCurrent();
            DCBindingContainer bc = (DCBindingContainer)ctx.getCurrentBindingsEntry();
            DCIteratorBinding iterator = bc.findIteratorBinding("voEmp1Iterator");

            // Определить количество записей

            RowSetIterator rsi = iterator.getRowSetIterator();


            System.out.println("||||||||||||||||||||||||||");
            System.out.println("Всего записей: " + rsi.getRowCount());
            System.out.println("||||||||||||||||||||||||||");


            // Данные из строки, на которую указывает итератор

            Row r = rsi.getCurrentRow();


            System.out.println("||||||||||||||||||||||||||");
            System.out.println("Текущее значение: " + (String)r.getAttribute("Ename"));
            System.out.println("||||||||||||||||||||||||||");


            // Вывести все данные


               rsi.reset();

                while (rsi.getCurrentRow() != null) {

                System.out.println("||||||||||||||||||||||||||");
                System.out.print(rsi.getCurrentRowIndex());
                System.out.print(") ");

                System.out.println((String)rsi.getCurrentRow().getAttribute("Ename"));

                System.out.println("||||||||||||||||||||||||||");

                rsi.next();

                }

                rsi.closeRowSetIterator();



            System.out.println("");
            System.out.println("-- Method buttonPressed() END -------!!!");
            System.out.println("----------------------------");

            return null;

        }
    }

<br/>

<strong>Результат:</strong>

    ----------------------------
    -- Method buttonPressed() BEGIN -------!!!
    ---Sat Nov 24 17:08:43 MSK 2012------


    ||||||||||||||||||||||||||
    Всего записей: 14
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    Текущее значение: SMITH
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    0) SMITH
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    1) ALLEN
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    2) WARD
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    3) JONES
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    4) MARTIN
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    5) BLAKE
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    6) CLARK
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    7) SCOTT
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    8) KING
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    9) TURNER
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    10) ADAMS
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    11) JAMES
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    12) FORD
    ||||||||||||||||||||||||||
    ||||||||||||||||||||||||||
    13) MILLER
    ||||||||||||||||||||||||||

    -- Method buttonPressed() END -------!!!
    ----------------------------
