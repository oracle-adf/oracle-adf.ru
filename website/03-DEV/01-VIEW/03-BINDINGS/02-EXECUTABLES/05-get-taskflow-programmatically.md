---
layout: page
title: Oracle ADF - Получить программно TaskFlow
description: Oracle ADF - Получить программно TaskFlow
keywords: Oracle ADF, view, bindings, TaskFlow
permalink: /dev/view/bindings/executables/taskflow/get-taskflow-programmatically/
---

# [Oracle ADF] Получить программно TaskFlow

```java
public static DCTaskFlowBinding getTaskFlow(String taskFlowName) {
    BindingContext bindingCtx = BindingContext.getCurrent();
    DCBindingContainer dcbCon = (DCBindingContainer)bindingCtx.getCurrentBindingsEntry();
    DCTaskFlowBinding taskFlow = (DCTaskFlowBinding)dcbCon.findExecutableBinding(taskFlowName);
    return taskFlow;
}
```

<br/>

http://mahmoudoracle.blogspot.ru/2012/12/controlling-taskflow-programatically.html
