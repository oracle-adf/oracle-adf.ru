---
layout: page
title: Oracle ADF > Bindings Layer > executables
description: Oracle ADF > Bindings Layer > executables
keywords: Oracle ADF, view, bindings, executables
permalink: /dev/view/bindings/executables/
---

# [Oracle ADF] Bindings Layer / Executables

<br/>

BindingsUtils.java

https://bitbucket.org/oracle-adf/adf-utils/src/087f677cba287be7b1c3cebf038a20ed2cbff7e4/src/main/java/org/javadev/adf/utils/BindingsUtils.java

<br/>

### Получить программно DCIteratorBinding текущей страницы:

{% highlight java linenos %}

    DCIteratorBinding myDCIteratorBinding = ADFUtils.findIterator(myIteratorName);

{% endhighlight %}

<br/>

### Получить программно DCIteratorBinding произвольной страницы (не проверял, но должно работать, если нет то сделать по смыслу):

{% highlight java linenos %}

    DCIteratorBinding myDCIteratorBinding = ADFUtils.findIterator(myBindingContainerName, myIteratorName);

{% endhighlight %}

<br/>

### Еще один вариант

    private DCIteratorBinding findGanttBinding() {
        DCBindingContainer dcBindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding _binding = (DCIteratorBinding)dcBindings.get("MYL1Iterator");
        return _binding;
    }

<br/>

### Доступ к параметрам файла PageDef.

    DCBindingContainer dcBindings = ADFUtils.getDCBindingContainer();
    DCParameter param = dcBindings.findParameter("deptno");
    String paramValue = (String)param.getValue();

<br/>

### Примеры реализации:

<ul>
    <li>accessorIterator</li>
    <li><a href="/dev/view/bindings/executables/iterator/">Iterator</a></li>
    <li>methodIterator</li>
    <li>page</li>
    <li>queryRegion</li>
    <li>searchRegion</li>
    <li>shuttleRegion</li>
</ul>

<br/>

<ul>
    <li><a href="/dev/view/bindings/executables/taskflow/get-taskflow-programmatically/">Получить программно TaskFlow</a> (не проверял)</li>
</ul>
