---
layout: page
title: Oracle ADF - Создание события нажатия на кнопку
description: Oracle ADF - Создание события нажатия на кнопку
keywords: Oracle ADF, view, events
permalink: /dev/view/events/programmatically/example/1/
---

<br/>

# [Oracle ADF] -> События (Events) -> Создание события нажатия на кнопку

UPD! Срабаиывае не везде!!!
Поэтому в ADF-UTILS добавил метод runButtonByJS:

https://bitbucket.org/oracle-adf/adf-utils/src/0ac62ba404606a29305ed4af7461d6aa9ce59095/src/main/java/org/javadev/adf/utils/UIUtils.java?at=master&fileviewer=file-view-default

<br/>

{% highlight java linenos %}

import javax.faces.event.ActionEvent;

ActionEvent actionEvent = new ActionEvent(this.cb_load);
actionEvent.queue();

{% endhighlight %}

<br/>

this.cb_load - привязанная в бине кнопка.

<br/>

### Делал для RichButton

{% highlight java linenos %}

import oracle.adf.view.rich.component.rich.nav.RichButton;
import javax.faces.event.ActionEvent;

RichButton button = (RichButton) UIUtils.getUIComponentByAbsoluteID("component_absolute_id_string");
ActionEvent actionEvent = new ActionEvent(button);
actionEvent.queue();

{% endhighlight %}

<br/>

### Делал для RichCommandMenuItem

<br/>

{% highlight java linenos %}

RichCommandMenuItem rcmi = (RichCommandMenuItem) UIUtils.getUIComponentByAbsoluteID("component_absolute_id_string");

ActionEvent actionEvent = new ActionEvent(rcmi);
actionEvent.queue();

{% endhighlight %}

<br/>

Правда, компонент должен уже существовать. Иначе не все так хорошо.

Invoking Button Action Programmatically,Queuing ActionEvent  
http://www.awasthiashish.com/2013/04/invoking-button-action.html

<br/>

### DisclosureEvent для springboard

{% highlight java linenos %}

DisclosureEvent evt = null;

for (UIComponent child : this.springboard.getChildren()) {
RichShowDetailItem detailItem = (RichShowDetailItem) child;

    if (detailItem.getId().equals("tasks")) {
        detailItem.setDisclosed(true);
        evt = new DisclosureEvent(detailItem, true);
    } else {
        detailItem.setDisclosed(false);
    }

}

{% endhighlight %}
