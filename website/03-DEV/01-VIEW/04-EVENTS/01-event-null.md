---
layout: page
title: Oracle ADF - События (Events) - Выбранные и невыбранные значения в компонентах
description: Oracle ADF - События (Events) - Выбранные и невыбранные значения в компонентах
keywords: Oracle ADF, view, events
permalink: /dev/view/events/null/
---

# [Oracle ADF] События (Events) - Выбранные и невыбранные значения в компонентах

<br/>

**Для всяких компонентов, где нужно вводить текст руками**

<ul>
    <li>inputListOfValues</li>
    <li>...</li>
</ul>

<br/>

    public void myMethod(ValueChangeEvent valueChangeEvent) {

        if(!( "".equals(valueChangeEvent.getNewValue().toString()))){
            System.out.println("NOT NULL");

        } else {
            System.out.println("NULL");
        }
    }

<br/>
<br/>

<br/>

**Для всяких компонентов, где нужно выбирать**

<ul>
    <li>selectManyCheckbox</li>
    <li>...</li>
</ul>

<br/>

        public void myMethod(ValueChangeEvent valueChangeEvent) {

            if (null != valueChangeEvent.getNewValue() ){
                System.out.println("NOT NULL");
            } else {
                System.out.println("NULL");
            }
        }

<br/>

или даже вроде так можно. (будет много лишнего времени, попробую)

    if (null != valueChangeEvent){
