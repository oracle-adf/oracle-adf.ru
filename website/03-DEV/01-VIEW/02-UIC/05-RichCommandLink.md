---
layout: page
title: RichCommandLink
permalink: /dev/view/uic/RichCommandLink/
---


# [Oracle ADF] RichLink


    <af:commandLink text="#{list.character}" id="cl1"
                    partialSubmit="true"
                    actionListener="#{Test.alphabetButtonPressed}">
    </af:commandLink>

<br/>

    public void alphabetButtonPressed(ActionEvent actionEvent) {
        RichCommandLink rl = (RichCommandLink) actionEvent.getComponent();
        System.out.println("linkPressed " + rl.getText());
    }
