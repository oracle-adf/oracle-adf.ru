---
layout: page
title: Oracle ADF - Link
permalink: /dev/view/uic/link/
---

<br/>

# [Oracle ADF] - Link (af:link)


<br/>

    <af:link id="reloadBtn" icon="/images/menu/func_refresh_16_ena.png"
         hoverIcon="/images/menu/func_refresh_16_hov.png"
         depressedIcon="/images/menu/func_refresh_16_onb.png"
         actionListener="#{Dashboard.onMyMethodExec}"
         shortDesc="Обновить"
         binding="#{Dashboard.reloadBtn}"/>


<br/>
<br/>


    public void onMyMethodExec(ActionEvent actionEvent) {

        RichLink rl = (RichLink) actionEvent.getComponent();
        System.out.println("linkPressed " + rl.getText());

        UIUtils.printUIComponentInfo(rl);

    }
