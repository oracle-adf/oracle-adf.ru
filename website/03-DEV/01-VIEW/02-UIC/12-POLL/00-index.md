---
layout: page
title: Oracle ADF RichPoll
permalink: /dev/view/uic/RichPoll/
---

# [Oracle ADF] RichPoll


    public void tickLoad(PollEvent pollEvent) {
        RichPoll poll = (RichPoll) pollEvent.getComponent();
        System.out.println("Poll setting to -1");
        poll.setInterval(-1);
        AdfFacesContext.getCurrentInstance().addPartialTarget(poll);
    }


<br/>

**Примеры использования:**

<br/>

    <ul>
    	<li><a href="/dev/view/uic/select/one/">Таймер Обратного отсчета с помощью Poll</a></li>
    </ul>



<br/>

Остановить работу pool можно передав в него параметр -1

    poll.setInterval(-1);



<br/>
<br/>

См также:

Using the af:poll component  
https://www.youtube.com/watch?v=4vJuHXL4uho

Using af:poll to refresh and push data in page and databound table in ADF  
http://www.awasthiashish.com/2013/06/using-afpoll-to-refresh-and-push-data.html

Show current Date and Time on Page in Oracle ADF (refresh Date/time Programmatically)  
http://www.awasthiashish.com/2013/12/show-current-date-and-time-on-page-in.html
