---
layout: page
title: Oracle ADF Таймер Обратного отсчета с помощью Poll
permalink: /dev/view/uic/RichPoll/count-down-timer/
---

# [Oracle ADF] Таймер Обратного отсчета с помощью Poll



Код получился так себе. Нужно рефакторить и рефакторить.

Интервал обновления установлен в 60 секунд.

Попытки обновления outputText, ни к чему не приводили.
Пришлось положить его в panelGroupLayout.

При рефреше, таблицы начинали моргать.
Решили попробовать переделать с помощью javascript.



    <af:panelGroupLayout id="timer_pg">
        <af:outputText binding="#{CountDownTimer.timer}" id="pgl21">
        </af:outputText>
    </af:panelGroupLayout>
    <af:poll id="p4" pollListener="#{CountDownTimer.refreshTime}" timeout="36000000"
             interval="60000"/>


<br/>
<br/>



    import static java.lang.Math.toIntExact;

    import java.sql.PreparedStatement;
    import java.sql.ResultSet;

    import java.util.Calendar;
    import java.util.Date;
    import java.util.GregorianCalendar;

    import javax.annotation.PostConstruct;

    import oracle.adf.view.rich.component.rich.output.RichOutputText;
    import oracle.adf.view.rich.context.AdfFacesContext;

    import org.apache.myfaces.trinidad.event.PollEvent;

****

    public class CountDownTimer {

        private static String COUNT_DOWN_TIMER_TEXT = "STARTING";
        private static int COUNT_DOWN_TIMER_SECONDS;

        private GregorianCalendar TIME_FROM_DB;
        private GregorianCalendar LOCAL_TIME;

        // --------------------------------

        private RichOutputText timerText;

        public void setTimer(RichOutputText timer) {
            this.timerText = timer;
        }

        public RichOutputText getTimer() {
            return timerText;
        }

        // --------------------------------

        @PostConstruct
        public void init(){
            TIME_FROM_DB = getOrderApprovalTime();
        }

        // --------------------------------



        public void refreshTime(PollEvent pollEvent){

            LOCAL_TIME = getLocalTime();

            long differenceInSeconds = (TIME_FROM_DB.getTimeInMillis() - LOCAL_TIME.getTimeInMillis()) / 1000;

            COUNT_DOWN_TIMER_SECONDS = toIntExact(differenceInSeconds);


            if (COUNT_DOWN_TIMER_SECONDS > 0){
                decreaseTimerCount();
                this.timerText.setValue(COUNT_DOWN_TIMER_TEXT);
                int[] res = splitToComponentTimes(COUNT_DOWN_TIMER_SECONDS);

                    String hours;

                    if (res[0] == 0){
                        hours = "00";
                    } else if (res[0] == 1){
                        hours = "01";
                    } else if (res[0] == 2){
                        hours = "02";
                    } else if (res[0] == 3){
                        hours = "03";
                    } else if (res[0] == 4){
                        hours = "04";
                    } else if (res[0] == 5){
                        hours = "05";
                    } else if (res[0] == 6){
                        hours = "06";
                    } else if (res[0] == 7){
                        hours = "07";
                    } else if (res[0] == 8){
                        hours = "08";
                    } else if (res[0] == 9){
                        hours = "09";
                    }
                    else {
                        hours = Integer.toString(res[1]);
                    }

                    String minutes;

                    if (res[1] == 0){
                        minutes = "00";
                    } else if (res[1] == 1){
                        minutes = "01";
                    } else if (res[1] == 2){
                        minutes = "02";
                    } else if (res[1] == 3){
                        minutes = "03";
                    } else if (res[1] == 4){
                        minutes = "04";
                    } else if (res[1] == 5){
                        minutes = "05";
                    } else if (res[1] == 6){
                        minutes = "06";
                    } else if (res[1] == 7){
                        minutes = "07";
                    } else if (res[1] == 8){
                        minutes = "08";
                    } else if (res[1] == 9){
                        minutes = "09";
                    }
                    else {
                        minutes = Integer.toString(res[1]);
                    }

                COUNT_DOWN_TIMER_TEXT = hours + ":" + minutes;

                UIUtils.updateUIComponentByAbsoluteID(CONSTANTS_UI.TIMER_PANEL_GROUP);
            }

            if (COUNT_DOWN_TIMER_SECONDS <= -12*60*60){
                init();
            }
        }

        public static int[] splitToComponentTimes(int input){
            int hours = (int) input / 3600;
            int remainder = (int) input - hours * 3600;
            int mins = remainder / 60;
            remainder = remainder - mins * 60;
            int secs = remainder;

            int[] ints = {hours , mins , secs};
            return ints;
        }


        private void decreaseTimerCount() {
            if (COUNT_DOWN_TIMER_SECONDS <= 0){
                timerText.setVisible(false);
                AdfFacesContext.getCurrentInstance().addPartialTarget(timerText);
            }

            COUNT_DOWN_TIMER_SECONDS = COUNT_DOWN_TIMER_SECONDS-60;
        }



        private GregorianCalendar getLocalTime(){
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            calendar.clear(Calendar.MILLISECOND);

            return calendar;

        }

        private GregorianCalendar getOrderApprovalTime(){

            String sql = CONSTANTS_SQL_QUERIES.TIME_FROM_DB;
            Integer hours = 0;
            Integer minutes = 0;

            PreparedStatement st = null;
            ResultSet rs = null;
            st = DBUtils.getDBTransaction().createPreparedStatement(sql,1);
            try {
                    rs = st.executeQuery();
                    if (rs != null && rs.next()) {
                            hours = rs.getInt(1);
                            minutes = rs.getInt(2);

                            GregorianCalendar cal = new GregorianCalendar();
                            cal.setTime(new Date());
                            cal.set(Calendar.HOUR_OF_DAY, hours);
                            cal.set(Calendar.MINUTE, minutes);
                            cal.set(Calendar.SECOND, 0);
                            cal.set(Calendar.MILLISECOND, 0);

                            return cal;
                        }

                        return null;
                    }
            catch (Exception ex){
                return null;
            }
            finally {
                try {
                        st.close();
                        rs.close();
                }
                catch (Exception e) {
                    System.out.println("Exception1 Occured " + e.getMessage());
                }
            }
        }

    } // The End of Class;
