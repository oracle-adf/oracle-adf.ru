---
layout: page
title: Oracle ADF INPUT - OUTPUT
description: Oracle ADF INPUT - OUTPUT
keywords: Oracle ADF, view, uic, input-output
permalink: /dev/view/uic/input-output/
---

<br/>

# [Oracle ADF] INPUT / OUTPUT

<br/>

### Text

<ul>
	<li>
		<a href="/dev/view/uic/InputText/">InputText</a>
	</li>
	<li>
		<a href="/dev/view/uic/RichOutputText/">OutputText и RichOutputText</a>
	</li>
	<li>
		<a href="/dev/view/uic/RichActiveOutputText/">activeOutputText и  RichActiveOutputText</a>
	</li>
</ul>

<br/>

### Label

<ul>

    <li>
    	<a href="/dev/view/uic/RichOutputLabel/">OutputLabel RichOutputLabel</a>
    </li>

</ul>

<br/>

### Date and Time

<ul>
	<li><a href="/dev/view/uic/io/input/date/">InputDate и RichInputDate</a></li>
</ul>

<br/>

### InputListOfValues

<ul>
	<li><a href="/dev/view/uic/InputListOfValues/">InputListOfValues и RichInputListOfValues</a></li>
</ul>
