---
layout: page
title: RichOutputLabel
description: Oracle ADF - RichOutputLabel
keywords: Oracle ADF, view, uic, RichOutputLabel
permalink: /dev/view/uic/RichOutputLabel/
---

# [Oracle ADF] RichOutputLabel

    public void setTextToFragmentComponent(String fragm, String compId, String val) {
        UIComponent msgConteiner;
        RichOutputLabel rop = new RichOutputLabel();

        rop.setValue(val);

        msgConteiner = Utils.getComponent(fragm).findComponent(compId);
        msgConteiner.getChildren().clear();
        if (val != null) {
            msgConteiner.getChildren().add(0, rop);
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(msgConteiner);
    }
