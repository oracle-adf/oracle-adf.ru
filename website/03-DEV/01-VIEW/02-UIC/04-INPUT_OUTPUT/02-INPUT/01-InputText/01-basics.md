---
layout: page
title: Oracle ADF > InputText базовые вещи
description: Oracle ADF - InputText базовые вещи
keywords: Oracle ADF, view, uic, InputText базовые вещи
permalink: /dev/view/uic/InputText/get-values-by-enter/
---

# [Oracle ADF] InputText базовые вещи

<br/>

<strong>Длина поля inputText задается параметром columns="25"</strong>

    <af:inputText label="Код" id="itActivationCode" columns="25"/>

<strong>Положение текста относительно inputText задается параметром labelAlignment</strong>

    labelAlignment="start"
    labelAlignment="top"

    Пример:

    <af:panelFormLayout id="pfl5" labelAlignment="start">
        <af:inputText label="No" id="itNo"/>
    </af:panelFormLayout>

<br/>

### Считать InputText с формы

    UIComponent l_page = Utils.getComponent("r5");
    RichInputText ritStrahSumm = (RichInputText)(l_page.findComponent("it5m_"));

<br/>

    public static UIComponent getComponent(String compId) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = fctx.getViewRoot();
        return viewRoot.findComponent(compId);
    }
