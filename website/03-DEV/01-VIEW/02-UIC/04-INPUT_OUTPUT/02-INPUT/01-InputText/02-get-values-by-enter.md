---
layout: page
title: Oracle ADF > InputText Считать значение по ENTER
description: Oracle ADF > InputText Считать значение по ENTER
keywords: Oracle ADF, view, uic, InputText, Считать значение по ENTER
permalink: /dev/view/uic/InputText/basics/
---

# [Oracle ADF] InputText Считать значение по ENTER

<br/>

Пусть будет с panelBox и с кнопкой (как на проекте)

{% highlight xml linenos %}

<af:panelBox text="Дополнительная информация" id="pb5"
showDisclosure="false" styleClass="panelBox-filter" showMaximize="never">

      <f:facet name="toolbar"/>

      <af:panelGroupLayout id="pfl1" layout="horizontal" halign="start">
          <af:inputText id="it3" columns="40" value="#{MyBean.searchByAdditionalInformation}">
              <af:clientListener method="handleEnterEvent" type="keyPress"/>
          </af:inputText>
          <af:button actionListener="#{MyBean.searchByAdditionalInformation}"
                               id="searchLink" icon="/images/menu/func_search_16_ena.png"/>
      </af:panelGroupLayout>

</af:panelBox>

{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

private String searchByAdditionalInformation;

public void setSearchByAdditionalInformation(String searchByAdditionalInformation) {
this.searchByAdditionalInformation = searchByAdditionalInformation;
}

public String getSearchByAdditionalInformation() {
return searchByAdditionalInformation;
}

public void searchByAdditionalInformation(ActionEvent actionEvent) {
System.out.println(" RESULT " + searchByAdditionalInformation);

    if (!((searchText == null) || (searchText.length() == 0))) {
          // сделать то
      } else {
          // сделать се
      }

}

{% endhighlight %}

<br/>
<br/>

Это вообще подключаемый к странице js.
Смысл такой. По Enter на странице ищется компонент с id "searchLink".
И кликается по нему. А на саму кнопку вешается событие ее нажатия.

При необходимости ее можно сделать скрытой.

<br/>

{% highlight java linenos %}

function handleEnterEvent(evt) {
var \_keyCode = evt.getKeyCode();
if (\_keyCode == AdfKeyStroke.ENTER_KEY) {
var comp = evt.getSource();
var button = comp.findComponent('searchLink');
AdfActionEvent.queue(button, button.getPartialSubmit());
evt.cancel();
}
}

{% endhighlight %}
