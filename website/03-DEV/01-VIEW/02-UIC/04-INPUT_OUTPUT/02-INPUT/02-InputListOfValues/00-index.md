---
layout: page
title: Oracle ADF > InputListOfValues и RichInputListOfValues
description: Oracle ADF > InputListOfValues и RichInputListOfValues
keywords: Oracle ADF, view, uic, InputListOfValues
permalink: /dev/view/uic/InputListOfValues/
---

# Oracle ADF > InputListOfValues и RichInputListOfValues

<br/>

### Пример сгенерированного inputListOfValues

<ul>
    <li><a href="/dev/view/uic/InputListOfValues/dummy/">Пример сгенерированного inputListOfValues</a></li>
</ul>

<br/>

### Получение данных из InputListOfValues

<ul>
    <li><a href="/dev/view/uic/InputListOfValues/get-value/">Получить из inputListOfValues нужные данные</a></li>
</ul>

<br/>
<br/>

Все, что дальше, может быть уже устарело (для меня).

<br/>
<br/>

<strong>Creating List Of Values in Oracle ADF</strong>

Блин, видео вообще похоже не в тему (нужно потом как-нибудь посмотреть и разобраться, что оно здесь делает):

<br/>

<div align="center">
    <iframe width="640" height="480" src="https://www.youtube.com/embed/NO3i0zOXCzo" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

<br/>

### Получение данных из InputListOfValues (Архив)

Может быть есть намного проще способы.

<ul>
    <li><a href="/dev/view/uic/InputListOfValues/get-value/by-vo/">Получить из inputListOfValues нужные данные с помощью VO</a></li>
    <li><a href="/dev/view/uic/InputListOfValues/get-value/by-bindings/">Получить из inputListOfValues нужные данные с помощью bindings</a></li>
</ul>

<br/>

### AutoSuggestBehavior для InputListOfValues

На самом деле нужно делать следующим образом:

    SelectItem si = new SelectItem(rw.getAttribute("someAttribute"), (String) rw.getAttribute("someAttribute"));

Чтобы было, что-то вроед label / value. Одно для отображения, другое подставляется в InputListOfValues в качестве передаваемого значения.

<ul>
    <li><a href="/dev/view/uic/InputListOfValues/suggested-items-programmatically/">Создание suggested items программно для inputListOfValues</a></li>
    <li><a href="/dev/view/uic/InputListOfValues/add-autoSuggestBehavior/">Добавляем autoSuggestBehavior в inputListOfValues</a> (Запутанный варант, не рекомендую использовать)</li>
</ul>

<br/>

Working with the AutoSuggest Behavior  
http://www.oracle.com/technetwork/developer-tools/jdev/explaining-autosuggestbehavior-092525.html

<br/>

### Дополнительно

ADF Basics: Set multiple LOV's on attribute and conditionally switch using LOV Switcher  
http://www.awasthiashish.com/2015/02/adf-basics-set-multiple-lovs-on.html
