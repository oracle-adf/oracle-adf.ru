---
layout: page
title: Oracle ADF > Пример сгенерированного inputListOfValues
description: Oracle ADF > Пример сгенерированного inputListOfValues
keywords: Oracle ADF, view, uic, InputListOfValues
permalink: /dev/view/uic/InputListOfValues/dummy/
---

# Oracle ADF > Пример сгенерированного inputListOfValues

<br/>

Я создавал пока только руками. Как сделать, с помощью мастера, я пока не знаю.

```xml

<af:inputListOfValues id="lov_StoreNameId"
popupTitle="Search and Select: #{bindings.lov_item_desc.hints.label}"
value="#{bindings.lov_item_desc.inputValue}" label="Товар 1"
model="#{bindings.lov_item_desc.listOfValuesModel}"
required="#{bindings.lov_item_desc.hints.mandatory}" columns="35"
shortDesc="#{bindings.lov_item_desc.hints.tooltip}" autoSubmit="true"
valueChangeListener="#{MyBean.myMethod}"
placeholder="Фильтр по магазину">
<f:validator binding="#{bindings.lov_item_desc.validator}"/>
<af:autoSuggestBehavior suggestedItems="#{bindings.lov_item_desc.suggestedItems}"/>
</af:inputListOfValues>

```

В Bindings создается элемент listOfValues, который и привзязывается к итератору, а тот в свою очередь к datacontrol

```java

public void myMethod(ValueChangeEvent valueChangeEvent) {
// some logic
}

```
