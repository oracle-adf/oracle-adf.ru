---
layout: page
title: Oracle ADF Добавляем autoSuggestBehavior в inputListOfValues (Запутанный варант, не рекомендую использовать)
description: Oracle ADF Добавляем autoSuggestBehavior в inputListOfValues
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/InputListOfValues/add-autoSuggestBehavior/
---

<br/>

# [Oracle ADF] Добавляем autoSuggestBehavior в inputListOfValues (Запутанный варант, не рекомендую использовать)

<br/>

Имею VO1 выводящий Id и Name.

Нужно создать поле ввода, в которое должно подставляться Name, когда пользователь начинает набирать его руками.

    1) Создаю VO2

        select :p_attr_id attr_id, :p_attr_name attr_name from dual

        Bind Vaiables:

        p_attr_id -> Integer
        p_attr_name -> String


    Где цифра - ключевой аттрибут

<br/>

    VO2

    Attributes -> Details -> каждое поле свойство Updatable: Always

    Attributes --> List of Values --> На "Name" --> Добавляю VO1.

    ListAttribute: Name

    В список view Attribure добавляю оба параметра.

<br/>

    Далее вкаладка UI Hints:

    Default List Type: Input Test with List of Values.

    В Selected: --> Name

    Галочка на Query List Automatically

    ОК.

<br/>

    Перетаскиваю VO на форму как Form

    В List Of Values добавляю компонент autoSuggestBehavior
    В нем suggestedItems: Method Expressoin Builder --> Bindings --> Name --> suggestedItems
