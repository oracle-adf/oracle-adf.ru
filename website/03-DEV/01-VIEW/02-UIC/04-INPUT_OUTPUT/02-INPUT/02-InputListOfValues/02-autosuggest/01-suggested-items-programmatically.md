---
layout: page
title: Создание suggested items программно для inputListOfValues
description: Создание suggested items программно для inputListOfValues
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/InputListOfValues/suggested-items-programmatically/
---

# Создание suggested items программно для inputListOfValues

Пример:

На странице

{% highlight xml linenos %}

<af:inputListOfValues \*\*\* "/>
<af:autoSuggestBehavior suggestedItems="#{pageFlowScope.MyBean.onSuggestItems}"/>
</af:inputListOfValues>

{% endhighlight %}

<br/><br/>

{% highlight java linenos %}

public List<SelectItem> onSuggestStores(String searshStr) {

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_VO);

    String WhereClauseParams = "";
    WhereClauseParams = "UPPER (SOME_ATTRIBUTE) LIKE UPPER ('%' || '" +  searshStr + "' || '%')";
    vo.setWhereClause(WhereClauseParams);
    vo.executeQuery();

    // Достаточно часто, нужно после execture сбросить добавленный WheWhereClauseParams
    VOUtils.clearWhereClauseToViewObject(vo);

    // Чтобы возвращались все строки
    // У меня без установки этого параметра всегда цикл выполнялся только 1 раз
    vo.setRangeSize(-1);

    ArrayList<SelectItem> selectItems = new ArrayList<SelectItem>();

    for (Row rw : vo.getAllRowsInRange()) {

        SelectItem si = new SelectItem((String) rw.getAttribute("MY_ATTRIBUTE"));
        selectItems.add(si);
    }
    return selectItems;

}

{% endhighlight %}

<br/><br/>

Другой вариант, с использованием итератора copy + paste

<br/><br/>

{% highlight java linenos %}

public List<SelectItem> onSuggestItems(String searshStr) {

    ADFUtils.findIterator("MyIteratorName").getViewObject().setNamedWhereClauseParam("p_searsh", searshStr);
    ADFUtils.findIterator("MyIteratorName").getViewObject().executeQuery();

    ArrayList<SelectItem> selectItems = new ArrayList<SelectItem>();
    for (Row rw : ADFUtils.findIterator("MyIteratorName").getAllRowsInRange()) {
        SelectItem si = new SelectItem(rw.getAttribute("ItemValue"), (String) rw.getAttribute("ItemValue"));
        String item = (String) rw.getAttribute("Item");

        if (item == null) {
            si.setDisabled(true);
        }
        selectItems.add(si);
    }
    return selectItems;

}

{% endhighlight %}
