---
layout: page
title: Oracle ADF > Получить из inputListOfValues нужные данные с помощью VO
description: Oracle ADF > Получить из inputListOfValues нужные данные с помощью VO
keywords: Oracle ADF, view, uic, InputListOfValues
permalink: /dev/view/uic/InputListOfValues/get-value/by-vo/
---

# Получить из inputListOfValues нужные данные с помощью VO

Пользователь выбирает название, нужно получить id

```xml

<af:inputListOfValues
valueChangeListener="#{pageFlowScope.MyBean.myMethod}"
\*\*\*
</af:inputListOfValues>

```

<br/>

{% highlight java linenos %}

public void onItemSelect(ValueChangeEvent valueChangeEvent) {

    if (null != valueChangeEvent){

    // if(!( "".equals(valueChangeEvent.getNewValue().toString()))){

        ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_VO);

        String WhereClauseParams = "";
        WhereClauseParams = "SOME_VALUE_IN_DB = '" +  valueChangeEvent.getNewValue().toString() + "' ";
        vo.setWhereClause(WhereClauseParams);
        vo.executeQuery();

        VOUtils.printViewObjectInfo(vo);


        // Если может быть больше 1 значения, разкомментировать
        // vo.setRangeSize(-1);

        String myID = "";

        for (Row rw : vo.getAllRowsInRange()) {
            myID = (String) rw.getAttribute("IdAttribute");
        }

        doSomething1();
    } else {
        doSomething2();
    }

    doSomething3();

}

{% endhighlight %}
