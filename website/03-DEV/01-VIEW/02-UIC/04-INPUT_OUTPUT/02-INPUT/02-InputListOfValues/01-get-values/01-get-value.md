---
layout: page
title: Oracle ADF > Получить из inputListOfValues нужные данные
description: Oracle ADF > Получить из inputListOfValues нужные данные
keywords: Oracle ADF, view, uic, InputListOfValues
permalink: /dev/view/uic/InputListOfValues/get-value/
---

<br/>

# Oracle ADF > Получить из inputListOfValues нужные данные

{% highlight java linenos %}

public void onSomethingChanged(ValueChangeEvent valueChangeEvent) {

    // Чтобы итератор указывал на правильную позицию
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());

    // RichInputListOfValues lov = (RichInputListOfValues)valueChangeEvent.getComponent();
    Object value = ADFUtils.findIterator("MY_ITERATOR_NAME").getCurrentRow().getAttribute("MY_ATTRIBUTE_NAME");

    System.out.println("Selected Value " + value.toString());

}

{% endhighlight %}
