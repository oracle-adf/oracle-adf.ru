---
layout: page
title: Oracle ADF - Получить из inputListOfValues нужные данные с помощью bindings
description: Oracle ADF - Получить из inputListOfValues нужные данные с помощью bindings
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/InputListOfValues/get-value/by-bindings/
---

# [Oracle ADF] Получить из inputListOfValues нужные данные с помощью bindings

<br/>

Есть inputListOfValues.

Нужно получить идентификатор выбранного элемента а не его название.
Название вообще без каких-либо действий можно получить, а чтобы получить ID, нужно еще и повозиться.

Если зайти во ViewObject-->Query или ViewObject-->Query-->Attribute Mappings, то интересующее нас поле пристутствует.

Тогда делаем слудующее.

    // Если не выполнить эти 2 команды, то возвращаться будут не текущие данные а ранее выбранные.
    FacesContext contxt = FacesContext.getCurrentInstance();
    valueChangeEvent.getComponent().processUpdates(contxt);


    Integer PromoID = (Integer)BindingsUtils.getValFromIterator(ITERATOR, ID_FIELD);

<br/>

    package ru.javadev.project.view.rimutils.findvaluefrombindings;

    import oracle.adf.model.BindingContext;
    import oracle.adf.model.binding.DCBindingContainer;
    import oracle.adf.model.binding.DCIteratorBinding;

    import oracle.jbo.Row;
    import oracle.jbo.ViewObject;

    public class BindingsUtils {


        public static Object getValFromIterator(String iteratorName, String attrName){

            try {
               DCIteratorBinding dcItteratorBindings =  ADFUtils.findIterator(iteratorName);
               ViewObject vo = dcItteratorBindings.getViewObject();
               Row rowSelected = vo.getCurrentRow();
               return (rowSelected.getAttribute(attrName));

            } catch (Exception e) {
                System.out.println("*getValFromIteratorByIteratorName*" + e.getMessage());
                return "";
            }
        }
    }
