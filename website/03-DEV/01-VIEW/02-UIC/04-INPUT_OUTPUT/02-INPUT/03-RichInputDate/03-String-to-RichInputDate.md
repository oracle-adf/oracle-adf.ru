---
layout: page
title: Oracle ADF - Установка даты для элемента RichInputDate
description: Oracle ADF - Установка даты для элемента RichInputDate
keywords: Oracle ADF, view, uic, RichInputDate
permalink: /dev/view/uic/io/input/date/String-to-RichInputDate/
---

# [Oracle ADF] Установка даты для элемента RichInputDate

Репо с исходниками
https://bitbucket.org/oracle-adf/adf-utils/

Class:  
ADFDatesUtils.java

    public static RichInputDate StringToRichInputDate (String inputString, String dateformat){

        System.out.println("===================================");
        System.out.println("--- StringToRichInputDate ---");

        try {

            RichInputDate myDate = new RichInputDate();
            DateFormat inputFormat = new SimpleDateFormat(dateformat);
            Date date = inputFormat.parse(inputString);

            // String res = inputFormat.format(date);
            myDate.setValue(date);
            return myDate;

        } catch (Exception ex){
            System.out.println("*** Exception || JavaSQLDateToRichInputDate || " + ex.toString());
            return new RichInputDate();
        }
    }

<br/>

    System.out.println("date1 " + ADFDatesUtils.StringToRichInputDate("11.10.1999", "dd.MM.yyyy"));

<br/><br/>

Оказалось, чтобы установить дату для RichInputDate достаточно выполнить следующее (При условии использования данных из <a href="https://bitbucket.org/oracle-adf/adf-utils/">библиотеки</a>).

    this.date1.setValue(Dates.getFirstDayOfMonth());
    this.date2.setValue(Dates.getLastDayOfMonth());

Где this.date1 - элемент RichInputDate на форме

<br/><br/>

### Какой-то старый пример пока не буду удалять

<br/>

    System.out.println("-------------------------");

    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    Date date = new Date();

    System.out.println("----------------------");
    System.out.println(dateFormat.format(date));
    System.out.println("----------------------");



    OUTPUT: 03.11.2013

<br/>

<strong>Установка даты для элемента RichInputDate</strong>

    // Здесь я получаю из XML данные которые хочу вставить в RichInputDate
    String carOwnDocumentDate =  Utils.getXmlVal(Utils.getXmlNode(root, "carOwnDocumentDateDay")) + "." + Utils.getXmlVal(Utils.getXmlNode(root, "carOwnDocumentDateMonth"))  + "." +  Utils.getXmlVal(Utils.getXmlNode(root, "carOwnDocumentDateYear"));

    // Здесь я просто нахожу этот элемент на форме
    RichInputDate rid_carOwnDocumentDate = (RichInputDate)(Utils.getComponent("r2").findComponent("carOwnDocumentDate"));


    DateFormat inputFormat = new SimpleDateFormat("dd.mm.yyyy");
    Date date = inputFormat.parse(carOwnDocumentDate);
    // String tmpRegDate = inputFormat.format(date);

    // Установить параметры элементу
    rid_carOwnDocumentDate.setValue(date);

    // Обновить программно
    AdfFacesContext.getCurrentInstance().addPartialTarget(rid_carOwnDocumentDate);
