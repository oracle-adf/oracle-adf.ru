---
layout: page
title: Oracle ADF - RichInputDate
description: Oracle ADF - RichInputDate
keywords: Oracle ADF, view, uic, RichInputDate
permalink: /dev/view/uic/io/input/date/
---

# [Oracle ADF] RichInputDate

<br/>

**Приблизительно так:**

<br/>

    <af:inputDate
                 id="id1"
                 valueChangeListener="#{pageFlowScope.MyBean.onFilterByDateChanged}"
                 autoSubmit="true" placeholder="Дата">
    </af:inputDate>

<br/>

**Работа с компонентом:**

    public void onFilterByDateChanged(ValueChangeEvent valueChangeEvent) {

        RichInputDate rid = (RichInputDate)valueChangeEvent.getComponent();

        String dateStr = ADFDatesUtils.RichInputDateToString(rid);
        java.sql.Date sqlDate = ADFDatesUtils.RichInputDateToJavaSQLDate(rid);

        System.out.println("");
        System.out.println("res1 " + dateStr);
        System.out.println("res2 " + sqlDate.toString());
        System.out.println("");

    }

<br/>

**Обработка пустого события**

    public void onMyDateChanged(ValueChangeEvent valueChangeEvent) {

        if (valueChangeEvent.getNewValue() != null){

            doThis1();

        } else {
            doThis2();
        }

    }

<br/>

### Установить дату в null:

    this.date1.setValue(null);

-- мб. этот вариант тоже рабочий

    date1.resetValue();

<br/>

### Другие примеры (текущие):

<ul>
    <li>
        <a href="/dev/view/uic/io/input/date/get-value-from-RichInputDate/">Получить данные из RichInputDate в консоль</a>
    </li>
</ul>

<br/>

### Другие примеры (архивные):

<ul>

    <li><a href="http://www.awasthiashish.com/2014/05/adf-basics-selecting-and-showing-time.html" rel="nofollow">ADF Basics : Selecting and showing time-stamp in ADF Faces using af:inputDate & af:convertDateTime</a></li>

    <li><a href="/dev/view/uic/io/input/date/RichInputDate-To-Java-SQL-Date/">RichInputDate To Java SQL Date</a></li>
    <li><a href="/dev/view/uic/io/input/date/String-to-RichInputDate/"> Установка даты для элемента RichInputDate</a></li>

    <li><a href="/dev/view/uic/io/input/date/sample-3/">Пример 3</a></li>
    <li><a href="/dev/view/uic/io/input/date/sample-4/">Пример 4</a></li>

</ul>
