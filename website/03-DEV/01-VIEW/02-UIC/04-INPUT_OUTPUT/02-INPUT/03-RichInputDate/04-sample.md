---
layout: page
title: Oracle ADF - RichInputDate
description: Oracle ADF - RichInputDate
keywords: Oracle ADF, view, uic, RichInputDate
permalink: /dev/view/uic/io/input/date/sample-3/
---

# Дата и время:

Имеем переменную с типом Date jurPersonRegDate в формате 10.11.2011

Приводим ее к строке и далее разбираем дату на отдельные переменные для дня, месяца и года.

<br/>

    // Дата регистрации юр.лица


        if (this.jurPersonRegDate.getValue().toString()!=""){

            xml_tmp += "<jurPersonRegDateDay>" + this.dateToStr(jurPersonRegDate.getValue()).substring(0, 2).toString() + "</jurPersonRegDateDay>";
            xml_tmp += "<jurPersonRegDateMonth>" + this.dateToStr(jurPersonRegDate.getValue()).substring(3, 5).toString() + "</jurPersonDocumentDateMonth>";
            xml_tmp += "<jurPersonRegDateYear>" + this.dateToStr(jurPersonRegDate.getValue()).substring(6, 10).toString() + "</jurPersonRegDateYear>";



        } else {

            xml_tmp += "<jurPersonRegDateDay></jurPersonRegDateDay>";
            xml_tmp += "<jurPersonRegDateMonth></jurPersonRegDateMonth>";
            xml_tmp += "<jurPersonRegDateYear></jurPersonRegDateYear>";

            }

<br/>

     public String dateToStr(Object obj) {
        String ret = "";
        if (obj != null) {
        Date dt;
        try {
        String str = (String)obj;
        dt = toDate(str, "dd.mm.yyyy");
        return str;
        } catch (Exception e) {
        dt = (Date)obj;
    }
