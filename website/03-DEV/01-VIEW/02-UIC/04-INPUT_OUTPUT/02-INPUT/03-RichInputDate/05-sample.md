---
layout: page
title: Oracle ADF - RichInputDate
description: Oracle ADF - RichInputDate
keywords: Oracle ADF, view, uic, RichInputDate
permalink: /dev/view/uic/io/input/date/sample-4/
---

# Дата и время:

    // Дата регистрации

    private RichInputDate jurPersonRegDate;


    public void setJurPersonRegDate(RichInputDate jurPersonRegDate) {
        this.jurPersonRegDate = jurPersonRegDate;
    }

    public RichInputDate getJurPersonRegDate() {
        return jurPersonRegDate;
    }

<br/>

    // Дата регистрации: число

    String jurPersonRegDateDay="";

    try {

        if (!Utils.getXmlVal(Utils.getXmlNode(root, "jurPersonRegDateDay")).equals("")){
                jurPersonRegDateDay = Utils.getXmlVal(Utils.getXmlNode(root, "jurPersonRegDateDay"));
            }

    } catch (Exception e) {

        System.out.println("*** Exception " + e);
    }


    // Дата регистрации: месяц


    String jurPersonRegDateMonth="";

    try {

        if (!Utils.getXmlVal(Utils.getXmlNode(root, "jurPersonRegDateMonth")).equals("")){
                jurPersonRegDateMonth = Utils.getXmlVal(Utils.getXmlNode(root, "jurPersonRegDateMonth"));
            }

    } catch (Exception e) {

        System.out.println("*** Exception " + e);
    }




    // Дата регистрации: год

    String jurPersonRegDateYear="";

    try {

        if (!Utils.getXmlVal(Utils.getXmlNode(root, "jurPersonRegDateYear")).equals("")){
                jurPersonRegDateYear = Utils.getXmlVal(Utils.getXmlNode(root, "jurPersonRegDateYear"));
            }

    } catch (Exception e) {

        System.out.println("*** Exception " + e);
    }



    if ((jurPersonRegDateDay != "") & (jurPersonRegDateMonth != "") & (jurPersonRegDateYear != "")){
    String jurPersonRegDateResult = jurPersonRegDateDay + "." + jurPersonRegDateMonth + "." + jurPersonRegDateYear;

        System.out.println("Результат выполнения jurPersonRegDateResult " + jurPersonRegDateResult);


        DateFormat inputFormat = new SimpleDateFormat("dd.mm.yyyy");


        Date date = inputFormat.parse(jurPersonRegDateResult);
        String tmpRegDate = inputFormat.format(date);
        jurPersonRegDate.setValue(tmpRegDate);


    }

http://stackoverflow.com/questions/2009207/java-unparseable-date-exception  
http://stackoverflow.com/questions/10649782/java-cannot-format-given-object-as-a-date
