---
layout: page
title: Oracle ADF - Получить данные из RichInputDate в консоль
description: Oracle ADF - Получить данные из RichInputDate в консоль
keywords: Oracle ADF, view, uic, RichInputDate
permalink: /dev/view/uic/io/input/date/get-value-from-RichInputDate/
---

# [Oracle ADF] Получить данные из RichInputDate в консоль

<br/>

    <af:inputDate label="От" id="id5" binding="#{MyBeanBean.date1}"/>

<br/>

        RichInputDate date1;

        public void setDate1(RichInputDate date1) {
            this.date1 = date1;
        }

        public RichInputDate getDate1() {
            return date1;
        }

<br/>

    System.out.println("date1: " + date1.getValue().toString());

Получаю:

    date1: Mon Nov 02 00:00:00 MSK 2015

<br/>

### Сконвертировать дату RichInputDate в "удобный" текстовый формат

    public static String RichInputDateToString(RichInputDate rich_input_date, String dateformat){
        try{
            SimpleDateFormat df = new SimpleDateFormat(dateformat);
            Object o1 = rich_input_date.getValue();;
            return df.format(o1).toString();

        }catch(Exception ex){
            return "";
        }
    }

<br/>

    String local_date1 = RichInputDateToString(date1, "dd.MM.yyyy");

Получаю:

    02.11.2015

Репо с исходниками
https://bitbucket.org/oracle-adf/adf-utils/

Class:  
ADFDatesUtils.java
