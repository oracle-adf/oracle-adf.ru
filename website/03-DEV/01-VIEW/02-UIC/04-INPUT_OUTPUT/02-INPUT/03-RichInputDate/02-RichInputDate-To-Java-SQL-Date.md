---
layout: page
title: RichInputDate To Java SQL Date
description: Oracle ADF - RichInputDate To Java SQL Date
keywords: Oracle ADF, view, uic, RichInputDate To Java SQL Date
permalink: /dev/view/uic/io/input/date/RichInputDate-To-Java-SQL-Date/
---

# [Oracle ADF] RichInputDate To Java SQL Date

<br/>

Чтобы во ViewObject передавались параметры типа Date приходится преобразовать RichInputDate в Java SQL Date

<br/>

    public static java.sql.Date RichInputDateToJavaSQLDate(RichInputDate rich_input_date){
        try{
            java.util.Date java_util_date = (java.util.Date)rich_input_date.getValue();
            java.sql.Date result = new java.sql.Date(java_util_date.getTime());
            return result;

        }catch(Exception ex){
            System.out.println("*** Exception || RichInputDateToJavaSQLDate || " + ex.toString());
            java.util.Date java_util_date = new Date();
            return new java.sql.Date(java_util_date.getTime());
        }
    }

<br/>

    ADFDatesUtils.RichInputDateToJavaSQLDate(date1);

date1 - элемент типа RichInputDate

Репо с исходниками
https://bitbucket.org/oracle-adf/adf-utils/

Class:  
ADFDatesUtils.java
