---
layout: page
title: Компоненты и примеры работы с ними
permalink: /dev/view/uic/
---

# Компоненты и примеры работы с ними:

<ul>
    <li><a href="/dev/view/uic/conponents-list/">Список компонентов</a></li>
</ul>

<br/>

### Обновление компонентов на форме:

<ul>
    <li><a href="/dev/view/uic/update/">Обновление компонентов на форме</a></li>
</ul>

<br/>

### Region:

<li>
	<a href="/dev/view/uic/region/">Region</a>
</li>

<br/>

### TaskFlow:

<li>
	<a href="/dev/view/uic/taskflow/">TaskFlow</a>
</li>

<br/>

### Panels

<ul>
    <li><a href="/dev/view/uic/panels/">Panels</a></li>
</ul>

<br/>

### ShowDetailItem

<ul>
	<li><a href="/dev/view/uic/RichShowDetailItem/">ShowDetailItem</a></li>
	<li><a href="/dev/view/uic/RichShowDetailItem/switch-programmatically/">Переключить ShowDetailItem программно</a></li>
</ul>

<br/>

### SELECT

<ul>
	<li><a href="/dev/view/uic/select/">Select</a></li>
</ul>

<br/>

### INPUT / OUTPUT

<ul>
	<li><a href="/dev/view/uic/input-output/">Input/Output</a></li>
</ul>

<br/>

### Buttons

<ul>
	<li>
		<a href="/dev/view/uic/RichButton/">Button и RichButton</a> RichCommandButton (Deprecated)
	</li>
</ul>

<br/>

### Links

<ul>
    <li>
        <a href="/dev/view/uic/link/">af:link</a>
    </li>
	<li>
		<a href="/dev/view/uic/RichCommandLink/">RichCommandLink</a> (Наверное устарел)
	</li>
    <li>
        <a href="/dev/view/uic/commandImageLink/">commandImageLink</a>
    </li>
</ul>

<br/>

### Popup

<ul>
	<li>
		<a href="/dev/view/uic/popup/">Popup</a>
	</li>
</ul>

<br/>

### CommandMenuItem

<ul>
	<li><a href="/dev/view/uic/command-menu-item/">CommandMenuItem</a></li>
</ul>

<br/>

### Table, Tree, or Tree Table

<ul>
	<li><a href="/dev/view/uic/tables/">Table, Tree, or Tree Table</a></li>
</ul>

<br/>

### Train

<ul>
	<li><a href="/dev/view/uic/RichTrain/">RichTrain</a></li>
</ul>

<br/>

### List:

<ul>
    <li><a href="/dev/view/list/">List</a></li>
</ul>

<br/>

### RichPoll (Таймер):

<ul>
    <li><a href="/dev/view/uic/RichPoll/">RichPoll</a></li>
</ul>

<br/>

### Диаграммы, графики и т.д.:

<ul>
    <li><a href="/dev/view/uic/graph/"> Диаграммы, графики и т.д.</a></li>

</ul>

<br/>

### How to create a Custom ADF Component:

<ul>
    <li><a href="http://techutils.in/2015/09/21/how-to-create-a-custom-adf-component/" rel="nofollow">How to create a Custom ADF Component</a></li>
</ul>

<br/>

### Создание компонентов программно:

<ul>
	<li><a href="https://www.youtube.com/watch?v=BjVJ9f3Bykw" rel="nofollow">Adding Component Programmatically in ADF</a></li>
    <li><a href="http://www.awasthiashish.com/2016/04/create-adf-choice-list-and-apply.html" rel="nofollow">Create ADF Choice List and apply selectItems to it programmatically</a></li>
    <li><a href="http://www.awasthiashish.com/2015/09/apply-valuechangelistener-to.html" rel="nofollow">Apply ValueChangeListener to programmatically created ADF Faces components</a></li>
    <li><a href="http://www.awasthiashish.com/2015/09/apply-validator-to-programmatically.html" rel="nofollow">Apply Validator to programmatically created ADF Faces components</a></li>
    <li><a href="http://www.awasthiashish.com/2015/06/create-and-set-clientattribute-to.html" rel="nofollow">Create and set clientAttribute to ADF Faces component programmatically to pass value on client side JavaScript</a></li>
    <li><a href="http://www.awasthiashish.com/2015/05/apply-actionlistener-to.html" rel="nofollow">Apply ActionListener to programmatically created buttons/link in ADF</a></li>
    <li><a href="http://www.awasthiashish.com/2015/05/get-value-from-programmatically-created.html" rel="nofollow">Get Value from programmatically created components , Iterate over parent component to get child values in ADF Faces</a></li>
</ul>
