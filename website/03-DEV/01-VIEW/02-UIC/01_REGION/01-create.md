---
layout: page
title: Oracle ADF Создание REGION (Вынести часть логики со страницы в отдельный REGION)
description: Oracle ADF Создание REGION (Вынести часть логики со страницы в отдельный REGION)
keywords: Oracle ADF, view, uic, region, create
permalink: /dev/view/uic/region/create/
---

# [Oracle ADF] Создание REGION (Вынести часть логики со страницы в отдельный REGION)

<br/>

    Шаг 1

    Создаю Фрагмент New --> Create ADF Page Fragment

    File Name: my_test.jsff
    Document Type: JSP XML

    Page Layout: Create Blank Page

<br/>

    Должен быть с такими блоками (по крайней мере для того, чтобы вставлять af:panelStretchLayout):

{% highlight xml linenos %}

<?xml version='1.0' encoding='UTF-8'?>

<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.1" xmlns:af="http://xmlns.oracle.com/adf/faces/rich"
xmlns:f="http://java.sun.com/jsf/core">

</jsp:root>

{% endhighlight %}

У меня такие в последний раз получились для panelStretchLayout, dvt и еще чего-то:

<br/>

{% highlight xml linenos %}

<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.1"
xmlns:af="http://xmlns.oracle.com/adf/faces/rich"
xmlns:dvt="http://xmlns.oracle.com/dss/adf/faces"
xmlns:f="http://java.sun.com/jsf/core"
xmlns:c="http://java.sun.com/jsp/jstl/core">

</jsp:root>

{% endhighlight %}

<br/>

    Шаг 2

    new --> From Gallery --> ADF Task Flow (JSF/Facelets)

    File Name: my-test-task-flow.xml

    + Create as Bounded Task Flow
    + Create with Page Fragments

<br/>

    Шаг 3

    Перетаскиваю фрагмент на task flow

<br/>

    Шаг 4

    Перетаскиваем Task Flow на страницу --> as Region

<br/>

    Шаг 5

    При необходимости в файле jazn-data.xml добавляю taskflow в список разрешенных с точки зрения security.

    Resource Grants:
    --> Tresource Type: task Flow
    --> Source Project: MyProject

    Нахожу taskflow правой кнопкой мыши --> Add to Existing Entitlement

<br/>

### Пример работы с регионом (вроде какой-то хреновый пример)

    public void myMethod(ActionEvent actionEvent) {

        RichRegion region = (RichRegion)actionEvent.getComponent().findComponent("REGION_NAME);

        if(region != null){
            System.out.println("REFRESH REGION " + region.getId());
            region.refresh(FacesContext.getCurrentInstance());
        }
    }

<br/>

<ul>
    <li><a href="http://www.baigzeeshan.com/search/label/Page%20Fragments">Посмотреть, что за зверь</a></li>
</ul>

<br/>

### Скорее всего устаревшая информация

<br/>
<br/>

После того, как вынесли часть логики проекта в регионы, повторяющуюся на нескольких формах, не отображались данные из ViewObject. Сами данные мы успешно получали в консоли.

Оказалось, что для того, чтобы данные отбражались, необходимо было установить свойство isolated в taskflow.

Область видимости в выносимом фрагменте для компонентов мы использовали: viewScope. Получалось что-то вида actionListener="#{viewScope.myBean.onSomethingHappeded}">

BackingBeanScope не подошла. При ее использовании для работы с компонентами во фрагменте, валились nullpointerexception при обращении к ним.

Может кому будет полезно. А так запись, чтобы не забыть.

<br/>

### Practice

<ul>
    <li><a href="/dev/view/uic/region/region-controller/">Implements RegionController</a></li>
</ul>

<br/>

### Динамические регионы

Пока использую, но до конца не разобрался, как работает.

Пример:

http://www.baigzeeshan.com/2010/06/working-with-dynamic-regions-in-oracle.html
http://www.learnoracleadf.com/2012/06/adf-dynamic-region-working-with-oracle.html

<!--

<ul>
    <li><a href="/dev/view/fragments/sergey/">Хитроебущий воркараунт от Сергея</a></li>
</ul>

-->

<br/>

### Page Fragment Lifecycle listener:

<ul>
    <li>https://docs.oracle.com/cd/E17904_01/web.1111/b31974/taskflows_complex.htm#ADFFD22035</li>
    <li><a href="https://community.oracle.com/thread/2527395?tstart=0">Workaround от Frank’a</a></li>
</ul>
