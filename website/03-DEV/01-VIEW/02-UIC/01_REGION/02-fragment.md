---
layout: page
title: Oracle ADF Fragments
description: Oracle ADF Fragments
keywords: Oracle ADF, view, uic, region, fragments
permalink: /dev/view/fragments/sergey/
---

# Не нужно это использовать! Этот код привязан к проекту.

Мне он нужен чтобы копипастить

```js
<!-- Статистика за год -->
<af:gridCell marginStart="5px" rowSpan="1" width="40%" id="gc9" halign="stretch"
            valign="stretch" marginEnd="5px">
    <af:panelBox text="Статистика за год" id="pb8" showDisclosure="false"
                styleClass="fuse-box">
        <f:facet name="toolbar">
            <af:link id="l3" icon="/images/menu/max_ena.png"
                    hoverIcon="/images/menu/max_ovr.png"
                    depressedIcon="/images/menu/max_dis.png"
                    actionListener="#{storeDetailBean.maximize}">
                <af:setPropertyListener to="#{pageFlowScope.CurrentTaskFlowId}"
                                        type="action"
                                        from="/WEB-INF/store-one-year-statistics-task-flow.xml#store-one-year-statistics-task-flow"/>
                <af:setPropertyListener to="#{pageFlowScope.CurrentPanelBoxText}"
                                        type="action" from="Статистика"/>
            </af:link>
        </f:facet>
        <af:region value="#{bindings.storeoneyearstatisticstaskflow1.regionModel}"
                    id="r9"/>
    </af:panelBox>
</af:gridCell>
<!-- Статистика за год -->
```

<br/>
<br/>

<af:region value="#{bindings.dynamicRegion1.regionModel}" id="r2"/>

**${supplierDetailBean.dynamicTaskFlowId}**

<br/>
<br/>

```java
package ru.javadev.xxx.view;

import javax.el.ELContext;
import javax.el.MethodExpression;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichDeck;
import oracle.adf.view.rich.component.rich.layout.RichGridCell;
import oracle.adf.view.rich.component.rich.layout.RichPanelBox;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailHeader;
import oracle.adf.view.rich.component.rich.nav.RichLink;
import oracle.adf.view.rich.context.AdfFacesContext;

import ru.javadev.adf.utils.ADFPopupUtils;
import ru.javadev.xxx.view.common.templates.GlobalButtonsTemplateInterface;

public class storeDetailBean implements GlobalButtonsTemplateInterface{
    private RichDeck deckIndicators;
    private RichPanelBox panelBoxIndicators;
    private RichGridCell gridCellItem;
    private RichPanelBox panelBoxItem;
    private RichLink linkItemMaximized;
    private RichPanelBox panelBoxOrder;
    private RichPanelBox panelBoxSquare;
    private RichPanelBox panelBoxSOH;
    private RichLink linkSOHMaximized;
    private RichDeck deck;
    private String taskFlowId = "/WEB-INF/store-item-chart-task-flow.xml#store-item-chart-task-flow";

    public storeDetailBean() {
    }

    public void selectIndicators(ValueChangeEvent valueChangeEvent) {
        String choice = valueChangeEvent.getNewValue().toString();
        if (choice.equals("ReplMethod"))
        {
                panelBoxIndicators.setText("Метод пополнения");
            }
        else
        {
                panelBoxIndicators.setText(choice);
            }


        deckIndicators.setDisplayedChild(choice);
        AdfFacesContext.getCurrentInstance().addPartialTarget(panelBoxIndicators);

    }

    public void setDeckIndicators(RichDeck deckIndicators) {
        this.deckIndicators = deckIndicators;
    }

    public RichDeck getDeckIndicators() {
        return deckIndicators;
    }

    public void setPanelBoxIndicators(RichPanelBox panelBoxIndicators) {
        this.panelBoxIndicators = panelBoxIndicators;
    }

    public RichPanelBox getPanelBoxIndicators() {
        return panelBoxIndicators;
    }




    public void setPanelBoxItem(RichPanelBox panelBoxItem) {
        this.panelBoxItem = panelBoxItem;
    }

    public RichPanelBox getPanelBoxItem() {
        return panelBoxItem;
    }



    public void setPanelBoxOrder(RichPanelBox panelBoxOrder) {
        this.panelBoxOrder = panelBoxOrder;
    }

    public RichPanelBox getPanelBoxOrder() {
        return panelBoxOrder;
    }

    public void setPanelBoxSquare(RichPanelBox panelBoxSquare) {
        this.panelBoxSquare = panelBoxSquare;
    }

    public RichPanelBox getPanelBoxSquare() {
        return panelBoxSquare;
    }




    @Override
    public void actions(ActionEvent actionEvent) {
        // TODO Implement this method
    }

    @Override
    public void calculate(ActionEvent actionEvent) {
        // TODO Implement this method
    }

    @Override
    public void done(ActionEvent actionEvent) {
        ADFPopupUtils.hideParentPopup(actionEvent.getComponent());
    }

    @Override
    public void close(ActionEvent actionEvent) {
        ADFPopupUtils.hideParentPopup(actionEvent.getComponent());
    }

    @Override
    public void save(ActionEvent actionEvent) {
        // TODO Implement this method
    }

    @Override
    public void saveandclose(ActionEvent actionEvent) {
        // TODO Implement this method
    }

    @Override
    public void cancel(ActionEvent actionEvent) {
        ADFPopupUtils.hideParentPopup(actionEvent.getComponent());
    }


    public void minimize(ActionEvent actionEvent) {
        deck.setDisplayedChild("restored");
    }

    public void maximize(ActionEvent actionEvent) {
        deck.setDisplayedChild("maximized");
    }

    public void setDeck(RichDeck deck) {
        this.deck = deck;
    }

    public RichDeck getDeck() {
        return deck;
    }


    public TaskFlowId getDynamicTaskFlowId() {
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        String currentTaskFlowId = (String)adfFacesContext.getPageFlowScope().get("CurrentTaskFlowId");
        System.out.println("----------- CurrentTaskFlowId = " + currentTaskFlowId + " ---------------------------");
        if (currentTaskFlowId == null || currentTaskFlowId.trim().equals("") == true) {
        currentTaskFlowId = taskFlowId;
        }
        return TaskFlowId.parse(currentTaskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }
}
```
