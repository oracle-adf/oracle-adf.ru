---
layout: page
title: Oracle ADF Implements RegionController
description: Oracle ADF Implements RegionController
keywords: Oracle ADF, view, uic, region, RegionController
permalink: /dev/view/uic/region/region-controller/
---

# [Oracle ADF] Implements RegionController

<br/>

На обычных страницах все решается просто. Добавляется beforephase и afterphase. Во фрагментах вроде как посложнее.

Я ранее не делал, но похоже время пришло.

Нужно, чтобы при открытии фрагмента в сессию записывались нужные мне данные.

Нус, поехали.

Создаю класс, имплементирующий RegionController.

Я пока еще ничего своего не писал.

Получилось вот, что:

<br/>

    package ru.javadev.xxx.utils;

    import oracle.adf.model.RegionContext;
    import oracle.adf.model.RegionController;

    public class FragmentController implements RegionController {

        @Override
        public boolean refreshRegion(RegionContext regionContext) {
            // TODO Implement this method
            return false;
        }

        @Override
        public boolean validateRegion(RegionContext regionContext) {
            // TODO Implement this method
            return false;
        }

        @Override
        public boolean isRegionViewable(RegionContext regionContext) {
            // TODO Implement this method
            return false;
        }

        @Override
        public String getName() {
            // TODO Implement this method
            return null;
        }
    }

<br/>

Захожу в исходники страницы \*.jsff с проектом --> bindings.

Из bindings иду в Page Definition File.

Курсор на

    <pageDefinition xmlns

В Properties:

ControllerClass: FragmentController

Скриншот из блога индуса:

<div align="center">

    <img src="http://1.bp.blogspot.com/-xqLkSjTf0VQ/U_5lWviQBOI/AAAAAAAACQs/6jXDx0u0dRA/s1600/8-27-2014%2B7-06-50%2BPM.jpg" alt="Скриншот из блога индуса" />

</div>

<br/>

Так, при старте, выполняется refreshRegion, но выполняется много раз.
Нужно, сделать, чтобы он поменьше раз это делл.

    @Override
    public boolean refreshRegion(RegionContext regionContext) {

        System.out.println("WE ARE HERE 1");

        int refreshFlag = regionContext.getRefreshFlag();
            if(refreshFlag==RegionBinding.RENDER_MODEL){
                System.out.println("WE ARE HERE 2");
            }

        return false;
    }

> P.S. я на самом деле не уверен, но возможно еть и более простой вариант. В некоторых случаях это может сработать.
> Превязываем, какой нибудь бин. Делаем, чтобы он создавался при старте и с помощью POSTCONSTRUCT можно выполнить нужную нам логику.

По материалам:

<ul>
    <li>http://www.techartifact.com/blogs/2013/09/call-method-on-page-load-of-jsff-jsf-fragment-in-oracle-adf.html#sthash.FxQwqqS6.GIF9kafS.dpbs</li>
    <li>http://docs.oracle.com/cd/E23943_01/apirefs.1111/e10653/oracle/adf/model/RegionController.html</li>
    <li>http://vijaysadhu.blogspot.ru/2014/08/implement-regioncontroller-for.html</li>
</ul>
