---
layout: page
title: Oracle ADF RichTrain
permalink: /dev/view/uic/RichTrain/
---

# [Oracle ADF] RichTrain





Следующим способом, ничего толкового для себя не нашел.
Каких-то методов, которые помогли бы мне получить полезную информацю не увидел.

    public void onTestAction(ActionEvent actionEvent) {

        UIComponent uic = UIUtils.getUIComponentByAbsoluteID("r2:pt1:pt_t1");
        RichTrain richTrain = (RichTrain)uic;

    }


<br/>

Зато следующим способом смог найти название выбранного activity в train. Мне вполне достаточно.


    ControllerContext controllerContext = ControllerContext.getInstance();
    ViewPortContext currentViewPortCtx = controllerContext.getCurrentViewPort();

    TaskFlowContext taskFlowCtx = currentViewPortCtx.getTaskFlowContext();
    TaskFlowTrainModel taskFlowTrainModel = taskFlowCtx.getTaskFlowTrainModel();

    TaskFlowTrainStopModel currentStop = taskFlowTrainModel.getCurrentStop();

    System.out.println("ActivityId " + currentStop.getLocalActivityId());




How to programmatically navigate ADF trains:  
http://www.oracle.com/technetwork/developer-tools/adf/learnmore/82-programmatically-navigate-trains-396873.pdf
