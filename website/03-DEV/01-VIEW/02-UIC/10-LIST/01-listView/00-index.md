---
layout: page
title: Oracle ADF > ListView
permalink: /dev/view/list/listview/
---


# [Oracle ADF] ListView




<ul>
    <li><a href="/dev/view/list/listview/get-value-from-list-view-by-bindings/">Получить программно значение аттрибута выделенной строки в ListView</a></li>
    <li><a href="/dev/view/list/listview/put-parameter-from-listview-in-popup-programmatically/">Передать в popup параметр из ListView программно</a> (Рекомендовано к использованию)</li>
    <li><a href="/dev/view/list/listview/get-selected-element-in-bean/">listView получить выбранный элемент списка в бине</a></li>
    <li><a href="/dev/view/list/listview/get-selected-element-programmatically/">Передача параметра в Popup из ListView</a></li>
    <li><a href="http://www.awasthiashish.com/2015/02/custom-selection-listener-for.html" rel="nofollow">Custom selection listener for af:listView, Invoke selectionListener programmatically</a></li>
    <li><a href="http://www.awasthiashish.com/2015/02/better-ui-for-data-collection-using.html" rel="nofollow">Better UI for data collection using af:listView, Enable selection in ADF Faces List component</a></li>
</ul>
