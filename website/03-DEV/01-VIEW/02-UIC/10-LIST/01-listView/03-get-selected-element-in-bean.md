---
layout: page
title: listView получить выбранный элемент списка в бине
permalink: /dev/view/list/listview/get-selected-element-in-bean/
---


# [Oracle ADF] ListView получить выбранный элемент списка в бине

Имеем ListView

    <af:listView value="#{bindings.XXXXXXXXXXXX.collectionModel}" var="var1"
                 emptyText="#{bindings.XXXXXXXXXXXX.viewable ? 'No data to display.' : 'Access Denied.'}"
                 id="id1"
                 selectionListener="#{bindings.XXXXXXXXXXXX.collectionModel.makeCurrent}"
                 selectedRowKeys="#{bindings.XXXXXXXXXXXX.collectionModel.selectedRow}"
                 selection="single">

        <af:listItem id="id1">
            <af:panelGridLayout id="pg1">

                ****

                <af:gridRow id="gr1">
                    <af:gridCell id="gc1">
                        <af:link id="l1" text="#{item.bindings.XXXXXXXXXXXXXXXX.inputValue}"
                                 action="#{pageFlowScope.myBean.myMethod(var1.bindings.myValue.inputValue)}">

                        </af:link>
                    </af:gridCell>

                    ****

                </af:gridRow>
            </af:panelGridLayout>
        </af:listItem>
    </af:listView>


<br/>
<br/>


    public String myMethod(Object myValue) {
        System.out.println("myValue = " + myValue);
    }


<br/>
<br/>


myValue прописан в Bindins, причем в tree.



Позднее, нужно будет подготовить тествовый пример.
