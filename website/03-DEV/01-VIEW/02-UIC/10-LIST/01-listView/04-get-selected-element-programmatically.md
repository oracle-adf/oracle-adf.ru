---
layout: page
title: Передача параметра в Popup из ListView
permalink: /dev/view/list/listview/get-selected-element-programmatically/
---


# Передача параметра в Popup из ListView

В общем мы давольно таки долго трахались с этой задачей.

Правда больше к ней с месяц никто не подходил.
Поэтому ХЗ повержено ли зло окончательно или нет.

В общем в

    <af:listView value="#{bindings.DepartmentsView.collectionModel}" var="item"
     emptyText="#{bindings.DepartmentsView.viewable ? 'No data to display.' : 'Access Denied.'}"
     fetchSize="#{bindings.DepartmentsView.rangeSize}" id="lv1" selection="single"
     selectionListener="#{bindings.DepartmentsView.treeModel.makeCurrent}">

Обратить внимание на значения в аттрибутах selection и на selectionListener


**Пример:**  
https://bitbucket.org/oracle-adf/send-parameters-into-pupup-from-listview
