---
layout: page
title: Oracle ADF Передать в popup параметр из ListView программно
permalink: /dev/view/list/listview/put-parameter-from-listview-in-popup-programmatically/
---


# [Oracle ADF] Передать в popup параметр из ListView программно


Имеем listView, нужно, чтобы по клике на af:link в нем открывался popup и в него передавался параметр. В данном случае с помощью PageFlowScope.


    <af:listView var="item"

        *******>

                    <af:gridCell>
                        <af:link id="l6"
                                 action="#{pageFlowScope.MyBean.doAction(item.item_value)}">
                        </af:link>
                    </af:gridCell>


                    *****

                </af:gridRow>
            </af:panelGridLayout>
        </af:listItem>
    </af:listView>


<br/>

    public String doAction(Object item_value) {

        System.out.println("*********************************");
        System.out.println("item_value = " + item_value);
        System.out.println("*********************************");
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("p_item_value", item_value);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        hints.add(RichPopup.PopupHints.HintTypes.HINT_LAUNCH_ID,this.p1);
        this.p1.show(hints);
        return null;
    }


<br/>

Popup:


    <af:popup id="p1" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source"
              childCreation="deferred" binding="#{pageFlowScope.SomeBean.p1}"
              popupCanceledListener="#{pageFlowScope.SomeBean.closePopup}">
        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" styleClass="fuse-popup">
            <af:region value="#{bindings.myTaskFlow1.regionModel}" id="r1"/>
        </af:panelWindow>
    </af:popup>


<br/>

То, что сгенерировалось jdeveloper'ом

    <taskFlow id="myTaskFlow1" taskFlowId="/WEB-INF/myTaskFlow1.xml#myTaskFlow1"
                  activation="deferred" xmlns="http://xmlns.oracle.com/adf/controller/binding" Refresh="ifNeeded">
          <parameters>
            <parameter id="p_item_value" value="#{pageFlowScope.p_item_value}"/>
          </parameters>
        </taskFlow>


<br/>

        <input-parameter-definition id="__3">
              <name>p_item_value</name>
              <value>#{pageFlowScope.p_item_value}</value>
              <class>java.lang.Integer</class>
              <required/>
        </input-parameter-definition>



<br/>

В нашем случае вызывается еще и execute with params с этим полем.

    <bindings>
      <action IterBinding="OrdheadViewIterator" id="ExecuteWithParams" RequiresUpdateModel="true"
              Action="executeWithParams" InstanceName="AppModuleDataControl.OrdheadView"
              DataControl="AppModuleDataControl">
        <NamedData NDName="p_item_value" NDValue="#{pageFlowScope.p_item_value}" NDType="java.lang.Integer"/>
      </action>
    </bindings>




<br/>

### Можно также получить всю выбранную строку следующим образом:

    <af:gridCell>
        <af:link id="l6"
                 action="#{pageFlowScope.MyMethod.doAction(item)}">
        </af:link>
    </af:gridCell>

<br/>


Сначала посмотрели какой класс получается:

    public String doAction(Object object) {

        System.out.println("*********************************");
        System.out.println("ORDER_NO = " + object.getClass());
        System.out.println("*********************************");

    }

И дальше уже модифицировали метод следующим образом:

    public String doAction(FacesCtrlHierNodeBinding object) {

        System.out.println("*********************************");
        System.out.println("Attribute_1 = " + object.getAttribute("Attribute_1"));
        System.out.println("Attribute_2 = " + object.getAttribute("Attribute_2"));
        System.out.println("*********************************");

        ****
    }


Т.к. мне нужна дата, пришлось делать следующим образом:

    java.sql.Date myDate = new java.sql.Date(((Timestamp)object.getAttribute("myDate")).getTime());
