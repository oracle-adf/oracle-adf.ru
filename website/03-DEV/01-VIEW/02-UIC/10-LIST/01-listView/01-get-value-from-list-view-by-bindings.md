---
layout: page
title: Oracle ADF Получить программно значение аттрибута выделенной строки в ListView
permalink: /dev/view/list/listview/get-value-from-list-view-by-bindings/
---


# [Oracle ADF] Oracle ADF Получить программно значение аттрибута выделенной строки в ListView


Есть LiveView. Можно выделить строку. Есть кнопка. По нажатию на которую, нужно получить аттрибут из этой строки.


    Object myId = ADFUtils.findIterator("MY_ITERATOR_NAME").getCurrentRow().getAttribute("MY_ATTRIBUTE");
    System.out.println("RES" + myId.toString());



<br/>

### Еще варианты:

    String keyStr = ADFUtils.findIterator("IteratorName").getCurrentRowKeyString();
