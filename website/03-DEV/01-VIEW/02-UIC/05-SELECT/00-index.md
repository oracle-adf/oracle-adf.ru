---
layout: page
title: Oracle ADF Select
permalink: /dev/view/uic/select/
---

# Oracle ADF Select

<br/>

### SelectOne\*

<ul>
	<li><a href="/dev/view/uic/select/one/">SelectOne</a></li>
</ul>

<br/>

### SelectMany\*

<ul>
	<li><a href="/dev/view/uic/select/many/">SelectMany</a></li>
</ul>
