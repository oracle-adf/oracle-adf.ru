---
layout: page
title: RichSelectOneRadio
permalink: /dev/view/uic/select/one/radio/
---


<h1>[Oracle ADF] SelectOneRadio</h1>

<br/>

    <af:selectOneRadio
        label='Радиокнопка'
        id="sor1"
        valueChangeListener="#{MyBean.MyMethod}"
        layout="horizontal"
        autoSubmit="true"
        value="0">

            <af:selectItem label="нет" value="0" id="si4"/>
            <af:selectItem label="да" value="1" id="si5"/>
    </af:selectOneRadio>


<br/>


    RichSelectOneRadio myRSOR = (RichSelectOneRadio)(l_tab.findComponent("component_name" ));
    myRSOR.setValue("1");
    AdfFacesContext.getCurrentInstance().addPartialTarget(myRSOR);
