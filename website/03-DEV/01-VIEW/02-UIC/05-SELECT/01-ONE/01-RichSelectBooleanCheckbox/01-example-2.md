---
layout: page
title: RichSelectBooleanCheckbox
permalink: /dev/view/uic/SelectBooleanCheckbox/example/2/
---


# [Oracle ADF] SelectBooleanCheckbox и RichSelectBooleanCheckbox


<br/>

По нажатию на чекбокс появляется и пропадает меню


    import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;

<br/>


   <af:selectBooleanCheckbox
    text=""
    label="VIP-Klient"
    id="isVip"
    binding="#{main_bean_ul.isVip}"
    valueChangeListener="#{main_bean_ul.onChangeVipClientCheckBox}"
    autoSubmit="true"

    />


<br/>

    // VIP-Klient


    private RichSelectBooleanCheckbox isVip;


    public void setIsVip(RichSelectBooleanCheckbox isVip) {
        this.isVip = isVip;
    }

    public RichSelectBooleanCheckbox getIsVip() {
        return isVip;
    }


<br/>

         // Вывести на экран панель с данными фактической регистрации юр лица.
        public void onChangeVipClientCheckBox(ValueChangeEvent valueChangeEvent) {

            System.out.println("");
            System.out.println("-----------------------------------------");
            System.out.println("---" + (new Date()).toString() + "------");
            System.out.println("Class: main_ul ");
            System.out.println("Method: onChangeVipClientCheckBox ");

            changeShowFactAddress();
        }



<br/>


           public void changeShowFactAddress() {

            System.out.println("");
            System.out.println("-----------------------------------------");
            System.out.println("---" + (new Date()).toString() + "------");
            System.out.println("Class: main_ul ");
            System.out.println("Method: changeShowFactAddress ");
            System.out.println("");


            System.out.println("VIP ? " + isVip.getValue().toString());

            if (isVip.getValue().toString()=="true") {
                 osnovanieVIP.setVisible(true);
             }else {
                osnovanieVIP.setVisible(false);
                                                           }
            AdfFacesContext.getCurrentInstance().addPartialTarget(osnovanieVIP);
        }

    }
