---
layout: page
title: RichSelectBooleanCheckbox
permalink: /dev/view/uic/SelectBooleanCheckbox/example/1/
---


# [Oracle ADF] SelectBooleanCheckbox и RichSelectBooleanCheckbox


<br/>


    <af:selectBooleanCheckbox
          label="Отображать только незаполненные позиции" id="sbc1xx"
          autoSubmit="true" immediate="true" valueChangeListener="#{pageFlowScope.MyBean.MyMethod}"/>


<br/>
<br/>


    public void showOnlyEmpty(ValueChangeEvent valueChangeEvent) {

        RichSelectBooleanCheckbox checkbox = (RichSelectBooleanCheckbox)valueChangeEvent.getComponent();

        if (checkbox.getValue().equals(true)) {
            System.out.println("TRUE");
        } else {
            System.out.println("FALSE");
        }
    }
