---
layout: page
title: RichSelectBooleanCheckbox
permalink: /dev/view/uic/SelectBooleanCheckbox/
---


# [Oracle ADF] SelectBooleanCheckbox и RichSelectBooleanCheckbox


<br/><br/>

<strong>(Актуальные примеры) :</strong>


<ul>
    <li><a href="/dev/view/uic/SelectBooleanCheckbox/example/1/">Пример 1</a></li>
</ul>


<br/><br/>

<strong>(Устаревшие примеры) :</strong>


<ul>
    <li><a href="/dev/view/uic/SelectBooleanCheckbox/example/2/">Пример 2</a></li>
</ul>
