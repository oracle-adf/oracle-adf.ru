---
layout: page
title: SelectOneListbox и RichSelectOneListbox
permalink: /dev/view/uic/select/one/listbox/
---

# [Oracle ADF] SelectOneListbox и RichSelectOneListbox

<br/>

Еще с одним элементом нужно научиться работать.

<br/>

https://bitbucket.org/oracle-adf/richselectonelistbox

<br/>

Add and delete values in POJO based selectOneListbox/selectOneChoice in ADF  
http://www.awasthiashish.com/2016/01/add-and-delete-values-in-pojo-based.html
