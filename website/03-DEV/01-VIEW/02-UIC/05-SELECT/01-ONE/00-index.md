---
layout: page
title: RichSelectOne
permalink: /dev/view/uic/select/one/
---

# [Oracle ADF] RichSelectOne*


<br/>

### Radio

<ul>

	<li><a href="/dev/view/uic/select/one/radio/">SelectOneRadio и RichSelectOneRadio</a></li>

</ul>


<br/>

### BooleanCheckbox

<ul>
	<li><a href="/dev/view/uic/SelectBooleanCheckbox/">SelectBooleanCheckbox и RichSelectBooleanCheckbox</a></li>
</ul>


<br/>

### SelectOneChoice

<ul>

	<li><a href="/dev/view/uic/select/one/choice/">SelectOneChoice и RichSelectOneChoice</a></li>
	<li><a href="http://www.awasthiashish.com/2015/04/build-selectonechoice-to-show.html" rel="nofollow">Build selectOneChoice to show hierarchical (tree style) data programmatically in ADF</a></li>
</ul>


<br/>

### SelectOneListbox

<ul>

	<li><a href="/dev/view/uic/select/one/listbox/">SelectOneListbox и RichSelectOneListbox</a></li>

</ul>
