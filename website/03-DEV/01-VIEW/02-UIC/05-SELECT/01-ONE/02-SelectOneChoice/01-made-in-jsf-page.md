---
layout: page
title: Oracle ADF > SelectOneChoice создан в коде странице
permalink: /dev/view/uic/select/one/choice/made-in-jsf-page/
---


# Oracle ADF > SelectOneChoice создан в коде странице


<br/><br/>

**Пример:**

{% highlight xml linenos %}

<af:selectOneChoice label="Без Сортировки" id="soc1" styleClass="page-title"
                    mode="compact" simple="true"
                    value="0" autoSubmit="true"
                    valueChangeListener="#{MyBeanBean.onMyBeanDateChanged}">
  <af:selectItem label="Без Сортировки" value="0"
                 id="si0"/>
  <af:selectItem label="Текущий день" value="1"
                 id="si1"/>
  <af:selectItem label="Вчера" value="2"
                 id="si2"/>
  <af:selectItem label="Эта неделя" value="3"
                 id="si3"/>
  <af:selectItem label="Текущий месяц" value="4"
                 id="si4"/>
  <af:selectItem label="Текущий квартал" value="5"
                 id="si5"/>
</af:selectOneChoice>

{% endhighlight %}

<br/><br/>


{% highlight java linenos %}

public void onMyBeanDateChanged (ValueChangeEvent valueChangeEvent) {

    System.out.println("===========================");
    System.out.println(valueChangeEvent.getNewValue());
    System.out.println("===========================");


// Or

    RichSelectOneChoice soc = (RichSelectOneChoice)valueChangeEvent.getComponent();
    String var1 = soc.getValue().toString();


}

{% endhighlight %}

<br/><br/>

SOC может возвращать как значение так и индекс.

{% highlight java linenos %}

    System.out.println("");
    System.out.println(valueChangeEvent.getNewValue().toString());
    System.out.println("");

{% endhighlight %}

Для настройки, в Bindings перейти на страницу pageDef.

Параметр SelectItemValueMode:

Выбрать что нужно: ListObject или LixtIndex.

Т.е. выбранное значение или его индекс.
