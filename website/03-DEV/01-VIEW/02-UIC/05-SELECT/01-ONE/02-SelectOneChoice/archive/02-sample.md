---
layout: page
title: RichSelectOneChoice Sample 1
permalink: /dev/view/uic/select/one/choice/02-sample/
---


<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>


<strong>Задать параметры selectOneChoice непосредственно на странице:</strong>

    <af:selectOneChoice id="soc1">
       <af:selectItem label="label1" id="selectItem1" value="value1" />
       <af:selectItem label="label2" id="selectItem2" value="value2" />
       <af:selectItem label="label3" id="selectItem3" value="value3" />
    </af:selectOneChoice>
