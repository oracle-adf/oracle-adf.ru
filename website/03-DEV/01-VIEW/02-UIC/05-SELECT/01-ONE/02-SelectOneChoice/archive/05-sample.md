---
layout: page
title: RichSelectOneChoice Sample 4
permalink: /dev/view/uic/select/one/choice/05-sample/
---


<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>



<strong>Задать параметры selectOneChoice используя viewobject и bindings</strong>


viewObject с именем voLanguages


    <af:selectOneChoice id="soc1" label="Языки программирования" autoSubmit="true"
                   value="#{bindings.languages.inputValue}">

       <af:forEach items="#{bindings.voLanguages.rangeSet}" var="row">
          <f:selectItem itemLabel="#{row.id}" itemValue="#{row.language}"/>
       </af:forEach>
    </af:selectOneChoice>



value="#{bindings.languages.inputValue}" -  данные, которые будут переданы в переменную во viewObject (если ничего не путаю)




    select '1' as id , 'Java' as language from dual
    union
    select '2' as id, 'C++' as language  from dual
    union
    select '3' as id, 'C#' as language  from dual
    union
    select '4' as id, 'PL/SQL' as language  from dual
    union
    select '5' as id, 'Python' as language  from dual



<br/>

Правой клавишей мыши по view1.jsf --> "go to page definition"


executables -->  Создал итератор и связал его с voLanguages.




bindings --> создать элемент типа "tree"


    Root Data Source: AppModuleDataControl.voLanguages1
    Tree Level Rules: ru.javadev.model.view.voLanguages
    В display attributes добавляю id и languages.


Получилось:

    Bindings: voLanguages1
    Executables: voLanguages1iterator
    DataControl: voLanguages1
