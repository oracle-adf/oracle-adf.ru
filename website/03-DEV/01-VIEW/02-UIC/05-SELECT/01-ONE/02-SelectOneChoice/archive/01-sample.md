---
layout: page
title: RichSelectOneChoice
permalink: /dev/view/uic/select/one/choice/01-sample/
---


<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>


<br/>
<strong>Обычный SelectOneChoice</strong>

    <selectOneChoice
        id="soc1"
        simple="true"
        value="#{Bean.list.itemNo}"
        unselectedLabel="Please Select">

    selectItems
        value="#{Bean.listItems}"/>
    </af:selectOneChoice>

Блок selectItems - отвечает за то, какие элементы будут отображаться в SelectOneChoice


У нас, он обычно выглядит следующим образом:


    <af:selectOneChoice id="soc1" label="Языки программирования" autoSubmit="true">

       <af:forEach items="#{bindings.voLanguages.rangeSet}" var="row">
          <f:selectItem itemLabel="#{row.id}" itemValue="#{row.language}"/>
       </af:forEach>
    </af:selectOneChoice>


<br/>


    UIComponent tab1 = Utils.getComponent("tabName");
    RichSelectOneChoice mySelectOneChoice = (RichSelectOneChoice)(tab1.findComponent("socDivisAccomp"));


    mySelectOneChoice.setValue("0");
    // mySelectOneChoice.getValue().toString();


    // Для обновления значения элемента на форме
    AdfFacesContext.getCurrentInstance().addPartialTarget(mySelectOneChoice);


----------------------------------------------------------


    public static UIComponent getComponent(String compId) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = fctx.getViewRoot();
        return viewRoot.findComponent(compId);
    }


<br/>

    <af:selectOneChoice id="soc1" label="Языки программирования" autoSubmit="true"

    binding="#{mainBean.customerName}"
    >

       <af:forEach items="#{bindings.voLanguages.rangeSet}" var="row">
          <f:selectItem itemLabel="#{row.id}" itemValue="#{row.language}"/>
       </af:forEach>
    </af:selectOneChoice>


<br/>

    // id = 10
    customerName.setValue(id);
    // customerName.getValue().toString();

    // Для обновления значения элемента на форме
    AdfFacesContext.getCurrentInstance().addPartialTarget(customerName);


<br/>

Иногда SelectOneChoice имеет длину большую чем нужно. В нашем случае поле у одной страны было очень большим
и оно портило весь внешний вид формы.


Удалось победить следующим образом:

    <af:panelFormLayout id="pfl11"
    					labelAlignment="start"
    					inlineStyle="width:288px;">
    	<af:selectOneChoice label="Страна"
				id="countryRegPerson"
				autoSubmit = "true"
				disabled="true"

				valueChangeListener="#{main_bean.onChangeCountryRegPerson}">

    		<af:forEach items="#{bindings.ViewObjCountries11.rangeSet}" var="row1">
    			<f:selectItem  id="si5" itemLabel="#{row1.Country}" itemValue="#{row1.Code}"/>
    		</af:forEach>

    	</af:selectOneChoice>
    </af:panelFormLayout>


<br/><br/>

В общем параметр <strong> inlineStyle="width:288px;"</strong> как раз и отвечает за размер на форме элемента.


Получить выбранное значение и предыдущее значение SelectOneChoice.


    public void onCurrencyChanged(ValueChangeEvent valueChangeEvent) {

    String oldValue = valueChangeEvent.getOldValue().toString();
    String newValue = valueChangeEvent.getNewValue().toString();

    System.out.println(" valueChangeEvent " + oldValue);
    System.out.println(" valueChangeEvent " + newValue);
    }


    valueChangeEvent Рубль
    valueChangeEvent Швейцарский франк
