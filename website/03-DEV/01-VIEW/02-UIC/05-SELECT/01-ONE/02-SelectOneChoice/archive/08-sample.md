---
layout: page
title: RichSelectOneChoice Sample 7
permalink: /dev/view/uic/select/one/choice/08-sample/
---


<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>


Считать данные из SelectOneChoice


    try {
        JUCtrlListBinding listBinding = null;
        BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
        listBinding = (JUCtrlListBinding) bindings.get("ViewObj");
        String jobIdValue = (String)listBinding.getRowIterator().getCurrentRow().getAttribute("Name");
        System.out.println(jobIdValue);

    } catch (Exception e) {
        System.out.println("SOMETHIG BAD HAPPENED !!! :(");
        //e.printStackTrace();
    }


<br/>

Пример 2.



    System.out.println(GetValFromIterator("ViewObj", "Name"));
    System.out.println(GetValFromIterator("ViewObj", "Id"));  



    public static String GetValFromIterator(String vo, String attrName) {
        try {
            BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
            JUCtrlListBinding fclb = (JUCtrlListBinding)bindings.get(vo);
            return fclb.getRowIterator().getCurrentRow().getAttribute(attrName).toString();
        } catch (Exception e) {
            System.out.println("*** GetValFromIterator Exeption :" + e.getMessage());
            return "";
        }
    }
