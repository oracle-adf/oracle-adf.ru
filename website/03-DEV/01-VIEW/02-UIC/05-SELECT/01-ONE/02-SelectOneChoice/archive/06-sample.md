---
layout: page
title: RichSelectOneChoice Sample 5
permalink: /dev/view/uic/select/one/choice/06-sample/
---



<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>


<strong>Задать параметры selectOneChoice используя viewobject</strong>


    <af:selectOneChoice
            id="mySelect1"
            label="Список1">

       <f:selectItems
       id="si51"
       value="#{bindings.valuesFromEmp.items}"
        />

    </af:selectOneChoice>



voEmp

    select * from emp



view1PageDef.xml

    <pageDefinition xmlns="http://xmlns.oracle.com/adfm/uimodel" version="11.1.2.62.76" id="view1PageDef"
                    Package="ru.javadev.view.pageDefs">
      <parameters/>
      <executables>
        <variableIterator id="variables"/>
        <iterator id="voEmp1Iterator" Binds="voEmp1" DataControl="AppModuleDataControl" RangeSize="25"/>
      </executables>


      <bindings>
        <list id="valuesFromEmp" IterBinding="voEmp1Iterator" ListOperMode="navigation"  >
          <AttrNames>
            <Item Value="Ename"/>
          </AttrNames>
        </list>
      </bindings>


    </pageDefinition>



AppModule.xml

    <AppModule
      xmlns="http://xmlns.oracle.com/bc4j"
      Name="AppModule"
      Version="11.1.2.62.76"
      ClearCacheOnRollback="true">



      <ViewUsage
        Name="voEmp1"
        ViewObjectName="ru.javadev.model.voEmp"/>

    </AppModule>
