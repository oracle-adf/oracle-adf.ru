---
layout: page
title: RichSelectOneChoice Sample 2
permalink: /dev/view/uic/select/one/choice/03-sample/
---

<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>


<strong>Задать параметры selectOneChoice в бине:</strong>

<br/>

        <af:selectOneChoice
                  id="mySelect1"
                  label="Список1">
    <f:selectItems
    id="si51"
    value="#{mainBean.listOfItems}"
               />



<br/>

    package ru.javadev.view;

    import java.util.ArrayList;
    import java.util.List;

    import javax.faces.model.SelectItem;


    import oracle.adf.model.BindingContext;
    import oracle.adf.model.binding.DCBindingContainer;
    import oracle.adf.model.binding.DCIteratorBinding;


    import oracle.binding.BindingContainer;

    import oracle.jbo.RowIterator;
    import oracle.jbo.uicli.binding.JUCtrlListBinding;

    public class mainBean {



      private List<SelectItem> listOfItems;

        public void setListOfItems(List<SelectItem> listOfItems) {
            this.listOfItems = listOfItems;
        }


        public List<SelectItem> getListOfItems() {


            List<SelectItem> listOfItems = new ArrayList<SelectItem>();
            listOfItems.add(new SelectItem("CUST1","Customer1"));
            listOfItems.add(new SelectItem("CUST2","Customer2"));
            listOfItems.add(new SelectItem("CUST3","Customer3"));

            return listOfItems;


        }




        public String cb1_action() {

            System.out.println("=================================");



            System.out.println("------------------------------------");
            System.out.println("-- INSTRUCTIONS COMPLETED -------!!!");
            System.out.println("---" + (new Date()).toString() + "------");
            System.out.println(customerName.getValue().toString());

            return null;
        }
    }
