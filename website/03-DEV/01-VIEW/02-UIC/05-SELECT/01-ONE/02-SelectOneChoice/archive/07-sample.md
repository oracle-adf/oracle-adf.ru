---
layout: page
title: RichSelectOneChoice Sample 6
permalink: /dev/view/uic/select/one/choice/07-sample/
---



<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>



Считать ID SelectOneChoice



    <af:selectOneChoice
                  id="mySelect1"
                  label="Список1"
                  value="#{bindings.Ename.inputValue}"
                  binding="#{mainBean.customerName}"
                  >
    <f:selectItems
    id="si51"
    value="#{bindings.Ename.items}"
               />


<br/>


    bindings:Ename
    Executables:voEmp1Iterator
    DataControl: voEmp1


<br/>

vo:

    select * from emp


<br/>

    package ru.javadev.view;

    import java.util.ArrayList;
    import java.util.Date;
    import java.util.List;

    import javax.faces.context.FacesContext;
    import javax.faces.model.SelectItem;


    import oracle.adf.model.BindingContext;
    import oracle.adf.model.binding.DCBindingContainer;
    import oracle.adf.model.binding.DCIteratorBinding;


    import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

    import oracle.binding.AttributeBinding;
    import oracle.binding.BindingContainer;

    import oracle.jbo.Row;
    import oracle.jbo.RowIterator;
    import oracle.jbo.RowSetIterator;
    import oracle.jbo.uicli.binding.JUCtrlListBinding;

    public class mainBean {



        private RichSelectOneChoice  customerName;

        public void setCustomerName(RichSelectOneChoice customerName) {
            this.customerName = customerName;
        }

        public RichSelectOneChoice getCustomerName() {
            return customerName;
        }


        public String cb1_action() {

            System.out.println("=================================");

            System.out.println("------------------------------------");
            System.out.println("-- INSTRUCTIONS COMPLETED -------!!!");
            System.out.println("---" + (new Date()).toString() + "------");
            System.out.println(customerName.getValue().toString());

            return null;
        }


    }

<br/>


По нажатию на кнопку, считывается ID сотрудника.




    =================================
    ------------------------------------
    -- INSTRUCTIONS COMPLETED -------!!!
    ---Mon Jan 21 01:56:12 MSK 2013------
    5
    =================================
    ------------------------------------
    -- INSTRUCTIONS COMPLETED -------!!!
    ---Mon Jan 21 01:56:38 MSK 2013------
    2
    =================================
    ------------------------------------
    -- INSTRUCTIONS COMPLETED -------!!!
    ---Mon Jan 21 02:10:35 MSK 2013------
    8


<br/><br/>

Как считать значение, которое отображается в selectonechoice простым способом, с помощью одной команды, я пока не знаю.
