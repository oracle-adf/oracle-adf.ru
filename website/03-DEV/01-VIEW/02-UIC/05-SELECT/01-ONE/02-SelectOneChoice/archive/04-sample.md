---
layout: page
title: RichSelectOneChoice Sample 3
permalink: /dev/view/uic/select/one/choice/04-sample/
---



<h1>[Oracle ADF] SelectOneChoice и RichSelectOneChoice</h1>


<strong>Задать параметры selectOneChoice (c параметрами label и value) используя viewobject и бин</strong>




    <af:selectOneChoice
    label="Страна1"
    id="mySelect1"

    value="#{bindings.ViewObjCountries1.inputValue}"
    binding="#{main_bean.countryChoice1}"

    >
       <f:selectItems value="#{main_bean.listOfItems}"
                   id="si51"/>
    </af:selectOneChoice>


<br/>


    private List listOfItems = new ArrayList();

        public void setListOfItems(List listOfItems){
          this.listOfItems = listOfItems;
        }



        public List getListOfItems(){
          return listOfItems;
        }



       ========

        BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
        JUCtrlListBinding fclb = (JUCtrlListBinding)bindings.get("ViewObjCountries1");
        RowIterator rIter = fclb.getRowIterator();


        SelectItem si;

        for (int i = 0; i < rIter.getRowCount(); i++) {
            si = new SelectItem();
            si.setLabel(rIter.getRowAtRangeIndex(i).getAttribute("Country").toString());
            si.setValue(rIter.getRowAtRangeIndex(i).getAttribute("Code").toString());
            listOfItems.add(si);
        }
