---
layout: page
title: Oracle ADF > Создать SelectOneChoice в формате Tree
permalink: /dev/view/uic/select/one/choice/select-one-choice-if-tree-format/
---

# Oracle ADF > Создать SelectOneChoice в формате Tree

### В bindings:

Создаем новый control с типом Tree.

![oracle adf Tree Binding](/img/elements/Tree-Binding1.png "oracle adf Tree Binding"){: .center-image }

![oracle adf Tree Binding](/img/elements/Tree-Binding2.png "oracle adf Tree Binding"){: .center-image }

<br/>

### Далее на странице

<br/><br/>

```xml

<af:selectOneChoice label="Пользователь" id="soc_users"
autoSubmit="true" clientComponent="true"
valueChangeListener="#{MyBeanBean.onMyBeanUserChanged}">
    <af:forEach items="#{bindings.Users1.rangeSet}" var="row1">
    <f:selectItem id="si12m" itemLabel="#{row1.Lastname} #{row1.Firstname}" itemValue="#{row1.Login}"/>
    </af:forEach>
</af:selectOneChoice>

```

<br/><br/>

```java

public void onMyBeanUserChanged(ValueChangeEvent valueChangeEvent) {
System.out.println("User Changed");

    RichSelectOneChoice user_login = (RichSelectOneChoice)valueChangeEvent.getComponent();
    System.out.println("User login " + user_login.getValue().toString());

}

```

PS. Можно также получить. В чем разница ХЗ.

```java

    RichSelectOneChoice rsoc =
     (RichSelectOneChoice)valueChangeEvent.getSource();

```

<br/><br/>

В результате в SelectOneChoice мы имеем фамилию и имя пользователя, а в консоли получаем его login.
При желании таким же образом можно получить его ID в базе.
