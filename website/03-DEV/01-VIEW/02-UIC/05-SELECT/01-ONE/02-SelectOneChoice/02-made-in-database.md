---
layout: page
title: Oracle ADF > SelectOneChoice создан SQL запросом в базе (ключ - значение)
permalink: dev/view/uic/select/one/choice/made-in-database/
---


# Oracle ADF >SelectOneChoice создан SQL запросом в базе (ключ - значение)

Самый простой вариант - с ключ - значением.

Где нужно при выборе получать ключ.

Bindings:

    New List --> Standalone select multiple values list (does not update a base data source)


<br/>

    Multi Select Base Attribute: (key)
    Multi Select Display Attribute: (Value)

<br/>


Даю название листу, например MyList


<br/>

    <af:selectOneChoice id="soc61" simple="true"
                        autoSubmit="true"
                        valueChangeListener="#{pageFlowScope.MyBean.onSelect}">
           <f:selectItems value="#{bindings.MyList.items}" id="si7"/>
            <f:validator binding="#{bindings.MyList.validator}"/>
    </af:selectOneChoice>


<br/>

В Bindings перейти на страницу pageDef и выбрать созданный List.

Параметр SelectItemValueMode установить в ListObject.
