---
layout: page
title: Oracle ADF > SelectOneChoice и RichSelectOneChoice
permalink: /dev/view/uic/select/one/choice/
---


# Oracle ADF > SelectOneChoice и RichSelectOneChoice



<br/><br/>

<strong>(Актуальные примеры) :</strong>


<ul>
    <li><a href="/dev/view/uic/select/one/choice/made-in-jsf-page/">SelectOneChoice создан в коде странице</a></li>
    <li><a href="/dev/view/uic/select/one/choice/made-in-database/">SelectOneChoice создан SQL запросом в базе (ключ - значение)</a></li>
    <li><a href="/dev/view/uic/select/one/choice/get-value/">Получить выбранное значение из SelectOneChoice</a></li>
    <li><a href="/dev/view/uic/select/one/choice/select-one-choice-if-tree-format/">Создать SelectOneChoice в формате Tree</a></li>
</ul>


<br/><br/>

<strong>Установить значение по умолчанию для SelectOneChoice:</strong>


<ul>
    <li><a href="/dev/view/uic/select/one/choice/set-default-value/">Установить значение по умолчанию для SelectOneChoice программно</a></li>
    <li><a href="/dev/view/uic/select/one/choice/set-default-value/example-2/">Установить значение по умолчанию для SelectOneChoice </a></li>
</ul>

<br/>


<strong>Найдено, нужно проверить, может лучше, чем делаю я:</strong>

<ul>
    <li><a href="http://www.awasthiashish.com/2016/04/adf-basics-get-display-and-base-value.html" rel="nofollow">ADF Basics: Get display and base value of POJO based SelectOneChoice</a></li>
    <li><a href="http://www.awasthiashish.com/2014/10/set-values-in-afselectmanychoice.html" rel="nofollow">Set values in af:selectManyChoice programmatically - Oracle ADF</a></li>
    <li><a href="http://rohanwalia.blogspot.ru/2015/01/get-selectonechoice-codevalues-in.html" rel="nofollow">Get SelectOneChoice Code/Values in Managed Bean - ADF</a></li>
    <li><a href="http://www.awasthiashish.com/2014/05/getting-selected-value-not-index.html" rel="nofollow">Getting selected value (not index) & display value of select one choice programmatically in ADF</a></li>
</ul>

<br/><br/>

<strong>(Архив, который нужно переработать) Другие примеры работы с SelectOneChoice:</strong>

<br/><br/>

<a href="/dev/view/uic/select/one/choice/01-sample/">Пример 1</a><br/>
<a href="/dev/view/uic/select/one/choice/02-sample/">Пример 2 (Задать параметры selectOneChoice непосредственно на странице) </a><br/>
<a href="/dev/view/uic/select/one/choice/03-sample/">Пример 3 (Задать параметры selectOneChoice в бине и распечатать выбранное значение в консоли)</a><br/>
<a href="/dev/view/uic/select/one/choice/04-sample/">Пример 4 (Задать параметры selectOneChoice используя viewobject и бин)</a><br/>
<a href="/dev/view/uic/select/one/choice/05-sample/">Пример 5 (Задать параметры selectOneChoice используя viewobject и bindings)</a><br/>
<a href="/dev/view/uic/select/one/choice/06-sample/">Пример 6 (Задать параметры selectOneChoice используя viewobject)</a><br/>
<a href="/dev/view/uic/select/one/choice/07-sample/">Пример 7 (Считать ID SelectOneChoice)</a><br/>
<a href="/dev/view/uic/select/one/choice/08-sample/">Пример 8 (Считать данные из SelectOneChoice )</a><br/>
