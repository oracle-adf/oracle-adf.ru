---
layout: page
title: Oracle ADF > Получить выбранное значение из SelectOneChoice
permalink: /dev/view/uic/select/one/choice/get-value/
---


# Oracle ADF > Получить выбранное значение из SelectOneChoice

<br/>

### Вариант 1 (100% рабочий):

<br/>

{% highlight xml linenos %}

<af:selectOneChoice value="#{bindings.AttributeBindingsName.inputValue}"
                    autoSubmit="true"
                    label="#{bindings.AttributeBindingsName.label}"
                    required="#{bindings.AttributeBindingsName.hints.mandatory}"
                    shortDesc="#{bindings.AttributeBindingsName.hints.tooltip}" id="soc1"
                    valueChangeListener="#{MyBean.onSelect}">
    <f:selectItems value="#{bindings.AttributeBindingsName.items}" id="si1"/>
    <f:validator binding="#{bindings.AttributeBindingsName.validator}"/>
</af:selectOneChoice>

{% endhighlight %}


<br/>

**В bindings определен как list**

{% highlight java linenos %}

public void onSelect(ValueChangeEvent valueChangeEvent) {

    // Эта строка нужна, чтобы итератор переключился на выбранную сторку VO
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());


    // RichSelectOneChoice soc = (RichSelectOneChoice)valueChangeEvent.getComponent();
    // System.out.println("Index: " + soc.getValue().toString());

    Object value = ADFUtils.findIterator("ITERATOR_NAME").getCurrentRow().getAttribute("ATTRIBUTE_NAME");
    System.out.println("Value " + value.toString());
}

{% endhighlight %}

<br/>

Разумеется аттрибут должен быть определен во ViewObject.



<br/>

### Вариант 2 (100% рабочий):

Использовать, когда SelectOneChoice возвращает индекс элемента, а не его значение:

В этом случае, можно сделать так.


{% highlight java linenos %}

public void onSelect(ValueChangeEvent valueChangeEvent) {

    RichSelectOneChoice soc = (RichSelectOneChoice)valueChangeEvent.getComponent();

    // Value

    Object value = ADFUtils.findIterator("ITERATOR_NAME").getRowAtRangeIndex((Integer)soc.getValue()).getAttribute("ATTRIBUTE_NAME");
    System.out.println("Value " + value.toString());
}

{% endhighlight %}
