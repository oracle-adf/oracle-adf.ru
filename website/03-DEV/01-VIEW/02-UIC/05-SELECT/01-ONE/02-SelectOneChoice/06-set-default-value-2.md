---
layout: page
title: Oracle ADF > Установить значение по умолчанию для SelectOneChoice
permalink: /dev/view/uic/select/one/choice/set-default-value/example-2/
---


# Oracle ADF > Установить значение по умолчанию для SelectOneChoice



Чтобы не делать проверку на null.

А я тут сидел и писал конструкции вроде следующей:

    value="#{row.MyAttribute == null ? 'C' : row.MyAttribute}"

Просто во VO, написать:

    nvl (MyTable.MyAttribute, 'C' ) MyAttribute


<br/><br/>


Если необходимо подставить какой-нибудь ключ по умолчанию, и при его установке отображается только текст, а сам выбор отсутствует. Нужно найти VO который хранит этот ключ, зайти в аттрибуты, выбрать нужный аттрибут и установить Updateble: Always
