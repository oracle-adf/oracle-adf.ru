---
layout: page
title: Установить программно checkbox в SelectManyCheckbox (DEPRECATED)
permalink: /dev/view/uic/select/many/checkbox/setup-checkbox-programmatically/
---

# [Oracle ADF] Установить программно checkbox в SelectManyCheckbox (DEPRECATED)

<br/>

### DEPRECATED !!! Не советую так делать! Не советую это все читать! Писал глупец АХаХаХаХаХаХаХ....

Чтобы разобраться, даже создавал тикет на community:  
https://community.oracle.com/thread/3880605


Хотя эта хуета всеравно не работает как мне нужно.  
Таким образом я могу пока установить 1 флажок. А что же делать если нужно 2 и более?
Отпишусь как разберусь. Или помогите мне если знаете.

Оказалось, что для того, чтобы установить только 1 элемент, ничего из этого и не нужно.
Но, возможно будет полезно, если нужно установить какие-нибудь значки по умолчанию.


Обратить внимание на value. В нем хранится список установленных checkbox.


{% highlight xml linenos %}

<af:selectManyCheckbox id="my_selectManyCheckbox"  
                  valueChangeListener="#{MyBeanBean.onMyBeanPriorityChanged}"  
                  autoSubmit="true"  
                  required="false"
                  value="#{MyBeanBean.selectedCheckboxes}">  
                      <af:selectItem label="High" value="1" id="si4"/>  
                      <af:selectItem label="Middle" value="2" id="si5"/>  
                      <af:selectItem label="Low" value="3" id="si6"/>  
</af:selectManyCheckbox>

{% endhighlight %}


<br/>

Список чекбоксов в бине, может выглядеть вот так:


{% highlight java linenos %}

private List<String> selectedCheckboxes;  

public void setSelectedCheckboxes(List<String> selectedCheckboxes) {  
    this.selectedCheckboxes = selectedCheckboxes;  
}  


public List<String> getSelectedCheckboxes() {    

    // Разкомментировать, если нужно по умолчанию установить checkbox
    //        List<String> selectedCheckboxes = new ArrayList<String>();  
    //        selectedCheckboxes.add("2");

    return selectedCheckboxes;  
}  

{% endhighlight %}


<br/>

Собственно нахожу этот элемент на форме в бине и ....

{% highlight java linenos %}

RichSelectManyCheckbox my_selectManyCheckbox= (RichSelectManyCheckbox)task_filter_page_template.findComponent("my_selectManyCheckbox");  
my_selectManyCheckbox.setValue("1");

{% endhighlight %}

setValue - добавляет в список чекбокс у которого в value установлено значение 1.
