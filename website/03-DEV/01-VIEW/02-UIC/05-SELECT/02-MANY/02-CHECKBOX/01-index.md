---
layout: page
title: SelectManyCheckbox  - получить выбранные Checkbox'ы
permalink: /dev/view/uic/select/many/checkbox/
---

# [Oracle ADF] SelectManyCheckbox - получить выбранные Checkbox'ы


<br/>

{% highlight xml linenos %}

<af:selectManyCheckbox id="smc1"
binding="#{TasksBean.tasksStatusFilter}"
        valueChangeListener="#{TasksBean.onCheckboxSelected}"
        autoSubmit="true"
        required="false">

    <af:selectItem
        label="Новая"
        value="New"
        id="si1"
        />

    <af:selectItem label="В работе" value="InWork" id="si2"/>
    <af:selectItem label="Завершена" value="Completed" id="si3"/>
</af:selectManyCheckbox>

{% endhighlight %}

<br/>

{% highlight java linenos %}

RichSelectManyCheckbox tasksStatusFilter;

 public void setTasksStatusFilter(RichSelectManyCheckbox tasksStatusFilter) {
     this.tasksStatusFilter = tasksStatusFilter;
 }

 public RichSelectManyCheckbox getTasksStatusFilter() {
     return tasksStatusFilter;
 }

 ****


 public void onCheckboxSelected (ValueChangeEvent valueChangeEvent) {
     System.out.println("================");
     System.out.println("BUTTON PRESSED BEGIN: ");
     System.out.println("================");

     System.out.println(tasksStatusFilter.getValue().toString());

     System.out.println("================");
     System.out.println("BUTTON PRESSED END: ");
     System.out.println("================");
 }

{% endhighlight %}

 При выборе всех чекбоксов в консоли имеем:


    ================
    BUTTON PRESSED BEGIN:
    ================
    [New, InWork, Completed]
    ================
    BUTTON PRESSED END:
    ================


<br/>

### Лучше

{% highlight java linenos %}

public void onCheckboxSelected (ValueChangeEvent valueChangeEvent) {

    ArrayList<String> selectedListElements = (ArrayList<String>)valueChangeEvent.getNewValue();

    for (int i = 0; i < selectedListElements.size(); i++) {
         System.out.println(selectedListElements.get(i));
    }


    if (selectedListElements.contains("New")){
            doTrue();
        } else {
            doFalse();
        }
}

{% endhighlight %}
