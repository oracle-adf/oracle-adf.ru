---
layout: page
title: SelectManyCheckbox  - установить Checkbox'ы программно по нажатию на кнопку
permalink: /dev/view/uic/select/many/checkbox/set-default-values-for-checkboxes-programmatically/
---

# [Oracle ADF] SelectManyCheckbox - Установить Checkbox'ы по умолчанию программно


Бин у меня имеет SessionScope область видимости.

<br/>

Сам SelectManyCheckbox такой же как и в предыдущей теме.


{% highlight java linenos %}


private RichSelectManyCheckbox statusCheckbox = new RichSelectManyCheckbox();

public void setStatusCheckbox(RichSelectManyCheckbox statusCheckbox) {
    this.statusCheckbox = statusCheckbox;
}

public RichSelectManyCheckbox getStatusCheckbox() {
    return statusCheckbox;
}

// ----------------------------------------------------

@PostConstruct
public void init(){

    List<String> selectedCheckboxes = new ArrayList<String>();  
    selectedCheckboxes.add("1");
    selectedCheckboxes.add("3");

    statusCheckbox.setValue(selectedCheckboxes);

}

{% endhighlight %}



<br/>

Сейчас думаю, что вариант с заданием значения поля в конструкторе предпочтительнее, но я его не тестил.

    private RichSelectManyCheckbox statusCheckbox;

<br/>


    constructor:

    statusCheckbox = new RichSelectManyCheckbox();
