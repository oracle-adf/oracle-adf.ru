---
layout: page
title: SelectManyCheckbox как лучше с ним работать
permalink: /dev/view/uic/select/many/checkbox/should-be/
---

# [Oracle ADF] SelectManyCheckbox как лучше с ним работать


Для фильтрации по 1-му полю:


{% highlight xml linenos %}

<af:selectManyCheckbox id="type_smc"
                   valueChangeListener="#{MyBean.onCheckboxSelected}"
                   autoSubmit="true" required="false" label="Тип заказа" simple="true">

    <af:forEach items="#{bindings.TasksCategory_List.rangeSet}" var="listrow">
        <f:selectItem id="si10" itemValue="#{listrow.Id}" itemLabel="#{listrow.Name}"/>
    </af:forEach>
</af:selectManyCheckbox>

{% endhighlight %}

<br/>

TasksCategory_List - tree

Также как и для SelectOneChoice. (Но в примере с SelectOneChoice есть какртинки.)


<br/>

{% highlight java linenos %}

public void onCheckboxSelected (ValueChangeEvent valueChangeEvent) {

    if (null != valueChangeEvent.getNewValue()){

        ArrayList<Integer> selectedListElements = null;
        selectedListElements = (ArrayList<Integer>)valueChangeEvent.getNewValue();

        ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_VO_NAME);

        ViewCriteria vc = vo.createViewCriteria();
        vc.setName(CONSTANTS_VC.ANY_VC_NAME);

        for (Integer si : selectedListElements){
            ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
            vcr1.setAttribute("Table_Filed_From_ViewObject","= " + si);
            vc.add(vcr1);
        }

        vo.applyViewCriteria(vc, true);
        vo.executeQuery();

    } else {
        VOUtils.clearViewCriteriaByNameAndExecuteViewObject(CONSTANTS_VO.MY_VO_NAME, CONSTANTS_VC.ANY_VC_NAME);
    }
}

{% endhighlight %}
