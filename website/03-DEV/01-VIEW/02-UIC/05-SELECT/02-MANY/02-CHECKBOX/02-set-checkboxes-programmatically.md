---
layout: page
title: SelectManyCheckbox  - установить Checkbox'ы программно по нажатию на кнопку
permalink: /dev/view/uic/select/many/checkbox/set-checkboxes-programmatically/
---

# [Oracle ADF] SelectManyCheckbox  - установить Checkbox'ы программно по нажатию на кнопку


<br/>

Есть selectManyCheckbox.
Я привязываю его к бину

{% highlight xml linenos %}

<af:selectManyCheckbox id="smc1"
                            autoSubmit="true"
                            required="false"
                            binding="#{MyBean.statusCheckbox}">

                        <af:selectItem label="Новая" value="1" id="si1" />
                        <af:selectItem label="В работе" value="2" id="si2"/>
                        <af:selectItem label="Завершена" value="3" id="si3"/>
</af:selectManyCheckbox>

{% endhighlight %}

Есть кнопка

{% highlight xml linenos %}
<af:button text="Test Button" id="b1" action="#{MyBean.onClick}"/>
{% endhighlight %}


<br/>

{% highlight java linenos %}

private RichSelectManyCheckbox statusCheckbox;

public void setStatusCheckbox(RichSelectManyCheckbox statusCheckbox) {
    this.statusCheckbox = statusCheckbox;
}

public RichSelectManyCheckbox getStatusCheckbox() {
    return statusCheckbox;
}

// -------------------------------------------

public String onClick() {

    List<String> selectedCheckboxes = new ArrayList<String>();

    selectedCheckboxes.add("2");
    selectedCheckboxes.add("3");

    statusCheckbox.setValue(selectedCheckboxes);

    UIUtils.updateUIComponent(statusCheckbox);

    return null;
}

{% endhighlight %}
