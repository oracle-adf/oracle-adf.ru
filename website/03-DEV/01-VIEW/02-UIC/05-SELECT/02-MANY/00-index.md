---
layout: page
title: RichSelectMany
permalink: /dev/view/uic/select/many/
---

# [Oracle ADF] RichSelectMany*


<br/>

### SelectManyChoice

<ul>
	<li><a href="/dev/view/uic/select/many/choice/">SelectManyChoice  - получить выбранные select'ы</a></li>
</ul>

<br/>

### SelectManyCheckbox

<ul>
	<li><a href="/dev/view/uic/select/many/checkbox/">SelectManyCheckbox  - получить выбранные Checkbox'ы</a></li>

	<li><a href="/dev/view/uic/select/many/checkbox/set-checkboxes-programmatically/">SelectManyCheckbox  - установить Checkbox'ы программно по нажатию на кнопку</a></li>

	<li><a href="/dev/view/uic/select/many/checkbox/set-default-values-for-checkboxes-programmatically/">Установить Checkbox'ы по умолчанию программно</a></li>

	<li><a href="/dev/view/uic/select/many/checkbox/should-be/">SelectManyCheckbox как лучше с ним работать</a></li>
	<li><a href="/dev/view/uic/select/many/checkbox/setup-checkbox-programmatically/">Установить программно checkbox в SelectManyCheckbox</a> (DEPRECATED) </li>
</ul>


<br/>

### SelectManyListbox

<ul>
	<li><a href="/dev/view/uic/select/many/listbox/">SelectManyListbox</a></li>
</ul>

<br/>

### SelectManyShuttle

<ul>
	<li><a href="/dev/view/uic/select/many/shuttle/">SelectManyShuttle</a></li>
</ul>


<br/><br/>

Get Multiple Selected Rows In ADF  
http://adfindepth.blogspot.ru/2016/07/get-multiple-selected-rows-in-adf.html


<br/><br/>

Programmatically Select all values in ADF BC based selectMany (af:selectManyCheckbox, af:selectManyChoice, af:selectManyListbox, af:selectManyShuttle) component  
http://www.awasthiashish.com/2015/06/programmatically-select-all-values-in.html


<br/><br/>

Programmatically populate values in ADF Faces multiSelect component (af:selectManyCheckbox, af:selectManyChoice, af:selectManyListbox, af:selectManyShuttle)  
http://www.awasthiashish.com/2015/01/programmatically-populate-values-in-adf.html
