---
layout: page
title: CommandMenuItem
permalink: /dev/view/uic/select/many/listbox/
---

# [Oracle ADF] CommandMenuItem


<br/>

    <af:selectManyListbox value="#{bindings.My_List.inputValue}"
                          label="#{bindings.My_List.label}" id="sml1" autoSubmit="true"
                          simple="true" size="7"
                          valueChangeListener="#{MyBean.onSelected}"
                          contentStyle="width:310px;">
        <f:selectItems value="#{bindings.My_List.items}" id="si7"/>
        <f:validator binding="#{bindings.My_List.validator}"/>
    </af:selectManyListbox>


<br/>

My_List - type list

<br/>

    public void onSelected (ValueChangeEvent valueChangeEvent) {

        ViewObject vo = VOUtils.getViewObjectByName("MY-VO-NAME");

        if (null != valueChangeEvent.getNewValue()){

            Object[] selectedListElements = (Object[])valueChangeEvent.getNewValue();

            ViewCriteria vc = vo.createViewCriteria();
            vc.setName("ANY-UNIQUE-VC-NAME");

            for (int si=0; si<selectedListElements.length; si++){
                ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
                vcr1.setAttribute("My-Attribute-From-VO","= " + selectedListElements[si]);
                vc.add(vcr1);
            }

            vo.applyViewCriteria(vc, true);

        } else {
            // do something if checkboxes is not set
            VOUtils.clearViewCriteriaByName(vo, "ANY-UNIQUE-VC-NAME");

        }

        vo.executeQuery();
    }
