---
layout: page
title: Oracle ADF - SelectManyShuttle
permalink: /dev/view/uic/select/many/shuttle/
---

# [Oracle ADF] SelectManyShuttle



<br/>

Будем делать долго по мере необходимости разобраться в этом компоненте:


<br/>

    <af:selectManyShuttle

        ****

        <f:selectItems value="#{pageFlowScope.MyBean.getAllItems}" id="si3"/>

    </af:selectManyShuttle>


<br/>


<br/>

    private List myItems;


<br/>

    public List getAllItems() {
        myItems = ADFUtils.selectItemsForIterator("MyIteratorIterator", "MyAttr1", "MyAttr2", "MyAttr3");
        return myItems;
    }



<br/>
<br/>
<br/>


    value="#{pageFlowScope.MyBean.selectedValues}"



<br/>
<br/>


    private List<Long> selectedValues;

    public void setSelectedValues(List<Long> selectedValues) {
        this.selectedValues = selectedValues;
    }

    public List<Long> getSelectedValues() {
        if (selectedValues == null) {
            selectedValues = new ArrayList<>();
            for (Row row : ADFUtils.findIterator("MY_OTHER_ITERATOR").getAllRowsInRange()) {
                Long sup = (Long) row.getAttribute("FilterValue");
                selectedValues.add(sup);
            }
        }
        return selectedValues;
    }
