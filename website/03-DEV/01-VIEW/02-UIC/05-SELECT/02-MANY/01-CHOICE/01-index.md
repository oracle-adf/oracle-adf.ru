---
layout: page
title: SelectManyChoice  - получить выбранные select'ы
permalink: /dev/view/uic/select/many/choice/
---

# [Oracle ADF] SelectManyChoice  - получить выбранные select'ы


<br/>

{% highlight xml linenos %}

<af:selectManyChoice
       value="#{bindings.MyView.inputValue}"
       label="#{bindings.MyView.label}" id="smc1" autoSubmit="true"
       valueChangeListener="#{pageFlowScope.MyBean.onSomethingChanged}">
  <f:selectItems value="#{bindings.MyView.items}" id="si3"/>
</af:selectManyChoice>

{% endhighlight %}

<br/>

Понадобилось удалить строку с фильтром.
Пока не удалил, компонент пытался преобразовать строку в integer, хотя в аттрибутах было установлено, что это строка.

<br/>

{% highlight java linenos %}

public void onSomethingChanged(ValueChangeEvent valueChangeEvent) {

    Object[] selectedListElements = (Object[])valueChangeEvent.getNewValue();

    for (int si=0; si<selectedListElements.length; si++){

        System.out.println("selected " + selectedListElements[si] );
   }
}
{% endhighlight %}
