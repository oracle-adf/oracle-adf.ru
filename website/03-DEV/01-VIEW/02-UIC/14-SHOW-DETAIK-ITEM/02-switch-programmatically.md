---
layout: page
title: Переключить ShowDetailItem программно
permalink: /dev/view/uic/RichShowDetailItem/switch-programmatically/
---

# Переключить ShowDetailItem программно

<br/>

Смысл в том, чтобы найти 2 RichShowDetailItem. Один, который текущий, для него установить setDisclosed в false, а у нужно в true.

    UIComponent uic1 = UIUtils.findComponent("CONPONENT1_ABSOLUTE_ID");
    UIComponent uic2 = fuse_ui_shell_pg.findComponent("CONPONENT2_ABSOLUTE_ID");

    RichShowDetailItem rsdi1 = (RichShowDetailItem) uic1;
    RichShowDetailItem rsdi2 = (RichShowDetailItem) uic2;

    rsdi1.setDisclosed(false);
    rsdi2.setDisclosed(true);

    AdfFacesContext.getCurrentInstance().addPartialTarget(rsdi1);
    AdfFacesContext.getCurrentInstance().addPartialTarget(rsdi2);
