---
layout: page
title: ShowDetailItem
permalink: /dev/view/uic/RichShowDetailItem/
---


# [Oracle ADF] ShowDetailItem


    <af:showDetailItem text="showDetailItem 2" id="sdi1" icon="/images/tab/combo.png"
                       stretchChildren="first"
                       binding="#{MyBeanBean.showDetailItem1}"
                       disclosureListener="#{MyBeanBean.disclosure_tab1}">

                        ****

        </dvt:areaChart>
    </af:showDetailItem>


<br/>


    private RichShowDetailItem showDetailItem1;


        public void setShowDetailItem1(RichShowDetailItem showDetailItem1) {
            this.showDetailItem1 = showDetailItem1;
        }

        public RichShowDetailItem getShowDetailItem1() {
            return showDetailItem1;
        }

<br/>

Нужно для каждого showDetailItem в panelTabbed создавать отдельный listener.

    public void onShowDetailItemSelected(DisclosureEvent disclosureEvent) {

            System.out.println("disclosureEvent " + disclosureEvent.toString());

            if (disclosureEvent.isExpanded() == true) {
               System.out.println("isExpanded isExpanded isExpanded isExpanded");
        }
    }
