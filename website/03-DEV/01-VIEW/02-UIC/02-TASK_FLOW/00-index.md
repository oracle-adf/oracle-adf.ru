---
layout: page
title: Oracle ADF TaskFlow
description: Oracle ADF TaskFlow
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/taskflow/
---

# [Oracle ADF] TaskFlow

TaskFlow бывают Bounded и Unbounded.

В общем я всегда создавал Bounded TF. Указывал в них default вьюху и если нужно передавал параметры. + настраивал security.

<br/>

### Oracle Adf Bounded and Unbounded Taskflow and difference between Bounded and Unbounded Taskflow.

Bounded TF have single entry point and can have zero or more exit points. Whereas Unbounded TF has one or more entry point for the application (adfc-config.xml).

Bounded TF must have a default activity (Which is the entry point) whereas Unbounded TF cannot have default activity.

Bounded TF can be parameterized where as UnBounded TF cannot be parameterized.

We can create multiple Unbounded TF but during runtime they all become into one unbounded TF, thus resulting into inefficiency whereas it’s not true in the case of Bounded TF.

Bounded TF is re-usable whereas UnBounded TF is not.

We can implement Undo feature with Bounded TF( because it supports Transaction) but UnBounded TF doesn’t.

In Nutshell, We have to have one Unbounded TF and many Bounded TF to take advantage of many features such as re-usability, train stop, data sharing between task flows , transaction, etc.

http://mavendeveloper.com/2011/09/what-is-the-difference-between-bounded-and-unbounded-taskflows/

Или:

Разница: https://www.youtube.com/watch?v=97AOUPJ0bvU

<br/>

### Примеры работы с TaskFlow:

<ul>

    <li><a href="/dev/view/uic/taskflow/activate-taskflow-popup/">Активация TaskFlow в Popup</a></li>
    <li><a href="/dev/view/uic/taskflow/activete-taskflow/">Активация TaskFlow только при необходимости (И таким же способом пассивация)</a></li>

    <li><a href="/dev/view/uic/taskflow/finalize-taskflow-popup/">Finalize TaskFlow</a></li>

    <li><a href="/dev/view/uic/taskflow/get-current-page-name/">Узнать на какой странице находимся</a></li>

    <li><a href="/dev/view/uic/taskflow/initialize-taskflow/">Инициализация TaskFlow при старте</a></li>

    <li><a href="/dev/view/uic/taskflow/get-tasks-flow-id/">Получить Task Flow ID программно</a> (не нашел практического применения)</li>

    <li><a href="https://www.youtube.com/watch?v=XiPDlr0tMps" rel="nofollow">Calling view activity programatically in a Bounded Task Flow</a> (YouTube) </li>

    <li><a href="http://juddi-oracle.blogspot.ru/2011/06/passing-parameters-from-adf-task-flow.html" rel="nofollow">Passing Parameters from ADF Task Flow with Fragments into ADF Bounded Task Flow</a></li>

    <li><a href="https://www.youtube.com/watch?v=kNOI3q7lSw4" rel="nofollow">Parent Action Activity in ADF Task Flow</a> (YouTube) </li>

</ul>

<br/>

### Save Points для TaskFlow для сохранения их состояния:

<ul>
    <li><a href="https://docs.oracle.com/cd/E17904_01/web.1111/b31974/taskflows_complex.htm#ADFFD22035">Save Points для TaskFlow для сохранения их состояния</a></li>
</ul>

<br/>

### Еще какие-то примеры:

<ul>

    <li><a href="http://www.ateam-oracle.com/improving-adf-page-rendering-time/" rel="nofollow">Improving ADF Page Rendering Time : Progressively Loading Taskflows</a></li>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/ADF_TaskFlow_StepByStep.pdf" rel="nofollow">Демонстрация работы с Task Flow</a></li>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/ADF_TaskFlowWithPopup_StepByStep.pdf" rel="nofollow">Использование Popup в Task Flow (Step By Step)</a></li>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/ADF_TaskFlow_Appendix_StepByStep.pdf" rel="nofollow">Еще немного о Task Flow (Step By Step)</a></li>

</ul>

<br/><br/>

Оказывается и так можно (только я не нашел практического применения):

<br/>

{% highlight java linenos %}
TaskFlowId taskFlow = new TaskFlowId(
"WEB-INF/departments-task-flow-definition.xml",
"departments-task-flow-definition");
{% endhighlight %}

<br/>

    https://bitbucket.org/oracle-adf/write-and-read-values-from-pageflowscope-of-a-region-bounded

<br/>

    https://bitbucket.org/oracle-adf/get-the-pageflowscope-of-a-region-bounded-task-flow

<br/>
<br/>

Чего-то не нашел как выпонить программно ReturnActivity.

Подозреваю, что как-то:

<br/>

{% highlight java linenos %}
ControllerContext ccontext = ControllerContext.getInstance();
String viewId = "taskFlowReturn1";
ccontext.getCurrentViewPort().setViewId(viewId);
{% endhighlight %}

<br/>
<br/>

Буду признателен за подсказку

<br/>
<br/>

**Интересные примеры:**

http://naive-amseth.blogspot.ru/2010/12/testcase-programmatically-navigating-in.html  
https://community.oracle.com/thread/1129942?tstart=0

Create taskFlow and region binding at run-time , show n numbers of regions using multiTaskFlow- Oracle ADF  
http://www.awasthiashish.com/2014/08/create-taskflow-and-region-binding-at.html

Set and Get Value from Taskflow parameter (pageFlowScope variable) from Managed Bean- ADF  
http://www.awasthiashish.com/2013/05/set-value-in-taskflow-parameter-from.html
