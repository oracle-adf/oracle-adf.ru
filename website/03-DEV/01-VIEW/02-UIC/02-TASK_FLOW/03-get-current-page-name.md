---
layout: page
title: Oracle ADF Samples
description: Oracle ADF Samples
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/taskflow/get-current-page-name/
---

# [Oracle ADF] Oracle ADF Узнать на какой странице находимся

Я использовал такой способ, чтобы найти на нужно странице "Train" нужную мне скрытую кнопку, которую нужно программно нажать.

<br/>

{% highlight java linenos %}

---

ControllerContext controllerContext = ControllerContext.getInstance();
ViewPortContext currentViewPortCtx = controllerContext.getCurrentViewPort();

TaskFlowContext taskFlowCtx = currentViewPortCtx.getTaskFlowContext();
TaskFlowTrainModel taskFlowTrainModel = taskFlowCtx.getTaskFlowTrainModel();

TaskFlowTrainStopModel currentStop = taskFlowTrainModel.getCurrentStop();

System.out.println("To String " + currentStop.getLocalActivityId());

if (currentStop.getLocalActivityId().equals("myPage1")) {
some logic
}

---

{% endhighlight %}
