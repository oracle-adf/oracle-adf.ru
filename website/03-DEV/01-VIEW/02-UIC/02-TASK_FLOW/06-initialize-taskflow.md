---
layout: page
title: Oracle ADF Инициализация TaskFlow при старте
description: Oracle ADF Инициализация TaskFlow при старте
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/taskflow/initialize-taskflow/
---

<br/>

# [Oracle ADF] Инициализация TaskFlow при старте

<br/>

Найдите в свойствах TaskFlow

    Initializer: #{pageFlowScope.MyBean.initTaskFlow}

Здесь мы в PageFlowScpoe записываем какие-то данные:

<br/>

    public class MyBean {


        public void initTaskFlow() {
            toDo1();
        }

        private void toDo1() {


            // any logic

        }

    } // The End of Class;
