---
layout: page
title: Oracle ADF > Finalize TaskFlow
description: Oracle ADF > Finalize TaskFlow
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/taskflow/finalize-taskflow-popup/
---

# [Oracle ADF] Finalize TaskFlow (в конкретном случае имеем Popup)

Смысл. Если пользователь не завершит работу с taskflow, а на странице имеются poll которые периодически вызываются, то ненужные никому вычисления так и будут происходить.

Заходим в taskflow.

Перетаскиваем на него значек "Task Flow Return"

От jsf страниц к нему стрелки ("Control Flow Case").

И названия для них, например "passivate".

<br/>

Далее на странице делаю скрытую кнопку.

{% highlight xml linenos %}
<af:button text="PASSIVATE_TASKFLOW" id="b_passivate" action="passivate" visible="false"
binding="#{pageFlowScope.MyBean.passivationBtn1}"/>
{% endhighlight %}

По ее нажатии будет выполняться действие описанное в taskflow как action="passivate".

<br/>

Остается нажать эту кнопку, при нужном событии:

ADF-UTILS:

https://bitbucket.org/oracle-adf/adf-utils/src/0ac62ba404606a29305ed4af7461d6aa9ce59095/src/main/java/org/javadev/adf/utils/UIUtils.java?at=master&fileviewer=file-view-default

Например:

{% highlight java linenos %}

---

UIUtils.runButtonByJS(this.passivationBtn1);
ADFPopupUtils.closePopup(actionEvent.getComponent());

---

{% endhighlight %}

<br/>

Если при следующем обращении к taskflow будет белый экран.

<br/><br/>

{% highlight xml linenos %}
<af:popup childCreation="deferred" autoCancel="disabled" id="p3"
popupFetchListener="#{MyPageBean.onPopupOpen}">
<af:panelWindow id="pw2" closeIconVisible="false" modal="true" styleClass="fuse-popup"
contentHeight="748" contentWidth="1256" stretchChildren="first">
<af:region value="#{bindings.myTaskFlow.regionModel}" id="r2"
binding="#{MyPageBean.myRegion}"/>
</af:panelWindow>
</af:popup>
{% endhighlight %}

<br/><br/>

{% highlight java linenos %}

private RichRegion myRegion;

---

Геттер и Сеттер для myRegion

---

{% endhighlight %}

<br/><br/>

{% highlight java linenos %}

public void onPopupOpen(PopupFetchEvent popupFetchEvent) {

    if (null != myRegion){
        myRegion.refresh(FacesContext.getCurrentInstance());
    }

}

{% endhighlight %}
