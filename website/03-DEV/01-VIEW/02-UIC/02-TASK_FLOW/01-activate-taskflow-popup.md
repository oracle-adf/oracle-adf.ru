---
layout: page
title: Oracle ADF > Активация TaskFlow в Popup
description: Oracle ADF > Активация TaskFlow в Popup
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/taskflow/activate-taskflow-popup/
---

# [Oracle ADF] Активация TaskFlow в Popup

<br/>

Имеем на странице регион.
Он находится в Popup

Нужно, чтобы данные загружались только при обращении.

    <af:link text="Общие настройки" id="cl3" partialSubmit="true">
        <af:showPopupBehavior popupId="::myPopupID"/>
    </af:link>

<br/><br/>

    <af:popup id="myPopupID" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source"
              popupFetchListener="#{MyBean.onPopup}">
        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" contentHeight="680"
                        contentWidth="1004" stretchChildren="first" styleClass="fuse-popup">
            <af:region value="#{bindings.myTaskFlow.regionModel}" id="r5"
                       binding="#{MyBean.myRegion}"/>
        </af:panelWindow>
    </af:popup>

<br/><br/>

MyBean прописан в adfc-config.xml как requiest

<br/><br/>

    public class MyBean {

        private RichRegion myRegion;

        public void setmyRegion(RichRegion myRegion) {
            this.myRegion = myRegion;
        }

        public RichRegion getmyRegion() {
            return myRegion;
        }


        public void onPopup(PopupFetchEvent popupFetchEvent) {


            ScopeUtils.setVarToPageFlowScope("isActive", true);

            myRegion.refresh(FacesContext.getCurrentInstance());
        }
    }

<br/>
<br/>

<br/>
<br/>

Заходим на страницу my-task-flox.xml

<br/>

**Input Parameter Definitions:**

<br/>

    Name: isActive
    Class: java.lang.Boolean
    Value: #{pageFlowScope.isActive}

<br/>

**В Executables страницы находим наше TaskFlow**

<br/>

    activation: condition
    active: #{pageFlowScope.isActive == null ? false: pageFlowScope.isActive}

<br/>

**PageParameters:**

<br/>

    From Value: isActive
    To Value: #{pageFlowScope.isActive}

<!--
<br/>

### Тоже самое но не заработало! Нужно будет попробовать потом.




**Не тестировалось! Оказалось этот вариант не подходим в данный момент и нет времени разбираться с этим кейсом.**

<br/>
<br/>

    <af:link text="Настройки" id="cl3">
        <af:setPropertyListener from="#{true}" to="#{pageFlowScope.isActive}" type="action"/>
        <af:showPopupBehavior popupId="::myPopup"/>
    </af:link>


<br/>
<br/>



    <af:popup id="myPopup" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source">
        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" contentHeight="680"
                        contentWidth="1004" stretchChildren="first" styleClass="fuse-popup">
            <af:region value="#{bindings.myTaskFlow.regionModel}" id="r5"/>
        </af:panelWindow>
    </af:popup>


<br/>
<br/>


Заходим на страницу my-task-flox.xml

<br/>

Input Parameter Definitions:

    Name: isActive
    Class: java.lang.Boolean
    Value: #{pageFlowScope.isActive}


<br/>

PageParameters:

    From Value: isActive
    To Value: #{pageFlowScope.isActive}


<br/>
<br/>


Преходим на bindings этой страницы


<br/>
<br/>


В Executables находим наше TaskFlow

    activation: condition
    active: #{pageFlowScope.isActive} -->
