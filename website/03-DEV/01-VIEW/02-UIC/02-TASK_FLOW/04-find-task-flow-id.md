---
layout: page
title: Oracle ADF Получить Task Flow программно
description: Oracle ADF Получить Task Flow программно
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/taskflow/get-tasks-flow-id/
---

# [Oracle ADF] Получить Task Flow ID программно

Располагаю на странице кнопку, по клику на которую получаю Taask Flow ID.

<br/>

    import oracle.adf.controller.ControllerContext;
    import oracle.adf.controller.TaskFlowContext;
    import oracle.adf.controller.TaskFlowId;
    import oracle.adf.controller.ViewPortContext;

    ****

    public static TaskFlowId getTaskFlowId() {
        ControllerContext controllerContext = ControllerContext.getInstance();
        ViewPortContext currentViewPort = controllerContext.getCurrentViewPort();
        TaskFlowContext taskFlowContext = currentViewPort.getTaskFlowContext();
        TaskFlowId taskFlowId = taskFlowContext.getTaskFlowId();
        return taskFlowId;
    }

    public String onTest() {

        TaskFlowId tfId = getTaskFlowId();
        System.out.println("[Locat Task Flow Id] " + tfId.getLocalTaskFlowId());
        System.out.println("[Fully Qualified Task Flow Name] " + tfId.getFullyQualifiedName());

        return null;
    }
