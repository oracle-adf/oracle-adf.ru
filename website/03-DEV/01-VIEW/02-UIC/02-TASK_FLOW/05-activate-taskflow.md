---
layout: page
title: Oracle ADF Активация TaskFlow только при необходимости
description: Oracle ADF Активация TaskFlow только при необходимости
keywords: Oracle ADF, view, uic
permalink: /dev/view/uic/taskflow/activete-taskflow/
---

# [Oracle ADF] Активация TaskFlow только при необходимости

В общем можно увеличить скорость работы приложения, если не открывать сразу все taskflow.

Для этого в свойствах taskflow (на странице \*\*\*PageDef.xml):

    activation: conditional
    active: #{SessionStateBean.activateDashboard}

<br/>

    public class SessionStateBean {

        private boolean activateDashboard = false;

        ****


        public void delay(DisclosureEvent disclosureEvent) {
            RichShowDetailItem sdi = (RichShowDetailItem) disclosureEvent.getSource();
            objectNavigatorItem = sdi.getText();
            if (disclosureEvent.isExpanded()) {
                switch (sdi.getId()) {
                case "dashboard-nav":
                    activateDashboard = true;
                    break;
                default:
                    break;
                }
            }
        }
    }

<br/>

Вариант выполнение логики в случае, если taskflow активировано.

    public class SessionStateBean {

        SessionStateBean ssb = (SessionStateBean) JSFUtils.getManagedBeanValue("SessionStateBean");

        if (ssb.isActivateDashboard()) {
            ****
        }
    }
