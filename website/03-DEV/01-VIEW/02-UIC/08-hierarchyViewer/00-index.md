---
layout: page
title: Hierarchy Viewers
permalink: /dev/view/uic/hierarchyViewer/
---

Пример:

Hierarchy_Viewer_With_Popup  
https://bitbucket.org/oracle-adf/ADF-App-Collection/tree/master/Hierarchy_Viewer_With_Popup


<strong>Implmenting Search in the Hierarchy Viewer</strong>

<br/><br/>

<div align="center">

<iframe width="640" height="480" src="https://www.youtube.com/embed/cbycgV1SOdE" frameborder="0" allowfullscreen></iframe>

</div>

<br/><br/>

<br/><br/>

<strong>Какие-то наработки:</strong>

<ul>
    <li><a href="/dev/view/uic/hierarchyViewer-popup/">Popup в Hierarchy (можно сделать приблизительно таким образом)</a></li>
    <li><a href="/dev/view/uic/hierarchyViewer-get-selected-values-in-bean/">Hierarchy получить id выбранного элемента в бине</a></li>
</ul>




<br/><br/>

<strong>Ссылки, оставшиеся когда пытался разобраться:</strong>

30 Using Hierarchy Viewer Components<br/>
https://docs.oracle.com/middleware/1221/adf/develop-faces/GUID-6CE8A319-C101-4DFF-A260-3272ACB8FE14.htm#ADFUI9951

<br/><br/>

43 Creating Databound Hierarchy Viewer, Treemap, and Sunburst Components
https://docs.oracle.com/middleware/1221/adf/develop/GUID-E1B8BEF9-7A1E-4DFF-A2BA-01BA63FBFFA8.htm#ADFFD23559

<br/><br/>


12.6.4 What You May Need to Know About Programmatically Expanding and Collapsing Nodes  
https://docs.oracle.com/middleware/1221/adf/develop-faces/GUID-0E90B043-0304-4337-9857-EA2632C328E5.htm#ADFUI9960

29.7.2 Configuring a Hierarchy Viewer to Invoke a Popup Window  
https://docs.oracle.com/middleware/1212/adf/ADFUI/dv_hviewer.htm#ADFUI10090

http://docs.oracle.com/cd/E16162_01/web.1112/e16182/graphs_charts.htm#ADFFD19594

https://docs.oracle.com/cd/E15523_01/web.1111/b31973/dv_hviewer.htm#ADFUI11503

http://www.youtube.com/watch?v=mg7nbyOYGm8

http://rajvenugopalpost.blogspot.ru/2013/12/build-adf-hierarchy-viewer-component.html
