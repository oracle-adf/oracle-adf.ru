---
layout: page
title: Popup в Hierarchy (можно сделать приблизительно таким образом)
permalink: /dev/view/uic/hierarchyViewer-popup/
---

# Popup в Hierarchy (можно сделать приблизительно таким образом)

    <af:link id="l3111" text="#{node.AttributeName}">
       <af:showPopupBehavior popupId="::p3" triggerType="action"/>
    </af:link>

<br/>

    <af:popup id="p3" contentDelivery="lazyUncached" eventContext="launcher"
              launcherVar="source">
      <af:setPropertyListener from="#{source.currentRowData.AttributeName}"
                              to="#{pageFlowScope.p_page_outcome_parameter}" type="popupFetch"/>
      <af:panelWindow id="pw11" closeIconVisible="false" modal="true">
            <af:region value="#{bindings.myRegion.regionModel}" id="r11">
                <?audit suppress oracle.adfdt.controller.adfc.source.audit.DuplicateRegion?>
            </af:region>
        </af:panelWindow>
    </af:popup>
