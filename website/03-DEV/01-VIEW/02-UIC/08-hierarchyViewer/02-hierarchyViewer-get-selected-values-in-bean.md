---
layout: page
title: Hierarchy получить id выбранного элемента в бине
permalink: /dev/view/uic/hierarchyViewer-get-selected-values-in-bean/
---

# Hierarchy получить id выбранного элемента в бине

```xml
<dvt:hierarchyViewer id="hv1" var="node" value="#{bindings.SupsParent_Her1.treeModel}"

    ****

            <af:link id="link_hierarchi_01"
                    action="#{pageFlowScope.myClass.myMethod(node.AttributeName)}">
                <af:outputText value="#{node.AttributeName}"truncateAt="30"/>
            </af:link>
```

<br/>

    private transient RichPopup myPopup;

<br/>

    public String myMethod(Object attribute) {
      System.out.println("attribute = " + attribute);


      // В данном конкретном случае логика

      AdfFacesContext.getCurrentInstance().getPageFlowScope().put("p_supplier", p_sups);
      RichPopup.PopupHints hints = new RichPopup.PopupHints();
      hints.add(RichPopup.PopupHints.HintTypes.HINT_LAUNCH_ID, this.myPopup);
      this.p2.show(hints);
      return null;
    }
