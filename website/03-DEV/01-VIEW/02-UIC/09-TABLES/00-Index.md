---
layout: page
title: Oracle ADF  Table, Tree, or Tree Table
permalink: /dev/view/uic/tables/
---

# [Oracle ADF] Table, Tree, or Tree Table

<br/>

**С таблицами, как ни странно, почти не работаю!**


<br/>

### Table

<li>
	<a href="/dev/view/uic/tables/table/">Table</a>
</li>


<br/>

### Tree

<li>
	<a href="/dev/view/uic/tables/tree/">Tree</a>
</li>


<br/>

### Tree Table

<li>
	<a href="/dev/view/uic/tables/treetable/">treeTable</a>
</li>




<br/>
<br/>


**Passing a Row as a Value**
**Exporting Data from Table, Tree, or Tree Table**
https://docs.oracle.com/cd/E24382_01/web.1112/e16181/af_table.htm#ADFUI9962
