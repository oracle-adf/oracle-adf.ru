---
layout: page
title: Oracle ADF Table
permalink: /dev/view/uic/tables/table/
---

# [Oracle ADF] Table


<br/>

### Работа с таблицами:

<li>
	<a href="/dev/view/uic/tables/table/get-selected-row/">Получить выбранную строку в бине</a>
</li>



<br/>

### Фильтрация в таблице:

<li>
	<a href="/dev/view/uic/tables/table/filters/">Фильтрация в таблице</a>
</li>


<br/>

### Создание AF:Query:

1) Создать VC для VO.
2) В панели со списком DataContols выбрать VC и перетащить на страницу как Query.

<br/>

Скрыть из Дополнительно ненужные аттрибуты: <br/>
https://tompeez.wordpress.com/2012/01/06/jdev-afquery-hide-some-attributes-from-query-panel-but-show-them-in-the-result-table/

<br/>

### Какие-то еще примеры:

<li>

	<a href="/dev/view/uic/tables/table/get-selected-id/">Получить в бине выбранный идентификатор</a>

	<a href="/dev/view/uic/tables/table/work-with-table/">Нужно пройтись по таблице программно</a>

</li>


<br/>

### Другие примеры:


Растянуть таблицу. (точнее 1 поле будет заполнять все оставшиеся пространство)  
https://blogs.oracle.com/TangHao/entry/how_to_use_af_table


Get the current selected row of af:table  
http://oracleadfamitt.blogspot.ru/2014/05/get-current-selected-row-of-aftable.html


HOW TO GET A SELECTED ROW OR ROWS IN AN ADF TABLE – OUR EXAMPLE  

**App:**  
https://bitbucket.org/oracle-adf/adf-table-row-selection-sample-application

**Docs:**  
http://www.johnbrunswick.com/2011/08/adftable-get-selected-row-or-rows/


<br/>

Get selected row (single/multiple) from POJO based table in ADF:  
http://www.awasthiashish.com/2015/07/get-selected-row-singlemultiple-from.html


Populate af:table programmatically from managead bean using POJO  
http://www.awasthiashish.com/2014/11/populate-aftable-programmatically-from.html


Populate Dynamic table and form using af:dynamicComponent and dynamic viewObject - Oracle ADF  
http://www.awasthiashish.com/2014/11/populate-dynamic-table-and-form-using.html


Apply Filter on af:table column programmatically ,Invoke 'FilterableQueryDescriptor' through managed bean  
http://www.awasthiashish.com/2013/11/apply-filter-on-aftable-column.html

Expand and Collapse an af:treeTable programmatically in ADF Faces (Oracle ADF)
http://www.awasthiashish.com/2013/10/expand-and-collapse-aftreetable.html


Programmatically populate values in a af:selectOneChoice component in ADF  
http://www.awasthiashish.com/2013/08/programmatically-populate-values-in.html


Invoke ADF Table Selection Listener, custom selection listener for af:table
http://www.awasthiashish.com/2013/01/invoke-adf-table-selection-listener.html


Expand and Collapse an af:treeTable programmatically in ADF Faces (Oracle ADF)  
http://www.awasthiashish.com/2013/10/expand-and-collapse-aftreetable.html


ADF Training | Performing Multiple Row Selection with Oracle ADF Tables  
https://www.youtube.com/watch?v=DQFSB4SDBT0
