---
layout: page
title: Oracle ADF > Нужно пройтись по таблице программно
permalink: /dev/view/uic/tables/table/work-with-table/
---

# [Oracle ADF] > Нужно пройтись по таблице программно

И в цикле проставить что-то полезное


<br/>

{% highlight java linenos %}


public String allAprove() {
    RowKeySet rksSelectedRows = this.t1.getSelectedRowKeys();
    Key key = (Key) ((List) rksSelectedRows.iterator().next()).get(0);

    MyVoViewImpl myVo =
            (MyVoViewImpl) ADFUtils.findIterator("MyIterator").getViewObject();

    RowSetIterator rs = myVo.createRowSetIterator(null);

    MyVoViewRowImpl currRow = myVo.getCurrentRow();

    rs.reset();

    String valAppr = currRow.getApproved();

    oracle.jbo.domain.Date myDate = currRow.getNotBeforeDate();

    while (rs.hasNext()) {
        MyVoViewRowImpl row = (MyVoViewRowImpl) rs.next();

        if (currRow.getQtyOrdered().compareTo(BigDecimal.ZERO) > 0){
            row.setApproved(valAppr);
        }

        row.setNotBeforeDate(myDate);
    }

    AdfFacesContext.getCurrentInstance().addPartialTarget(t1);
    return null;
}

{% endhighlight %}


<br/>

<strong>В Имплементации добавлен метод</strong>

<br/>


{% highlight java linenos %}

@Override
public MyVoViewRowImpl getCurrentRow() {
    return (MyVoViewRowImpl) super.getCurrentRow();
}

{% endhighlight %}
