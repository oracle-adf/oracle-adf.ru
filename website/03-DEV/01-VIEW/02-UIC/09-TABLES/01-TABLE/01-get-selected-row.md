---
layout: page
title: Oracle ADF Получить выбранную строку в бине
permalink: /dev/view/uic/tables/table/get-selected-row/
---

# [Oracle ADF] > Получить выбранную строку в бине

<br/>

### Вариант 1

Итератор находится на нужной строке!


{% highlight java linenos %}

MYViewObjRowImpl myRow =
    (MYViewObjRowImpl) ADFUtils.findIterator("MyIterator").getCurrentRow();


-- Просто для примера, что может быть
Long myId = myRow.getID();

{% endhighlight %}




<br/>

### Вариант 2

{% highlight xml linenos %}

<af:table value="#{bindings.MyIterator.collectionModel}" var="row"
          rows="#{bindings.MyIterator.rangeSize}"
          scrollPolicy="scroll"
          rowBandingInterval="0" fetchSize="#{bindings.MyIterator.rangeSize}" id="t2"
          columnStretching="multiple" contentDelivery="immediate">

***

                       <af:gridRow height="auto" id="gr9">
                           <af:gridCell marginStart="5px" width="100%" marginEnd="5px" id="gc10" halign="center">
                               <af:link id="l1" actionListener="#{pageFlowScope.orderSetCurRow.goToOrderDetail}"
                                        text="#{row.OrderNo}">
                                   <f:attribute name="row" value="#{row}"/>
                               </af:link>
                           </af:gridCell>
                       </af:gridRow>

***

{% endhighlight %}


<br/>


{% highlight java linenos %}

public void goToOrderDetail(ActionEvent actionEvent) {

    System.out.println();
    System.out.println("WE CLICKED ON LINK");
    System.out.println();

    FacesCtrlHierNodeBinding object = (FacesCtrlHierNodeBinding)actionEvent.getComponent().getAttributes().get("row");

    java.sql.Date writtenDate = new java.sql.Date(((Timestamp)object.getAttribute("WrittenDate")).getTime());

}

{% endhighlight %}

<br/>

### Вариант 3

{% highlight xml linenos %}

<af:table

***
value="#{bindings.SUPPLIERS.collectionModel}"
selectedRowKeys="#{bindings.SUPPLIERS.collectionModel.selectedRow}"
selectionListener="#{backingBeanScope.MyBean.onTableSuppSelect}"

***

{% endhighlight %}


<br/>

SUPPLIERS - tree


<br/>

Можно считать аттрибуты определенные в таблице. Если аттрибуты нужны а в таблице их нет, следует добавить их и сделать скрытыми.

<br/>


{% highlight java linenos %}

public void onTableSuppSelect(SelectionEvent selectionEvent) {

    ExpressionUtils.invokeEL("#{ bindings.SUPPLIERS.collectionModel.makeCurrent}",
                             new Class[] { SelectionEvent.class }, new Object[] { selectionEvent });

    FacesCtrlHierNodeBinding selectedRow = (FacesCtrlHierNodeBinding) suppTable.getSelectedRowData();

    Long supplier = (Long) selectedRow.getAttribute("Supplier");
}

{% endhighlight %}
