---
layout: page
title: Oracle ADF > Получить в бине выбранный идентификатор
permalink: /dev/view/uic/tables/table/get-selected-id/
---

# [Oracle ADF] > Получить в бине выбранный идентификатор


Есть таблица.   
Нужно получить в бине выбранный идентификатор:

<br/>

**UPD: Наверное лучше сразу передавать row, а дальше в бине уже делать все что захочется!**

<br/>

{% highlight java linenos %}

<af:table  var="row"

    *****

<af:column>
    <af:link id="l1" text="Редактировать" action="#{MyBean.myMethod(row.Login)}" />
</af:column>

{% endhighlight %}


<br/>

{% highlight java linenos %}

@SuppressWarnings("unchecked")
public String myMethod(Object login) {

    System.out.println("");
    System.out.println("login " + login);
    System.out.println("");

    return null;
}

{% endhighlight %}


<br/>

### Еще пример:



{% highlight java linenos %}

private RichTable t1;

public void setT1(RichTable t1) {
    this.t1 = t1;
}

public RichTable getT1() {
    return t1;
}

{% endhighlight %}




<br/>



{% highlight java linenos %}

if (null != this.t1.getSelectedRowKeys()) {

    RowKeySet selectedEmps = this.t1.getSelectedRowKeys();
    Iterator selectedEmpIter = selectedEmps.iterator();

    while (selectedEmpIter.hasNext()) {
        Key key = (Key) ((List) selectedEmpIter.next()).get(0);
        Row row =
            ADFUtils.findIterator("MY_ITERATOR_NAME").findRowByKeyString(key.toStringFormat(true));

        Date today = (Date) ADFContext.getCurrent().getSessionScope().get("mydate");
        row.setAttribute("Date", today);
    }

    this.commit();

    ADFUtils.findIterator("MY_ITERATOR_NAME").executeQuery();
}

{% endhighlight %}





<br/>


{% highlight java linenos %}

private void commit() {
    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    OperationBinding operationBinding = bindings.getOperationBinding("Commit");
    operationBinding.execute();
}

{% endhighlight %}


<br/>

### Еще пример:


{% highlight java linenos %}

public void performDelete(ActionEvent action)
{
  UIXTable table = getTable();
  Iterator selection = table.getSelectedRowKeys().iterator();
  Object oldKey = table.getRowKey();
  while(selection.hasNext())
  {
    Object rowKey = selection.next();
    table.setRowKey(rowKey);
    MyRowImpl row = (MyRowImpl) table.getRowData();
    //custom method exposed on an implementation of Row interface.
    row.markForDeletion();
  }
  // restore the old key:
  table.setRowKey(oldKey);
}

// Binding methods for access to the table.
public void setTable(UIXTable table) { _table = table; }
public UIXTable getTable() { return _table; }
private UIXTable _table;

{% endhighlight %}




https://docs.oracle.com/cd/E24382_01/web.1112/e16181/af_table.htm#ADFUI216
