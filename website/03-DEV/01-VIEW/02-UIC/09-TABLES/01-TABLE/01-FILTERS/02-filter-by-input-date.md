---
layout: page
title: Oracle ADF > Фильтрация в таблице по inputDate
permalink: /dev/view/uic/tables/table/filters/filter-by-input-date/
---

# [Oracle ADF] > Фильтрация в таблице по inputDate


<br/>

UPD: VC должна быть создана декларативно а не создавться где-то произвольно в коде. Иначе потом некоторым программистам может быть сложно понять, почему VO выполняется именно так. В коде нужно получать это VC и уже в него подставлять данные.

<br/>

{% highlight xml linenos %}

<af:table

    ***

    <af:column

        ****

            <af:inputDate value="#{bindings.myBindings.DateCreation.value}"
                          id="id11" valueChangeListener="#{pageFlowScope.MyBean.onFilterByDateChanged}"
                  autoSubmit="true" placeholder="#{commonBundle['common.date']}">
                <af:convertDateTime pattern="#{bindings.Tasks_List.hints.DateCreation.format}"/>
            </af:inputDate>
        </f:facet>
    </af:column>

{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

    public void onFilterByDateChanged(ValueChangeEvent valueChangeEvent) {

        ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_SUPER_VO);

        if (valueChangeEvent.getNewValue() != null){

            RichInputDate rid = (RichInputDate)valueChangeEvent.getComponent();

            java.sql.Date inputDate = ADFDatesUtils.RichInputDateToJavaSQLDate(rid);

            ViewCriteria vc = vo.createViewCriteria();
            vc.setName(CONSTANTS_VC.MY__DATE__VC);

            ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
            vcr1.setAttribute("DateCreation","= " + inputDate);
            vc.add(vcr1);

            vo.applyViewCriteria(vc, true);

        } else {
            VOUtils.clearViewCriteriaByName(vo, CONSTANTS_VC.MY__DATE__VC);
        }

        vo.executeQuery();
    }

{% endhighlight %}
