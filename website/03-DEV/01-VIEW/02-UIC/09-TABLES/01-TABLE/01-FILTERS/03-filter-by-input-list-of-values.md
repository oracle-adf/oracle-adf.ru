---
layout: page
title: Oracle ADF > Фильтрация в таблице с помощью inputListOfValues
permalink: /dev/view/uic/tables/table/filters/filter-by-input-list-of-values/
---

# [Oracle ADF] > Фильтрация в таблице с помощью inputListOfValues


{% highlight xml linenos %}

<f:facet name="filter">
    <af:inputListOfValues id="storesLOV" visible="true"
                          autoSubmit="true" usage="search"
                          autoComplete="off" simple="true"
                          valueChangeListener="#{pageFlowScope.MyBean.onResultsStoreSelect}">
        <af:autoSuggestBehavior minChars="3"
                                suggestedItems="#{pageFlowScope.MyBean.onSuggestStores}"/>
    </af:inputListOfValues>
</f:facet>

{% endhighlight %}


<br/><br/>

{% highlight java linenos %}


public void onResultsStoreSelect(ValueChangeEvent valueChangeEvent) {

    ***

    if (locationId.equals(CONSTANTS_VARIABLES.LOCATION_NOT_FOUND)) {
        VOUtils.clearViewCriteriaByName(vo, CONSTANTS_VC.MY_SUPER_PUPER_VC);
    } else {
        ViewCriteria vc = vo.createViewCriteria();
        vc.setName(CONSTANTS_VC.MY_SUPER_PUPER_VC);

        ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
        vcr1.setAttribute("Store", "= " + locationId);
        vc.add(vcr1);

        vo.applyViewCriteria(vc, true);
    }

    vo.executeQuery();

    UIUtils.updateUIComponent(myTable);
}

{% endhighlight %}
