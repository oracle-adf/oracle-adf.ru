---
layout: page
title: Oracle ADF > Фильтрация в таблице по SelectOneChoice
permalink: /dev/view/uic/tables/table/filters/filter-by-soc/
---

# [Oracle ADF] > Фильтрация в таблице по SelectOneChoice


<br/>

UPD: VC должна быть создана декларативно а не создавться где-то произвольно в коде. Иначе потом некоторым программистам может быть сложно понять, почему VO выполняется именно так. В коде нужно получать это VC и уже в него подставлять данные.

<br/>

### Вариант 1 (Так делал я)

{% highlight xml linenos %}

<af:table

    ***

    <af:column ***
               filterable="true">

               ***

<f:facet name="filter">
    <af:selectOneChoice id="soc5"  simple="true"
                        label="#{commonBundle['common.priority']}" autoSubmit="true"
                        valueChangeListener="#{pageFlowScope.MyBean.onPriorityFilter}">
            <af:selectItem label="#{commonBundle['common.notSelectedLevel']}" value="-1" id="si3"/>
            <af:selectItem label="#{commonBundle['common.high']}" value="1" id="si4"/>
            <af:selectItem label="#{commonBundle['common.middle']}" value="2" id="si5"/>
            <af:selectItem label="#{commonBundle['common.low']}" value="3" id="si6"/>
    </af:selectOneChoice>
</f:facet>   

</af:column>

{% endhighlight %}


<br/>

{% highlight java linenos %}

public void onPriorityFilter(ValueChangeEvent valueChangeEvent) {

    String selectedPriority = valueChangeEvent.getNewValue().toString();

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_VO_NAME);

    // Если вариант -1 (из константы)
    if (selectedPriority.equals(CONSTANTS_VARIABLES.PRIORITY_NOT_SELECTED)){
        VOUtils.clearViewCriteriaByName(vo, CONSTANTS_VC.MY_SUPER_VC_FOR_THIS_CASE);
    } else {

        ViewCriteria vc = vo.createViewCriteria();
        vc.setName(CONSTANTS_VC.MY_SUPER_VC_FOR_THIS_CASE);

        ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
        vcr1.setAttribute("Priority","= " + selectedPriority);
        vc.add(vcr1);

        vo.applyViewCriteria(vc, true);
    }
    vo.executeQuery();
}

{% endhighlight %}

<br/>

### Вариант 2 (Так делал коллега)

Итак.

Есть колонка

    <af:column filterable="true"

<br/>


Нужно, чтобы в заголовке пользователь мог выбрать один из вариантов фильтра в SelectOneChoice.


Я такие вещи раньше всегда делал с помощью VC, но тут нашел код не такой как обычно делаю я:

<br/>


{% highlight xml linenos %}

***
<f:facet name="filter">
    <af:panelGroupLayout id="pgl13">
        <af:selectOneChoice
                            label="#{bindings.MyValuesDescView1.label}"
                            shortDesc="#{bindings.MyValuesDescView1.hints.tooltip}"
                            id="soc5"
                            rendered="#{bindings.ValueDesc.inputValue != null}"
                            autoSubmit="true"                                                                            
                            value="#{pageFlowScope.MyBean.valueDescFilter}">
            <f:selectItem value="A" id="si11" itemLabel="A"
                          itemValue="A"/>
            <f:selectItem value="B" id="si10" itemLabel="B"
                          itemValue="B"/>
            <f:selectItem value="C" id="si5" itemLabel="C"
                          itemValue="C"/>
        </af:selectOneChoice>
        <af:inputText  id="it5"
                      rendered="#{bindings.ValueDesc.inputValue == null and bindings.Value.inputValue != null}"
                      value="#{vs.filterCriteria.Value}"/>
    </af:panelGroupLayout>
</f:facet>
</af:column>


{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

// А этот, если я правильно понял, нужен, чтобы установить нужное значение в заголовке таблицы. Т.е. нужное значение для SOC

public String getValueDescFilter(){
    RichTable tbl = (RichTable)JSFUtils.resolveExpression("#{pageFlowScope.MyBean.myTable}");
    FilterableQueryDescriptor filterQD = (FilterableQueryDescriptor)tbl.getFilterModel();
    ConjunctionCriterion cc = filterQD.getFilterConjunctionCriterion();
    List<Criterion> lc = cc.getCriterionList();
    for (Criterion c : lc){
        if (c instanceof AttributeCriterion){
            AttributeCriterion ac = (AttributeCriterion) c;
            if ((ac.getAttribute().getName().equalsIgnoreCase("ValueDesc"))){
                System.out.println("GET FILTER VALUE " + ac.getValue());
                if(ac.getValue().equals(1))  return "A";
                if(ac.getValue().equals(2))  return "B";
                if(ac.getValue().equals(3))  return "C";
            }
        }
    }
    return null;
}


// При выборе элемента SOC вызывается этот метод
public void setValueDescFilter(String filterValue){

    RichTable tbl = (RichTable)JSFUtils.resolveExpression("#{pageFlowScope.MyBean.myTable}");
    FilterableQueryDescriptor filterQD = (FilterableQueryDescriptor)tbl.getFilterModel();
    ConjunctionCriterion cc = filterQD.getFilterConjunctionCriterion();
    List<Criterion> lc = cc.getCriterionList();
    for (Criterion c : lc){
        if (c instanceof AttributeCriterion){
            AttributeCriterion ac = (AttributeCriterion) c;
            if ((ac.getAttribute().getName().equalsIgnoreCase("ValueDesc"))){
                System.out.println("SET FILTER VALUE " + filterValue);
                ac.setValue(filterValue);
            }
        }
    }
}


{% endhighlight %}
