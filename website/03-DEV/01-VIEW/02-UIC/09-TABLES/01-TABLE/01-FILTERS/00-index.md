---
layout: page
title: Oracle ADF Table > Фильтрация в таблице
permalink: /dev/view/uic/tables/table/filters/
---

# [Oracle ADF] Table > Фильтрация в таблице

<br/>

<li>
    <a href="/dev/view/uic/tables/table/filters/filter-by-soc/">Фильтрация в таблице по SelectOneChoice</a>
    <a href="/dev/view/uic/tables/table/filters/filter-by-input-date/">Фильтрация в таблице по inputDate</a>
    <a href="/dev/view/uic/tables/table/filters/filter-by-input-list-of-values/">Фильтрация в таблице с помощью inputListOfValues</a>
</li>
