---
layout: page
title: Oracle ADF Tree
permalink: /dev/view/uic/tables/tree/
---

# [Oracle ADF] Tree


ADF Training | How to Create Oracle ADF Trees and Set Record Currency
https://www.youtube.com/watch?v=ON5AGc24DSc


<br/><br/>

<ul>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/TreeParamsAndCriteriaInADF.pdf" rel="nofollow">Иерархическое дерево в ADF-приложении, его параметры и критерии</a></li>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/TreeModification.pdf" rel="nofollow">Делаем дерево более симпатичным</a></li>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/PseudoTreeInADF.pdf" rel="nofollow">Делаем свое Псевдо Дерево</a></li>
</ul>
