---
layout: page
title: Oracle ADF Подписать график по вертикали и горизонтали
permalink: /dev/view/uic/calendar/add-description/
---

# Подписать график по вертикали и горизонтали:

```xml
<dvt:areaChart

	***

	<dvt:chartXAxis id="cxa1" title="Шкала X"/>
	<dvt:chartYAxis id="cya1" title="Шкала Y"/>

	***

</dvt:areaChart>
```
