---
layout: page
title: Calendar
permalink: /dev/view/uic/calendar/
---


# Calendar

Пока разбираюсь.
Ранее не работал.

Лучше всего для начала посмотреть вот это приложение.

https://bitbucket.org/oracle-adf/adf-calendar-step-by-step-implementation

При обновлении до 12 версии, какая-то ерунда во viewCriteria.

Мне чтобы заработало на моем приложении, пришлось тоже самое сделать.
Т.е. создать bind переменные во viewobject без viewobject.

Кто знает как лучше, подскажите.  

Мне нужен был только функционал, чтобы показывал активити (вроде так называется) и все. Никаких попапов, никаких добавлений и всего этого.

<br/>

### Получить дату по элементу на который кликнули.

     calendarListener="#{CalendarBean.calendarListener}"

 <br/>

    public void calendarListener(CalendarEvent calendarEvent) {
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("============ CalendarEvent ================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");

        System.out.println(calendarEvent.getTriggerDate().clone());

    }



<br/>

### Чтобы руками создать activities.

If your application does not use the Fusion technology stack, then you create your own implementation of the CalendarModel class and the associated CalendarActivity and CalendarProvider classes. The classes are abstract classes with abstract methods. You must provide the functionality behind the methods, suitable for your implementation of the calendar. For more information, see Section 15.2, "Creating the Calendar."

The calendar component displays activities based on those activities and the provider returned by the CalendarModel class. By default, the calendar component is read-only. That is, it can display only those activities that are returned. You can add functionality within supported facets of the calendar so that users can edit, create, and delete activities. When certain events are invoked, popup components placed in these corresponding facets are opened, which can allow the user to act on activities or the calendar.


https://docs.oracle.com/cd/E17904_01/web.1111/b31973/af_calendar.htm#ADFUI10119

Мой вопрос на comminity:
https://community.oracle.com/thread/3879874?sr=inbox


ЗДЕСЬ ЧТО-ТО ЕСТЬ:
https://community.oracle.com/thread/2552936?start=0&tstart=0

Нужно начинать читать, наверное вот с этого.  
https://docs.oracle.com/middleware/1213/adf/tag-reference-faces/tagdoc/af_calendar.html

http://www.nodalpoint.com/adf-calendar-implementation/

http://www.oracle.com/technetwork/developer-tools/jdev/calendar-091799.html

http://docs.oracle.com/cd/E12839_01/web.1111/b31973/af_calendar.htm#ADFUI10105


Google Календарь в ADF приложении:  
https://technology.amis.nl/2008/01/01/accessing-google-calendar-from-oracle-adf-application-displaying-events-in-gantt-chart/
