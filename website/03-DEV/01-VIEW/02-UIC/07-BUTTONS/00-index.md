---
layout: page
title: Обработка нажатия кнопки в бине (Button и RichButton)
permalink: /dev/view/uic/RichButton/
---

<h1>[Oracle ADF] Обработка нажатия кнопки в бине (Button и RichButton)</h1>


RichCommandButton (Deprecated)


    <af:commandButton text="добавить" id="cb4" actionListener="#{MainBean.myMethod}"/>

<br/>


В MainBean

    public void myMethod(ActionEvent actionEvent) {


        // Получить эту кнопку в бине

        RichButton rcb =  (RichButton) actionEvent.getSource();


        if(!(rcb instanceof RichButton)){
            System.out.println("IT IS NOT RichButton");
            // RunTime Exception при желании
            return;
        }

        System.out.println("RichButton");
        System.out.println(rcb.getClientId());

        
        // DO SOMETHING
        run();

    }
