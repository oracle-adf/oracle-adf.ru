---
layout: page
title: Oracle ADF - Panels
description: Oracle ADF - Panels
keywords: Oracle ADF, view, uic, Panels
permalink: /dev/view/uic/panels/
---

# Panels

<ul>
    <li><a href="/dev/view/uic/panels/panel-springboard/">Springboard</a></li>
	<li><a href="/dev/view/uic/panels/panel-box//">PanelBox</a></li>
	<li><a href="/dev/view/uic/panels/panel-group-layout/">PanelGroupLayout</a></li>
	<li><a href="/dev/view/uic/panels/panel-splitter/">PanelSplitter</a></li>
</ul>
