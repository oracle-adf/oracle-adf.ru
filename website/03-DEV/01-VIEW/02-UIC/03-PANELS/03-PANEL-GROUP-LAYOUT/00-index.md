---
layout: page
title: Oracle ADF - PanelGroupLayout
description: Oracle ADF - PanelGroupLayout
keywords: Oracle ADF, view, uic, PanelGroupLayout
permalink: /dev/view/uic/panels/panel-group-layout/
---

# [Oracle ADF] PanelGroupLayout и RichPanelGroupLayout

    RichPanelGroupLayout pgl = (RichPanelGroupLayout)l_page.findComponent("pgl90" );
    pgl.setVisible(true);
    AdfFacesContext.getCurrentInstance().addPartialTarget(pgl);
