---
layout: page
title: Oracle ADF - PanelSplitter
description: Oracle ADF - PanelSplitter
keywords: Oracle ADF, view, uic, PanelSplitter
permalink: /dev/view/uic/panels/panel-splitter/
---

# PanelSplitter

    <?xml version='1.0' encoding='UTF-8'?>
    <jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.1"
              xmlns:f="http://java.sun.com/jsf/core"
              xmlns:h="http://java.sun.com/jsf/html"
              xmlns:af="http://xmlns.oracle.com/adf/faces/rich">
      <jsp:directive.page contentType="text/html;charset=UTF-8"/>
      <f:view>
        <af:document id="d1">
          <af:resource type="javascript">
            function onPanelSplitterClick(event) {
                component = event.getSource();
                AdfCustomEvent.queue(component, "PanelSplitterServerEvent", null, true);
                event.cancel();
            }
          </af:resource>
          <af:form id="f1">
            <af:panelSplitter id="ps1" binding="#{PanelSplitterBean.panelSplitter}">
              <f:facet name="first">
                <af:outputText value="Facet1" id="ot1"/>
              </f:facet>
              <f:facet name="second">
                <af:outputText value="Facet2" id="ot2"/>
              </f:facet>
              <af:clientListener type="propertyChange" method="onPanelSplitterClick"/>
              <af:serverListener type="PanelSplitterServerEvent" method="#{PanelSplitterBean.panelSplitterServerEventListener}"/>
              </af:panelSplitter>
          </af:form>
        </af:document>
      </f:view>
    </jsp:root>

<br/>

    package com.samples;

    import oracle.adf.view.rich.component.rich.layout.RichPanelSplitter;
    import oracle.adf.view.rich.render.ClientEvent;

    public class PanelSplitterBean {
        private RichPanelSplitter panelSplitter;

        public PanelSplitterBean() {
        }

        public void setPanelSplitter(RichPanelSplitter panelSplitter) {
            this.panelSplitter = panelSplitter;
        }

        public RichPanelSplitter getPanelSplitter() {
            return panelSplitter;
        }

        public void panelSplitterServerEventListener(ClientEvent clientEvent) {
            if (panelSplitter.isCollapsed()) {
                // Do necessary logic for collapsed
                System.out.println(":::: Collapased ::");
            } else {
                // Do necessary logic for Expanded
                System.out.println(":::: Expanded ::");
            }
        }
    }

https://community.oracle.com/thread/2393730

Programmatically expanding and collapsing an af:panelSplitter
http://www.ateam-oracle.com/programmatically-expanding-and-collapsing-an-afpanelsplitter/
