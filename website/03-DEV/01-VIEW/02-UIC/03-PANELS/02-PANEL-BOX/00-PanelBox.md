---
layout: page
title: Oracle ADF - PanelBox
description: Oracle ADF - PanelBox
keywords: Oracle ADF, view, uic, PanelBox
permalink: /dev/view/uic/panels/panel-box/
---

# [Oracle ADF] PanelBox

    RichPanelBox pbAdmin = (RichPanelBox)Utils.getComponent("tabDataContract").findComponent("pbAdminBox4");

    if (Utils.getSession().getAttribute("uid").equals("ADMIN")) {
        pbAdmin.setVisible(true);
    } else {
        pbAdmin.setVisible(false);
    }

    AdfFacesContext.getCurrentInstance().addPartialTarget(pbAdmin);
