---
layout: page
title: Oracle ADF - Springboard
description: Oracle ADF - Springboard
keywords: Oracle ADF, view, uic, Springboard
permalink: /dev/view/uic/panels/panel-springboard/
---

# [Oracle ADF] - Springboard

### Распечатать ноды springboard

{% highlight xml linenos %}

<af:panelSpringboard id="object-navigator"
binding="#{SessionStateBean.springboard_component}"

         ***

{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

private transient RichPanelSpringboard springboard_component;

for (UIComponent child : this.springboard_component.getChildren()) {

    RichShowDetailItem detailItem = (RichShowDetailItem) child;

    System.out.println(" id " + detailItem.getId() + " text " +  detailItem.getText() + " isDisclosed() " + detailItem.isDisclosed());

}

{% endhighlight %}
