---
layout: page
title: Обновить страницу программно
permalink: /dev/view/uic/update/refresh-page-programmatically/
---

# Обновить страницу программно (Copy Paste)

Мое приложение все в фрагментах и как следствие данный способ не подходит.
Пока оставлю примеры кода, может когда и понадобится.


### Example 1

    protected void refreshFragmentProgarmmatically() {

        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();

        ViewHandler vh = context.getApplication().getViewHandler();
        UIViewRoot page = vh.createView(context, viewId);

        context.setViewRoot(page);

    }


<br/>


### Example 2


    protected void refreshFragmentProgarmmatically() {

        FacesContext fctx = FacesContext.getCurrentInstance();
        String refreshpage = fctx.getViewRoot().getViewId();
        ViewHandler ViewH = fctx.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fctx, refreshpage);
        UIV.setViewId(refreshpage);
        fctx.setViewRoot(UIV);

    }
