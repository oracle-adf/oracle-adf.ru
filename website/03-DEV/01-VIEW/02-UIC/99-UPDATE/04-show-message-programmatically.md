---
layout: page
title: Вывести программно сообщение на экран
permalink: /dev/view/uic/update/show-message-programmatically/
---

# Вывести программно сообщение на экран

    import javax.faces.application.FacesMessage;
    import javax.faces.context.FacesContext;

    ***

    FacesMessage msg = new FacesMessage(" = My Message =");
    msg.setSeverity(FacesMessage.SEVERITY_INFO);
    FacesContext.getCurrentInstance().addMessage(null, msg);
