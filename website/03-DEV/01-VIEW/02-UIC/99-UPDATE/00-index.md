---
layout: page
title: Oracle ADF Обновление компонентов на форме
permalink: /dev/view/uic/update/
---

# [Oracle ADF] Обновление компонентов на форме


Самый простой вариант - использовать:


    partialTriggers="componentID"


<br/>

Если по каким-то причинам он не подходит, можно посмотреть следующие способы. Написанные способы расположены от более полезных, к менее.


<br/>


<ul>
    <li><a href="/dev/view/bindings/contextual-events/on-vo-update/">Contextual Events > Обновление компонентов на форме при выполнении ViewObject</a></li>

    <li><a href="/dev/view/uic/update/update-component-programmatically/">Найти в бине компонент с формы и принудительно обновить его</a></li>

    <li><a href="/dev/view/uic/update/refresh-page-programmatically/">Обновить страницу программно</a></li>

    <li><a href="/dev/view/uic/update/show-message-programmatically/">Вывести программно сообщение на экран</a></li>
</ul>
