---
layout: page
title: Обработка нажатия кнопки в бине
permalink: /dev/view/uic/commandImageLink/
---

<h1>[Oracle ADF] Обработка нажатия кнопки в бине</h1>


В данном случае имеем commandImageLink


    <af:commandImageLink id="cil2" inlineStyle="font-size:100%;"
                         icon="/images/naviBarFaces/fuse-icon-calendar-next-ena.png"
                         partialSubmit="true" visible="true"
                         actionListener="#{MainBean.search_by_id_button}"/>

<br/>


В MainBean

    public void search_by_id_button(ActionEvent actionEvent) {
        searchByID();
    }
