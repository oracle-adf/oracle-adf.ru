---
layout: page
title: Popup
permalink: /dev/view/uic/popup/popup-with-panelWindow/
---

# [Oracle ADF] Popup with panelWindow

<br/>

    <af:link id="l15" action="show_item_detail"
             text="#{item.bindings.ItemDesc.inputValue}"
             styleClass="minor-link" clientComponent="true">
        <af:clientAttribute name="prm_item"
                            value="#{item.bindings.Item.inputValue}"/>
        <af:showPopupBehavior popupId="::p1"
                              triggerType="action"/>
    </af:link>


<br/>

    <af:popup id="p1" contentDelivery="lazyUncached" launcherVar="source"  eventContext="launcher"
              popupFetchListener="#{Update.deptPopUpFetchListener}">
        <af:setPropertyListener from="#{source.attributes.prm_item}" to="#{pageFlowScope.p_item}"
                                type="popupFetch"/>
        <af:panelWindow id="pw1" modal="true" styleClass="fuse-popup" closeIconVisible="false">

            <af:region value="#{bindings.itemdetailtaskflow1.regionModel}" id="r1"/>
        </af:panelWindow>
    </af:popup>

<br/>


    public void deptPopUpFetchListener(PopupFetchEvent popupFetchEvent) {

        System.out.println("======================");
        System.out.println("POPUP POPUP POPUP");
        System.out.println("======================");

    }



http://docs.oracle.com/cd/E23549_01/apirefs.1111/e12419/tagdoc/af_popup.html


<br/>

# Продолжаем копать popup


    <af:popup id="p2" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source"
              popupFetchListener="#{Update.deptPopUpFetchListener1}"
              attributeChangeListener="#{Update.deptPopUpFetchListener2}"
              popupCanceledListener="#{Update.deptPopUpFetchListener3}">


        <af:setPropertyListener from="#{source.attributes.p_income_parameter}" to="#{pageFlowScope.p_income_parameter}"
                                type="popupFetch"/>  

        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" styleClass="fuse-popup">
            <af:region value="#{bindings.supplierdetailtaskflow2.regionModel}" id="r1"/>
        </af:panelWindow>
    </af:popup>

<br/>

Могуть быть listeners:

* popupFetchListener
* attributeChangeListener
* popupCanceledListener


<br/>

Мне нужно передать данные из #{source.attributes.p_income_parameter} в бин. Хз пока как это сделать и вообще как это лучше сделать.

<br/>

**Копание привело к следующим выводам:**

Значение переменной myVar устанавливается уже после того как вызывается deptPopUpFetchListener.

Поэтому она имеет либо null значение, либо выбранное ранее. Как добиться того, чтобы передавалось текущее я не нашел. Вроде попробовал все имеющиеся варианты из предоставленных вариантов. Код приведен ниже. Буду признателен за помощь. Стандартный компонент, наверное нужно делать каким-то другим способом.


    <af:popup id="p2" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source"
              popupFetchListener="#{Update.deptPopUpFetchListener}"
              binding="#{Update.richpopup}">

        <af:setPropertyListener from="#{source.attributes.p_income_parameter}" to="#{Update.myVar}"
                                type="popupFetch"/>  

        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" styleClass="fuse-popup">
            <af:region value="#{bindings.supplierdetailtaskflow2.regionModel}" id="r1"/>
        </af:panelWindow>

        <af:outputText value="#{source.attributes.p_income_parameter}" id="ot8"/>

    </af:popup>

<br/>

    private String myVar;


    public void setMyVar(String myVar) {
        this.myVar = myVar;
    }

    public String getMyVar() {
        return myVar;
    }

<br/>

public void deptPopUpFetchListener(PopupFetchEvent popupFetchEvent) {

    System.out.println("======================");
    System.out.println("POPUP");
    System.out.println("======================");

    System.out.println("========== myVAR ============");
    System.out.println(myVar);
    System.out.println("========== END ============");  
}


<br/><br/>

Наткнулся при чтении в Fusion Developer Guide, нужно будет потом проверить!

<br/><br/>

<img src="http://storage2.static.itmages.ru/i/15/1214/h_1450078667_9498581_b4be7e1fdc.png" alt="Fusion Developer Guide" border="0" />

<br/><br/>

<img src="http://storage3.static.itmages.ru/i/15/1214/h_1450078711_9848240_01d609dc8e.png" alt="Fusion Developer Guide" border="0" />



<br/>

### UPD

Возможное решение  
https://oracle-adf.ru/dev/view/uic/get-selected-element-programmatically/
