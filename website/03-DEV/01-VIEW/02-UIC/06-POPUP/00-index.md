---
layout: page
title: Oracle ADF Popup
permalink: /dev/view/uic/popup/
---


# [Oracle ADF] Popup


В popup лучше сразу вставлять Region, если конечно не требуется, вывести что-то похожее на сообщение.

<ul>
	<li><a href="/dev/view/uic/popup/show-popup-programmatically/">Вызов Popup программно</a></li>
	<li><a href="/dev/view/uic/popup/close-popup-programmatically/">Закрыть Popup программно</a></li>
	<li><a href="/dev/view/uic/popup/popup-with-dialog/">Popup with dialog</a></li>
	<li><a href="/dev/view/uic/popup/popup-with-panelWindow/">Popup with panelWindow</a></li>
	<li><a href="/dev/view/uic/popup/popup-input-parameters/">Передача параметров в Popup</a></li>
	<li><a href="/dev/view/uic/popup/popup-by-javascript/">Вызов Popup с помощью JavaScript</a></li>
</ul>


<br/>

Потерялся. Долго не мог найти, метод который вызывается при закрытии popup. Все на самом деле очень просто. Этот метод:

	popupCanceledListener="#{pageFlowScope.MyBean.closePopup}"


<br/>

Refreshing parent table on Closing Pop Up using returnListener : ADF  (С примером приложения)
http://rohanwalia.blogspot.ru/2013/09/adf-refreshing-parent-table-on-closing.html
