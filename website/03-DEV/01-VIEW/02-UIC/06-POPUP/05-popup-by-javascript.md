---
layout: page
title: Вызов Popup с помощью JavaScript
permalink: /dev/view/uic/popup/popup-by-javascript/
---

# [Oracle ADF] Вызов Popup с помощью JavaScript

Я вообще-то стал ADF заниматься, чтобы не программировать на JavaScript.

Есть panelGridLayout.
Нужно, чтобы по клику по нему вызывался Popup.

ХЗ правильно/неправильно, но я сделал так.

Сделал af:link с Popup.  
af:link - внутри panelGridLayout.

<br/>

    <af:link id="popup_link" text="#{item.bindings.SupName.inputValue}">
       <af:clientAttribute name="p_sups" value="#{item.bindings.Supplier.inputValue}"/>
       <af:showPopupBehavior popupId="::p2" triggerType="action"/>
    </af:link>


<br/>

Сразу после определения panelGridLayout добавил:

    <af:clientListener method="popupByMouseclick" type="click"/>


<br/>

И собственно JavaScript, который по клике на panelGridLayout вызывает действие аналогичное нажатию на link.


    <af:resource type="javascript">

       function popupByMouseclick(mouseEvent){

         var comp = mouseEvent.getSource();
         var button = comp.findComponent('popup_link');

         AdfActionEvent.queue(button, button.getPartialSubmit());
         evt.cancel();
       }
     </af:resource>


<br/>

Сам Popup выглядит следующим образом:


    <af:popup id="p2" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source">
        <af:setPropertyListener from="#{pageFlowScope.DisplayType == 'her' ? source.currentRowData.Supplier : source.attributes.p_sups}"
                                to="#{pageFlowScope.p_supplier}" type="popupFetch"/>
        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" styleClass="fuse-popup">
            <af:region value="#{bindings.supplierdetailtaskflow2.regionModel}" id="r1"/>
        </af:panelWindow>
    </af:popup>
