---
layout: page
title: Вызов Popup программно
permalink: /dev/view/uic/popup/show-popup-programmatically/
---


# [Oracle ADF] Вызов Popup программно


Есть Popup.


    <af:popup childCreation="deferred" autoCancel="disabled" id="p2"
              binding="#{UsersBean.removeUserPopup}">
        ***
    </af:popup>


Нужно по нажатию на кнопку, в бине вызвать этот popup.



    <af:button

        ***
               actionListener="#{UsersBean.onRemoveUserButtonClicked}"
               immediate="true">
    </af:button>


<br/>


    public void onRemoveUserButtonClicked(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        hints.add(RichPopup.PopupHints.HintTypes.HINT_LAUNCH_ID, this.removeUserPopup);
        this.removeUserPopup.show(hints);
    }
