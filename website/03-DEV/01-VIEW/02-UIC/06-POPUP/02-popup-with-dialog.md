---
layout: page
title: Popup with dialog
permalink: /dev/view/uic/popup/popup-with-dialog/
---


# [Oracle ADF] Popup with dialog

<br/>

### Обработка нажатия кнопок в dialog

Handling the OK and CANCEL buttons in af:dialog  
https://www.youtube.com/watch?v=kYhDs-2mApc


    <af:popup childCreation="deferred" autoCancel="disabled" id="p14" binding="#{MyBeanBean.timePeriodDialog}">
          <af:dialog id="d7" title="handling Ok and cancel event}" type="okCancel" dialogListener="#{MyBeanBean.dialogListener1}">
                  <af:inputDate label="От" id="id5"/>
                  <af:inputDate label="По" id="id6"/>
              </af:dialog>
      </af:popup>


<br/>

    public void dialogListener1(DialogEvent dialogEvent)
      {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
        {
         // write your custom code for ok event
        } else
        {
          // write your custom code for cancel event
       }
      }
