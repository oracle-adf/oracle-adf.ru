---
layout: page
title: Oracle ADF Закрыть Popup программно
permalink: /dev/view/uic/popup/close-popup-programmatically/
---


# [Oracle ADF] Закрыть Popup программно


    @Override
    public void close(ActionEvent actionEvent) {    
        PopupUtils.hideParentPopup(actionEvent.getComponent());
    }


<br/>


**PopupUtils.hideParentPopup**  
https://bitbucket.org/oracle-adf/adf-utils/blob/master/src/main/java/org/javadev/adf/utils/PopupUtils.java
