---
layout: page
title: Передача параметров в Popup
permalink: /dev/view/uic/popup/popup-input-parameters/
---

# [Oracle ADF] Передача параметров в Popup


Имеется NavigationPane

    <af:navigationPane hint="bar" styleClass="RIMToggleBar" id="np1">
        <af:commandNavigationItem id="cni1"
                                  selected="#{pageFlowScope.DisplayType == 'list' or pageFlowScope.DisplayType == null}"
                                  icon="#{pageFlowScope.DisplayType == 'list' or pageFlowScope.DisplayType == null ? '/images/menu/png/func_list-view_16_act.png' : '/images/menu/png/func_list-view_16_ena.png'}">
            <af:setPropertyListener type="action" from="list"
                                    to="#{pageFlowScope.DisplayType}"/>
        </af:commandNavigationItem>
        <af:commandNavigationItem id="cni2"
                                  selected="#{pageFlowScope.DisplayType == 'grid'}"
                                  icon="#{pageFlowScope.DisplayType == 'grid' ? '/images/menu/png/func_matrix_16_act.png' : '/images/menu/png/func_matrix_16_ena.png'}">
            <af:setPropertyListener from="grid" to="#{pageFlowScope.DisplayType}"
                                    type="action"/>
        </af:commandNavigationItem>
        <af:commandNavigationItem id="cni3"
                                  selected="#{pageFlowScope.DisplayType == 'hierarchy'}"
                                  icon="#{pageFlowScope.DisplayType == 'hierarchy' ? '/images/menu/png/func_worldglobe_16_act.png' : '/images/menu/png/func_worldglobe_16_ena.png'}">
            <af:setPropertyListener from="her" to="#{pageFlowScope.DisplayType}"
                                    type="action"/>
        </af:commandNavigationItem>
    </af:navigationPane>

<br/>

На разных страницах должен вызываться POPUP.
В одном случае из таблицы.

    ***
    <af:gridRow marginTop="5px" height="auto" marginBottom="5px" id="gr9">
        <af:gridCell marginStart="5px" width="auto" marginEnd="5px" id="gc8">
            <af:link id="l3" text="#{row.SupName}">
                <af:clientAttribute name="p_income_parameter" value="#{row.Supplier}"/>
                <af:showPopupBehavior popupId="::p2" triggerType="action"/>
            </af:link>
        </af:gridCell>
    </af:gridRow>
    ***

<br/>

В другом из Hierarchy View

    ***
    <af:link id="l3111" text="#{node.Supplier} #{node.SupName}">
        <af:showPopupBehavior popupId="::p2" triggerType="action"/>
    </af:link>
    ***

<br/>

2 Popup одновременно работать отказались. Вызывался только 1-й прописанный в коде. Может это и баг фреймворка, не знаю.<br/>

Оказалось, что можно сделать следующим образом:

    <af:popup id="p2" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source">
        <af:setPropertyListener

        from="#{pageFlowScope.DisplayType == 'hierarchy' ? source.currentRowData.Supplier : source.attributes.p_income_parameter}"
        to="#{pageFlowScope.p_page_outcome_parameter}"
                                type="popupFetch"/>
        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" styleClass="fuse-popup">
            <af:region value="#{bindings.supplierdetailtaskflow2.regionModel}" id="r1"/>
        </af:panelWindow>
    </af:popup>


<br/>

### Работающие примеры, если использовать по 1-му на странице.

<br/>

    <af:popup id="p2" contentDelivery="lazyUncached" eventContext="launcher" launcherVar="source">
        <af:setPropertyListener from="#{source.attributes.p_income_parameter}" to="#{pageFlowScope.p_page_outcome_parameter}"
                                type="popupFetch"/>
        <af:panelWindow id="pw1" closeIconVisible="false" modal="true" styleClass="fuse-popup">
            <af:region value="#{bindings.supplierdetailtaskflow2.regionModel}" id="r1"/>
        </af:panelWindow>
    </af:popup>

<br/>

    <af:popup id="p3" contentDelivery="lazyUncached" eventContext="launcher"
              launcherVar="source">
      <af:setPropertyListener from="#{source.currentRowData.Supplier}"
                              to="#{pageFlowScope.p_page_outcome_parameter}" type="popupFetch"/>
      <af:panelWindow id="pw11" closeIconVisible="false" modal="true" styleClass="fuse-popup">
            <af:region value="#{bindings.supplierdetailtaskflow2.regionModel}" id="r11">
                <?audit suppress oracle.adfdt.controller.adfc.source.audit.DuplicateRegion?>
            </af:region>
        </af:panelWindow>
    </af:popup>
