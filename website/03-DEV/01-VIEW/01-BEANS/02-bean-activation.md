---
layout: page
title: Oracle ADF - Активация бина
description: Oracle ADF - Активация бина
keywords: Oracle ADF, view, beans, Активация бина
permalink: /dev/view/beans/bean-activation/
---

# Oracle ADF > Активация бина

Чтобы бин создался раньше, чем должен по умолчанию, возможен следующий способ.

Создаем на форме, которая будет загружена раньше скрытое поле, которое должно получать данные из этого бина.

{% highlight xml linenos %}
<af:activeOutputText value="#{pageFlowScope.MyBean.dummy}" id="dummy" visible="false"/>
{% endhighlight %}

<br/>

{% highlight java linenos %}
public String getDummy(){
return "ABCDEFGHIKLM";
}
{% endhighlight %}
