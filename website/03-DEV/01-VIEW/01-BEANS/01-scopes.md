---
layout: page
title: Oracle ADF - Bean Scopes
description: Oracle ADF - View Layer - Beans
keywords: Oracle ADF, view, beans, scopes
permalink: /dev/view/beans/scopes/
---

# [Oracle ADF] Bean Scopes

<br/>

### Мне нужно просто записать (чтобы постоянно не искать их в коде):

    Application scope: #{applicationScope.myBean.myMethod}
    Session scope: #{sessionScope.myBean.myMethod}
    Page flow scope: #{pageFlowScope.myBean.myMethod}
    View scope: #{viewScope.myBean.myMethod}
    Request scope: #{requestScope.myBean.myMethod}
    Backing bean scope: #{backingBeanScope.myBean.myMethod}

<br/>

Теперь достаточно набрать в браузере на сайте: /dev/view/beans/scopes/ и копипастить!

<br/>

### Смотри также:

https://oracle-adf.com/docs/info/scopes-in-fusion-web-applications/

**Dirty scopes:**  
https://bitbucket.org/oracle-adf/adfc-dirty-scopes
