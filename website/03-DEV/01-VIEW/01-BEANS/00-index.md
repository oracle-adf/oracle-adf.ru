---
layout: page
title: Oracle ADF - View Layer - Beans
description: Oracle ADF - View Layer - Beans
keywords: Oracle ADF, view, beans
permalink: /dev/view/beans/
---

<br/>

# [Oracle ADF] Oracle ADF - View Layer - Beans

<br/>

<ul>
    <li><a href="/dev/view/beans/bean-activation/">Активация бина</a></li>
    <li><a href="/dev/view/beans/scopes/">Scopes</a></li>
</ul>
