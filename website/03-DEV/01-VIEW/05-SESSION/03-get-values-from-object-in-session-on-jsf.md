---
layout: page
title: Oracle ADF > Передать в сессию объект
description: Oracle ADF > Передать в сессию объект
keywords: Oracle ADF, view, session, object
permalink: /dev/view/session/get-values-from-object-in-session-on-jsf/
---

# [Oracle ADF] Получить данные из объекта, хранящегося в сессии данные на фореме jsf:

Д.б сериализуемым!

public class MyAttr implements Serializable {

    @SuppressWarnings("compatibility:4706824693917293616")
    private static final long serialVersionUID = 1L;

    // ----------- Singleton ---------------------

    private static MyAttr MyAttr;

    private MyAttr() {
            init();
        };

    public static MyAttr getInstance() {
        if (MyAttr == null) {
            MyAttr = new MyAttr();
        }
        return MyAttr;
    }

    // ------------------------------------

    private String myLocale;

    public String getmyLocale() {
        return myLocale;
    }

}

<br/>

// Пишу по памяти !!!
// Вроде так д.б

    MyObject myObject = new myObject();
    SessionUtils.writeObjectToSession("MY_ATTR", MyAttr.getInstance());

<br/>

И в jsf можно работать следующим образом:

    #{sessionScope['MY_ATTR'].myLocale}
