---
layout: page
title: Oracle ADF > Передать в сессию объект
description: Oracle ADF > Передать в сессию объект
keywords: Oracle ADF, view, session, object
permalink: /dev/view/session/objects/
---

# [Oracle ADF] Передать в сессию объект:

Записать в сесию объект:

    MyObject myObject = new myObject();
    SessionUtils.writeObjectToSession("myObject", myObject);

Забрать объект из сессии:

    public MyObject getSelectedObjectFromSession(String myObject) throws NullPointerException{
        Object myObject1 = SessionUtils.getObjectFromSession(myObject);
        return (MyObject)myObject1;
    }
