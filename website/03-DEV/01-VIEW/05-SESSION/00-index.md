---
layout: page
title: Oracle ADF - Работа с сессионными параметрами
description: Oracle ADF - Работа с сессионными параметрами
keywords: Oracle ADF, view, session
permalink: /dev/view/session/
---

# [Oracle ADF] Работа с сессионными параметрами:

Задавайте сессионные переменные в верхнем регистре, чтобы их можно было удобнее читать!

<ul>
    <li><a href="/dev/view/session/strings/">Работа с сессионными строковыми параметрами</a></li>
    <li><a href="/dev/view/session/objects/">Передать в сессию объект</a></li>
    <li><a href="/dev/view/session/get-values-from-object-in-session-on-jsf/">Получить данные из объекта, хранящегося в сессии данные на фореме jsf</a></li>

</ul>

<br/>

Еще:

<br/>

<ul>
    <li><a href="https://docs.oracle.com/cd/E13222_01/wls/docs92/webapp/sessions.html#wp150317" rel="nofollow">Setting and Getting Session Name/Value Attributes</a></li>
</ul>
