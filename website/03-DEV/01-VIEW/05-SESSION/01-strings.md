---
layout: page
title: Работа с сессионными строковыми параметрами
description: Работа с сессионными строковыми параметрами
keywords: Oracle ADF, view, session, strings
permalink: /dev/view/session/strings/
---

# Работа с сессионными строковыми параметрами:

<br/>

Сами методы Utils лежат на [butbucket](https://bitbucket.org/oracle-adf/adf-utils/).

// Установить значение параметра в сессию:

    SessionUtils.getSession().setAttribute("parameter1", "1111");
    SessionUtils.getSession().setAttribute("parameter2", "2222");

// Распечатать значение параметра из сессии:

    System.out.println("parameter1 " +  SessionUtils.getSession().getAttribute("parameter1").toString());
    System.out.println("parameter1 " +  SessionUtils.getSession().getAttribute("parameter2").toString());
