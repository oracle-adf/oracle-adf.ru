---
layout: page
title: Oracle ADF Rest WebServices
permalink: /dev/ws/rest/
---

# [Oracle ADF] Rest WebServices:


### Вариант 1: Нужно передавать в REST только 1 значение из базы. (Без всяких передач параметров и т.д.)

1) Создаю VO

2) Добавляю его в AppModule


<br/>

    1. Создать Rest сервис на нужный DataControl.

    AppModule.xml -> Web Service --> REST

    WebServiceName = DataControlName_REST


    В AppModules ()если они не были настроены ранее) настроить парамерты подключения. В моем случае для local.

    В настройках Security разрешить обращение к сервису.

    jazn-data.xml

    Resource Grants:

    Resource type --> ADF REST Reource
    Source Project: Model

    Выбираем наш сервис --> В grand to добавляем роли (Add Application Role) anonymous-role и authenticated-role, чтобы можно было протестировать.

Далее:

    2. Стартовать этот сервис на weblogic
    3. Посмотреть данные:

    http://localhost:7101/MY-RESTWebService/rest/12.2.1.1/WebServiceName?onlyData=true


<br/>

    http://127.0.0.1:7101/MY-RESTWebService/rest/
    http://127.0.0.1:7101/MY-RESTWebService/rest/12.2.1.1/describe
    http://127.0.0.1:7101/MY-RESTWebService/rest/12.2.1.1/describe/WebServiceName?onlyData=true


<br/>

### Изменить количество возвращаемыех REST записей.

На странице настроек REST.
Выделить этот Rest и перейти в executables --> Rest Iterator --> Common --> RangeSize: (чтобы без ограничений или -1)


<br/>


### Вариант 2:  Передача параметров в REST service

    1. Создать ViewCriteria c передаваемыми параметрами.
    2. В RowFinder добавить новый RowFinder
    3. AppModule создать новый DataControl с окончанием на _REST

Вызывать:

    http://XXXXX:7101/RESTWebService/MY_SERVICE_REST?finder=RowFinderByStore;store=33

Результаты вставить в JET.
Ну или куда еще.



<br/>

Перейти в Row Finders --> Напротив Variable Кликнуть "New Expression method".   

В появившееся окно добавить что-нибудь. Например 1.

Перейти в исходники.  Удалить блок

    <TransientExpression
              Name="VariableScript"
              trustMode="untrusted"
              CodeSourceName="****"/>



<br/>

### Ссылки:


<ul>
    <li><a href="https://blogs.oracle.com/imc/entry/publishing_adf_business_components_as">Publishing ADF Business Components as REST/JSON services in Jdeveloper 12.2</a></li>
</ul>


Consuming a REST service from your ADF 12.2.1 application  
https://technology.amis.nl/2016/01/11/consuming-rest-service-in-your-adf-12-2-1-application/


RESTful Web Services (RowFinder etc.)  
https://docs.oracle.com/middleware/1221/adf/develop/GUID-589F3905-5A8D-402D-B2D2-3BEEB2D7DDD4.htm#ADFFD54192
https://docs.oracle.com/middleware/1221/adf/develop/GUID-DFAABA52-5DB4-4F80-835E-A06F947A84DA.htm#ADFFD53872


<br/>
<br/>

REST based CRUD with Oracle ADF Business Components  
https://blogs.oracle.com/shay/entry/rest_based_crud_with_oracle


ADF BC 12c New Feature - Row Finder  
http://andrejusb.blogspot.ru/2013/07/adf-bc-12c-new-feature-row-finder.html
http://andrejusb.blogspot.ru/2013/11/find-by-key-and-view-criteria-row.html

http://practiceadf.blogspot.ru/2013/08/16-oracle-adf-jdeveloper-12c-rowfinder.html
