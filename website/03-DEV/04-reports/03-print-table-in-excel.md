---
layout: page
title: Oracle ADF Печать Отчетов - Печать таблицы в Excel
permalink: /dev/reports/print-table-in-excel/
---

# Oracle ADF Печать Отчетов - Печать таблицы в Excel



{% highlight xml linenos %}

<af:button

                ***
                  binding="#{AnalysisExportToExcel.clientExportButton1}">
              <af:exportCollectionActionListener type="excelHTML"
                                          exportedId="tableID"
                                          filename="MyFile.xls"
                                          title="Tables"/>
       </af:button>

{% endhighlight %}


Осталось создать скрытую таблицу с id tableID
