---
layout: page
title: Oracle ADF Печать Отчетов - Какие-то другие варианты
permalink: /dev/reports/other/
---

# Oracle ADF Печать Отчетов - Какие-то другие варианты

Я печатал печатные формы на ADF для страховой с помощью XSLT-FO чего-то там. Из ADF отправлялся XML, на сервер, который генерировал PDF. Настроено было до меня, поэтому описать пока не могу. Думаю, что-то вроде:


<br/>

ADF-11g-Printing-Directly-From-Your-Application   
https://bitbucket.org/oracle-adf/adf-11g-printing-directly-from-your-application


Creating pdf file using Apache PDFBox API in ADF Faces and opening it in new window -Oracle ADF  
http://www.awasthiashish.com/2013/08/creating-pdf-file-using-apache-pdfbox.html


<br/>
<br/>


<br/>


### JasperReport

Создание отчетов в ADF при помощи JasperReport  
http://www.oracle-adf.info/2014/04/adf-jasperreport.html


**Jasper-Report-In-ADF-Application**  
https://bitbucket.org/oracle-adf/jasper-report-in-adf-application

http://www.gebs.ro/blog/oracle/jasper-reports-in-adf/

http://lucbors.blogspot.ru/2015/09/adf-12c-using-jasper-reports-en.html

http://o7planning.org/web/fe/default/en/document/18660/jasperreport-tutorial-for-beginners

<br/>
<br/>


<br/>

### Apache FOP

Generate PDF report in ADF (Apache FOP, XSL-FO etc)   
http://kohlivikram.blogspot.ru/2009/04/generate-pdf-report-in-adf.html

<br/>

### Apache POI (the Java API for Microsoft Documents) (На community вижу, что активно юзают именно это)


<br/>
<br/>


### BI Publisher

Integrating Oracle BI Publisher Reports into Oracle ADF Applications  
http://www.oracle.com/technetwork/middleware/bi-publisher/oracle-bip-adf-integration-2549495.pdf

<br/>
<br/>

### Oracle Reports (Устаревший)

https://www.youtube.com/watch?v=7n7LVaBc164

<br/>
<br/>

### etc:

Running Reports from an ADF Application  
http://it.toolbox.com/blogs/jjflash-oracle-journal/running-reports-from-an-adf-application-69686


How-to save - "print" - DVT graphs to a file  
http://www.oracle.com/technetwork/developer-tools/adf/learnmore/index-101235.html


Adding PNG image to Pdf document  
http://www.java2s.com/Code/Java/PDF-RTF/AddingPNGimagetoPdfdocument.htm  
http://www.java2s.com/Code/Jar/c/com.lowagie.htm  

<br/>

How to export the specific columns in excel  
http://prabhat-java.blogspot.ru/2016/01/how-to-export-specific-columns-in-excel.html
