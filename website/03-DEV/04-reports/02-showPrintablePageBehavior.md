---
layout: page
title: Oracle ADF Печать Отчетов - showPrintablePageBehavior
permalink: /dev/reports/showPrintablePageBehavior/
---

# Oracle ADF Печать Отчетов - showPrintablePageBehavior


    <af:button

    ***

        <af:showPrintablePageBehavior/>
    </af:button>


ПО нажатию на кнопку открывается новое окно, в котором наше изображение уже подготовлено для печати из браузера.

Нужно обратить внимание, что при печати, нужно чтобы графики лежали в panelSplitter (и можно еще вложить в этот panelSplitter panelStretchLayout), иначе графики могут печататься с дефолтными значениями. Т.е. на графике показывает одно, а при печати на выходе имеем совершенно другое.

<br/>

Печать графиков в файл для версии до 12.1:

В 11 версии можнро было делать так:
https://community.oracle.com/thread/3916181?start=0&tstart=0

еще пример:

http://www.oracle.com/technetwork/developer-tools/adf/learnmore/index-101235.html


How-to save - "print" - DVT graphs to a file
https://community.oracle.com/thread/3891401


<br/>

### Не показывать на форме приожения но печатать в отчете

    visible="#{adfFacesContext.outputMode eq 'printable'}"

<br/>

### Показывать на форме приожения но не печатать в отчете

    rendered="#{adfFacesContext.outputMode != 'printable'}"
