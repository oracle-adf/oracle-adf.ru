---
layout: page
title: Oracle ADF Печать Отчетов - Печать отчетов в pdf
permalink: /dev/reports/print-in-pdf/
---

# Oracle ADF Печать Отчетов - Печать отчетов в pdf

Если нужно предварительно выполнить какую-то логику, можно воспользоваться этим

<a href="/dev/reports/print-table-in-excel/"><strong>способом</strong></a>.

<br/>
<br/>

Смотри статью:  
http://waslleysouza.com.br/en/2015/03/export-table-as-a-pdf-file-in-adf/


<br/>

И приложение из этой статьи:  

https://bitbucket.org/oracle-adf/adfpdfapp/src
