---
layout: page
title: Oracle ADF Печать Отчетов
permalink: /dev/reports/
---

# Oracle ADF Печать Отчетов:

<br/>

<ul>
    <li><a href="/dev/reports/showPrintablePageBehavior/"><strong>С помощью showPrintablePageBehavior</strong></a></li>
    <li><a href="/dev/reports/other/"><strong>Печать таблицы в Excel</strong></a></li>
    <li><a href="/dev/reports/print-in-pdf/"><strong>Печать отчетов в pdf</strong></a></li>
    <li><a href="/dev/reports/other/"><strong>Какие-то другие варианты</strong></a></li>
</ul>
