---
layout: page
title: Oracle ADF Печать Отчетов - Печать отчетов в pdf
permalink: /dev/reports/print-in-pdf-my/
---

# Oracle ADF Печать Отчетов - Печать отчетов в pdf

Если нужно предварительно выполнить какую-то логику, можно воспользоваться этим

<a href="/dev/reports/print-table-in-excel/"><strong>способом</strong></a>.

<br/>
<br/>

**Нужны библиотеки:**

{% highlight xml linenos %}

<dependency>
    <groupId>avalon-framework</groupId>
    <artifactId>avalon-framework</artifactId>
    <version>4.1.3</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<dependency>
    <groupId>org.apache.avalon.framework</groupId>
    <artifactId>avalon-framework-api</artifactId>
    <version>4.3.1</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<dependency>
    <groupId>org.apache.avalon.framework</groupId>
    <artifactId>avalon-framework-impl</artifactId>
    <version>4.3.1</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/org.apache.xmlgraphics/fop -->
<dependency>
    <groupId>org.apache.xmlgraphics</groupId>
    <artifactId>fop</artifactId>
    <version>1.1</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/batik/batik-all -->
<dependency>
    <groupId>com.kenai.nbpwr</groupId>
    <artifactId>org-apache-batik-all</artifactId>
    <version>1.7-201003011305</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
<dependency>
    <groupId>commons-io</groupId>
    <artifactId>commons-io</artifactId>
    <version>2.5</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/commons-logging/commons-logging -->
<dependency>
    <groupId>commons-logging</groupId>
    <artifactId>commons-logging</artifactId>
    <version>1.2</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/xalan/serializer -->
<dependency>
    <groupId>xalan</groupId>
    <artifactId>serializer</artifactId>
    <version>2.7.1</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/xalan/xalan -->
<dependency>
    <groupId>xalan</groupId>
    <artifactId>xalan</artifactId>
    <version>2.7.0</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/xerces/xercesImpl -->
<dependency>
    <groupId>xerces</groupId>
    <artifactId>xercesImpl</artifactId>
    <version>2.7.1</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/xml-apis/xml-apis -->
<dependency>
    <groupId>xml-apis</groupId>
    <artifactId>xml-apis</artifactId>
    <version>2.0.2</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/xml-apis/xml-apis -->
<dependency>
    <groupId>xml-apis</groupId>
    <artifactId>xml-apis</artifactId>
    <version>2.0.2</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>
<!-- https://mvnrepository.com/artifact/org.apache.xmlgraphics/xmlgraphics-commons -->
<dependency>
    <groupId>org.apache.xmlgraphics</groupId>
    <artifactId>xmlgraphics-commons</artifactId>
    <version>2.1</version>
    <type>jar</type>
    <scope>compile</scope>
</dependency>

{% endhighlight %}

<br/>
<br/>

{% highlight xml linenos %}

    <af:button
                   **** >

               <af:fileDownloadActionListener method="#{pageFlowScope.MyBean.exportToPDF}" contentType="application/pdf" filename="SelectedOrdersSchedule.pdf"/>
    </af:button>

{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

private static ADFLogger _logger = ADFLogger.createADFLogger(OrdersSchedulePDFBean.class);
private static String TEMPLATE_PDF_EMPLOYEES = "stylesheets/ordersSchedulefo.xsl";
private static String TEMPLATE_FOP_CONFIG = "pdf/conf/fop.xconf";    
DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();

private InputStream getTemplate(String template) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    return facesContext.getExternalContext().getResourceAsStream(template);
}

public void exportToPDF(FacesContext facesContext, OutputStream outputStream) {

    OrdersSchedule ordersSchedule = (OrdersSchedule) JSFUtils.getManagedBeanValue("pageFlowScope.OrdersScheduleBean");        
    RichTable table = ordersSchedule.getScheduleTable();

    if (table.getSelectedRowKeys().getSize() > 0) {

                RowKeySet rksSelectedRows = table.getSelectedRowKeys();
                Iterator itrSelectedRows = rksSelectedRows.iterator();

                StringBuilder id = new StringBuilder("");
                while (itrSelectedRows.hasNext()) {
                    Key key = (Key) ((List) itrSelectedRows.next()).get(0);
                    id.append(key.getKeyValues()[0]);

                    if (itrSelectedRows.hasNext()) {
                        id.append(",");
                    }
                }

                StringBuilder whereClause = new StringBuilder();
                whereClause.append("OrdersSchedule.OS_ID IN (");
                whereClause.append(id);
                whereClause.append(")");

                DCIteratorBinding dcIteratorBindings = ADFUtils.findIterator("OrdersSchedulePDFView1Iterator");
                ViewObject vo = dcIteratorBindings.getViewObject();
                vo.setWhereClause(whereClause.toString());
                vo.executeQuery();

                try {
                    convertDOM2PDF(vo.writeXML(0, XMLInterface.XML_OPT_ALL_ROWS), getTemplate(TEMPLATE_PDF_EMPLOYEES), outputStream);

                } catch (Exception e) {
                    _logger.severe("ConvertDOM2PDF exception", e);
                }
    }
}


private void convertDOM2PDF(Node xml, InputStream template, OutputStream out) {
    try {  
    Configuration cfg = cfgBuilder.build(getTemplate(TEMPLATE_FOP_CONFIG));
        FopFactory fopFactory = FopFactory.newInstance();
    fopFactory.setUserConfig(cfg);	    

        // configure foUserAgent as desired
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

        try {
            // Construct fop with desired output format and output stream
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            FOURIResolver uriResolver = (FOURIResolver) fopFactory.getURIResolver();
            uriResolver.setCustomURIResolver(new ClasspathUriResolver());

            // Setup Identity Transformer
            TransformerFactory factory = new TransformerFactoryImpl();
            Transformer transformer = factory.newTransformer(new StreamSource(template));

            // Setup input for XSLT transformation
            Source src = new DOMSource(xml);

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            transformer.transform(src, res);
        } finally {
            out.close();
        }

    } catch (Exception e) {
        _logger.severe("Unexpected exception doing complex thing", e);            
    }
}

{% endhighlight %}

<br/>
<br/>


**ordersSchedulefo.xsl**

<br/>

{% highlight xml linenos %}

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"	exclude-result-prefixes="fo">
<xsl:template match="OrdersSchedulePDFView">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
        <!-- Определение макета страницы, в том числе полей -->
        <fo:layout-master-set>			
            <!-- Размер страницы и полей -->
            <fo:simple-page-master
                master-name="all"
                page-height="11in"
                page-width="8.5in"
                margin-top="0.25in"
                margin-bottom="0.25in"
                margin-left="1in"
                margin-right="1in">
            <!--Макет основной области -->
            <fo:region-body margin-top="2in"
                margin-bottom="1in" />
            <!--Макет верхнего колонтитула -->
            <fo:region-before extent="2in" />
            <!--Макет нижнего колонтитула -->
            <fo:region-after extent="1in" />
            </fo:simple-page-master>
        </fo:layout-master-set>
		<xsl:for-each select="./OrdersSchedulePDFViewRow">
        <!-- Создание последовательности страниц-->
        <fo:page-sequence master-reference="all">
            <!-- Создание верхнего колонтитула -->
            <fo:static-content flow-name="xsl-region-before">               
                <fo:block font-size="18pt"
                    font-family="Arial"
                    line-height="1.5em"
                    background-color="black"
                    color="white"
                    text-align="center"
                    ><xsl:value-of select="ScheduleDesc"/></fo:block>
            </fo:static-content>
            <!-- Создание нижнего колонтитула -->
            <fo:static-content flow-name="xsl-region-after">
                <!-- Добавление номера страницы к нижнему колонтитулу -->
                <fo:block text-align="end"
                font-size="10pt"
                font-family="Arial">
                    Страница <fo:page-number />
                </fo:block>
            </fo:static-content>
            <!-- Вывод основного содержимого документа -->
            <fo:flow flow-name="xsl-region-body">

                <fo:block text-align="right" font-family="Arial">
				<fo:inline font-weight="bold">Дата начала:</fo:inline>
					<xsl:value-of select="StartDate"/></fo:block>    
				<fo:block text-align="right" font-family="Arial">
				<fo:inline font-weight="bold">Дата окончания:</fo:inline>
					<xsl:value-of select="EndDate"/></fo:block>                 
				<fo:block font-weight="bold" background-color="black" color="white" padding="2pt" font-family="Arial">Поставщик</fo:block>
                <fo:table width="100%">					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>          
								<fo:block text-align="left" font-family="Arial">
								<fo:inline font-weight="bold">Наименование:  </fo:inline>
									<xsl:value-of select="SupName"/></fo:block>            
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell>
								<fo:block text-align="left" font-family="Arial">			
								<fo:inline font-weight="bold">Код:  </fo:inline>
									<xsl:value-of select="Supplier"/></fo:block>            
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell>
								<fo:block text-align="left" font-family="Arial">			
								<fo:inline font-weight="bold">Расписание:  </fo:inline>
									<xsl:value-of select="Ridesc"/></fo:block>            
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell>
								<fo:block text-align="left" font-family="Arial">			
								<fo:inline font-weight="bold">Тип доставки:  </fo:inline>
									<xsl:value-of select="DeliveryType"/></fo:block>            
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell>
								<fo:block text-align="left" font-family="Arial">			
								<fo:inline font-weight="bold">Плечо доставки:  </fo:inline>
									<xsl:value-of select="DeliveryArm"/></fo:block>            
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>

								<fo:block font-weight="bold" background-color="black" color="white" padding="2pt" font-family="Arial">Магазин</fo:block>
                <fo:table width="100%">					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block text-align="left" font-family="Arial">			
								<fo:inline font-weight="bold"><xsl:value-of select="LocName"/></fo:inline>
								</fo:block>            
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
								<fo:block font-weight="bold" background-color="black" color="white" padding="2pt" font-family="Arial">Товар</fo:block>
                <fo:table width="100%">					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block text-align="left" font-family="Arial">			
								<fo:inline font-weight="bold"><xsl:value-of select="ItemDesc"/></fo:inline>
								</fo:block>            
							</fo:table-cell>
						</fo:table-row>		
					</fo:table-body>		
				</fo:table>				
            </fo:flow>
        </fo:page-sequence>
		</xsl:for-each>
    </fo:root>
</xsl:template>
</xsl:stylesheet>

{% endhighlight %}

<br/>
<br/>

**fop.xconf**

<br/>
<br/>


{% highlight xml linenos %}

<?xml version="1.0"?>
<!-- $Id: fop.xconf 1339442 2012-05-17 01:42:56Z gadams $ -->

<!--

This is an example configuration file for FOP.
This file contains the same settings as the default values
and will have no effect if used unchanged.

Relative config url's will be resolved relative to
the location of this file.

-->

<!-- NOTE: This is the version of the configuration -->
<fop version="1.0">

  <!-- Base URL for resolving relative URLs -->

  <!-- Source resolution in dpi (dots/pixels per inch) for determining the size of pixels in SVG and bitmap images, default: 72dpi -->
  <source-resolution>72</source-resolution>
  <!-- Target resolution in dpi (dots/pixels per inch) for specifying the target resolution for generated bitmaps, default: 72dpi -->
  <target-resolution>72</target-resolution>

  <!-- Default page-height and page-width, in case
       value is specified as auto -->
  <default-page-settings height="11in" width="8.26in"/>

  <!-- Information for specific renderers -->
  <!-- Uses renderer mime type for renderers -->
  <renderers>
    <renderer mime="application/pdf">
      <filterList>
        <!-- provides compression using zlib flate (default is on) -->
        <value>flate</value>

        <!-- encodes binary data into printable ascii characters (default off)
             This provides about a 4:5 expansion of data size -->
        <!-- <value>ascii-85</value> -->

        <!-- encodes binary data with hex representation (default off)
             This filter is not recommended as it doubles the data size -->
        <!-- <value>ascii-hex</value> -->
      </filterList>

      <fonts>
        <!-- embedded fonts -->
        <!--
        This information must exactly match the font specified
        in the fo file. Otherwise it will use a default font.

        For example,
        <fo:inline font-family="Arial" font-weight="bold" font-style="normal">
            Arial-normal-normal font
        </fo:inline>
        for the font triplet specified by:
        <font-triplet name="Arial" style="normal" weight="bold"/>

        If you do not want to embed the font in the pdf document
        then do not include the "embed-url" attribute.
        The font will be needed where the document is viewed
        for it to be displayed properly.

        possible styles: normal | italic | oblique | backslant
        possible weights: normal | bold | 100 | 200 | 300 | 400
                          | 500 | 600 | 700 | 800 | 900
        (normal = 400, bold = 700)-->
        <font-triplet name="Arial" style="normal" weight="bold"/>		
		<auto-detect/>
               <!-- <font kerning="yes" embed-url="/usr/share/fonts/Arial.ttf" embedding-mode="subset">
			<font-triplet name="Arial" style="normal" weight="bold"/>
		</font>
		<font kerning="yes" embed-url="/usr/share/fonts/Arial.ttf" embedding-mode="subset">
			<font-triplet name="Arial" style="normal" weight="normal"/>
		</font>
		<font kerning="yes" embed-url="/usr/share/fonts/Arialbd.ttf" embedding-mode="subset">
			<font-triplet name="Arial" style="normal" weight="bold"/>
		</font>
		<font kerning="yes" embed-url="/usr/share/fonts/Ariali.ttf" embedding-mode="subset">
			<font-triplet name="Arial" style="italic" weight="bold"/>
		</font>
		<font kerning="yes" embed-url="/usr/share/fonts/Arialbi.ttf" embedding-mode="subset">
			<font-triplet name="Arial" style="italic" weight="bold"/>
		</font>-->
      </fonts>

      <!-- This option lets you specify additional options on an XML handler -->
      <!--xml-handler namespace="http://www.w3.org/2000/svg">
        <stroke-text>false</stroke-text>
      </xml-handler-->

    </renderer>

    <renderer mime="application/x-afp">
      <!--
           The bit depth and type of images produced
           (this is the default setting)
      -->
      <images mode="b+w" bits-per-pixel="8"/>
      <renderer-resolution>240</renderer-resolution>
      <line-width-correction>2.5</line-width-correction>
      <resource-group-file>resources.afp</resource-group-file>

      <fonts>
      <!--
           Below is an example using raster font configuration using FOP builtin base-14 font metrics.
           for Times Roman, Helvetica and Courier.

           Depending on AFP raster and outline font availability on your installation you will
           most likely need to modify the configuration provided below.

           See http://xmlgraphics.apache.org/fop/trunk/output.html#afp-configuration
           for details of FOP configuration for AFP
      -->

        <!-- Times Roman -->
        <font>
          <afp-font name="Times Roman" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0N20060" base14-font="TimesRoman"/>
            <afp-raster-font size="7" characterset="C0N20070" base14-font="TimesRoman"/>
            <afp-raster-font size="8" characterset="C0N20080" base14-font="TimesRoman"/>
            <afp-raster-font size="9" characterset="C0N20090" base14-font="TimesRoman"/>
            <afp-raster-font size="10" characterset="C0N20000" base14-font="TimesRoman"/>
            <afp-raster-font size="11" characterset="C0N200A0" base14-font="TimesRoman"/>
            <afp-raster-font size="12" characterset="C0N200B0" base14-font="TimesRoman"/>
            <afp-raster-font size="14" characterset="C0N200D0" base14-font="TimesRoman"/>
            <afp-raster-font size="16" characterset="C0N200F0" base14-font="TimesRoman"/>
            <afp-raster-font size="18" characterset="C0N200H0" base14-font="TimesRoman"/>
            <afp-raster-font size="20" characterset="C0N200J0" base14-font="TimesRoman"/>
            <afp-raster-font size="24" characterset="C0N200N0" base14-font="TimesRoman"/>
            <afp-raster-font size="30" characterset="C0N200T0" base14-font="TimesRoman"/>
            <afp-raster-font size="36" characterset="C0N200Z0" base14-font="TimesRoman"/>
          </afp-font>
          <font-triplet name="Times" style="normal" weight="normal"/>
          <font-triplet name="TimesRoman" style="normal" weight="normal"/>
          <font-triplet name="Times Roman" style="normal" weight="normal"/>
          <font-triplet name="Times-Roman" style="normal" weight="normal"/>
          <font-triplet name="Times New Roman" style="normal" weight="normal"/>
          <font-triplet name="TimesNewRoman" style="normal" weight="normal"/>
          <font-triplet name="serif" style="normal" weight="normal"/>
        </font>

        <!-- Times Roman Italic -->
        <font>
          <afp-font name="Times Roman Italic" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0N30060" base14-font="TimesItalic"/>
            <afp-raster-font size="7" characterset="C0N30070" base14-font="TimesItalic"/>
            <afp-raster-font size="8" characterset="C0N30080" base14-font="TimesItalic"/>
            <afp-raster-font size="9" characterset="C0N30090" base14-font="TimesItalic"/>
            <afp-raster-font size="10" characterset="C0N30000" base14-font="TimesItalic"/>
            <afp-raster-font size="11" characterset="C0N300A0" base14-font="TimesItalic"/>
            <afp-raster-font size="12" characterset="C0N300B0" base14-font="TimesItalic"/>
            <afp-raster-font size="14" characterset="C0N300D0" base14-font="TimesItalic"/>
            <afp-raster-font size="16" characterset="C0N300F0" base14-font="TimesItalic"/>
            <afp-raster-font size="18" characterset="C0N300H0" base14-font="TimesItalic"/>
            <afp-raster-font size="20" characterset="C0N300J0" base14-font="TimesItalic"/>
            <afp-raster-font size="24" characterset="C0N300N0" base14-font="TimesItalic"/>
            <afp-raster-font size="30" characterset="C0N300T0" base14-font="TimesItalic"/>
            <afp-raster-font size="36" characterset="C0N300Z0" base14-font="TimesItalic"/>
          </afp-font>
          <font-triplet name="Times" style="italic" weight="normal"/>
          <font-triplet name="TimesRoman" style="italic" weight="normal"/>
          <font-triplet name="Times Roman" style="italic" weight="normal"/>
          <font-triplet name="Times-Roman" style="italic" weight="normal"/>
          <font-triplet name="Times New Roman" style="italic" weight="normal"/>
          <font-triplet name="TimesNewRoman" style="italic" weight="normal"/>
          <font-triplet name="serif" style="italic" weight="normal"/>
        </font>

        <!-- Times Roman Bold -->
        <font>
          <afp-font name="Times Roman Bold" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0N40060" base14-font="TimesBold"/>
            <afp-raster-font size="7" characterset="C0N40070" base14-font="TimesBold"/>
            <afp-raster-font size="8" characterset="C0N40080" base14-font="TimesBold"/>
            <afp-raster-font size="9" characterset="C0N40090" base14-font="TimesBold"/>
            <afp-raster-font size="10" characterset="C0N40000" base14-font="TimesBold"/>
            <afp-raster-font size="11" characterset="C0N400A0" base14-font="TimesBold"/>
            <afp-raster-font size="12" characterset="C0N400B0" base14-font="TimesBold"/>
            <afp-raster-font size="14" characterset="C0N400D0" base14-font="TimesBold"/>
            <afp-raster-font size="16" characterset="C0N400F0" base14-font="TimesBold"/>
            <afp-raster-font size="18" characterset="C0N400H0" base14-font="TimesBold"/>
            <afp-raster-font size="20" characterset="C0N400J0" base14-font="TimesBold"/>
            <afp-raster-font size="24" characterset="C0N400N0" base14-font="TimesBold"/>
            <afp-raster-font size="30" characterset="C0N400T0" base14-font="TimesBold"/>
            <afp-raster-font size="36" characterset="C0N400Z0" base14-font="TimesBold"/>
          </afp-font>
          <font-triplet name="Times" style="normal" weight="bold"/>
          <font-triplet name="TimesRoman" style="normal" weight="bold"/>
          <font-triplet name="Times Roman" style="normal" weight="bold"/>
          <font-triplet name="Times-Roman" style="normal" weight="bold"/>
          <font-triplet name="Times New Roman" style="normal" weight="bold"/>
          <font-triplet name="TimesNewRoman" style="normal" weight="bold"/>
          <font-triplet name="serif" style="normal" weight="bold"/>
        </font>

        <!-- Times Roman Italic Bold -->
        <font>
          <afp-font name="Times Roman Italic Bold" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0N50060" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="7" characterset="C0N50070" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="8" characterset="C0N50080" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="9" characterset="C0N50090" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="10" characterset="C0N50000" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="11" characterset="C0N500A0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="12" characterset="C0N500B0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="14" characterset="C0N500D0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="16" characterset="C0N500F0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="18" characterset="C0N500H0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="20" characterset="C0N500J0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="24" characterset="C0N500N0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="30" characterset="C0N500T0" base14-font="TimesBoldItalic"/>
            <afp-raster-font size="36" characterset="C0N500Z0" base14-font="TimesBoldItalic"/>
          </afp-font>
          <font-triplet name="Times" style="italic" weight="bold"/>
          <font-triplet name="TimesRoman" style="italic" weight="bold"/>
          <font-triplet name="Times Roman" style="italic" weight="bold"/>
          <font-triplet name="Times-Roman" style="italic" weight="bold"/>
          <font-triplet name="Times New Roman" style="italic" weight="bold"/>
          <font-triplet name="TimesNewRoman" style="italic" weight="bold"/>
          <font-triplet name="serif" style="italic" weight="bold"/>
        </font>

        <!-- Helvetica -->
        <font>
          <afp-font name="Helvetica" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0H20060" base14-font="Helvetica"/>
            <afp-raster-font size="7" characterset="C0H20070" base14-font="Helvetica"/>
            <afp-raster-font size="8" characterset="C0H20080" base14-font="Helvetica"/>
            <afp-raster-font size="9" characterset="C0H20090" base14-font="Helvetica"/>
            <afp-raster-font size="10" characterset="C0H20000" base14-font="Helvetica"/>
            <afp-raster-font size="11" characterset="C0H200A0" base14-font="Helvetica"/>
            <afp-raster-font size="12" characterset="C0H200B0" base14-font="Helvetica"/>
            <afp-raster-font size="14" characterset="C0H200D0" base14-font="Helvetica"/>
            <afp-raster-font size="16" characterset="C0H200F0" base14-font="Helvetica"/>
            <afp-raster-font size="18" characterset="C0H200H0" base14-font="Helvetica"/>
            <afp-raster-font size="20" characterset="C0H200J0" base14-font="Helvetica"/>
            <afp-raster-font size="24" characterset="C0H200N0" base14-font="Helvetica"/>
            <afp-raster-font size="30" characterset="C0H200T0" base14-font="Helvetica"/>
            <afp-raster-font size="36" characterset="C0H200Z0" base14-font="Helvetica"/>
          </afp-font>
          <font-triplet name="Helvetica" style="normal" weight="normal"/>
          <font-triplet name="Arial" style="normal" weight="normal"/>
          <font-triplet name="sans-serif" style="normal" weight="normal"/>
          <font-triplet name="any" style="normal" weight="normal"/>
        </font>

        <!-- Helvetica Italic -->
        <font>
          <afp-font name="Helvetica Italic" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0H30060" base14-font="HelveticaOblique"/>
            <afp-raster-font size="7" characterset="C0H30070" base14-font="HelveticaOblique"/>
            <afp-raster-font size="8" characterset="C0H30080" base14-font="HelveticaOblique"/>
            <afp-raster-font size="9" characterset="C0H30090" base14-font="HelveticaOblique"/>
            <afp-raster-font size="10" characterset="C0H30000" base14-font="HelveticaOblique"/>
            <afp-raster-font size="11" characterset="C0H300A0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="12" characterset="C0H300B0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="14" characterset="C0H300D0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="16" characterset="C0H300F0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="18" characterset="C0H300H0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="20" characterset="C0H300J0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="24" characterset="C0H300N0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="30" characterset="C0H300T0" base14-font="HelveticaOblique"/>
            <afp-raster-font size="36" characterset="C0H300Z0" base14-font="HelveticaOblique"/>
          </afp-font>
          <font-triplet name="Helvetica" style="italic" weight="normal"/>
          <font-triplet name="Arial" style="italic" weight="normal"/>
          <font-triplet name="sans-serif" style="italic" weight="normal"/>
        </font>

        <!-- Helvetica (Semi) Bold -->
        <font>
          <afp-font name="Helvetica (Semi) Bold" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0H40060" base14-font="HelveticaBold"/>
            <afp-raster-font size="7" characterset="C0H40070" base14-font="HelveticaBold"/>
            <afp-raster-font size="8" characterset="C0H40080" base14-font="HelveticaBold"/>
            <afp-raster-font size="9" characterset="C0H40090" base14-font="HelveticaBold"/>
            <afp-raster-font size="10" characterset="C0H40000" base14-font="HelveticaBold"/>
            <afp-raster-font size="11" characterset="C0H400A0" base14-font="HelveticaBold"/>
            <afp-raster-font size="12" characterset="C0H400B0" base14-font="HelveticaBold"/>
            <afp-raster-font size="14" characterset="C0H400D0" base14-font="HelveticaBold"/>
            <afp-raster-font size="16" characterset="C0H400F0" base14-font="HelveticaBold"/>
            <afp-raster-font size="18" characterset="C0H400H0" base14-font="HelveticaBold"/>
            <afp-raster-font size="20" characterset="C0H400J0" base14-font="HelveticaBold"/>
            <afp-raster-font size="24" characterset="C0H400N0" base14-font="HelveticaBold"/>
            <afp-raster-font size="30" characterset="C0H400T0" base14-font="HelveticaBold"/>
            <afp-raster-font size="36" characterset="C0H400Z0" base14-font="HelveticaBold"/>
          </afp-font>
          <font-triplet name="Helvetica" style="normal" weight="bold"/>
          <font-triplet name="Arial" style="normal" weight="bold"/>
          <font-triplet name="sans-serif" style="normal" weight="bold"/>
        </font>

        <!-- Helvetica Italic (Semi) Bold -->
        <font>
          <afp-font name="Helvetica Italic (Semi) Bold" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0H50060" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="7" characterset="C0H50070" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="8" characterset="C0H50080" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="9" characterset="C0H50090" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="10" characterset="C0H50000" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="11" characterset="C0H500A0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="12" characterset="C0H500B0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="14" characterset="C0H500D0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="16" characterset="C0H500F0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="18" characterset="C0H500H0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="20" characterset="C0H500J0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="24" characterset="C0H500N0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="30" characterset="C0H500T0" base14-font="HelveticaBoldOblique"/>
            <afp-raster-font size="36" characterset="C0H500Z0" base14-font="HelveticaBoldOblique"/>
          </afp-font>
          <font-triplet name="Helvetica" style="italic" weight="bold"/>
          <font-triplet name="Arial" style="italic" weight="bold"/>
          <font-triplet name="sans-serif" style="italic" weight="bold"/>
        </font>

        <!-- Courier -->
        <font>
          <afp-font name="Courier" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0420060" base14-font="Courier"/>
            <afp-raster-font size="7" characterset="C0420070" base14-font="Courier"/>
            <afp-raster-font size="8" characterset="C0420080" base14-font="Courier"/>
            <afp-raster-font size="9" characterset="C0420090" base14-font="Courier"/>
            <afp-raster-font size="10" characterset="C0420000" base14-font="Courier"/>
            <afp-raster-font size="11" characterset="C04200A0" base14-font="Courier"/>
            <afp-raster-font size="12" characterset="C04200B0" base14-font="Courier"/>
            <afp-raster-font size="14" characterset="C04200D0" base14-font="Courier"/>
            <afp-raster-font size="16" characterset="C04200F0" base14-font="Courier"/>
            <afp-raster-font size="18" characterset="C04200H0" base14-font="Courier"/>
            <afp-raster-font size="20" characterset="C04200J0" base14-font="Courier"/>
            <afp-raster-font size="24" characterset="C04200N0" base14-font="Courier"/>
            <afp-raster-font size="30" characterset="C04200T0" base14-font="Courier"/>
            <afp-raster-font size="36" characterset="C04200Z0" base14-font="Courier"/>
          </afp-font>
          <font-triplet name="Courier" style="normal" weight="normal"/>
          <font-triplet name="monospace" style="normal" weight="normal"/>
        </font>

        <!-- Courier Italic -->
        <font>
          <afp-font name="Courier Italic" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0430060" base14-font="CourierOblique"/>
            <afp-raster-font size="7" characterset="C0430070" base14-font="CourierOblique"/>
            <afp-raster-font size="8" characterset="C0430080" base14-font="CourierOblique"/>
            <afp-raster-font size="9" characterset="C0430090" base14-font="CourierOblique"/>
            <afp-raster-font size="10" characterset="C0430000" base14-font="CourierOblique"/>
            <afp-raster-font size="11" characterset="C04300A0" base14-font="CourierOblique"/>
            <afp-raster-font size="12" characterset="C04300B0" base14-font="CourierOblique"/>
            <afp-raster-font size="14" characterset="C04300D0" base14-font="CourierOblique"/>
            <afp-raster-font size="16" characterset="C04300F0" base14-font="CourierOblique"/>
            <afp-raster-font size="18" characterset="C04300H0" base14-font="CourierOblique"/>
            <afp-raster-font size="20" characterset="C04300J0" base14-font="CourierOblique"/>
            <afp-raster-font size="24" characterset="C04300N0" base14-font="CourierOblique"/>
            <afp-raster-font size="30" characterset="C04300T0" base14-font="CourierOblique"/>
            <afp-raster-font size="36" characterset="C04300Z0" base14-font="CourierOblique"/>
          </afp-font>
          <font-triplet name="Courier" style="italic" weight="normal"/>
          <font-triplet name="monospace" style="italic" weight="normal"/>
        </font>

        <!-- Courier Bold -->
        <font>
          <afp-font name="Courier Bold" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0440060" base14-font="CourierBold"/>
            <afp-raster-font size="7" characterset="C0440070" base14-font="CourierBold"/>
            <afp-raster-font size="8" characterset="C0440080" base14-font="CourierBold"/>
            <afp-raster-font size="9" characterset="C0440090" base14-font="CourierBold"/>
            <afp-raster-font size="10" characterset="C0440000" base14-font="CourierBold"/>
            <afp-raster-font size="11" characterset="C04400A0" base14-font="CourierBold"/>
            <afp-raster-font size="12" characterset="C04400B0" base14-font="CourierBold"/>
            <afp-raster-font size="14" characterset="C04400D0" base14-font="CourierBold"/>
            <afp-raster-font size="16" characterset="C04400F0" base14-font="CourierBold"/>
            <afp-raster-font size="18" characterset="C04400H0" base14-font="CourierBold"/>
            <afp-raster-font size="20" characterset="C04400J0" base14-font="CourierBold"/>
            <afp-raster-font size="24" characterset="C04400N0" base14-font="CourierBold"/>
            <afp-raster-font size="30" characterset="C04400T0" base14-font="CourierBold"/>
            <afp-raster-font size="36" characterset="C04400Z0" base14-font="CourierBold"/>
          </afp-font>
          <font-triplet name="Courier" style="normal" weight="bold"/>
          <font-triplet name="monospace" style="normal" weight="bold"/>
        </font>

        <!-- Courier Italic Bold -->
        <font>
          <afp-font name="Courier Italic Bold" type="raster" codepage="T1V10500" encoding="Cp500">
            <afp-raster-font size="6" characterset="C0450060" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="7" characterset="C0450070" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="8" characterset="C0450080" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="9" characterset="C0450090" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="10" characterset="C0450000" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="11" characterset="C04500A0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="12" characterset="C04500B0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="14" characterset="C04500D0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="16" characterset="C04500F0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="18" characterset="C04500H0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="20" characterset="C04500J0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="24" characterset="C04500N0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="30" characterset="C04500T0" base14-font="CourierBoldOblique"/>
            <afp-raster-font size="36" characterset="C04500Z0" base14-font="CourierBoldOblique"/>
          </afp-font>
          <font-triplet name="Courier" style="italic" weight="bold"/>
          <font-triplet name="monospace" style="italic" weight="bold"/>
        </font>

         <!--
        Configure double-byte (CID Keyed font (Type 0)) AFP fonts with type="CIDKeyed".  

        example:
         <font>
        	<afp-font type="CIDKeyed" encoding="UnicodeBigUnmarked"  
        	codepage="T1120000" characterset="CZJHMNU"
        	base-uri="fonts" />
         	<font-triplet name="J-Heisei Mincho" style="normal" weight="normal" />
     	 </font>
        -->


      </fonts>
    </renderer>

    <renderer mime="application/postscript">
      <!-- This option forces the PS renderer to rotate landscape pages -->
      <!--auto-rotate-landscape>true</auto-rotate-landscape-->

      <!-- This option lets you specify additional options on an XML handler -->
      <!--xml-handler namespace="http://www.w3.org/2000/svg">
        <stroke-text>false</stroke-text>
      </xml-handler-->
    </renderer>

    <renderer mime="application/vnd.hp-PCL">
    </renderer>

    <!-- MIF does not have a renderer
    <renderer mime="application/vnd.mif">
    </renderer>
    -->

    <renderer mime="image/svg+xml">
      <format type="paginated"/>
      <link value="true"/>
      <strokeText value="false"/>
    </renderer>

    <renderer mime="application/awt">
    </renderer>

    <renderer mime="image/png">
      <!--transparent-page-background>true</transparent-page-background-->
    </renderer>

    <renderer mime="image/tiff">
      <!--transparent-page-background>true</transparent-page-background-->
      <!--compression>CCITT T.6</compression-->
    </renderer>

    <renderer mime="text/xml">
    </renderer>

    <!-- RTF does not have a renderer
    <renderer mime="text/rtf">
    </renderer>
    -->

  </renderers>

</fop>


{% endhighlight %}
