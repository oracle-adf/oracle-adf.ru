---
layout: page
title: Разработка с использованием Oracle JET
permalink: /dev/jet/
---

# Разработка с использованием Oracle JET:

<br/>

<div align="center">

    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLKCk3OyNwIzt-3ZLTmhZfdWfCZOEFwA3g" frameborder="0" allowfullscreen></iframe>

</div>


<br/>

### Компоненты:

<ul>
    <li><a href="http://www.oracle.com/webfolder/technetwork/jet/jetCookbook.html?component=home&demo=rootCollections">Компоненты</a></li>
</ul>


<br/>

### Examples:

http://www.oracle.com/webfolder/technetwork/jet/public_samples/WorkBetter/public_html/index.html  <br/>
http://www.oracle.com/webfolder/technetwork/jet/globalExamples.html  


<br/>

### Apps:

http://www.oracle.com/webfolder/technetwork/jet/globalGetStarted.html <br/>
https://github.com/oracle-jet/work-better-jet



<br/>

### Статьи

<ul>
    <li><a href="http://andrejusb.blogspot.ru/2015/12/improved-jet-rendering-in-adf.html">Improved JET Rendering in ADF</a></li>
</ul>


<br/>

### Темы на community

<br/>

<strong>Печать Oracle Jet графиков (ссылки на материалы которые может быть будут полезны по этой теме, но я не смог):</strong>

https://community.oracle.com/thread/4041898
https://community.oracle.com/thread/4033502
https://community.oracle.com/thread/4033953



https://community.oracle.com/community/development_tools/oracle-jet/blog/2017/05
