---
layout: page
title: Oracle ADF Распечатать view object в консоли с помощью бина
permalink: /dev/model/vo/vo/print-viewobject-in-console-by-bean/
---

# Распечатать view object в консоли с помощью бина

ADFUtils и VOUtils лежат в репозитории <a href="https://bitbucket.org/oracle-adf/adf-utils">Utils</a>

Вот так сейчас делаю:


    VOUtils.printViewObject(vo);


Сам  метод:

    public static void printViewObject(ViewObject vo)
        {
           // Execute the query, print results to the screen.
           vo.executeQuery();

           // Print the View Object's query
           System.out.println("==================");
           System.out.println("=== Query: ====");
           System.out.println(vo.getQuery());
           System.out.println("==================");

           System.out.println("=== ViewObject DATA =====");
             System.out.println("Rows Count " + vo.getRowCount());

             vo.reset();

             while (vo.getCurrentRow() != null) {

                  Row row = vo.getCurrentRow();
                  String rowDataStr = "";

                  int numAttrs = vo.getAttributeCount();

                  for (int columnNo = 0; columnNo < numAttrs; columnNo++){
                       Object attrData = row.getAttribute(columnNo);
                       rowDataStr += (attrData + "\t");
                    }

                    System.out.println((vo.getCurrentRowIndex() + 1) + ") " + rowDataStr);

                 vo.next();
              }


              System.out.println("=== NO MORE DATA =====");
        }
