---
layout: page
title: Oracle ADF > Отложить выполнение ViewObject
permalink: /dev/model/vo/vo/delay-execution/
---

# [Oracle ADF] Отложить выполнение ViewObject

1) Заходим на страницу где  находщится компонент, работающий с VO.

2) Встаем на его итератор.

Устанавливаем в refreshCondition, что-то вроде


    #{pageFlowScope.MyBean.activateChart}

3) А в самом бине

    private boolean activateChart = false;

4) Потом при при необходимом событии.
Из false делаем true и рефрешим компонент.
