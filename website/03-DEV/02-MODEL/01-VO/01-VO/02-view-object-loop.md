---
layout: page
title: Oracle ADF пройтись по VO в цикле
permalink: /dev/model/vo/vo/view-object-loop/
---

# Пройтись по VO в цикле


ADFUtils и VOUtils лежат в репозитории <a href="https://bitbucket.org/oracle-adf/adf-utils">Utils</a>



{% highlight java linenos %}

 ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.ITEMS_VO);
 String selectedItemId = "";

 for (Row rw : vo.getAllRowsInRange()) {
     if ((rw.getAttribute("MyAttr1")).equals("SOME_TEXT")){
         selectedItemId = (String)rw.getAttribute("MyAttr2");
     }
 }


{% endhighlight %}
