---
layout: page
title: Oracle ADF - ViewObject
permalink: /dev/model/vo/vo/
---


<br/>

# Oracle ADF > ViewObject

<br/>

### Работа с VO (ViewObject) в консоли weblogic

<ul>
    <li><a href="/dev/model/vo/vo/print-viewobject-in-console-by-bean/">Распечатать view object в консоли с помощью бина</a></li>

    <li><a href="/dev/model/vo/vo/view-object-loop/">Пройтись по VO в цикле</a></li>

    <li><a href="/dev/view/bindings/executables/iterator/print_values_from_vo_by_iterator/">Распечатать в консоли viewobject с помощью итератора</a> (Первый вариант лучше)</li>
</ul>

<br/>

### VO (ViewObject) Bind variables во ViewObject

<br/>

Можно использовать bind переменные программно. Получаем доступ к ViewObject и задаем значения bind переменных. Остается только выполнить измененный VO.

<ul>

    <li><a href="/dev/model/vo/vo/setup-view-object-bind-variables-programmatically/">Задать и считать программно bind variables во ViewObject</a></li>

    <li><a href="/dev/model/vo/vo/setup-view-object-bind-array-variable-programmatically/">Задать  программно bind variables типа Array во ViewObject</a></li>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/SetParamFromVOAMProperty.pdf" rel="nofollow">Получение значения параметра из свойства ViewObject/AppModule</a></li>

</ul>

<br/>

### Отложить выполнение ViewObject

<br/>

Наприме, нужно, чтобы при открытии формы DataControl (для меня тоже самое, что и ViewObject) сразу не выполнялся.

<ul>

    <li><a href="/dev/model/vo/vo/delay-execution/">Отложить выполнение ViewObject</a></li>
</ul>


<br/>

**Вот такой пример есть**

<br/>


{% highlight java %}

    public void onSomethingBadHappened(ValueChangeEvent valueChangeEvent) {

        Row[] rows =
            ADFUtils.findIterator("MyIterator").getViewObject().getFilteredRows("ItemDescription",
                                                                                 valueChangeEvent.getNewValue());

        if (rows.length > 0) {
            Object itemID = rows[0].getAttribute("Item");

            ***********
        }
    }

{% endhighlight %}


<br/>

**Очистка временных таблиц**

<br/>


{% highlight java %}

    ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("MY_APPLICATION_MODULE_NAME");
    ViewObject vo;
    vo = am.findViewObject("MY_VO_NAME");
    vo.resetExecuted();

{% endhighlight %}
