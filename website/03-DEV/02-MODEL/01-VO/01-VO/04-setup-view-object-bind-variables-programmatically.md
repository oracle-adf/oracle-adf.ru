---
layout: page
title: Oracle ADF > Задать и считать программно bind variables во ViewObject
permalink: /dev/model/vo/vo/setup-view-object-bind-variables-programmatically/
---

# [Oracle ADF] Задать и считать программно bind variables во ViewObject

Имеем ViewObject с bind variables.
Нужно задать значения программно.


{% highlight java linenos %}

import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewObject;

***

ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.VO_NAME);

String date1 = "2015-11-11";
String date2 = "2016-11-11";

VariableValueManager vm = vo.ensureVariableManager();

vm.setVariableValue("vo_param_date1", date1);
vm.setVariableValue("vo_param_date2", date2);

vo.executeQuery();

{% endhighlight %}


Просто получаем доступ к ViewObject и устанавливаем параметны для bind variables.

CONSTANTS_VO.VO_NAME - константа в которой прописано название нужного VO.


<br/><br/>
<br/>

### Cчитать программно bind variables из ViewObject


**Пример 1**

<br/>



{% highlight java linenos %}

ViewObject vo = VOUtils.getViewObjectByName(voName);
Long supIdFromVO = (Long)vo.getNamedWhereClauseParam("p_supplier");
System.out.println("supIdFromVO ");
System.out.println("  supIdFromVO " + supIdFromVO.toString());

{% endhighlight %}


<br/><br/>

**Пример 2**

<br/>

{% highlight java linenos %}

ADFUtils.findIterator("ITERATOR_NAME").getViewObject().getNamedWhereClauseParam("myVar");

{% endhighlight %}


<br/>

**Пишу далее без проверки:**

Если переменная типа date


{% highlight java linenos %}

public String getDate1() {
       oracle.jbo.domain.Date date1 =
           (oracle.jbo.domain.Date)
           ADFUtils.findIterator("ITERATOR_NAME").getViewObject().getNamedWhereClauseParam("date1");
       System.out.println("####################### DATE1 " + date1);
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
       return sdf.format(date1.getValue());
   }

{% endhighlight %}
