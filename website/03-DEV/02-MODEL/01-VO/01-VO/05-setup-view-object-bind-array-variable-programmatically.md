---
layout: page
title: Oracle ADF > Задать  программно bind variables типа Array во ViewObject
permalink: /dev/model/vo/vo/setup-view-object-bind-array-variable-programmatically/
---

# [Oracle ADF] Задать программно bind variables типа Array во ViewObject


Пока только 1 раз приходилось делать.
Передаю массив строк

Если делать стандартным способом, вроде передать массив, получаю:

**javax.faces.el.EvaluationException: oracle.jbo.JboException: Cannot insert/update Array without context information**

Но есть решение.



{% highlight java linenos %}

private ArrayList<String> SelectedElementsInArray = new ArrayList<String>();



public void onSelectManyChoiseChanged(ValueChangeEvent valueChangeEvent) {

    this.SelectedElementsInArray.removeAll(this.SelectedElementsInArray);

    Object[] selectedListElements = (Object[])valueChangeEvent.getNewValue();

    if (selectedListElements != null){
        for (int si=0; si<selectedListElements.length; si++){
            this.SelectedElementsInArray.add(selectedListElements[si].toString());
        }
    }
    System.out.println(Arrays.toString(this.SelectedElementsInArray.toArray()));
}


public void onBunntonRefreshClicked(ActionEvent actionEvent) {

    ViewObject vo = VOUtils.getViewObjectByName(MY_VO_NAME);
    VariableValueManager vm = vo.ensureVariableManager();

    HashMap context = new HashMap();
    context.put(DomainContext.ELEMENT_SQL_NAME, "CHARTABLETYPE");
    context.put(DomainContext.ELEMENT_TYPE, String.class);

    oracle.jbo.domain.Array resArr = new oracle.jbo.domain.Array(this.SelectedElementsInArray.toArray());
    resArr.setContext(null, null, context);

    vm.setVariableValue("my_array_attr", resArr);

    vo.executeQuery();
}
{% endhighlight %}


<br/>

!!! Возможно, что если нужно установить пустой массив, делается это так

  resArr = new oracle.jbo.domain.Array(new Object[] { "none-none" });



<br/>

### В SQL запросе (ViewObject) это может выглядеть следующим образом

{% highlight java linenos %}

and something_very_important in
 (select value(t)
     from table(cast(:my_array_attr as CHARTABLETYPE)) t)


{% endhighlight %}
