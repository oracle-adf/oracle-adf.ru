---
layout: page
title: Oracle ADF ROW
permalink: /dev/model/vo/row/
---

# [Oracle ADF] ROW


### Get Current Row

{% highlight java %}

    public Row getCurrentRow(String iterator) {
        DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding dcItteratorBindings = bindings.findIteratorBinding(iterator);
        ViewObject vo = dcItteratorBindings.getViewObject();
        return vo.getCurrentRow();
    }

{% endhighlight %}

<br/>

# Получить из VO где всего 1 запись

<br/>

### Строку

Делал в AppModuelImpl


{% highlight java %}

    public String getUserLocale() {

        String userLocaleAttribute = "Language";
        String voName = "UserLocale";

        ViewObjectImpl vo = (ViewObjectImpl) findViewObject(voName);
            Row row = vo.first();
            return (String) row.getAttribute(userLocaleAttribute);
    }

{% endhighlight %}

<br/>

### Дату типа timestamp

В аттрибутах поставил тип Date

<br/>

{% highlight java %}

    import java.util.Date;

{% endhighlight %}

<br/>   

{% highlight java %}

    public static Date getMyBusitessDate(){
        ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_DATE_VO);
        Row curRow = vo.first();
        oracle.jbo.domain.Date businessDate = (oracle.jbo.domain.Date)curRow.getAttribute("myBusinessDate");
        return businessDate.getValue();        
    }

{% endhighlight %}

<br/>

### Получить из VO где всего 1 запись дату. Дата timestamp

{% highlight java %}

    import java.time.LocalDate;
    import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

{% endhighlight %}

<br/>

{% highlight java %}

    public String getMyBusitessDate() {
        Row row = ADFUtils.findIterator("BusinessDateViewIterator").getCurrentRow();
        if (row != null) {
            Object myDate = row.getAttribute("MyDate");
            if (myDate instanceof Timestamp) {
                Timestamp timestamp = (Timestamp) myDate;
                LocalDate localDate = timestamp.toLocalDateTime().toLocalDate();
                return localDate.format(ISO_LOCAL_DATE);
            }
        }
        return "";       
    }


{% endhighlight %}

<br/>

### Найти значение поля во ViewObject по аттрибуту

<br/>

Приходит текст в valueChangeEvent. Нужно получить id.


{% highlight java linenos %}

public void onSomethingChanged (ValueChangeEvent valueChangeEvent) {

    if (null != valueChangeEvent.getNewValue()){

        String selectedDescription = valueChangeEvent.getNewValue().toString();

        ViewObject voItems = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_VO);

        String selectedId = "";

        for (Row rw : voItems.getAllRowsInRange()) {
            if ((rw.getAttribute("Description")).equals(selectedDescription)){
                selectedId = (String)rw.getAttribute("Id");
            }
        }

        System.out.println();
        System.out.println("selectedDescription " + selectedDescription);
        System.out.println("selectedId " + selectedId);

    }

}

{% endhighlight %}

id и Description - поля в базе.


<br/>

### CreateRow

(скопировано где-то)

    Row custRow = vo.first();
    if (custRow == null) {
        custRow = vo.createRow();
        ViewObject voNextId = am.findViewObject("MyVO");
        voNextId.executeQuery();

        custRow.setAttribute("MyAttribute1", voNextId.first().getAttribute("Newid"));

        custRow.setAttribute("MyAttribute2", row.getAttribute("UiId"));

        Object user = AdfFacesContext.getCurrentInstance()
                                     .getPageFlowScope()
                                     .get("p_user");

        custRow.setAttribute("UserId", user);

        vo.insertRow(custRow);

        DBTransaction txn = am.getDBTransaction();

        txn.postChanges();
        vo.executeQuery();

        custRow = vo.first();
    }

<br/>

### Еще 1 пример:


    Row curRow = ADFUtils.findIterator("IteratorName").getCurrentRow();
    curRow.setAttribute("Status", "A");

    DCBindingContainer bindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
    OperationBinding operationBinding = bindings.getOperationBinding("Commit");
    System.out.println("Execute Commit");
    operationBinding.execute();



<br/>

### Еще 1 пример:


    // Tree
    JUCtrlHierBinding res2 = (JUCtrlHierBinding) bindings.get("bindingName");

    for (Row r : res2.getAllRowsInRange()) {

        System.out.println(r.getAttribute("attributeId") + " " +  r.getAttribute("attributeName"));

    }


<br/>

### Мой примерчик:

    private void updateSettingsInDB(){

        // Получить значение из VO

        ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.SYSTEM_OPTIONS);
        Row curRow = vo.first();

        String OrgName = (String)curRow.getAttribute(ORG_NAME);
        String Datasource = (String)curRow.getAttribute(DATA_SOURCE);


        // Удалить VO

        vo.first().remove();

        ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().postChanges();

        vo.executeQuery();

        Row row = vo.createRow();
        row.setAttribute("SoId", 1L);

        ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().postChanges();

        vo.executeQuery();

        ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().commit();


        // Установить новые значения для VO

        Row curRow1 = vo.first();

        curRow1.setAttribute(ORG_NAME, OrgName);
        curRow1.setAttribute(DATA_SOURCE, Datasource);

        ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().commit();

    }

<br/>

### Пока последний

    public void createDepartmentRecord(Integer deptId) {
        ViewObject deptVo = this.getDepartmentsVO1();
        Row newRow=deptVo.createRow();
        newRow.setAttribute("DepartmentId", deptId);
        deptVo.insertRow(newRow);
    }


http://www.awasthiashish.com/2015/07/adf-basics-how-to-invoke-model-layer.html


<br/>

### А нет, не последний.

    public void createNewRecord(Integer deptId) {
        ViewObjectImpl myVO = this.getItemSuppCountryDim1();
        Row newRow = myVO.createRow();
        newRow.setAttribute("myAttr1", value1);
        newRow.setAttribute("myAttr2", value2);
        myVO.insertRow(newRow);
    }

<br/>

Если какая-то беда с обновлением таблицы в которую добавляется строка, можно попробовать сделать следующее: 

    -- после
    myVO.insertRow(newRow);
    
    -- выполнить еще
    Row[] rows = new Row[]{newRow};
    myVO.refreshCollection(rows, true, true);
    
    
хз что это значит и почему, но сказали, что работает. Поэтому и записываю.
