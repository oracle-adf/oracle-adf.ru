---
layout: page
title: Oracle ADF > Имплементация VO с помощью java классов (IMPL)
permalink: /dev/model/vo/java-class/view-object-class/
---

# [Oracle ADF] - Имплементация VO с помощью java классов (IMPL)

<br/><br/>

**Скопировано из обучающих материалов:**


    There are three optional Java classes that can be related to a view object. The first two in the list are the
    most commonly used:

    ** View object class, which represents the component that performs the query and controls the execution lifecycle

    ** View row class, which represents each row in the query result

<br/><br/>

<br/>

<ul>

    <li><a href="/dev/model/vo/java-class/view-object-class/">Имплементация VO с помощью java классов (View object class) [Переопределение всяких execute, doDml, etc.] </a></li>

    <li><a href="/dev/model/vo/java-class/view-row-class/">Имплементация VO с помощью java классов (View object class) [Действия над каждой записи VO]</a></li>

</ul>
