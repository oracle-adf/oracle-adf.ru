---
layout: page
title: Oracle ADF > Имплементация VO с помощью java классов (View row class)
permalink: /dev/model/vo/java-class/view-row-class/
---

# [Oracle ADF] - Имплементация VO с помощью java классов (View row class)


<br/>

### Пример переопределения каждой строки


    MyVoRowImpl row = (MyVoRowImpl) ADFUtils.findIterator("MyIterator").getCurrentRow();


<br/>

### Пример переопределения каждой строки

Смотри пример в разделе локализации. Там я переопределяю значение из базы, на нужное мне из bundle.
