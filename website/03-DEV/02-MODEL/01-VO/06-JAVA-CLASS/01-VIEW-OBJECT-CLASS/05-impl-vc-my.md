---
layout: page
title: Oracle ADF > Переопределение VC
permalink: /dev/model/vo/java-class/view-object-class/impl-vc-my/
---


# [Oracle ADF] -  Переопределение VC в IMPL


**Писал я! (Необходима особая аккуратность!)**

<br/>

Нужно, чтобы на одном и том же VO в одних случаях применялось VC а в других нет.
В данном случае в зависимости от параметра из getPageFlowScope.

<br/>


    private static final String ITEM_CRITERIA_NAME = "OrdheadItemCriteria";
    private static final String ITEM_PARAMETER_NAME = "p_item";
    private static final String INPUT_PARAMETER_VC_NAME = "p_item_like";

<br/>
***
<br/>

    @Override
    public void executeQuery() {

        String itemId = (String) ADFContext.getCurrent().getPageFlowScope().get(ITEM_PARAMETER_NAME);

        AppModuleImpl am = (AppModuleImpl)this.getApplicationModule();
        ViewObjectImpl voImpl = am.getMyIterator();

        ViewCriteriaManager vcm = voImpl.getViewCriteriaManager();


        if (null != itemId){

            ViewCriteria vc = vcm.getViewCriteria(ITEM_CRITERIA_NAME);

            VariableValueManager vvm = voImpl.ensureVariableManager();

            vvm.setVariableValue(INPUT_PARAMETER_VC_NAME, itemId);            

            voImpl.applyViewCriteria(vc, true);

        } else {
            vcm.removeApplyViewCriteriaName(ITEM_CRITERIA_NAME);
        }

        super.executeQuery();
    }
