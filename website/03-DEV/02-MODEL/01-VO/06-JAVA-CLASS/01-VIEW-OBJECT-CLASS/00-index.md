---
layout: page
title: Oracle ADF > Имплементация VO с помощью java классов (View object class)
permalink: /dev/model/vo/java-class/view-object-class/
---

# [Oracle ADF] - Имплементация VO с помощью java классов (View object class)

Можно переопределять методы, вроде "create" в Implementation с помощью java. Этот вариант я пока использовал только для создания поиска на страницах, для переопределения поведения при вызове VO (происходила печать VO в консоль) и для задания значения переменных взятых из родительского ViewObjec, когда ViewObjec выстроены в иерархию.

<br/>

<ul>

    <li><a href="/dev/model/vo/java-class/view-object-class/impl-owerriding-example/">Пример переопределения метода execute, чтобы получить в консоли его содержимое и передаваемых в него пераметров</a></li>

    <li><a href="/dev/model/vo/java-class/view-object-class/impl-set-bindvariable-example/">Установить параметры по умолчанию для BindVariables во VO IMPL</a></li>


    <li><a href="/dev/model/vo/java-class/view-object-class/impl-set-attribute-after-execute-vo/">Установить значение для аттирбута после вызова execute VO</a></li>

    <li><a href="/dev/model/vo/java-class/view-object-class/impl-vc-my/">Переопределение VC (пример 1)</a></li>

    <li><a href="/dev/model/vo/java-class/view-object-class/impl-vc/">Переопределение VC (пример 2)</a></li>

    <li><a href="http://buhgalter-online.kz/files/j2ee/adf/doDML_in_EntityObject.pdf" rel="nofollow">Про полезный метод doDML в EntityObject</a></li>

    <li><a href="https://www.youtube.com/watch?v=fEswwpwwhoI" rel="nofollow">Пример с RowImpl</a>(YouTube)</li>


    <li><a href="http://adfindepth.blogspot.ru/2016/07/programmatically-execute-view-criteria.html" rel="nofollow">Programmatically execute view criteria in ADF part 1</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/07/programmatically-execute-view-criteria_27.html" rel="nofollow">Programmatically execute view criteria in ADF part 2</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/how-to-call-appmoduleimpl-class-method.html" rel="nofollow">How To Call AppModuleImpl Class Method In Been Through Binding (First Method)</a></li>


    <li><a href="http://adfindepth.blogspot.ru/2016/06/how-to-call-appmoduleimpl-class-method_26.html" rel="nofollow">How To Call AppModuleImpl Class Method In Been Through Binding (Second Method)</a></li>

</ul>


<br/>

### Ссылки на примеры с других сайтов:

<br/>

<ul>

    <li><a href="https://www.youtube.com/watch?v=fEswwpwwhoI" rel="nofollow">Пример с RowImpl</a>(YouTube)</li>

    <li><a href="http://adfindepth.blogspot.ru/2016/07/programmatically-execute-view-criteria.html" rel="nofollow">Programmatically execute view criteria in ADF part 1</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/07/programmatically-execute-view-criteria_27.html" rel="nofollow">Programmatically execute view criteria in ADF part 2</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/how-to-call-appmoduleimpl-class-method.html" rel="nofollow">How To Call AppModuleImpl Class Method In Been Through Binding (First Method)</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/how-to-call-appmoduleimpl-class-method_26.html" rel="nofollow">How To Call AppModuleImpl Class Method In Been Through Binding (Second Method)</a></li>

</ul>



<br/>

### Пример когда 2 VO объеденены в иерархию

<br/>

2 viewObject объединены между собой в иерархию. При вызове родительского, параметры передаваемые в него не передаются детям и соответственно не применяются. Необходимо переопределять инплементацию.


<ul>
    <li><a href="/dev/model/vo/java-class/view-object-class/set-vo-variable/">Здесь</a></li>
</ul>



<br/>

### Пример переопределения каждой строки

Смотри пример в разделе локализации. Там я переопределяю значение из базы, на нужное мне из bundle.
