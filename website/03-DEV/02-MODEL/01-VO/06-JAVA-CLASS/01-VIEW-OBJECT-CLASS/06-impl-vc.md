---
layout: page
title: Oracle ADF > Переопределение VC
permalink: /dev/model/vo/java-class/view-object-class/impl-vc/
---


# [Oracle ADF] -  Переопределение VC в IMPL


Писал не я!


Сделано для af:query, чтобы в некоторых случаях отображалось что-то или скрывалось. (может и нет, я не вдавался в подробности)

<br/>


    private static final String ORDER_HEAD_VIEW_CRITERIA_NAME = "OrdheadViewCriteria";

<br/>
***
<br/>

    @Override
    public ViewCriteria getViewCriteria(String name) {


        ViewCriteria vc = super.getViewCriteria(name);

        if (vc != null && ORDER_HEAD_VIEW_CRITERIA_NAME.equals(name)) {
            String criteriaItemName = "";
            String resizeCriteriaItemName = "";
            Object parameter = null;
            String disableField = null;

            // If opened tab in store detail popup
            disableField = (String) ADFContext.getCurrent().getPageFlowScope().get(DISABLE_STORE_PARAMETER_NAME);
            if (disableField != null && "Y".equals(disableField)) {
                criteriaItemName = LOCATION_CRITERIA_ITEM_NAME;
                resizeCriteriaItemName = SUPPLIER_CRITERIA_ITEM_NAME;
                parameter = ADFContext.getCurrent().getPageFlowScope().get(LOCATION_PARAMETER_NAME);
            }

            // If opened tab in supplier detail popup
            disableField = (String) ADFContext.getCurrent().getPageFlowScope().get(DISABLE_SUPPLIER_PARAMETER_NAME);
            if (disableField != null && "Y".equals(disableField)) {
                criteriaItemName = SUPPLIER_CRITERIA_ITEM_NAME;
                resizeCriteriaItemName = LOCATION_CRITERIA_ITEM_NAME;
                parameter = ADFContext.getCurrent().getPageFlowScope().get(SUPPLIER_PARAMETER_NAME);
            }

            while (vc.hasNext()) {
                ViewCriteriaRow vcr = (ViewCriteriaRow)vc.next();
                if (vcr != null) {
                    ViewCriteriaItem[] vcis = vcr.getCriteriaItemArray();
                    if (vcis != null && vcis.length > 0) {
                        for (int j = 0; j < vcis.length; j++) {
                            ViewCriteriaItem vci = vcis[j];
                            if (vci != null && criteriaItemName.equals(vci.getName())) {
                                vci.setProperty(ViewCriteriaItemHints.CRITERIA_RENDERED_MODE,
                                               ViewCriteriaItemHints.CRITERIA_RENDERED_MODE_NEVER);
                                vci.setValue(parameter);
                                vc.saveState();
                            } else if (vci != null && resizeCriteriaItemName.equals(vci.getName())){
                                vci.setProperty(AttributeHints.ATTRIBUTE_CTL_DISPLAYWIDTH,
                                               20);
                                vc.saveState();
                            }
                        }
                    }
                }
            }
        }

        return vc;
    }


<br/>
<br/>

CRITERIA_RENDERED_MODE - отображение поля в фильтре <br/>
ATTRIBUTE_CTL_DISPLAYWIDTH - ширина фильтра в символах


<br/>
<br/>

Чтобы Bind Переменная не отображалась на форме --> встать на нее  --> UI Hints --> Hide (вроде так)
