---
layout: page
title: Oracle ADF > Подставить значения по умолчанию во viewObject
permalink: /dev/model/vo/java-class/view-object-class/set-vo-variable/
---

# [Oracle ADF] - Подставить значения по умолчанию во viewObject

### 2 viewObject объединены между собой в иерархию. При вызове родительского, параметры передаваемые в него не передаются детям и соответственно не применяются.

Можно переопределить инплементацию.

<br/>

### Вариант 1

Здесь смысл такой.
Переопределяем метод prepareRowSetForQuery.
Берем родительский VO. Считываем его параметры и подставляем в параметры VO над которым работаем.


date1, date2 - передаются из одного VO в другой. Разумеется такие параметры должны быть заданы в VO.


Если вложенность как у меня большая, то предупреждаю, что в таком случае нужно брать VO - самого главного предка а не своего родителя. Надеюсь понятно написал.

    @Override
    public void prepareRowSetForQuery(ViewRowSetImpl viewRowSetImpl) {

        AppModuleImpl am = (AppModuleImpl)this.getApplicationModule();
        ViewObjectImpl masterRows = am.getMyParentVO();

        if (masterRows != null && masterRows.getCurrentRow() != null) {

            Date date1 = (Date) masterRows.ensureVariableManager().getVariableValue("date1");
            Date date2 = (Date) masterRows.ensureVariableManager().getVariableValue("date2");

            viewRowSetImpl.ensureVariableManager().setVariableValue("date1", date1);
            viewRowSetImpl.ensureVariableManager().setVariableValue("date2", date2);
        }

        super.prepareRowSetForQuery(viewRowSetImpl);
    }




<br/>

### Вариант 2


Я не разобрался до конца.  
Вижу, что параметры приходят из PageFlowScope.

Такой ваиант работал плохо. При его использовании ломался AppModule.

    @Override
    protected void executeQueryForCollection(Object qc, Object[] params, int noUserParams) {

        for (Object p : params) {

            if ( ((Object[])p)[0].toString().equals("p_day_from")    ) {

                    ((Object[])p)[1] = getJboDateFromUtilDate((java.util.Date) ADFContext.getCurrent().getPageFlowScope().get("p_day_from"));
                }
                if ( ((Object[])p)[0].toString().equals("p_day_to")    ) {

                        ((Object[])p)[1] =getJboDateFromUtilDate((java.util.Date)ADFContext.getCurrent().getPageFlowScope().get("p_day_to"));
                    }
                }

        super.executeQueryForCollection(qc, params, noUserParams);
    }


<br/>
<br/>

        executeQueryForCollection

        protected void executeQueryForCollection(java.lang.Object qc,
                                                 java.lang.Object[] params,
                                                 int noUserParams)


This method is invoked right before the row set executes the query. If this method is overridden, the custom logic will be applied to all row sets.

In contrast, if the user overrides the view object's executeQuery(), the custom logic in it only applies only when the user calls executeQuery() on the view object. If he calls executeQuery() on secondary row sets, the custom logic in executeQuery() will not apply.

Parameters:

    qc - the query collection about to execute the query.
    params - the bind parameters that will be applied to the query.
    noUserParams - the number of user bind parameters supplied through the setWhereClauseParam calls.    


<br/><br/>

Setting the bind variable value for the destination View Object in the View Link  
http://www.jobinesh.com/2011/05/setting-bind-variable-value-for.html  
http://www.awasthiashish.com/2014/08/use-view-link-accessor-to-call.html  
