---
layout: page
title: Oracle ADF > Установить параметры по умолчанию для BindVariables во VO IMPL
permalink: /dev/model/vo/java-class/view-object-class/impl-set-bindvariable-example/
---


# [Oracle ADF] - Установить параметры по умолчанию для BindVariables во VO IMPL


Есть VO, в котором заданы BindVariables.

Нужно установить в них параметры по умолчанию, если они не заданы.

Например date1, date2

<br/>

{% highlight java linenos %}

@Override
public void prepareRowSetForQuery(ViewRowSetImpl vrs) {

    vv = vrs.ensureVariableManager().getVariableValue("date2");
    if (vv == null) {

        // Как пример, можно взять переменнуи и из pageflowscope
	    // String item = (String) ADFContext.getCurrent().getPageFlowScope().get("item");

        java.util.Date day_local = new java.util.Date();
        vrs.ensureVariableManager().setVariableValue("date2", day_local);
    }

    super.prepareRowSetForQuery(vrs);
}

{% endhighlight %}
