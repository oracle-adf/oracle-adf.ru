---
layout: page
title: Oracle ADF > Пример переопределения метода execute, чтобы получить в консоли его содержимое и передаваемых в него пераметров
permalink: /dev/model/vo/java-class/view-object-class/impl-owerriding-example/
---


# [Oracle ADF] - Пример переопределения execute в IMPL

ViewObject -> Java -> Java Classes

Generate View Object Class

Include bind vatiable accesson

<br/>


{% highlight java linenos %}

import oracle.jbo.Variable;
import oracle.jbo.VariableValueManager;
import oracle.jbo.server.ViewObjectImpl;

***

public void dumpQueryAndParameters()
{
    // get the query in it's current state
    String lQuery = getQuery();

    //get Valriables
    VariableValueManager lEnsureVariableManager = ensureVariableManager();
    Variable[] lVariables = lEnsureVariableManager.getVariables();
    int lCount = lEnsureVariableManager.getVariableCount();

    // Dump query
    System.out.println("---query--- " + lQuery);
    // if variables found dump them
    if (lCount > 0)
    {
        System.out.println("---Variables:");
        for (int ii = 0; ii < lCount; ii++)
        {
            Object lObject = lEnsureVariableManager.getVariableValue(lVariables[ii]);
            System.out.println("  --- Name: " + lVariables[ii].getName() + " Value: " +
                               (lObject != null ?  lObject.toString() : "null"));
        }
    }
}


@Override
public void executeQuery() {
    dumpQueryAndParameters();
    super.executeQuery();
}


{% endhighlight %}


<br/>

Перед execute выполняется наш метод.  
Он пишет в консоль ViewObject и переменные.

Если, что у нас в утилитах лежит метод VOUtils.PrintVO(vo) - который делает тоже самое без переопределения.
