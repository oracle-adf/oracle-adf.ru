---
layout: page
title: Oracle ADF > Установить значение для аттирбута после вызова execute VO
permalink: /dev/model/vo/java-class/view-object-class/impl-set-attribute-after-execute-vo/
---


# [Oracle ADF] - Установить значение для аттирбута после вызова execute VO


{% highlight java linenos %}


private static final int MAX_PRINT = 25;

@Override
public void executeQuery() {


    System.out.println();
    System.out.println("EXECUTE QUERY");

    super.executeQuery();
    afterExecuteVO();


    System.out.println("EXECUTE QUERY");
    System.out.println();

}

public void afterExecuteVO(){        
        printViewObjectAttributeValue(super.getViewObject(), "myAttr");
        setVoData(super.getViewObject(), "myAttr");
        printViewObjectAttributeValue(super.getViewObject(), "myAttr");
}


private static void setVoData(ViewObject vo, String myAttr){

    int maxRowForPrint = 25;

       vo.reset();
       vo.first();

    System.out.println("=== VO DATA =====");


       while ((vo.getCurrentRow() != null)) {

            Row row = vo.getCurrentRow();

            row.setAttribute(myAttr, "Я устанавливаю аттрибут программно");

           vo.next();

        }

        System.out.println("=== NO MORE DATA =====");
}


private static void printViewObjectAttributeValue(ViewObject vo, String attrName){

    int maxRowForPrint = MAX_PRINT;

     vo.reset();
     vo.first();

    System.out.println("=== VO ATTRIBUTE DATA =====");

       while ((vo.getCurrentRow() != null) && (maxRowForPrint > 0)) {

            Row row = vo.getCurrentRow();
            String rowDataStr = "";

                 Object attrData = row.getAttribute(attrName);
                 rowDataStr += (attrData + "\t");

              System.out.println((vo.getCurrentRowIndex() + 1) + ") " + rowDataStr);

           vo.next();

            maxRowForPrint--;
        }

        System.out.println("=== VO ATTRIBUTE DATA =====");
}


{% endhighlight %}
