---
layout: page
title: Oracle ADF - Распечатать ViewCriteria Items в бине
permalink: /dev/model/vo/vc/print-view-criteria-items/
---

<br/>

# [Oracle ADF] Распечатать ViewCriteria Items

<br/>

    VOUtils.printViewCriteriaItems(vc);

<br/>

    public static void printViewCriteriaItems(ViewCriteria vc) {

        List list = vc.getRows();

        Iterator iter1 = list.iterator();
        while (iter1.hasNext()) {

              ViewCriteriaRow row = (ViewCriteriaRow)iter1.next();

              List vcitems = row.getCriteriaItems();
              Iterator itemiter = vcitems.iterator();

             while (itemiter.hasNext()) {

               ViewCriteriaItem vcitem = (ViewCriteriaItem)itemiter.next();
               System.out.println("vcitemname in vcrow:"+vcitem.getName());
               System.out.println("vcitemvalue in vcrow:"+vcitem.getValue().toString());
             }
        }
    }

По материалам:

http://www.learnoracleadf.com/2015/04/adf-view-criteria-complex-code-snippets.html
