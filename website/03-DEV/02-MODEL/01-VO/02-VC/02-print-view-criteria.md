---
layout: page
title: Oracle ADF - Распечатать ViewCriteria для ViewObject в консоли
permalink: /dev/model/vo/vc/print-view-criteria/
---

<br/>

# [Oracle ADF] Распечатать ViewCriteria примененные к VO в консоли


Берем из Util нужный метод.  
https://bitbucket.org/oracle-adf/adf-utils/blob/master/VOUtils.java

// В общем все

    VOUtils.printViewCriteriaNames(vo);


// Только те, что были применены

    VOUtils.printApplyViewCriteriaNames(vo);
