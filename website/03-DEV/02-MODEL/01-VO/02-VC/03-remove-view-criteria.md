---
layout: page
title: Oracle ADF - Удалить ViewCriteria
permalink: /dev/model/vo/vc/remove-view-criteria/
---

<br/>


# [Oracle ADF] Удалить ViewCriteria


<br/>

    vcm.removeApplyViewCriteriaName(CONSTANTS_VC.MY_VC_NAME);

<br/>

    vcm.removeViewCriteria(vc.getName());

<br/>

    vcm.removeApplyViewCriteriaName(vc.getName());


<br/>

### Удалить все ViewCriteria

    vcm.clearViewCriterias();

http://adfcodebits.blogspot.ru/2010/04/bit-7-reseting-view-criteria-associated.html
