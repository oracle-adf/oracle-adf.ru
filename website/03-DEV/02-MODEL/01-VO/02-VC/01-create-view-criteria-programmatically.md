---
layout: page
title: Oracle ADF Создать программно ViewCriteria
permalink: /dev/model/vo/vc/create-view-criteria-programmatically/
---

# [Oracle ADF] Создать программно ViewCriteria

ADFUtils лежат в репозитории <a href="https://bitbucket.org/oracle-adf/adf-utils">Utils</a>


UPD: Не рекомендую так делать Пусть лучше VC будет создано декларативно на VO и всегда будет видно, что к VO может быть применено VC прям в IDE.

UPD:

Можно писать сразу вот так (без всяких равно):

{% highlight java linenos %}

vcr1.setAttribute("AttrubuteName", input);

{% endhighlight %}

<br/>

{% highlight java linenos %}

private void applyAlphabetFilter(String input){

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.VO_NAME);

    ViewCriteria vc = vo.createViewCriteria();
    vc.setName(CONSTANTS_VC.VC_NAME);

    ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
    vcr1.setAttribute("AttrubuteName","= " + input);
    vc.add(vcr1);

    vo.applyViewCriteria(vc, true);
    vo.executeQuery();

    VOUtils.printViewObjectInfo(vo);

    VOUtils.clearViewCriteriaByName(vo, CONSTANTS_VC.VC_NAME);

    AdfFacesContext.getCurrentInstance().addPartialTarget(this.myComponent);
}

{% endhighlight %}


AttrubuteName - Case Sensitive. Смотри его в аттрибутах ViewObject.


<br/>
<br/>

На самом деле, мне нужно было, чтобы начиналось с входящего параметра.  
Получилось вот так:

    vcr1.setAttribute("AttrubuteName", input + "%");

можно было бы сделать при необходимости и

    vcr1.setAttribute("AttrubuteName", input.toUpperCase() + "%");

<br/>
<br/>

Еще варианты с community:

https://community.oracle.com/thread/3916170?start=0&tstart=0

<br/>

{% highlight java linenos %}

    ViewCriteria vc = yourViewObject.createViewCriteria();  
    ViewCriteriaRow vcRow = vc.createViewCriteriaRow();  
    ViewCriteriaItem vcItem = vcRow.ensureCriteriaItem("YourDateAttr");  
    vcItem.setOperator(JboCompOper.OPER_BETWEEN);  
    vcItem.getValues().get(0).setValue(first_date);  
    vcItem.getValues().get(1).setValue(second_date);  
    vc.add(vcRow);  

{% endhighlight %}


<br/>



### Еще 1 рабочий вариант

{% highlight java linenos %}

public void onMyBeanPriorityChanged (ValueChangeEvent valueChangeEvent) {

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.VO_NAME);
    ViewCriteria vc = vo.createViewCriteria();

    vc.setName("MyViewCriteriaName_VC");

    ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();

    // -----------------------------------------

    ViewCriteriaItem priority = vcr1.ensureCriteriaItem("Priority");

    priority.setOperator("=");

    // priority.getValues().get(0).setValue(new Integer(1));
    priority.getValues().get(0).setValue("1");

    // -------------------------------

    vc.add(vcr1);

    // -------------------------------

    vo.applyViewCriteria(vc);
    vo.executeQuery();
}

{% endhighlight %}


https://docs.oracle.com/middleware/1213/adf/api-reference-model/oracle/jbo/ViewCriteria.html


<!--

https://udayarocks.wordpress.com/2011/06/08/how-to-reuse-a-view-object-vo-for-different-search-criteria-in-multiple-pages-in-adf/

-->

<br/>

### Еще один пример, уже OR


{% highlight java linenos %}

ViewCriteria vc = vo.createViewCriteria();
vc.setName("MyViewCriteriaName_VC");

ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
ViewCriteriaRow vcr2 = vc.createViewCriteriaRow();

// -----------------------------------------
// vcr1.setAttribute("Priority","= 1");
vcr1.setAttribute("Priority","IN (1,2)");

vcr2.setAttribute("Id","= 22");

// -------------------------------

vc.add(vcr1);
vc.add(vcr2);

// -------------------------------

vo.applyViewCriteria(vc);
vo.executeQuery();

{% endhighlight %}


<br/>

**Вот такой пример остался:**


{% highlight java linenos %}

public static void myMethod(ArrayList<String> selectedListElements){

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_VO);

    ViewCriteria vc = vo.createViewCriteria();
    vc.setName(CONSTANTS_VC.MY_VC);

    if (selectedListElements.contains("1")){
        ViewCriteriaRow vcr1 = vc.createViewCriteriaRow();
        vcr1.setAttribute("Priority","= 1");
        vc.add(vcr1);
    }

    if (selectedListElements.contains("2")){
        ViewCriteriaRow vcr2 = vc.createViewCriteriaRow();
        vcr2.setAttribute("Priority","= 2");
        vc.add(vcr2);
    }

    if (selectedListElements.contains("3")){
        ViewCriteriaRow vcr3 = vc.createViewCriteriaRow();
        vcr3.setAttribute("Priority","= 3");
        vc.add(vcr3);
    }

    vo.applyViewCriteria(vc, true);
    vo.executeQuery();
}

{% endhighlight %}


<br/>

#№№ Чтобы выполнялось условие AND, просто добавляем аттрибуты:

{% highlight java linenos %}

    vcr1.setAttribute("Priority","= 1");
    vcr1.setAttribute("Priority","IN (1,2)");

{% endhighlight %}


<br/>
<br/>

Самый офигенный пример.   
http://sqltech.cl/doc/oas10gR31/web.1013/b25947/bcquerying008.htm

И я даже сделал его копию  
https://archive.is/rHwyD

Если нужно разобраться с named критериями (давно разобрался).
http://myadfexp.blogspot.ru/2013/04/programmatically-applying-view-criteria.html  
http://sqltech.cl/doc/oas10gR31/web.1013/b25947/bcadvvo004.htm


Working with Named View Criteria Programmatically  
https://www.youtube.com/watch?v=6zv7NXUsZnc

http://andrejusb.blogspot.ru/2010/03/applying-view-criteria-from-application.html

Может быть что-то есть:  
http://orafmtech.blogspot.ru/2011/03/view-criteria-explained.html  
https://ravindranathi.wordpress.com/tag/adf-view-criteria/

Нужно поглубже почитать:  
http://adfpractice-fedor.blogspot.ru/2012/09/dynamic-view-criterias-from-where.html
