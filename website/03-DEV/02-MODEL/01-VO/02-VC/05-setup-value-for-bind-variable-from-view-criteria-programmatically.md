---
layout: page
title: Oracle ADF Задать программно значение bind variable, определенной во ViewCriteria декларативно
permalink: /dev/model/vo/vc/setup-value-for-bind-variable-from-view-criteria-programmatically/
---

# [Oracle ADF] Задать программно значение bind variable, определенной во ViewCriteria декларативно

<br/>

![View Criteria With Bind Variables](/img/view-object/view-criteria/ViewCriteria1.png "View Criteria With Bind Variables"){: .center-image }

<br/><br/>

![View Criteria With Bind Variables](/img/view-object/view-criteria/ViewCriteria2.png "View Criteria With Bind Variables"){: .center-image }

<br/>

```java

public void myButtonPressed(ActionEvent actionEvent) {

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.VO_NAME);
    VariableValueManager vvm = vo.ensureVariableManager();

    vvm.setVariableValue(ITEM_SHOULD_CONTAINS, "МАРКЕТИНГ");

    ViewCriteriaManager vcm = vo.getViewCriteriaManager();
    ViewCriteria vc = vcm.getViewCriteria(CONSTANTS_VC.MY_SUPER_PUPER_VC);

    vo.applyViewCriteria(vc,true);
    vo.executeQuery();


    -- Убрать если после execute VC больше не нужно
    -- VOUtils.clearViewCriteriaByName(vo, CONSTANTS_VC.INDICATOR_DETAIL__CALC_RESULTS__NBOX_VC);
}

```

<br/>

А если потом нужно убрать

<br/>

{% highlight java linenos %}

public void myButtonPressed(ActionEvent actionEvent) {

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.VO_NAME);
    VOUtils.clearViewCriteriaByName(vo, CONSTANTS_VC.INDICATOR_DETAIL__CALC_RESULTS__NBOX_VC);
    vo.executeQuery();

}

{% endhighlight %}

<br/>

**Еще пример остался:**

```java

public static void tasksFilterByPriority2(ArrayList<String> selectedListElements){

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_VO);

    VariableValueManager vvm = vo.ensureVariableManager();


    if (selectedListElements.contains("1")){
        vvm.setVariableValue(priorityId, "1");
    }

    if (selectedListElements.contains("2")){
        vvm.setVariableValue(priorityId, "2");
    }

    if (selectedListElements.contains("3")){
        vvm.setVariableValue(priorityId, "3");
    }

    ViewCriteriaManager vcm = vo.getViewCriteriaManager();
    ViewCriteria vc = vcm.getViewCriteria(CONSTANTS_VC.MY_VC);

    vo.applyViewCriteria(vc,true);
    vo.executeQuery();

    VOUtils.clearViewCriteriaByName(vo, CONSTANTS_VC.MY_VC);

}

```
