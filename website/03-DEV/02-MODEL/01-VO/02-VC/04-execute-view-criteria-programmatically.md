---
layout: page
title: Oracle ADF VC создано декларативно. Нужно применить его программно при каком-то событии
permalink: /dev/model/vo/vc/execute-view-criteria-programmatically/
---

# [Oracle ADF] VC создано декларативно. Нужно применить его программно при каком-то событии


<br/>


Имеем, созданные ViewCriteria декларативно.    
Одно из них с именем ordersYesterDay.  
Нужно применить его к ViewObject в бине.  

<br/>


{% highlight java linenos %}

import oracle.jbo.ViewObject;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewCriteria;


***

public void myMethod (ValueChangeEvent valueChangeEvent) {

    ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.VO_NAME);

    ViewCriteriaManager vcm = vo.getViewCriteriaManager();
    ViewCriteria vc = vcm.getViewCriteria(CONSTANTS_VC.VC_NAME);

    vo.applyViewCriteria(vc);
    vo.executeQuery();
}

{% endhighlight %}

<br/>

Спасибо за умные мысли:  
http://www.oracle-adf.info/2014/01/viewcriteria-java-jdeveloper.html
