---
layout: page
title: Oracle ADF - Использование SQL конструкций во ViewCriteria
permalink: /dev/model/vo/vc/sql-constructions-in-view-criteria/
---

<br/>

# [Oracle ADF] Использование SQL конструкций во ViewCriteria


По материалам:

http://andrejusb.blogspot.ru/2013/07/advanced-view-criteria-implementation.html


Скриншоты с блога Андреуса


<br/>

<img src="http://3.bp.blogspot.com/-0_Osp-vzBAc/UdCeU2Lwi3I/AAAAAAAAJys/82T7NnmHIUk/s910/1.png" alt="Использование во VC SQL конструкций" border="0" />


<img src="http://4.bp.blogspot.com/-YS0WANFfe-Q/UdCfhiEiHjI/AAAAAAAAJy8/PGUD916zw0o/s912/2.png" alt="Использование во VC SQL конструкций" border="0" />


<img src="http://3.bp.blogspot.com/-Zu_LeAZ2Pvw/UdCfxfJv8kI/AAAAAAAAJzE/xs38imoeLLA/s591/5.png" alt="Использование во VC SQL конструкций" border="0" />


<br/><br/>


### Вариант 1. (Не нужно передавать переменные в запрос)

<br/>


**Нужно в исходниках VO для VC явно прописывать. IsSqlFragment="true" и UpperColumns="0"**

И в некоторых случаях явно указывать оператор, если он отстутствует в списке предлагаемых. В моем случае это Operator="IN"

UpperColumns="0" тоже должен быть определен. Хз, что он делает но без него не работает.


В общем такая возможность понадобилась для ограничения выводимых данных. Есть таблица, в которой перечислены объекты, с которыми может работать пользователь. С помощью создаваемого VC, добавляется конструкция, которая сообщаяет, что нужно выводить только те наборы данных, где есть соответствие с другой таблицей.

    <ViewCriteriaRow
       Name="StoreViewSQLCriteria_row_0"
       UpperColumns="1">
       <ViewCriteriaItem
         Name="StoreSearch"
         ViewAttribute="StoreSearch"
         Operator="IN"
         Conjunction="AND"
         IsSqlFragment="true"
         Value="(select t.allowed_shop from filter_test_vc t)"
         Required="Optional"
         UpperColumns="0"/>
     </ViewCriteriaRow>


У меня также заработал вот такой вариант.

    <ViewCriteriaRow
      Name="LoggenInUserAndPermissionVC_DEFAULT_row_0"
      UpperColumns="1">
      <ViewCriteriaItem
        Name="StoreSearch"
        ViewAttribute="StoreSearch"
        Operator="IN"
        Conjunction="AND"
        Required="Optional">
        <CompOper
          Name="StoreSearch_Oper"
          ToDo="-2"
          Oper="RemoveAll"
          MinCardinality="1"
          MaxCardinality="1"/>
        <CompOper
          Name="StoreSearch_Oper1"
          ToDo="1"
          OperDescStrCode="StoreSearch_Oper1_CompOper_0"
          Oper="IN"
          MinCardinality="0"
          MaxCardinality="0">
          <TransientExpression
            trustMode="trusted"
            Name="ExpressionScript"><![CDATA[ return " IN (select t.allowed_shop from filter_test_vc t)"]]></TransientExpression>
        </CompOper>
      </ViewCriteriaItem>


Делал его способом описанным ниже.


<br/>

### Вариант 2. (Нужно передавать переменные в запрос)


Нужно зайти в Custom Operations и заполнить поля приблизительно сделующим образом.

<img src="http://storage2.static.itmages.ru/i/16/0127/h_1453907975_2425838_55f256d934.png" alt="Использование во VC SQL конструкций" border="0" />

При добавлении строки, убрать галочку "save expression to groovy file"


Потом выбрать созданный оператор из списка. Он будет выглядеть несколько "необычно".

У меня получился вот такой результат:

    <ViewCriteriaItem
      Name="StoreSearch"
      ViewAttribute="StoreSearch"
      Operator="IN"
      Conjunction="AND"
      Required="Optional">
      <CompOper
        Name="StoreSearch_Oper"
        ToDo="-2"
        Oper="RemoveAll"
        MinCardinality="1"
        MaxCardinality="1"/>
      <CompOper
        Name="StoreSearch_Oper1"
        ToDo="1"
        OperDescStrCode="StoreSearch_Oper1_CompOper_0"
        Oper="IN"
        MinCardinality="1"
        MaxCardinality="1">
        <TransientExpression
          Name="ExpressionScript"><![CDATA[ return " IN (select t.allowed_shop from filter_test_vc t where t.login=:user_name)"]]></TransientExpression>
      </CompOper>
    </ViewCriteriaItem>


И собственно переменная.

      <Variable
        Name="user_name"
        Kind="where"
          Type="String"
          DefaultValue="myUser">
        <Properties>
          <SchemaBasedProperties>
            <DISPLAYHINT
              Value="Hide"/>
          </SchemaBasedProperties>
        </Properties>
      </Variable>



Осталось заменить Default значение данными из SecurityContext.

<br/>
<br/>


Upd. можно и названия вызываемых фукнций писать.

    Как например,  (select myFunc from dual)

Или даже лучше просто myFunc
