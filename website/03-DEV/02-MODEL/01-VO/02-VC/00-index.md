---
layout: page
title: Oracle ADF - ViewCriteria
permalink: /dev/model/vo/vc/
---


<br/>

# Oracle ADF > ViewCriteria


<br/><br/>

UPD 1:

View Criteria на странице VO которые используются по умолчанию, называть вроде "DEFAULT_LOGGED_IN_USER_VC".

<br/>

UPD 2: VC должны быть созданы декларативно, а не создавться где-то произвольно в коде. Иначе потом некоторым программистам может быть сложно понять, что, почему и где происходит с VO.

В коде нужно получать это VC и уже в него подставлять данные. И заодно будет сразу видно какие могут быть применены VC к VO.



<br/><br/>

**VC (ViewCriteria)**

Такие о***ные штуки эти ViewCriteria. Их можно насоздавать декларативно в параметрах ViewObject, нужные добавить в application модуль, чтобы они выполнялись по умолчанию, а остальные по мере необходимости вызывать программно. Можно присваивать им имена, устанавливать и сбрасывать их программно.

ViewObject хранит список присвоенных ему ViewCriteria.

Помимо всего прочего, можно в них использовать Expression Language, Groovy и даже SQL конструкции.


<br/>

Добавить к VO дополнительный VC.

Для этого достаточно добавить в вызов в качестве второго параметра true

    vo.applyViewCriteria(vc,true);

Если нужно перезаписать

    vo.applyViewCriteria(vc);



<br/>
<br/>

**Basics:**

<br/>

<ul>
    <li><a href="/dev/model/vo/vc/execute-view-criteria-programmatically/">VC создано декларативно. Нужно применить его программно при каком-то событии</a></li>
    <li><a href="/dev/model/vo/vc/setup-value-for-bind-variable-from-view-criteria-programmatically/">Задать программно значение bind variable, определенной во ViewCriteria декларативно</a></li>
    <li><a href="/dev/model/vo/vc/create-view-criteria-programmatically/">Создать программно ViewCriteria</a> (Не рекомендую так делать)</li>
    <li><a href="/dev/model/vo/vc/print-view-criteria/">Распечатать ViewCriteria для ViewObject в консоли</a></li>
    <li><a href="/dev/model/vo/vc/remove-view-criteria/">Удалить (отменить применение) ViewCriteria</a></li>
    <li><a href="http://andrejusb.blogspot.ru/2010/02/default-value-for-date-field-in-query.html" rel="nofollow">Задание ViewCriteria декларативно (на примере задания даты)</a></li>
    <li><a href="http://techutils.in/2014/07/15/adf-programatically-applying-and-creating-view-criteria/" rel="nofollow">ADF – Programatically Applying and Creating View Criteria</a></li>
</ul>


<br/>

**Advanced:**

<br/>

<ul>

    <li><a href="/dev/model/vo/vc/sql-constructions-in-view-criteria/">Использование SQL конструкций во ViewCriteria</a></li>

    <li><a href="https://docs.oracle.com/middleware/1213/adf/api-reference-model/oracle/jbo/ViewCriteriaRow.html" rel="nofollow">Class ViewCriteriaRow</a></li>

    <li><a href="http://adfpractice-fedor.blogspot.ru/2012/09/dynamic-view-criterias-from-where.html" rel="nofollow">Dynamic view criterias from where clauses</a></li>

    <li><a href="/dev/model/vo/vc/print-view-criteria-items/">Распечатать ViewCriteria Items в бине</a></li>

    <li><a href="http://www.learnoracleadf.com/2013/05/programatic-handle-to-adf-query_5.html" rel="nofollow">Programatic handle to ADF Query Component - Add View Criteria dynamically</a></li>

    <li><a href="https://middlewarebylink.wordpress.com/2012/05/26/how-to-use-a-view-criteria-with-the-in-operator/" rel="nofollow">How to use a View Criteria with the IN operator</a></li>

    <li><a href="http://techutils.in/2015/08/19/insert-rows-in-adf-view-object-programatically/" rel="nofollow">Insert Rows in ADF View Object Programatically</a></li>

    <li><a href="http://adfcodebits.blogspot.ru/2010/11/bit-25-hiding-view-criteria-item-based.html" rel="nofollow">Hiding a View Criteria item based on some condition</a></li>

</ul>
