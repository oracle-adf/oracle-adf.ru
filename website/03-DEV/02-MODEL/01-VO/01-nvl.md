---
layout: page
title: Oracle ADF - Обработка nvl во viewObject
permalink: /dev/model/vo/nvl/
---


<br/>

# [Oracle ADF] Обработка nvl во viewObject:


Во view object определены переменные, нужно чтобы если они не заданы, не возникало null pointer exception.

    and my.day_ >= nvl(:p_day_from, trunc(sysdate) - 7)         
    and my.day_ <= nvl(:p_day_to, trunc(sysdate))   


<br/>

### Еще примеры:

Есть код:

    and trunc(h.some_date) between :date1 and :date2

Нужно, чтобы по умолчанию было с начала года по сегодняшнюю дату.

Пробуем...

    and trunc(h.some_date) between nvl(:date1, TRUNC (SYSDATE, 'Y')) and nvl(:date2, TRUNC (SYSDATE))

<br/>

### И последний, наиболее часто используемый:

    WHERE mytable.id = nvl(:myId, mytable.id)

<br/>
<br/>

<strong><a href="https://plsql.ru/other/dates/">Вычисление дат в Oracle</a></strong>
