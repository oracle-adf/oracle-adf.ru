---
layout: page
title: Oracle ADF Commit
permalink: /dev/model/vo/commit/
---

# [Oracle ADF] Commit

<br/>

Сначала смотри <a href="/dev/model/am/commit/">сюда</a>

Потом <a href="/dev/view/bindings/bindings/action/">сюда</a>

<br/>

Если ничего не подошло, может здесь есть еще какие-нибудь примеры:

<br/>

### Пример 4

```java
public void copyPresentAddress(ValueChangeEvent vce){
    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    Boolean sameAsPresentAddress = sameAddressCheckbox.isSelected();
    if(sameAsPresentAddress){
        ADFUtil.invokeEL("#{bindings.copyPresentAddress.execute}");
    } else{
        ADFUtil.invokeEL("#{bindings.resetPermanentAddress.execute}");
    }
}
```

<br/>

### Пример 5

    DBTransaction dbt =
        (DBTransaction) ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl")
        .getTransaction();
    ViewRowImpl vri =
        (ViewRowImpl) ADFUtils.findIterator("MyIterator").getCurrentRow();

    if (vri.getAttribute("MyAttribute").equals("P")) {
        vri.setAttribute("MyAttribute", "MyNewAttribute");
    }

    dbt.commit();

<br/>

### postChanges - сохранить изменения но не коммитить в базу

<br/>

**Примре 1**

    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().postChanges();

<br/>

**Примре 2**

    AppModuleImpl am = getAppModuleImpl();
    DBTransaction dbT = am.getDBTransaction();

    dbT.postChanges();

Если не порименить commit или rollback, то после завершении сессии данные потеряются.
Лучше завершать commit или rollback

<br/>

**Примре 3**

        AppModuleImpl am = (AppModuleImpl) ADFUtils.getApplicationModuleForDataControl(APP_MODULE_DATA_CONTROL);
        ViewObject voOrdersShedule = am.findViewObject("MyVO");

        ***

        am.getDBTransaction().postChanges();
