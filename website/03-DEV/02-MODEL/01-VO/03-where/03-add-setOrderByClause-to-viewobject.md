---
layout: page
title: (ORDER BY) Упорядочить ViewObject в бине
permalink: /dev/model/vo/where/add-setOrderByClause-to-viewobject/
---


# (ORDER BY) Упорядочить ViewObject в бине

<br/>

Базовый пример, может выглядить вот так

    String orderStr1 = " MY_DATE ";
    vo.setOrderByClause(orderStr1 + "desc");
    vo.executeQuery();

<br/>
<br/>


Имеем SelectOneChoice в котором можно выбрать порятк отображения данных из ViewObject.<br/>
Следующим образом, можно реализовать сортировку.

Ла я знаю, что можно и получше сделать.<br/>
Пока так.

    public void onTasksOrderChanged (ValueChangeEvent valueChangeEvent) {

        ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject vo = am.findViewObject("Tasks_List");

        switch (valueChangeEvent.getNewValue().toString()) {
                case "1":  sortByDate(vo);
                         break;
                case "2":  sortByPriority(vo);
                         break;
            }
    }


    private void sortByDate(ViewObject vo){

        String orderStr1 = " MY_DATE ";
        String orderStr2 = " MY_PRIORITY ";

        vo.setOrderByClause(orderStr1 + " desc, "  + orderStr2 );

        vo.executeQuery();
    }

    private void sortByPriority(ViewObject vo){

        String orderStr1 = " MY_PRIORITY ";
        String orderStr2 = " MY_DATE ";
        vo.setOrderByClause(orderStr1 + ", "  + orderStr2 + "desc");
        vo.executeQuery();
    }


Здесь описан еще и пример с setSortBy:
https://blogs.oracle.com/imc/entry/adf_bussiness_components_in_memory
