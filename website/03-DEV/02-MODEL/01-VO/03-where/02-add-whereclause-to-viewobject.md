---
layout: page
title: Добавить и применить Where Clause к ViewObject в бине
permalink: /dev/model/vo/where/add-whereclause-to-viewobject/
---


# Добавить и применить Where Clause к ViewObject в бине

<br/>

    public void onMethodRun (ValueChangeEvent valueChangeEvent) {

        ViewObject vo = VOUtils.getViewObjectByName(CONSTANTS_VO.MY_SUPER_VO);


            //        1 - Текущий день
            //        2 - Вчера    
            //        3 - Текущая неделя
            //        4 - Текущий месяц
            //        5 - Текущий квартал
            //        6 - Текущий год

            String fiterByDateChose = valueChangeEvent.getNewValue().toString();

            String WhereClauseParams = "";

            switch (fiterByDateChose) {
                    case "1":  WhereClauseParams = "TRUNC(DATE_FROM_DB_TABLE) = TRUNC(SYSDATE)";
                             break;
                    case "2":  WhereClauseParams = "TRUNC(DATE_FROM_DB_TABLE) = TRUNC(SYSDATE-1)";
                             break;
                    case "3":  WhereClauseParams = "TRUNC(DATE_FROM_DB_TABLE) BETWEEN TRUNC (SYSDATE, 'D') AND TRUNC(SYSDATE)";
                             break;
                    case "4":  WhereClauseParams = "TRUNC(DATE_FROM_DB_TABLE) BETWEEN TRUNC (SYSDATE, 'MM') AND TRUNC(SYSDATE)";
                             break;
                    case "5":  WhereClauseParams = "TRUNC(DATE_FROM_DB_TABLE) BETWEEN TRUNC (SYSDATE, 'Q') AND TRUNC(SYSDATE)";
                             break;
                    case "6":  WhereClauseParams = "TRUNC(DATE_FROM_DB_TABLE) BETWEEN TRUNC (SYSDATE, 'Y') AND TRUNC(SYSDATE)";
                             break;
                    default: WhereClauseParams = null;
                             break;
                }

            vo.setWhereClause(WhereClauseParams);

        vo.executeQuery();
        updateFormProgrammatically();
    }



<br/>

###  Удалить WhereClause

    vo.setWhereClause(null);


<br/>

###  Вроде каждому WhereClause можно задать индивидуальное имя конструкцией

    vo.setNamedWhereClauseParam(“some_param”, anyParam);

нужно проверить как-нибудь.




<br/><br/>

### Еще варианты:


    //Get ViewObject
    ViewObject vo = iter.getDepartments();

    //Apply desired WHERE Clause and declare bind variable name
    vo.setWhereClause("DEPARTMENT_NAME=:BindDepartmentName");

    //Define this variable as ViewObject Bind Variable , You can also set default value for this
    vo.defineNamedWhereClauseParam("BindDepartmentName", "Purchase", null);

    // Execute ViewObject to finally apply where clause
    vo.executeQuery();

http://www.awasthiashish.com/2015/07/adf-baiscs-define-and-remove-named-bind.html


<br/>

Правда у меня, эта хреновина не заработала пока.
