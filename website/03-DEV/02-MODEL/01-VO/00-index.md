---
layout: page
title: Oracle ADF - ViewObject
permalink: /dev/model/vo/
---


<br/>

# Oracle ADF > ViewObject

**Примеры применения к ViewObject критериев для выборки, фильтрации, группировок (ViewCriteria, Where, GroupBy, и т.д.):**


<br/><br/>


<div align="center" style="border-width: 4px; padding: 10px; border-style: inset; border-color: green; ">

<b>Чтобы в консоль печатался ViewoObject при его вызове</b>, можно установить параметр -Djbo.debugoutput=console в настройках Jdeveloper.

ViewController --> Run/Debug --> Edit Run Configuration --> Java Options --> <strong>-Djbo.debugoutput=console</strong>

<br/><br/>

Необходимо после этого перестартовать домен weblogic. Особенно актуально, когда приходится работать с Rest сервисами.

</div>


<!-- ViewObject:



entities -- Assoc -->

<!--

In addition to the entity objects, the Business Components from Tables wizard also generates
named association components that capture information about the relationships between entity
objects. JDeveloper derives default association names like SCountriesFkAssoc by converting the
foreign key constraint names to a Java-friendly name and adding the Assoc suffix. For each
association created, JDeveloper creates an appropriate XML document file and saves it in the
directory that corresponds to the name of its package

-->


<!--

views -- links


View Link - A view link represents the relationship between two view objects, which is usually, but not necessarily,
based on a foreign-key relationship between the underlying data tables. The view link associates a row
of one view object instance (the master object) with one or more rows of another view object instance
(the detail object). A master-detail relationship is established when a view link is created to associate
two view object instances.


service -- AppModules-->



<!--

Entity-based View Objects - таблицы баз объединяются в сущности в которых можно коммитить изменения.
Query-based View Objects - sql запрос в базу данных. Коммиты к таким VO не работают.
Programmatic - хз что еще programmatic
Static List - сами делаем статический набор записей. Всякие наборы для выпадающих меню. и т.д.


Отдельно LOV


-->


<br/><br/>

Очередной update в понимании, поэтому я думаю, что лучший вариант применения фильтрации к ViewObject следующий:

1) Если переменные во ViewObject можно задать с помощью SQL запроса (с обработкой nvl, case и д.р.), то лучшей обойтись без каких-либо дополнительных переменных, ViewCriteria. Но не стоит особо упираться и городить 4-х этажные SQL запросы, если можно сделать решение проще.

2-3) Если значене переменных нужно задать по умолчанию, то лучше это сделать с помощью выражений, в том числе Groovy выражений. А т.к. Groovy очень похож на java, то не так уж это и сложно.

2-3) Если нужно то применять, то отменять ViewCriteria. Лучше, по возможности, отказаться от них. Если нет, то они вполне ОК.

4) Добавлять конструкции Where если не удалось обойтись вышеперечисленными способами.

5) Создавать ViewObjec полностью динамически. Я пока не делал. Обходился.


<br/><br/>



<ul>
    <li><a href="http://adfindepth.blogspot.ru/2016/05/adf-business-components-part-1-enity.html" rel="nofollow">ADF Business Components Part 1 (Enity Object , View Objects , ApplicationModule )</a></li>
    <li><a href="http://adfindepth.blogspot.ru/2016/06/sql-query-base-view-object-manually.html" rel="nofollow">SQL Query Base View Object in ADF</a></li>
    <li><a href="http://adfindepth.blogspot.ru/2016/06/manually-creation-of-adf-business.html" rel="nofollow">Entity Object Base View Object in ADF</a></li>
    <li><a href="http://adfindepth.blogspot.ru/2016/06/static-list-base-view-object-manually.html" rel="nofollow">Static List Base View Object in ADF</a></li>


    <li><a href="http://www.learnoracleadf.com/2012/06/creating-master-detail-relationship.html" rel="nofollow">Creating a Master-Detail Relationship Using Oracle ADF</a></li>

</ul>


<br/><br/>

**Link in ADF**

<br/>

<ul>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/how-to-create-view-link-in-adf.html" rel="nofollow">How to Create View Link in ADF</a></li>
</ul>




<br/><br/>

**VO (ViewObject)**

<br/>

<ul>
    <li><a href="/dev/model/vo/vo/">ViewObject</a></li>
</ul>



<br/><br/>

**VC (ViewCriteria)**

<br/>

<ul>
    <li><a href="/dev/model/vo/vc/">ViewCriteria</a></li>
</ul>



<br/><br/>

**Where Clause**

<br/>

<ul>
    <li><a href="/dev/model/vo/where/">Where Clause</a></li>
</ul>


<br/>

### Имплементация VO с помощью java классов (IMPL)

<ul>
    <li><a href="/dev/model/vo/java-class/view-object-class/">Имплементация VO с помощью java классов (IMPL)</a></li>
</ul>


<br/><br/>

**Commit && Rollback**

<br/>

<ul>
    <li><a href="/dev/model/vo/commit/">Commit</a></li>
</ul>

<br/><br/>

**Обработка nvl во viewObject**

<br/>

<ul>
    <li><a href="/dev/model/vo/nvl/">Обработка nvl во viewObject</a></li>
</ul>


<br/><br/>

**Все, что связано с ROW у VO**

<br/>

<ul>
    <li><a href="/dev/model/vo/row/">ROW</a></li>
</ul>



<br/>

### Еще примеры:

**Динамические VO:**

ADF : Dynamic View Object  
http://mahmoudoracle.blogspot.ru/2012/05/adf-dynamic-view-object.html


<br/><br/>

**Proxy VO:**

Proxy ViewObject and Dynamic Editable UI in ADF
http://andrejusb.blogspot.ru/2012/03/proxy-viewobject-and-dynamic-editable.html



<br/><br/>

**Создание VO программно:**


https://bitbucket.org/oracle-adf/programmatic-view-object

Creating Dynamic View Object at Runtime programmatically - Oracle ADF  
http://www.awasthiashish.com/2013/07/creating-dynamic-view-object-at-runtime.html

Insert New Row in ADF ViewObject Programatically  
http://www.awasthiashish.com/2012/12/insert-new-row-in-adf-viewobject.html



<br/><br/>

**Primary Key:**  

Primary Key Auto Insert In ADF (Equivalent to before commit trigger in forms)  
http://adfindepth.blogspot.ru/2016/06/primary-key-auto-insert-in-adf-before.html


<br/><br/>

**Полезные примеры:**

http://docs.oracle.com/cd/E21043_01/web.1111/b31974/bcadvvo.htm#CEGJACDC


Most Used Codes in ADF (Iterate over ViewObject, get Value from pageFlow Scope variable)  
http://www.awasthiashish.com/2012/11/most-used-codes-in-adf-iterate-voget.html


<br/><br/>

Working Programmatically with View Objects  
https://docs.oracle.com/middleware/1212/adf/ADFFD/bcqueryprog.htm#ADFFD23744
