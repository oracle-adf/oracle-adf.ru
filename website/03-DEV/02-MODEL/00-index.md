---
layout: page
title: Model Layer (Data, DataContorls, SQL, PL/SQL)
permalink: /dev/model/
---


# Model Layer (Data, DataContorls, SQL, PL/SQL)

<br/>


### Don't Use Oracle's Driver (Thin XA) to Define WebLogic Data Source for ADF Applications
http://andrejusb.blogspot.ru/2012/05/dont-use-oracles-driver-thin-xa-to.html


<br/>

### Application Module:

<ul>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/packages-in-adf-best-practice-to-manage.html" rel="nofollow">Packages in ADF \ Best Practice to Manage Application</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/manually-creation-appmodule-manually.html" rel="nofollow">Manually Creation AppModule in ADF</a></li>
    <li><a href="/dev/model/am/get-access/">Получить доступ к AppModule и к AppModuleImpl в бине</a></li>
</ul>


<br/>

### Работа с ViewObject:

<ul>
    <li><a href="/dev/model/vo/">Примеры применения к ViewObject критериев для выборки, фильтрации, группировок (ViewCriteria, Where, GroupBy, и т.д.)</a></li>
</ul>


<br/>

### Invoke model layer methods from managed bean:

<ul>
    <li><a href="http://www.awasthiashish.com/2015/07/adf-basics-how-to-invoke-model-layer.html" rel="nofollow">ADF Basics: How to invoke model layer methods from managed bean (Best Practice to write business logic in ADF)</a></li>
</ul>



<br/>
<hr/>
<br/>

### SQL и PL/SQL:

<ul>
    <li><a href="/dev/model/sql-plsql/">Вызов из бинов</a></li>
</ul>
