---
layout: page
title: Удаление строки таблицы и поздание новой со значениями по умолчанию (Statement)
permalink: /dev/model/sql-plsql/sql/remove-and-add-default-row/
---

<br/>

# Удаление строки таблицы и поздание новой со значениями по умолчанию (Statement)

<br/>

{% highlight java linenos %}


// Удалит все
String removeRow = "delete from MY_TABLE";  

// Таблица заполняется значениями по умолчанию.
String insertRow = "insert into MY_TABLE (id) values (1)";


private void runSetup(String removeRow, String insertRow){

    Statement stmt = null;

    try {
        stmt = DBUtils.getDBTransaction().createStatement(1);
        stmt.executeUpdate(removeRow);
        stmt.executeUpdate(insertRow);
        ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().commit();
        }
    catch (SQLException ex ){
        ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().rollback();
        System.out.println("EXCEPTION " + ex.toString());
    }

    finally {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {

            }
        }
    }
}

{% endhighlight %}
