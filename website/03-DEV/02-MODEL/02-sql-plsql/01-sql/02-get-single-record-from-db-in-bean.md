---
layout: page
title: Пример получения из базы Oracle единственного значения из таблицы Oracle (PreparedStatement)
permalink: /dev/model/sql-plsql/sql/get-single-record-from-db-in-bean/
---

<br/>

# Пример получения из базы Oracle единственного значения из таблицы Oracle (PreparedStatement)

<br/>

{% highlight java linenos %}

private void myMethodExample(String userLogin){

    String sql;
    sql = "select tn.col1, tn.col2, tn.col3 from table_name tn where login = '" + userLogin + "'";

    String result1;
    String result2;
    String result2;

    PreparedStatement st = null;
    ResultSet rs = null;

    st = DBUtils.getDBTransaction().createPreparedStatement(sql,1);

    try {
            rs = st.executeQuery();

            if (rs != null && rs.next()) {
                    result1 = rs.getString(1);
                    result2 = rs.getString(2);
                    result3 = rs.getString(3);
                }
            }
    catch (Exception ex){
        System.out.println("Exception1 Occured " + ex.getMessage());
    }
    finally {
        try {
            pst.close();
            rs.close();
        }
        catch (Exception e) {
            System.out.println(" Exception2 Occured " + e.getMessage());
        }
    }
}

{% endhighlight %}
