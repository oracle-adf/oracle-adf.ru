---
layout: page
title: Вызов хранимой функции в базе Oracle из бина
permalink: /dev/model/sql-plsql/plsql/execute-stored-function-from-bean/
---

<br/>

# Вызов хранимой функции в базе Oracle из бина

### Посоны меня поставили на место (не совсем так но!!!)

В общем коммитов в хранимых процедурах и функциях в нашем проекте быть не должно.
Коммит выполнять командой

    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().commit();

<br/>
<br/>

### Собственно пример

Имеем хранимую функцию.  
Обращаю внимание, что текст функции не в {}.  
Я не обратил, минут 20 не понимал, что не так.  

    CREATE OR REPLACE PACKAGE user_tasks is

        FUNCTION is_current_pass_correct(
            username IN sec_users.login%TYPE,
            current_password IN sec_users.password%TYPE) RETURN NUMBER;

    end user_tasks;

<br/>

    CREATE OR REPLACE PACKAGE BODY user_tasks is

        FUNCTION is_current_pass_correct(
         username IN sec_users.login%TYPE,
         current_password IN sec_users.password%TYPE) return NUMBER IS

         password_count NUMBER;

         BEGIN
            select count(*) into password_count from sec_users se where
             se.login = username
             and se.password = current_password;

             return password_count;

         EXCEPTION
           WHEN OTHERS THEN
             return 0;
        END current_pass_check;

    END is_current_pass_correct;

<br/><br/>

<br/>


**UPD:**  

Вещи вроде begin ?:= user_tasks.is_current_pass_correct(?,?); end;, выношу в отедьный файл с константами!


    ***

    import java.sql.Types;
    import java.sql.SQLException;

    ***

<br/>

    private Boolean isInputPasswordCorrect(String userName, String currentPassword){

        CallableStatement st = null;
        String function_execute = "begin ?:= user_tasks.is_current_pass_correct(?,?); end;";

            try {
            st = DBUtils.getDBTransaction().createCallableStatement(function_execute, 1);

            st.registerOutParameter(1, Types.INTEGER);
            st.setString(2, userName);
            st.setString(3, currentPassword);
            st.execute();

             Integer out = st.getInt(1);

                if(out.equals(1)){
                    return true;
                } else {
                    return false;
                }

        } catch (SQLException ex) {
            return false;
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (Exception ex) {
                return false;
            }
        }
    }


====================

### UPD. Печатаю без проверки на реальном сервере!!!

UPD: Может так лучше:

<br/>
<br/>



    package orj.javadev.project.constants.oracle;

    public class CONSTANTS_ORACLE_STORED_FUNCTIONS {

        public static final String MY_STORED_FUNCTION_TXT = "begin ?:= sys.diutil.bool_to_int(my_package.my_stored_function(?,?)); end;";

    } // The End of Class;


<br/>
<br/>

Обращаю внимание на:

    //result - Boolean parameters are translated from/to integers:  0/1/null <--> false/true/null

<br/>

    public String myMethod() {

            ***

            CallableStatement st = null;

            try {

            st = DBUtils.getDBTransaction().createCallableStatement(CONSTANTS_ORACLE_STORED_FUNCTIONS.MY_STORED_FUNCTION_TXT, 1);


            // То, что будет возвращено
            st.registerOutParameter(1, Types.NUMERIC); //result - Boolean parameters are translated from/to integers:  0/1/null <--> false/true/null


            st.setLong(2, osid);
            st.registerOutParameter(3, Types.VARCHAR);

            st.execute();

            } catch (SQLException ex) {
                throw new JboException(ex);
            } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
          }  
        }
        return null;
    }


Хранимая функция возвращает true или false.


    FUNCTION my_stored_function(param1 IN NUMBER, param2 OUT VARCHAR2)
       RETURN BOOLEAN IS
       L_program VARCHAR2(11) := 'DELETE_RULE';
     BEGIN

      ***

       return TRUE;
     EXCEPTION
       WHEN OTHERS THEN
         o_err_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         LP_package || L_program,
                                         TO_CHAR(SQLCODE));
         return FALSE;
     END my_stored_function;  
