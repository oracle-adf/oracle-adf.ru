---
layout: page
title: Вызов хранимой процедуры в базе Oracle из бина
permalink: /dev/model/sql-plsql/plsql/execute-stored-procedure-from-bean/
---


### Посоны меня поставили на место (не совсем так но!!!)

В общем коммитов в хранимых процедурах и функциях в нашем проекте быть не должно.
Коммит выполнять командой

    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().commit();

<br/>


### Имеем хранимую процедуру:


    CREATE OR REPLACE PACKAGE user_tasks is

      PROCEDURE update_user_password(
              username IN sec_users.login%TYPE,
              new_password IN sec_users.password%TYPE,
              o_err_msg OUT VARCHAR2);

    end user_tasks;

<br/>

    CREATE OR REPLACE PACKAGE BODY user_tasks is

      PROCEDURE update_user_password(
              username IN sec_users.login%TYPE,
              new_password IN sec_users.password%TYPE,
              o_err_msg OUT VARCHAR2) is

      BEGIN

        update sec_users se set se.password = new_password where se.login = username;
        commit;


        EXCEPTION when OTHERS THEN
        o_err_msg := 'ERRORS';

      END;

    END user_tasks;


Нужно ее вызвать из бина.

Обращаю внимание, что возвращаемое значение типа Boolean из Oracle получить нельзя.


**UPD:**  

Вещи вроде {CALL user_tasks.update_user_password(?,?,?)}, выношу в отедьный файл с константами!


Собственно метод бина

    import java.sql.CallableStatement;
    import java.sql.SQLException;
    import java.sql.Types;

    ***

    private String update_user_password_in_database(String username, String new_password){

        CallableStatement st = null;
        String procedure_execute = "{CALL user_tasks.update_user_password(?,?,?)}";

            try {
            st = DBUtils.getDBTransaction().createCallableStatement(procedure_execute, 1);

            st.setString(1, username);
            st.setString(2, new_password);

            st.registerOutParameter(3, Types.VARCHAR);

            st.execute();

            String errMessage = null;
            errMessage = st.getString(3);

            if (StringUtils.isBlank(errMessage)){
                 errMessage = "SUCCESS";
             }

            return errMessage;

        } catch (SQLException ex) {
            return "Exception 1" + ex.toString();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (Exception ex) {
                return "Exception 2" + ex.toString();
            }
        }
    }   


<br/>


    package org.javadev.xxx.utils;

    import oracle.jbo.server.ApplicationModuleImpl;
    import oracle.jbo.server.DBTransaction;

    public class DBUtils {

        private static final String APP_MODULE_DATA_CONTROL = "AppModuleDataControl";

        public static DBTransaction getDBTransaction(){
            ApplicationModuleImpl am = (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl(APP_MODULE_DATA_CONTROL);
            DBTransaction db = null;
            db = am.getDBTransaction();            
            return db;
        }

    }
