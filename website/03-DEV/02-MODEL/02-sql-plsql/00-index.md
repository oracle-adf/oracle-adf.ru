---
layout: page
title: SQL и PL/SQL
permalink: /dev/model/sql-plsql/
---

# SQL и PL/SQL


<div align="center" style="border-width: 4px; padding: 10px; border-style: inset; border-color: red; ">

    В общем коммитов в хранимых процедурах и функциях в нашем проекте быть не должно!
    Коммит выполнять командой

    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().commit();

</div>


<br/>

### SQL:

<ul>

    <li><a href="/dev/model/sql-plsql/sql/remove-and-add-default-row/">Удаление строки таблицы и поздание новой со значениями по умолчанию (Statement)</a></li>

    <li><a href="/dev/model/sql-plsql/sql/get-single-record-from-db-in-bean/">Пример получения из базы Oracle единственного значения из таблицы Oracle (PreparedStatement)</a></li>
</ul>


<br/>

### PL/SQL:

<ul>
    <li><a href="/dev/model/sql-plsql/plsql/execute-stored-procedure-from-bean/">Вызов хранимой процедуры в базе Oracle из бина</a></li>
    <li><a href="/dev/model/sql-plsql/plsql/execute-stored-function-from-bean/">Вызов хранимой функции в базе Oracle из бина</a></li>
</ul>
