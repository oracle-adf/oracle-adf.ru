---
layout: page
title: Oracle ADF > AppModule Commit
permalink: /dev/model/am/commit/
---


# [Oracle ADF] AppModule Commit


<br/>

### Пример 1

    private void commit(){
        ApplicationModule app = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        app.getTransaction().commit();
    }

    private void rollBack(){
        ApplicationModule app = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        app.getTransaction().rollback();
    }

<br/>

### Пример 2 (я отдаю себе отчет, что здесь тоже самое написано)

В общем коммитов в хранимых процедурах и функциях в нашем проекте быть не должно.
Коммит выполнять командой

    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().commit();

или

    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().executeCommand("commit");



<br/>

### Пример 3

    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl").getTransaction().postChanges();
