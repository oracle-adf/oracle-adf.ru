---
layout: page
title: Oracle ADF > Получить доступ к AppModule и к AppModuleImpl в бине
permalink: /dev/model/am/get-access/
---


# [Oracle ADF] Получить доступ к AppModule и к AppModuleImpl в бине

<br/>

### Получить доступ к AppModule в бине:

    ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
    ViewObject vo = am.findViewObject("Tasks_List");


<br/>

### Получить доступ к AppModuleImpl в бине:

    myMethod(){
        AppModuleImpl appModuleImpl = getAppModuleImpl();
        ViewObjectImpl viewObjectImpl  = appModuleImpl.getMyVO();
    }

<br/>

    public static AppModuleImpl getAppModuleImpl(){

        oracle.adf.model.BindingContext bc = oracle.adf.model.BindingContext.getCurrent();
        JUApplication japp = (JUApplication)bc.getCurrentBindingsEntry().get("AppModuleDataControl");

        return (AppModuleImpl)japp.getApplicationModule();
    }


<br/>

### Получить ссылку на AppModuleImpl в AppModuleImpl:

    AppModuleImpl am = (AppModuleImpl)this.getApplicationModule();

<br/>

### PS. Я опять забыл для чего нужен AppModuleDefImpl
