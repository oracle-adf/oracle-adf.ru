---
layout: page
title: Oracle ADF - Application Module
permalink: /dev/model/am/
---


<br/>

# Oracle ADF > Application Module:


Все датаконтролы (я их еще назваю ViewObject хз правильно это или нет) прописаны в app module.

Поэтому коммиты можно делать прям так:

<ul>

    <li><a href="/dev/model/am/commit/">AppModule Commit</a></li>

</ul>


<br/>

<ul>

    <li><a href="/dev/model/am/get-access/">Получить доступ к AppModule и к AppModuleImpl в бине</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/packages-in-adf-best-practice-to-manage.html" rel="nofollow">Packages in ADF \ Best Practice to Manage Application</a></li>

    <li><a href="http://adfindepth.blogspot.ru/2016/06/manually-creation-appmodule-manually.html" rel="nofollow">Manually Creation AppModule in ADF</a></li>

</ul>
