---
layout: page
title: Personalization and MDS
permalink: /personalization-and-mds/
---

<br/>

# Personalization and MDS

Я пока не настраивал. У меня на проекте пока и секурити не до конца корректно работает. Так, что сначала докопаю секурити, тем более, что для персонализации секурити нужна.

Насколько я понимаю, персонализацию (чтобы у пользователя сохранялось состояние элементов которое он выбрал на форме) можно настроить, чтобы данные хранились локально в каталоге проекта или же с использование базы данных (догадайтесь какой).

Чтобы работало с базой, нужно устанавливать Oracle Fusion Middleware Metadata Repository Creation Utility (RCU), чтобы создать metadata repository в базе данных.


Бум разбираться. Благо материалов достаточно.

<ul>
    <li><a href="https://technology.amis.nl/2015/04/22/adf-12c-allow-user-to-personalize-the-form-items-at-run-time-using-mds-based-change-persistence/">ADF 12c – Allow user to personalize the form items at run time using MDS based Change Persistence</a></li>
    <li><a href="http://rohanwalia.blogspot.sg/2014/08/personalization-using-mds-in-oracle-adf.html">Personalization using MDS in Oracle ADF</a></li>
    <li><a href="http://www.youtube.com/watch?v=zya4XaU3Lo8">[YouTube]</a></li>
</ul>
