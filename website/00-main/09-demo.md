---
layout: page
title: Oracle ADF демонстрационные приложения
description: Oracle ADF демонстрационные приложения
keywords: Oracle ADF, Oracle ADF демонстрационные приложения
permalink: /demo/
---

# Oracle ADF демонстрационные приложения (если есть исходники на похожие примеры, поделитесь если не жалко)

### Alfa UI позволяет создавать достаточно симпатичные приложения.

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/JFdW4_nne0A" frameborder="0" allowfullscreen=""></iframe>
</div>

<br/><br/>

<div align="center">
	<iframe width="853" height="480" src="https://www.youtube.com/embed/79QQbQ-PDkM" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

### Демки в которых можно покликать, но нужна учетка на сайте Oracle.

1. https://demo.oracle.com/quicktour/qt-sales-cloud/app.html
2. https://demo.oracle.com/quicktour/qt-hcm-cloud/app.html
3. https://demo.oracle.com/quicktour/qt-erp-cloud/fin/index.html

Я придераюсь или даже в облаках Oracle, все работает достаточно медленно?

Возможно, что это и не Oracle ADF а Oracle JET.
