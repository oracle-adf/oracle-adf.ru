---
layout: page
title: Блоги ADF разработчиков на русском
description: Блоги ADF разработчиков на русском
keywords: Oracle ADF, Блоги ADF разработчиков на русском
permalink: /blogs/
---

# Блоги ADF разработчиков на русском:

<ul>
    <li><a href="http://practiceadf.blogspot.ru/">practiceadf</a> <a href="https://bitbucket.org/oracle-adf/practiceadf-rus">src</a></li>
    <li><a href="http://oracle-adf.info/">oracle-adf.info</a></li>
</ul>

### Блоги ADF разработчиков на английском:

<ul>
    <li><a href="https://oracle-adf.com/blogs/">здесь</a></li>
</ul>
