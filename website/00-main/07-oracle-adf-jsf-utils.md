---
layout: page
title: ADF Utils / JSF Utils
description: ADF Utils / JSF Utils
keywords: Oracle ADF, ADF Utils / JSF Utils
permalink: /oracle-adf-jsf-utils/
---

# Общий репозиторий для работы с ADF компонентами программно

<br/>

Смысл в том, что одни и теже методы, которые не привязаны к конкретному проекту, могут быть дорабатываться разными группами разработчиков. Это всякие методы по работе с датами, ViewObject, ViewCriteria, Security и т.д.

В общем если есть желание добавить или исправить то, что лежит сейчас в нашем репо, можете писать на эл. почту или присылать пулл реквесты. Объединив усилия, можно будет не тратить времени на то, что уже накопали другие разрабы а также сделать код более читабельным и менее требовательным к ресурсам.

Ну вот как пример.  
Для работоы с ViewObject, я испольую всего одну команду.

    ViewObject myViewObject = VOUtils.getViewObjectByName("ViewObjectName");

И сразу работаю с ним.

А чтобы получить информацию о SQL запросе, на основе которого построен ViewObject, bind variables и набор данных, достаточно выполнить.

    VOUtils.printViewObjectInfo(myViewObject);

Собственно репо, которое нужно сделать конфеткой.

<a href="https://bitbucket.org/oracle-adf/adf-utils">Основной</a>.

<br/>
<br/>

ADFUtils.java и JSFUtils.java были взяты из проекта - <a href="https://www.oracle.com/technetwork/developer-tools/jdev/index-095536.html">Oracle Fusion Order Demo Application</a>
