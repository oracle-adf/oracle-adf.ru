---
layout: page
title: Книги для изучения Oracle ADF
description: Книги для изучения Oracle ADF
keywords: Oracle ADF, Книги для изучения Oracle ADF
permalink: /books/
---

<br/>

### Книги, которые рекомендовал Эмин (middletier.blogspot.ru) для изучения Oracle ADF

1. Oracle ADF Real World Developer's Guide
2. Oracle ADF Enterprise Application Development Made Simple
