---
layout: page
title: Environment for Development Oracle ADF Application
description: Environment for Development Oracle ADF Application
keywords: Oracle ADF, Environment for Development Oracle ADF Application
permalink: /environment-for-adf-development/
---

# Environment for Development Oracle ADF Application

<a href="https://javadev.org/sitemap/">JDK Installation</a><br/>

<a href="https://oracle-dba.ru/database/installation/">Oracle DataBase Installation</a> (Обычно достаточно бесплатной версии XE, установленной локально на том же самом компьютере)<br/>

<a href="https://javadev.org/docs/appserv/weblogic/12c/installation/">Oracle Weblogic Installation</a> (Для разработки обычно используется встроенный Weblogic. В случае развертывания weblogic сервера на отдельном компьютере, нужно не забыть установить JRF компоненты)<br/>

<br/>

Кому в падлу все это настраивать, могут скачать готовую виртуальную машину, которую подготовили специалисты Oracle.

<a href="http://www.oracle.com/technetwork/database/enterprise-edition/databaseappdev-vm-161299.html" rel="nofollow">Oracle Database Virtual Machine</a><br/>

<br/>

### WebLogic 12.2.1 on Docker Containers

<div align="center">

<iframe width="640" height="480" src="https://www.youtube.com/embed/cgf8wzXnmb4" frameborder="0" allowfullscreen=""></iframe>

</div>

<p><br /><br /></p>

<p>Dockerfiles and etc:<br />
https://github.com/plsql/docker-for-oracle</p>
