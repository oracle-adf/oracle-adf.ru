---
layout: page
title: Видео материалы по изучению Oracle ADF на русском языке
description: Видео материалы по изучению Oracle ADF на русском языке
keywords: Oracle ADF, Видео материалы, русский язык
permalink: /videos/
---

# Видео материалы по изучению Oracle ADF на русском языке

<ul>
	<li><a href="/mironchik/">Презентация технологии Oracle Adf от Мирончика Игоря.</a></li>
	<li><a href="http://rutracker.org/forum/viewtopic.php?t=4758259">[Dmitry Nefedkin] Oracle ADF [2011, RUS]</a></li>
</ul>
