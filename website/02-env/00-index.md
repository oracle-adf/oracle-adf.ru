---
layout: page
title: Environment для разработки на Oracle ADF
description: Environment для разработки на Oracle ADF
keywords: Oracle ADF, Environment для разработки
permalink: /env/
---

# Environment для разработки на Oracle ADF

UPD: Oracle ADF on Docker Container (Возможно будет кому интересно, но у меня из коробки не разработало и я не захотел разбираться.) <br/>
https://andrejusb.blogspot.ru/2017/11/oracle-adf-on-docker-container.html

<br/>

Oracle предалагает следующие инструменты: Hudson, Archiva  
Мне видится более удобным решение: Jenkins, Nexus, SonarQube

<br/>

### Настройки, которые могут улучшить работу Jdeveloper

<ul>
    <li><a href="/env/jdeveloper-setting/"><strong>Настройки, которые могут улучшить работу Jdeveloper</strong></a></li>
</ul>

<br/>

### Oracle Public Maven Repository

<ul>
    <li><a href="/env/compile-adf-project-by-maven/"><strong>Скомпилировать ADF проект с помощью Maven</strong></a></li>
    <li><a href="/env/maven-repository/"><strong>Более подробно и с видео</strong></a></li>
</ul>

<br/>

### Собственный Maven репозиторий с ADF библиотеками

Есть как миниму 3 инструмента, чтобы развернуть свой репозиторий с ADF библиотеками. 2 бесплатных и артифактори.
Так как денег нам никто не даст, когда есть бесплатные аналоги, придется использовать то, что есть.

В документации Oracle для этих целей используется Archiva. Похоже они сами не смогли ее настроить для работы с репозиторием Oracle, поэтому во всех инструкцих настраивается через жопу. Т.е. предлагают поставить сначала jdeveloper, вытащить из него библиотеки и засунуть их в Archiva репозиторий. В общем, я "не шмогла" разобраться до конца как это все настроить. Точнее, нашлись более простые способы развернуть свой maven репозиторий и такой вариант меня вполне устраивает. (Если вдруг кто-то добавит вариант с Archiva я только за. Вот только хрен кто захочет этим заниматься)

В общем решение с пафосным названием Nexus (You Must Construct Additional Pylons!), может все сделать самостоятельно.

<ul>
    <li><a href="https://javadev.org/devtools/repository-management/nexus/"><strong>Nexus</strong></a> Я смог настроить только во 2-й версии. (Кто настроит работу с 3-й отпишитесь как)</li>
    <li><a href="/env/archiva/"><strong>Archiva (не доделал)</strong></a></li>
</ul>

<br/>

### Continious Integration

Надо заменить на Jenkins.

<ul>
    <li><a href="/env/hudson/"><strong>Hudson Continious Integration</strong></a></li>
</ul>

<br/>

### Автоматический анализ кода

Целиком ADF приложение для анализа залить в SonarQube у меня не получилось. Зато получилось залить его по частям. Отедьно ViewController и отдельно Model.

<ul>
    <li><a href="https://javadev.org/devtools/code-quality/sonarqube/installation/"><strong>SonarQube</strong></a></li>
</ul>

<br/>
<br/>

### ADF Logger:

<div align="center">

    <iframe width="640" height="480" src="https://www.youtube.com/embed/o-2r-Max3II" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### Остальное:

<ul>
    <li><a href="/env/disable-authentication/"><strong>Отключение аутентификации (чтобы постоянно не вводить логин и пароль при старте)</strong></a></li>
</ul>

<br/>

### ADF Runtime Libraries:

<ul>
    <li><a href="https://docs.oracle.com/middleware/1213/adf/administer/adf-admin-runtime-libs.htm" rel="nofollow">ADF Runtime Libraries</a></li>
</ul>

<br/>

### Deploying Application:

<ul>
    <li><a href="http://docs.oracle.com/cd/E24382_01/user.1112/e17455/deploying_apps.htm#OJDUG542" rel="nofollow">Deploying Application</a></li>
</ul>
