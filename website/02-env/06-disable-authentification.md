---
layout: page
title: Отключение аутентификации (чтобы постоянно не вводить логин и пароль при старте)
description: Отключение аутентификации
keywords: Oracle ADF, Отключение аутентификации
permalink: /env/disable-authentication/
---

# Отключение аутентификации (чтобы постоянно не вводить логин и пароль при старте)

    Эти 42 секунды на авторизацию,
    Я день за днем сложу в года

**adf-config.xml**

<br/>

    authorizationEnforce="false"
    authenticationRequire="false"

<br/>

    <sec:adf-security-child xmlns="http://xmlns.oracle.com/adf/security/config">
      <CredentialStoreContext credentialStoreClass="oracle.adf.share.security.providers.jps.CSFCredentialStore"
                              credentialStoreLocation="../../src/META-INF/jps-config.xml"/>
      <sec:JaasSecurityContext initialContextFactoryClass="oracle.adf.share.security.JAASInitialContextFactory"
                               jaasProviderClass="oracle.adf.share.security.providers.jps.JpsSecurityContext"
                               authorizationEnforce="false" authenticationRequire="false"/>
    </sec:adf-security-child>

<br/>

И в бине, который инициализируется при старте:

    @PostConstruct
    public void init(){
        writeSecContextToScope();
    }

<br/>

    private void writeSecContextToScope(){

        String user;

        if(SecUtils.getCurrentUser().equals("anonymous")){
            user = "default_user";
        } else {
            user = SecUtils.getCurrentUser();
        }

        ScopeUtils.setVarToSessionScope("loggedInUser", user);
    }
