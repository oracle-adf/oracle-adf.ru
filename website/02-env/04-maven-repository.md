---
layout: page
title: Oracle Public Maven Repository
description: Oracle Public Maven Repository
keywords: Oracle ADF, Oracle Public Maven Repository
permalink: /env/maven-repository/
---

# Using the Oracle Public Maven Repository With ADF Applications

<br/>

https://blogs.oracle.com/blueberry/entry/using_the_oracle_public_maven

https://www.youtube.com/watch?v=3a0OeM8hXJM

http://docs.oracle.com/middleware/1213/core/MAVEN/introduction.htm#MAVEN8755
