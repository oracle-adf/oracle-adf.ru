---
layout: page
title: Скомпилировать ADF проект с помощью Maven
description: Скомпилировать ADF проект с помощью Maven
keywords: Oracle ADF, Maven, Сборка
permalink: /env/compile-adf-project-by-maven/
---

# Скомпилировать ADF проект с помощью Maven

<br/>

### Устанавливаю JDK8

https://javadev.org/install/jdk/8/linux/centos/6/x64/

<br/>

### Устанавливаю MAVAN (3.X)

https://javadev.org/install/assembly-tools/linux/centos/6/x64/

<br/>

### Принимаю лицензионное соглашение

<br/>

Нужно залогиниться на сайте Oracle и принять хз что.  
https://www.oracle.com/webapps/maven/register/license.html

<br/>

    $ mkdir -p /home/developer/.m2/

<br/>

    $ vi /home/developer/.m2/settings.xml

<br/>

    <settings>
    <profiles>
        <profile>
          <id>main</id>
          <activation>
            <activeByDefault>true</activeByDefault>
          </activation>
          <repositories>
            <repository>
              <id>maven.oracle.com</id>
              <url>https://maven.oracle.com</url>
              <layout>default</layout>
              <releases>
                <enabled>true</enabled>
              </releases>
            </repository>
          </repositories>
          <pluginRepositories>
            <pluginRepository>
              <id>maven.oracle.com</id>
              <url>https://maven.oracle.com</url>
            </pluginRepository>
          </pluginRepositories>
        </profile>
      </profiles>
        <servers>
            <server>
               <id>maven.oracle.com</id>
               <username>username</username>
               <password>password</password>
               <configuration>
                 <basicAuthScope>
                   <host>ANY</host>
                   <port>ANY</port>
                   <realm>OAM 11g</realm>
                 </basicAuthScope>
                 <httpConfiguration>
                   <all>
                     <params>
                       <property>
                         <name>http.protocol.allow-circular-redirects</name>
                         <value>%b,true</value>
                       </property>
                     </params>
                   </all>
                 </httpConfiguration>
               </configuration>
             </server>
        </servers>
    </settings>

<br/>

### Тоже самое, но с зашифрованным паролем:

Генерируем пароль, на основании которого позднее будет сгенерировать пароль для доступа к репозиторию oracle.

    $ mvn --encrypt-master-password <password>

    password - любой пароль для себя

<br/>

    $ vi /home/developer/.m2/settings-security.xml

<br/>

    <settingsSecurity>
      <master>{3tHWPhlKDuIHgAHL6I1X3KerFr3ASMwDMhKWM1eoSvg=}</master>
    </settingsSecurity>

<br/>

    $ mvn --encrypt-password <password>

    password - пароль от аккаунта oracle

<br/>

    $ vi /home/developer/.m2/settings.xml

<br/>

У меня вот такая херня получилась:

    <settings
        xmlns="http://maven.apache.org/SETTINGS/1.1.0">
        <profiles>
            <profile>
                <id>default</id>
                <repositories>
                    <repository>
                        <id>maven.oracle.com</id>
                        <url>https://maven.oracle.com</url>
                        <layout>default</layout>
                        <releases>
                            <enabled>true</enabled>
                        </releases>
                    </repository>
                </repositories>
                <pluginRepositories>
                    <pluginRepository>
                        <id>maven.oracle.com</id>
                        <url>https://maven.oracle.com</url>
                    </pluginRepository>
                </pluginRepositories>
            </profile>
        </profiles>
        <servers>
            <server>
                <id>maven.oracle.com</id>
                <username>my-oracle-email@gmail.com</username>
                <password>{kLv2E777kHkH+sETsr7HGULLKeUIDFb+2Ij+Y1MbQLY=}</password>
                <configuration>
                    <basicAuthScope>
                        <host>ANY</host>
                        <port>ANY</port>
                        <realm>OAM 11g</realm>
                    </basicAuthScope>
                    <httpConfiguration>
                        <all>
                            <params>
                                <property>
                                    <name>http.protocol.allow-circular-redirects</name>
                                    <value>%b,true</value>
                                </property>
                            </params>
                        </all>
                    </httpConfiguration>
                </configuration>
            </server>
        </servers>
        <activeProfiles>
            <activeProfile>default</activeProfile>
        </activeProfiles>
    </settings>

<!--

<settings
    xmlns="http://maven.apache.org/SETTINGS/1.1.0"
          schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
    <profiles>
        <profile>
            <id>default</id>
            <repositories>
                <repository>
                    <id>maven.oracle.com</id>
                    <url>https://maven.oracle.com</url>
                    <layout>default</layout>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <id>maven.oracle.com</id>
                    <url>https://maven.oracle.com</url>
                </pluginRepository>
            </pluginRepositories>
        </profile>
        <profile>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories/>
        </profile>
    </profiles>
    <proxies/>
    <servers>
        <server>
            <id>maven.oracle.com</id>
            <username>my-oracle-email@gmail.com</username>
            <password>{kLv2E777kHkH+sETsrzHGULLKeUIDFb+2Ij+Y1MbQLY=}</password>
            <configuration>
                <basicAuthScope>
                    <host>ANY</host>
                    <port>ANY</port>
                    <realm>OAM 11g</realm>
                </basicAuthScope>
                <httpConfiguration>
                    <all>
                        <params>
                            <property>
                                <name>http.protocol.allow-circular-redirects</name>
                                <value>%b,true</value>
                            </property>
                        </params>
                    </all>
                </httpConfiguration>
            </configuration>
        </server>
    </servers>
    <mirrors></mirrors>
    <activeProfiles>
        <activeProfile>default</activeProfile>
    </activeProfiles>
</settings>

-->

### Проверка:

    $ cd /tmp/
    $ git clone https://bitbucket.org/oracle-adf/adf-utils
    $ cd ADF-UTILS/
    $ mvn package

Возможно, здесь получше все расписано:  
http://docs.oracle.com/middleware/1213/core/MAVEN/config_maven_repo.htm#MAVEN9018
