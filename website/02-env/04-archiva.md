---
layout: page
title: Developing Applications Using Continuous Integration
description: Developing Applications Using Continuous Integration
keywords: Oracle ADF, Archiva, Continuous Integration
permalink: /env/archiva/
---

# Developing Applications Using Continuous Integration

По материалам от корпорации зла.

http://docs.oracle.com/middleware/1221/core/MAVEN/toc.htm

Начал делать в контейнере Docker.

    $ docker pull oraclelinux:latest
    $ docker run -it --name myRepo oraclelinux

    # yum update -y

    # useradd developer

<br/>

### Устанавливаю JDK8

https://javadev.org/install/jdk/8/linux/centos/6/x64/

<br/>

### Устанавливаю MAVAN (3.X)

https://javadev.org/install/assembly-tools/linux/centos/6/x64/

<br/>

### Archiva Maven Repository Manager

## https://javadev.org/install/assembly-tools/linux/centos/6/x64/archiva/

<br/>

Типа Save.

Что получилось, можно забрать командой:

    $ docker pull marley/repo:latest

(1.425 GB)

    $ docker run -it -p 8081:8081 --name repo marley/repo:latest

Стартую Archiva (нужно подождать пару минут, пока она стартанет)

    $ archiva start

<br/>

В linux можно коннектиться к

http://localhost:8081/

<br/>

В Windows

    $ docker-machine ip
    192.168.99.100

http://192.168.99.100:8081/

<br/>

## Configuring Mirror Repositories

http://docs.oracle.com/middleware/1221/core/MAVEN/populating_archiva.htm#MAVEN8840

### Proxy Connectors: Удалил все

<br/>

### Repositories:

    ADD

    Id: mirror
    Name: Mirror
    Directory: ./repositories/mirror

    Select:
        Releases
        Block Re-deployment
        Scaned

    SAVE

<br/>

### Proxy Connectors:

    ADD

    Managed Repository: mirror
    Remote Repository: central

    SAVE

<br/>

### USERS --> Manage --> Guest --> Edit --> Edit Roles --> Repository Observer --> + mirror

    UPDATE

<br/>

### Creating Development, Production, Quality Assurance, and Test Repositories

### Repositories:

(dev, prod, qa, or test)

    ADD

    Id: dev
    Name: Oracle Dev
    Directory: ./repositories/dev

    Release: true
    Scanned: true

    SAVE

<br/>

    ADD

    Id: prod
    Name: Oracle Prod
    Directory: ./repositories/prod

    Release: true
    Scanned: true

    SAVE

<br/>

</br>

### USERS --> Manage --> Guest --> Edit --> Edit Roles --> Repository Observer --> + mirror

    UPDATE

</br>

### Rpository Groups:

    Add

    id: dev-group
    + snapshots, mirror, dev, prod

    SAVE

<br/>

    Add

    id: prod-group
    + snapshots, mirror, dev, prod

    SAVE

<br/>

### Creating a Deployment Capable User

### USERS --> Manage --> add --> Username: manager --> SAVE

### USERS --> Manage --> manager --> Edit --> Edit Roles --> Repository Manager --> + Snapshot, + Internal Repositories

<br/>

На этом по сути закончил пока.
У меня archiva не работает, зато nexus справляется.

<!--


<br/>

### Installing and Configuring Maven for Build Automation and Dependency Management

http://docs.oracle.com/middleware/1221/core/MAVEN/config_maven.htm#MAVEN9002


    Oracle Maven Synchronization Plug-In

    TO INSTALL:

    mvn install:install-file -DpomFile=oracle-maven-sync-12.2.1.pom -Dfile=oracle-maven-sync-12.2.1.jar

    TO DEPLOY:

    mvn deploy:deploy-file -DpomFile=oracle-maven-sync-12.2.1.pom -Dfile=oracle-maven-sync-12.2.1.jar
     -Durl=http://servername/archiva/repositories/internal -DrepositoryId=internal



Populating a Local Repository


ничего не понял


Populating a Remote Repository


mvn com.oracle.maven:oracle-maven-sync:push
 -DoracleHome=/path/to/oracleHome
 -DserverId=internal



 ...
 <profiles>
   <profile>
     <id>default</id>
 <repositories>
   <repository>
     <releases>
       <enabled>true</enabled>
       <updatePolicy>always</updatePolicy>
       <checksumPolicy>warn</checksumPolicy>
     </releases>
     <snapshots>
       <enabled>true</enabled>
       <updatePolicy>never</updatePolicy>
       <checksumPolicy>fail</checksumPolicy>
     </snapshots>
     <id>internal</id>
     <name>Team Internal Repository</name>
     <url>http://some.host/maven/repo/internal</url>
     <layout>default</layout>
   </repository>
 </repositories>
 </profile>
 </profiles>
 ...
 <server>
     <id>internal</id>
     <username>deployer</username>
     <password>welcome1</password>
   </server>
 ...
 <activeProfiles>
   <activeProfile>default</activeProfile>
 </activeProfiles>




 You should specify an encrypted password in the server section. For details on how to encrypt the server passwords, see:
 http://maven.apache.org/guides/mini/guide-encryption.html#How_to_encrypt_server_passwordsOpens a new Windows





 <plugin>
   <groupId>com.oracle.maven</groupId>
   <artifactId>oracle-maven-sync</artifactId>
   <version>12.2.1-0-0</version>
   <configuration>
     <serverId>internal</serverId>
     <oracleHome>/path/to/oracleHome</oracleHome>
     <testOnly>false</testOnly>
   </configuration>
 </plugin>



mvn com.oracle.maven:oracle-maven-sync:push














    $ mkdir ~/.m2/
    $ vi ~/.m2/settings.xml

<br/>

    <settings>
      <profiles>
        <profile>
           <id>default</id>
            <repositories>
              <repository>
                <id>dev-group</id>
                <name>Dev Group</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/dev-group</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>dev</id>
                <name>Dev</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/dev</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>prod-group</id>
                <name>Prod Group</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/prod-group</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>prod</id>
                <name>Prod</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/prod</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>qa-group</id>
                <name>QA Group</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/qa-group</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>qa</id>
                <name>QA</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/qa</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>test-group</id>
                <name>Test Group</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/test-group</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>test</id>
                <name>Test</name>
                <releases>
                  <enabled>true</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>false</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/test</url>
                <layout>default</layout>
              </repository>
              <repository>
                <id>archiva-snapshots</id>
                <name>Archiva Snapshots</name>
                <releases>
                  <enabled>false</enabled>
                  <updatePolicy>always</updatePolicy>
                  <checksumPolicy>warn</checksumPolicy>
                </releases>
                <snapshots>
                  <enabled>true</enabled>
                  <updatePolicy>never</updatePolicy>
                  <checksumPolicy>fail</checksumPolicy>
                </snapshots>
                <url>http://SERVER:PORT/archiva/repository/snapshots</url>
                <layout>default</layout>
              </repository>
            </repositories>
          </profile>
        </profiles>
      <servers>
        <server>
          <id>dev</id>
          <username>hudson</username>
          <password>PASSWORD</password>
        </server>
        <server>
          <id>dev-group</id>
          <username>hudson</username>
          <password>PASSWORD</password>
        </server>
        <server>
          <id>archiva-snapshots</id>
          <username>hudson</username>
          <password>PASSWORD</password>
        </server>
      </servers>
      <mirrors>
        <mirror>
          <id>dev-mirror</id>
          <name>All else</name>
          <url>http://SERVER:PORT/archiva/repository/dev-group</url>
          <mirrorOf>*</mirrorOf>
        </mirror>
      </mirrors>
      <activeProfiles>
        <activeProfile>default</activeProfile>
      </activeProfiles>
    </settings>




Project POM


    <repositories>
      <repository>
        <id>maven.oracle.com</id>
        <releases>
          <enabled>true</enabled>
        </releases>
        <snapshots>
          <enabled>false</enabled>
        </snapshots>
        <url>https://maven.oracle.com</url>
        <layout>default</layout>
      </repository>
    </repositories>
    <pluginRepositories>
      <pluginRepository>
        <id>maven.oracle.com</id>
        <url>https://maven.oracle.com</url>
      </pluginRepository>
    </pluginRepositories>


<br/>

### Configuring the Oracle Maven Repository


-->
