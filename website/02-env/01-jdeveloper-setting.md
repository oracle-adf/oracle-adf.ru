---
layout: page
title: Настройки, которые могут улучшить работу Jdeveloper
description: Настройки, которые могут улучшить работу Jdeveloper
keywords: Oracle ADF, Настройки, Улучшение
permalink: /env/jdeveloper-setting/
---

# Настройки, которые могут улучшить работу Jdeveloper

    Is your JDeveloper 12c too slow, follow these 3 basic steps and increase perfomance of JDeveloper 12c (12.1.3)

<br/>

### Step 1: Configure JVM settings in jdev.conf

<br/>

    Path: $MV_HOME$/jdeveloper/jdev/bin/jdev.conf

    # optimize the JVM for strings / text editing
    AddVMOption -XX:+UseStringCache
    AddVMOption -XX:+OptimizeStringConcat
    # AddVMOption -XX:+UseCompressedStrings -- данная настройка перестала работать

    # if on a 64-bit system, but using less than 32 GB RAM, this reduces object pointer memory size
    AddVMOption -XX:+UseCompressedOops

    # use an aggressive garbage collector (constant small collections)
    AddVMOption -XX:+AggressiveOpts

    # for multi-core machines, use multiple threads to create objects and reduce pause times
    AddVMOption -XX:+UseConcMarkSweepGC
    AddVMOption -DVFS_ENABLE=true
    AddVMOption -Dsun.java2d.ddoffscreen=false
    AddVMOption -XX:+UseParNewGC
    AddVMOption -XX:+CMSIncrementalMode
    AddVMOption -XX:+CMSIncrementalPacing
    AddVMOption -XX:CMSIncrementalDutyCycleMin=0
    AddVMOption -XX:CMSIncrementalDutyCycle=10

<br/>

### Step 2: Configure Jdeveloper memory settings in ide.conf

    Path: $MV_HOME$/jdeveloper/ide/bin/ide.conf

    # Set the default memory options for the Java VM which apply to both 32 and 64-bit VM's.
    # These values can be overridden in the user .conf file, see the comment at the top of this file.
    AddVMOption -Xms2048M
    AddVMOption -Xmx4096M

<br/>

### Step 3: Disable "Build After Save"

<br/>

Убрать Tools --> Preferences --> Code Editor --> Save Action --> Build Project after Save

<br/>

<!-- <a href="https://blogs.oracle.com/ArdaEralp/entry/jdev_build_after_save" rel="nofollow">Follow this</a>

<br/><br/>

Original:
<br/>
https://blogs.oracle.com/ArdaEralp/entry/performance_tuning_jdeveloper_12c

-->

<br/>

### Настройка JDeveloper, чтобы открывать страницы сразу в Source а не Design

Tools --> Preferences --> File Types --> Default Editors

<br/>

Source

<ul>
    <li>ADF Fragment File</li>
    <li>XHTML/Facelets Source</li>
    <li>JSP*</li>
</ul>

<!-- https://blogs.oracle.com/Didier/entry/how_to_open_the_jsp_files_in_t -->

<br/>

### Увеличение выделяемой памяти непосредственно для локального сервера Weblogic

В /home/${user_home}/Oracle/Middleware/user_projects/domains/${domain_name}/startWebLogic.sh нужно прописать параметры

    AddVMOption -Xms2048M
    AddVMOption -Xmx4096M

И потом убедиться, что он стартован с данными параметрами.
