---
layout: page
title: Sample ADF Application
description: Sample ADF Application
keywords: Oracle ADF, Sample Application
permalink: /apps/sample-adf-application/
---

# Sample ADF Application

<strong>Fusion Order Demo (FOD) - Sample ADF Application</strong><br/>
http://www.oracle.com/technetwork/developer-tools/jdev/index-095536.html<br/><br/>

<a href="https://bitbucket.org/oracle-adf/FusionOrderDemo_R2_1_revised.git"><strong>bitbucket</strong></a>

<strong>Oracle Fusion Order Demo Application For JDeveloper 11.1.1.x</strong><br/>
http://www.oracle.com/technetwork/developer-tools/jdev/learnmore/fod1111-407812.html<br/><br/>

<a href="https://bitbucket.org/oracle-adf/fusionorderdemo_r1ps5"><strong>bitbucket</strong></a>

<br/><br/>

<strong>Инсталляция приложения</strong>

1. Копируем и разархивируем FusionOrderDemo_R2.zip<br/>
2. Отрываем в JDeveloper проект Infrastucture.jws<br/>
   <br/>

3) Открываем проект MasterBuildScript в нем каталог Resources и открываем файл click build.properties

    # Base Directory for library lookup

    jdeveloper.home=/home/marley/Oracle/Middleware/jdeveloper
    src.home=..//..

<br/>

    # JDBC info used to create Schema
    jdbc.driver=oracle.jdbc.OracleDriver
    jdbc.urlBase=jdbc:oracle:thin:@192.168.1.10
    jdbc.port=1521
    jdbc.sid=ora112

<br/>

    # Information about the default setup for the demo user.
    db.adminUser=system
    db.demoUser=FOD
    db.demoUser.password=fusion
    db.demoUser.tablespace=USERS
    db.demoUser.tempTablespace=TEMP

<br/><br/>

4. Правой клавишей мыши по build.xml и выбрать "Run Ant Target" > "buildAll"<br/>

5. Вводим пароль от учетной записи system.

    BUILD SUCCESSFUL
    Total time: 1 minute 15 seconds

<strong>Запуск приложения</strong>

1. Отрываем в JDeveloper проект StoreFrontModule.jws<br/>

2. View --> DataBase --> Database Navigator<br/><br/>

StoredFrontModule --> FOD --> Правой кнопкой мыши --> properties <br/>

    Host Name: 192.168.1.10
    SID: ora112
    JDBC Port: 1521

    Test Connection --> Success!

3. Application Navigator правой кнопкой по StoreFrontService --> Rebuild StoreFronService.jpr

4) View --> Application Server Navigator

Правой кнопкой мыши на IntegratedWeblogicServer --> Start Server Instance.</br>
Дожидаемся старта сервера.

5. Application Navigator --> StoreFrontUI --> Web Content --> правой кнопкой мыши по home.jspx --> Run

<br/><br/>

<strong>Результат:</strong>

![Sample ADF Application](/img/app/01_Sample_ADF_Application/Sample_ADF_Application_01.png 'Sample ADF Application'){: .center-image }

<br/>
<br/>
<br/>

![Sample ADF Application](/img/app/01_Sample_ADF_Application/Sample_ADF_Application_02.png 'Sample ADF Application'){: .center-image }

<br/>
<br/>
<br/>

![Sample ADF Application](/img/app/01_Sample_ADF_Application/Sample_ADF_Application_03.png 'Sample ADF Application'){: .center-image }
