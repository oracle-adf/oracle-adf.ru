---
layout: page
title: ADF Summit
description: ADF Summit
keywords: ADF Summit
permalink: /exercises/adf-summit/
---

# Попытка собрать Oracle ADF приложение "ADF Summit" с помощью maven в командной строке linux

<br/>

https://www.youtube.com/watch?v=ht8JgEAmrI8

<br/>

### ADF Summit:

Application:

https://bitbucket.org/oracle-adf/summit_adf_core_12_2_1_maven

Я инсталлировал Oracle12 на centos 6.8 по инструкции, что на oracle-dba.ru

    # yum install -y git

    # su - oracle12

    $ cd /tmp/
    $ git clone https://bitbucket.org/oracle-adf/Summit-ADF

    $ cd /tmp/Summit-ADF/SummitADF_Schema1221/Database/scripts


    $ sqlplus system/manager

    SQL> spool result.txt
    SQL> @build_summit_schema.sql;
    SQL> spool off;
    SQL> exit

Создается пользователь: summit_adf

Приложение на локальном Weblogic развернулось.

<br/>

### Сгенерировал pom.xml

Как в описании к приложению:  
https://bitbucket.org/oracle-adf/Summit_ADF_Core_12_2_1_MAVEN

Поменял версию пакетов с 12.2.1-1-0 на 12.2.1-0-0, т.к. на maven.oracle.com еще нет последней версии.

Инсталлировал jdk, maven, развернул nexus и weblogic, но это пока не имеет значения.

Не удается пока собрать проект с помощью maven.

    $ cd /tmp/
    $ git clone https://bitbucket.org/oracle-adf/summit_adf_core_12_2_1_maven
    $ cd Summit_ADF_Core_12_2_1_MAVEN
    $ mvn compile -X

<br/>

Имеем 3 pom файла в каталогах:

MAIN POM:

    <?xml version="1.0" encoding="UTF-8" ?>
    <project xmlns="http://maven.apache.org/POM/4.0.0">
      <modelVersion>4.0.0</modelVersion>
      <groupId>oracle.summit</groupId>
      <artifactId>SummitADF_Core</artifactId>
      <version>1.0-SNAPSHOT</version>
      <description>Super POM for SummitADF_Core</description>
      <packaging>pom</packaging>
      <build>
        <resources>
          <resource>
            <directory>src/</directory>
            <includes>
              <include>**</include>
            </includes>
          </resource>
        </resources>
        <plugins>
          <plugin>
            <groupId>com.oracle.adf.plugin</groupId>
            <artifactId>ojmake</artifactId>
            <version>12.2.1-0-0</version>
            <configuration>
              <ojmake>
                ${oracleHome}/jdeveloper/jdev/bin/ojmake.exe
              </ojmake>
              <files>
                ${basedir}/SummitADF_Core.jws
              </files>
            </configuration>
            <executions>
              <execution>
                <phase>compile</phase>
                <goals>
                  <goal>compile</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>com.oracle.adf.plugin</groupId>
            <artifactId>ojdeploy</artifactId>
            <version>12.2.1-0-0</version>
            <configuration>
              <ojdeploy>
                ${oracleHome}/jdeveloper/jdev/bin/ojdeploy.exe
              </ojdeploy>
              <workspace>
                ${basedir}/SummitADF_Core.jws
              </workspace>
              <profile>
                SummitADF_application1
              </profile>
              <outputfile>
                ${project.build.directory}/${project.build.finalName}.ear
              </outputfile>
            </configuration>
            <executions>
              <execution>
                <phase>package</phase>
                <goals>
                  <goal>deploy</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      <parent>
        <groupId>com.oracle.adf</groupId>
        <artifactId>adf-parent</artifactId>
        <version>12.2.1-0-0</version>
        <relativePath></relativePath>
      </parent>
      <modules>
        <module>Model</module>
        <module>ViewController</module>
      </modules>
      <dependencies>
        <dependency>
          <groupId>oracle.summit</groupId>
          <artifactId>Model</artifactId>
          <version>1.0-SNAPSHOT</version>
          <type>jar</type>
        </dependency>
        <dependency>
          <groupId>oracle.summit</groupId>
          <artifactId>ViewController</artifactId>
          <version>1.0-SNAPSHOT</version>
          <type>war</type>
        </dependency>
      </dependencies>
    </project>

<br/>
<br/>

Summit_ADF_Core_12_2_1_MAVEN2/Model/pom.xml

    <?xml version="1.0" encoding="UTF-8" ?>
    <project xmlns="http://maven.apache.org/POM/4.0.0">
      <modelVersion>4.0.0</modelVersion>
      <groupId>oracle.summit</groupId>
      <artifactId>Model</artifactId>
      <version>1.0-SNAPSHOT</version>
      <description>Generated POM from JDeveloper for project Model</description>
      <dependencies>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Model-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>BC4J-Oracle-Domains</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>BC4J-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>BC4J-Security</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>BC4J-Tester</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>MDS-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>MDS-Runtime-Dependencies</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Oracle-JDBC</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Resource-Bundle-Support</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Common-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Oracle-XML-Parser-v2</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JDeveloper-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>SQLJ-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Connection-Manager</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>BC4J-Service-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JAX-WS-Web-Services</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Java-EE</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
      </dependencies>
      <build>
        <sourceDirectory>src/</sourceDirectory>
        <resources>
          <resource>
            <directory>${basedir}</directory>
            <includes>
              <include>*</include>
            </includes>
          </resource>
        </resources>
        <outputDirectory>classes/</outputDirectory>
        <plugins>
          <plugin>
            <groupId>com.oracle.adf.plugin</groupId>
            <artifactId>ojmake</artifactId>
            <version>12.2.1-0-0</version>
            <configuration>
              <ojmake>
                ${oracleHome}/jdeveloper/jdev/bin/ojmake.exe
              </ojmake>
              <files>
                ${basedir}/Model.jpr
              </files>
            </configuration>
            <executions>
              <execution>
                <phase>compile</phase>
                <goals>
                  <goal>compile</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>com.oracle.adf.plugin</groupId>
            <artifactId>ojdeploy</artifactId>
            <version>12.2.1-0-0</version>
            <configuration>
              <ojdeploy>
                ${oracleHome}/jdeveloper/jdev/bin/ojdeploy.exe
              </ojdeploy>
              <workspace>
                ${basedir}/../SummitADF_Core.jws
              </workspace>
              <project>
                Model
              </project>
              <profile>
                Summit_Interface
              </profile>
              <outputfile>
                ${project.build.directory}/${project.build.finalName}.${project.packaging}
              </outputfile>
            </configuration>
            <executions>
              <execution>
                <phase>package</phase>
                <goals>
                  <goal>deploy</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      <parent>
        <groupId>com.oracle.adf</groupId>
        <artifactId>adf-parent</artifactId>
        <version>12.2.1-0-0</version>
        <relativePath></relativePath>
      </parent>
    </project>

<br/><br/>

Summit_ADF_Core_12_2_1_MAVEN2/ViewController/pom.xml

    <?xml version="1.0" encoding="UTF-8" ?>
    <project xmlns="http://maven.apache.org/POM/4.0.0">
      <modelVersion>4.0.0</modelVersion>
      <groupId>oracle.summit</groupId>
      <artifactId>ViewController</artifactId>
      <version>1.0-SNAPSHOT</version>
      <description>Generated POM from JDeveloper for project ViewController</description>
      <packaging>war</packaging>
      <dependencies>
        <dependency>
          <groupId>oracle.summit</groupId>
          <artifactId>Model</artifactId>
          <version>1.0-SNAPSHOT</version>
          <type>jar</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JSP-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Page-Flow-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Controller-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Controller-Schema</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Faces-Runtime-11</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Common-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Web-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>MDS-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>MDS-Runtime-Dependencies</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Commons-Beanutils</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Commons-Logging</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Commons-Collections</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-DVT-Faces-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-DVT-Faces-Databinding-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JPS-Designtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Oracle-JEWT</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Faces-Databinding-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Common-Web-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Oracle-Extended-Page-Templates</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-DVT-Faces-Databinding-MDS-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Oracle-Page-Templates</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>compile</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>SQLJ-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JSTL</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Resource-Bundle-Support</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Resource-Bundle-Variable-Resolver</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JSTL-Tags</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Oracle-XML-Parser-v2</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JDeveloper-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>ADF-Model-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>BC4J-Runtime</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Oracle-JDBC</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Connection-Manager</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>BC4J-Oracle-Domains</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>JSF</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
        <dependency>
          <groupId>com.oracle.adf.library</groupId>
          <artifactId>Apache-Http-Components</artifactId>
          <version>12.2.1-0-0</version>
          <type>pom</type>
          <scope>provided</scope>
        </dependency>
      </dependencies>
      <build>
        <sourceDirectory>src/</sourceDirectory>
        <resources>
          <resource>
            <directory>${basedir}</directory>
            <includes>
              <include>*</include>
            </includes>
          </resource>
        </resources>
        <outputDirectory>classes/</outputDirectory>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-war-plugin</artifactId>
            <version>2.4</version>
            <configuration>
              <webappDirectory>
                public_html/
              </webappDirectory>
            </configuration>
          </plugin>
          <plugin>
            <groupId>com.oracle.adf.plugin</groupId>
            <artifactId>ojmake</artifactId>
            <version>12.2.1-0-0</version>
            <configuration>
              <ojmake>
                ${oracleHome}/jdeveloper/jdev/bin/ojmake.exe
              </ojmake>
              <files>
                ${basedir}/ViewController.jpr
              </files>
            </configuration>
            <executions>
              <execution>
                <phase>compile</phase>
                <goals>
                  <goal>compile</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>com.oracle.adf.plugin</groupId>
            <artifactId>ojdeploy</artifactId>
            <version>12.2.1-0-0</version>
            <configuration>
              <ojdeploy>
                ${oracleHome}/jdeveloper/jdev/bin/ojdeploy.exe
              </ojdeploy>
              <workspace>
                ${basedir}/../SummitADF_Core.jws
              </workspace>
              <project>
                ViewController
              </project>
              <profile>
                SummitADF_ViewController_webapp1
              </profile>
              <outputfile>
                ${project.build.directory}/${project.build.finalName}.war
              </outputfile>
            </configuration>
            <executions>
              <execution>
                <phase>package</phase>
                <goals>
                  <goal>deploy</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      <parent>
        <groupId>com.oracle.adf</groupId>
        <artifactId>adf-parent</artifactId>
        <version>12.2.1-0-0</version>
        <relativePath></relativePath>
      </parent>
    </project>

В общем я выпиливал из проекта следующие dependency.

    <dependency>
       <groupId>com.oracle.adf.library</groupId>
       <artifactId>BC4J-Tester</artifactId>
       <version>12.2.1-0-0</version>
       <type>pom</type>
       <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.oracle.adf.library</groupId>
      <artifactId>BC4J-Service-Runtime</artifactId>
      <version>12.2.1-0-0</version>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.oracle.adf.library</groupId>
      <artifactId>SQLJ-Runtime</artifactId>
      <version>12.2.1-0-0</version>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>

После того, как заменял пути для ojmake и ojdeploy, compile на рабочей машине проходила. Но не package.

При этом использовася репо, которое поднимал другой человек и он использовал Archiva с дополнительной установкой по инструкции, где также использовались пакеты, которые входят дистрибутив jdeveloper.

А с моим нексусом, в котором прописал логин и пароль для доступа к Maven от Oracle, не работает. Пока думаю, из за того, что на нем нет следующей зависимости.

    <dependency>
      <groupId>com.oracle.adf.library</groupId>
      <artifactId>JDeveloper-Runtime</artifactId>
      <version>12.2.1-0-0</version>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>

Сами эти ojdeploy и ojmake, те, что не exe вообще какие-то bash скрипты, которые дергают еще что-то.
Будем разбираться дальше.

Жду толковых советов, или не очень толковых.
