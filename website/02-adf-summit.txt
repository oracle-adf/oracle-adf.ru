
<br/>
<br/>


### Буду делать по шагам. пока ковыряюсь с подпроектом Model

    $ cd Model/
    $ mvn compile -X


Выпиливаюсь из pom.xml следующие зависимости, т.к. в репо их нет, т.к. думаю, что jdeveloper при генерации pom файла пихает в них вообще все.


    <dependency>
       <groupId>com.oracle.adf.library</groupId>
       <artifactId>BC4J-Tester</artifactId>
       <version>12.2.1-0-0</version>
       <type>pom</type>
       <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.oracle.adf.library</groupId>
      <artifactId>BC4J-Service-Runtime</artifactId>
      <version>12.2.1-0-0</version>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.oracle.adf.library</groupId>
      <artifactId>SQLJ-Runtime</artifactId>
      <version>12.2.1-0-0</version>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>


<br/>


<packaging>jar</packaging>



<!--

    <dependency>
      <groupId>com.oracle.adf.library</groupId>
      <artifactId>JDeveloper-Runtime</artifactId>
      <version>12.2.1-0-0</version>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>


-->



<br/><br/>

Меняю в скрипте адреса до ojmake и ojdeploy:

    ojmake
    ojdeploy


<br/><br/>



<!--
<br/>

Далее, добавил следующие dependency.


    <dependency>
      <groupId>com.oracle.adf.plugin</groupId>
      <artifactId>ojmake</artifactId>
      <version>12.2.1-0-0</version>
    </dependency>

    <dependency>
      <groupId>com.oracle.adf.plugin</groupId>
      <artifactId>ojdeploy</artifactId>
      <version>12.2.1-0-0</version>
    </dependency>

-->



Добавляю output:  /home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/Model/target/Model-1.0-SNAPSHOT.jar


<plugin>
       <groupId>com.oracle.adf.plugin</groupId>
       <artifactId>ojdeploy</artifactId>
       <version>12.2.1-0-0</version>
       <configuration>
         <ojdeploy>
        /u04/oracle/jdeveloper/jdev/bin/ojdeploy
</ojdeploy>
         <workspace>
           ${basedir}/../SummitADF_Core.jws
         </workspace>
         <project>
           Model
         </project>
         <profile>
           Summit_Interface
         </profile>
         <outputfile>
           /home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/Model/target/Model-1.0-SNAPSHOT.jar
         </outputfile>
       </configuration>
       <executions>
         <execution>
           <phase>package</phase>
           <goals>
             <goal>deploy</goal>
           </goals>
         </execution>
       </executions>
     </plugin>



<br/><br/>

directory

<br/><br/>

     <build>
         <sourceDirectory>src/</sourceDirectory>
         <resources>
           <resource>
             <directory>/home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/Model/</directory>
             <includes>
               <include>*</include>
             </includes>



<br/><br/>


 <files>
            /home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/Model/Model.jpr
          </files>


<br/><br/>

<workspace>
         /home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/SummitADF_Core.jws
       </workspace>


<br/>

       $ mvn package -X


<br/>

Завершился ошибкой, что-то deploy


<br/>
<br/>


### Буду делать по шагам. пока ковыряюсь с подпроектом ViewController

    $ cd ViewController/
    $ mvn compile -X




    <dependency>
      <groupId>com.oracle.adf.library</groupId>
      <artifactId>SQLJ-Runtime</artifactId>
      <version>12.2.1-0-0</version>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>


ссылки на ojmake и ojdeploy


/home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/ViewController/target/ViewController-1.0-SNAPSHOT.war




/home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/ViewController/

/home/xxx/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/ViewController/ViewController.jpr


<br/>
<br/>


### Буду делать по шагам. пока ковыряюсь с главным pom

    $ mvn compile -X



ссылки на ojmake и ojdeploy


/home/rim/yyyyy/Summit_ADF_Core_12_2_1_MAVEN2/target/Summit_ADF_Core_12_2_1_MAVEN2-1.0-SNAPSHOT.ear




    $ mvn compile -X



<br/>

### Добавляю Plugin для деплоя на weblogic:

    <plugin>
     <groupId>com.oracle.weblogic</groupId>
      <artifactId>weblogic-maven-plugin</artifactId>
       <configuration>
        <adminurl>t3://localhost:7001</adminurl>
        <user>weblogic</user>
        <password>weblogic1</password>
        <upload>true</upload>
        <action>redeploy</action>
        <remote>false</remote>
        <verbose>true</verbose>
        <source>MYAPP-1.0-SNAPSHOT.ear</source>
       <name>MYAPP-1.0-SNAPSHOT</name>
       <targets>AdminServer</targets>
    </configuration>
    <executions>
       <execution>
          <phase>install</phase>
            <goals>
              <goal>redeploy</goal>
            </goals>
       </execution>
     </executions>
    </plugin>







Update your JDBC settings
https://blogs.oracle.com/blueberry/entry/using_the_oracle_public_maven


### Добавил distributionManagement



<distributionManagement>
   <repository>
     <uniqueVersion>false</uniqueVersion>
     <id>dev</id>
     <name>Dev</name>
     <url>http://soatst.mycompany.local:8081/repository/dev/</url>
     <layout>default</layout>
   </repository>
   <snapshotRepository>
     <uniqueVersion>true</uniqueVersion>
     <id>archiva-snapshots</id>
     <name>Archiva Snapshots</name>
     <url>http://soatst.mycompany.local:8081/repository/snapshots</url>
     <layout>default</layout>
   </snapshotRepository>
 </distributionManagement>




 <br/> <br/>


 Introduction to Building Oracle ADF Projects with Maven
 https://docs.oracle.com/middleware/1221/core/MAVEN.pdf


 <br/>

     mvn compile
     This command runs the ojmake plug-in


     To package the project into an EAR file, execute the following command (note that this
     actually runs all steps up to package, including the compile again):


     mvn package
     This command runs the ojdeploy plug-in.



<br/> <br/>
Introducing Maven Support in JDeveloper 12c - ADF Insider







Automating Deployment of the Summit ADF Sample Application to the Oracle Java Cloud Service:

https://wbrianleonard.wordpress.com/2015/06/04/automating-deployment-of-the-summit-adf-sample-application-to-the-oracle-java-cloud-service/




http://adfhowto.blogspot.ru/2011/03/ojdeploy-deploying-adf-application-from.html





























==================================================================
==================================================================
==================================================================

    # chown -R weblogic12 /u01/
    # chown -R weblogic12 /u02/


<br/>

    $ vi ~/.bash_profile


<br/>

wget --no-check-certificate --no-cookies - --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u91-b14/jdk-8u91-linux-x64.tar.gz




#### JAVA 1.8.0 #######################

        export JAVA_HOME=/opt/jdk/current
        export PATH=${JAVA_HOME}/bin:$PATH

#### JAVA 1.8.0 #######################


    ########################
    # Weblogic Parameters

    ### MW_HOME  - Middelware Home
    ### WL_HOME - Weblogic Server Home

    	export MW_HOME=/u01/app/oracle/weblogic/12.1
        export WL_HOME=${MW_HOME}/wlserver_12.1

    ########################


<br/>

    $ source ~/.bash_profile

<br/>


    # yum install -y libXrender libXtst


    $ export DISPLAY=192.168.99.1:0.0

    $ java -jar fmw_12.2.1.1.0_wls.jar



    /u01/app/oracle/12.1/oracle_common/common/bin/
