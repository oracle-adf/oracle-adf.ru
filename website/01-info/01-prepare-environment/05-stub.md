---
layout: page
title: Создание заглушки для разраотки приложения
description: Создание заглушки для разраотки приложения
keywords: Oracle ADF, Создание заглушки для разраотки приложения
permalink: /prepare-environment/stub/
---

# [Oracle ADF] Создание "заглушки" для разраотки приложения

P.S. Если действительно кому-нибудь будет нужно, я опишу процедуру более подробно !!!

<br/>

1. Создаем новое приложение "Fusion Web Application (ADF)"<br/>

<br/>

2. Создаем профиль:

web.xml - правой кнопкой и выбираем "Create War Deployment Profile" с именем "view"

На вкладке "General" выбираем "Specify java EE Web Context Root:" /view

<br/>

3. View

ViewController --> Project Properties

Deployment --> Удаляем старый профиль. Оставляем view.

<br/>

4. Edit EAR Deployment Profile Properties

Deployment --> Текущий удаляем.

Создаем новый:

type: ear
name: app1

"Application name":
project-name-project-version

На вкладке "Application Assambly":
выбираем в дереве "ViewContorller.jpr"

-   view

<br/>

5. ViewController --> Web Content --> WEb-INF --> Кликаем adfc-config.xml<br/>
   Перетаскиваем на холст элемент view из панели Componet Pallete.<br/>

Кликаем по view

Name: view1
Document Type: Facelet<br/>
BlankPage<br/>
OK

<br/>

6. Редактируем web.xml. <br/>
   Делаем возможность подключаться к приложению по адресу /view а не /view/faces/view1.jsf

Добавляем следующий код:

      <welcome-file-list>
        <welcome-file>faces/view1.jsf</welcome-file>
      </welcome-file-list>

<br/>

7. Подключаемся к браузеру и убеждаемся, что стартует заглушка ADF.

http://localhost:7001/view/
