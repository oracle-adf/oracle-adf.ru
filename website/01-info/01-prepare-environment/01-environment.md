---
layout: page
title: Подготовка окружения для программирования с использованием Oracle ADF
description: Подготовка окружения для программирования с использованием Oracle ADF
keywords: Oracle ADF, подготовка окружения
permalink: /prepare-environment/environment/
---

# [Oracle ADF] Подготовка окружения для программирования с использованием Oracle ADF

<br/><br/>
<strong>Дистрибутивы Jdeveloper (включая сервер Weblogic для разработки):</strong><br/>
http://www.oracle.com/technetwork/developer-tools/jdev/downloads/index.html

<strong>Jdeveloper и Linux</strong>

Для инсталляции JDeveloper в <strong>Ubuntu Linux x64</strong>, необходимо предварительно инсталлировать следующие компоненты:<br/>

    # apt-get install -y ia32-libs

Для запуска jdeveloper в Linux, необходимо запустить на выполнение следующий файл:

    /home/${user_home}/Oracle/Middleware/jdeveloper/jdev/bin/jdev

Для запуска стартового окна приветствия (quickstart), выполните следующий файл:<br/>

    /home/${user_home}/Oracle/Middleware/utils/quickstart/quickstart.sh

Для старта домена weblogic:

    /home/${user_home}/Oracle/Middleware/user_projects/domains/${domain_name}/startWebLogic.sh

Можно также задать переменную <strong>JDEV_USER_DIR</strong>, где будут храниться пользовательские проекты по умолчанию:<br/>

<br/>

<strong>Инструкции по инсталляции сервера баз данных Oracle 11.2 в различных операционных системах:</strong><br/>
https://oracle-dba.ru/<br/>

<strong>Инструкция по инсталляции сервера приложений Oracle Weblogic 12 в операционной системе Oracle Linux</strong> (подготовка сервера для размещения уже готового приложения): <br/>
https://docs.google.com/document/d/11vBXmPQzIx2KnxuurIdVImCvyqwGca_u0j4gKmlVJuc/edit<br/>

<br/>

Если при деплое возникает ошибка "Error while processing library references." <br/>
На сервере отсутствуют необходимые библиотеки.

<br/>
Если это новое приложение ADF, можно попробовать проапгрейдить домен и выбрать элементы JRF.

<br/>

### Подготовленная виртуальная машина VirtualBox для изучения Oracle ADF и разбора примеров:

<br/>

<strong>Включает в себе:</strong><br/><br/>

<ul>
	<li>Oracle Enterprise Linux 5 Update 5</li>
	<li>Oracle Database 11g Release 2 Enterprise Edition (11.2.0.1)</li>
	<li>JDeveloper (11.1.1.4)</li>
</ul>

http://www.oracle.com/technetwork/community/developer-day/vbox-jdev-only-410835.html
