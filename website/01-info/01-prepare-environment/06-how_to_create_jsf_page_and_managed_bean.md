---
layout: page
title: Создание странички JSF и связанного с ней Бина
description: Создание странички JSF и связанного с ней Бина
keywords: Oracle ADF, Создание странички JSF и связанного с ней Бина
permalink: /prepare-environment/how-to-create-jsf-page-and-managed-bean/
---

# [Oracle ADF] Создание странички JSF и связанного с ней Бина

<br/>
<br/>

<div align="center">
    <iframe width="640" height="480" src="http://www.youtube.com/embed/XHpct8ijTWo" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

1. Создание JSF странички.

Web Content --> New

web Tier --> JSF --> JSF Page --> "customer.jsf"

<br/>

2. Создание Бина.

Web Content --> New

General --> Java Class --> имя создаваемого класса "Customer"

<br/>

3. adfc-config.xml

Diagram --> "customer.jsf" перетаскиваем на диаграмму
Owerview --> Managed Beans

Добавляем в Managed Beans
Name: CustomerBean
Class: ищем Customer
Scope: request
