---
layout: page
title: Настройка JDeveloper для работы с кодировкой UTF-8
description: Настройка JDeveloper для работы с кодировкой UTF-8
keywords: Oracle ADF, Jdeveloper, UTF-8
permalink: /prepare-environment/how-to-configure-jdeveloper-to-support-utf8-encoding/
---

# [Oracle ADF] Настройка JDeveloper для работы с кодировкой UTF-8

Не печатались русские буквы в консоли Weblogic.

<br/>

Шаг 1: <br/>
Tools --> Environment --> Encoding: UTF-8

Шаг 2: <br/>
На каждом из проектов в приложении: <br/>
Правой кнопкой мыши --> Project Properties --> Compiler --> Character Encoding: UTF-8

Шаг 3: <br/>
После того, как шаги 1 и 2 выполнены, заменить текст на странице текстом в кодировке UTF-8<br/>

<br/><br/>

См. также:<br/>
http://docs.oracle.com/cd/E17984_01/doc.898/e14693/appa_configuring_jdev.htm
