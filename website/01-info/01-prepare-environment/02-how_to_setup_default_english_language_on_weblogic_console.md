---
layout: page
title: Установка английского языка по умолчанию для консоли weblogic
description: Установка английского языка по умолчанию для консоли weblogic
keywords: Oracle ADF, Weblogic console, язык
permalink: /prepare-environment/how-to-setup-default-english-language-in-the-weblogic-console/
---

# [Oracle ADF] Установка английского языка по умолчанию для консоли weblogic (Речь идео о обычном сервере не встроенном)

Собственно, в консоли печатаются ошибки. Если они написаны на русском языке и вам понятно в чем причина, то это конечно хорошо.

Но если непонятно, то вам помогут (скорее всего только индусы). А они к сожалению, могут говорить (в большинстве своем) только на 2-х языках. Скорее всего, индусский вы не знаете.

Собственно на работе мне приходится работать за локальным сервером weblogic под windows.

Чтобы сервер нормально отображал русские буквы и сообщения об ошибках печатались на английском, я сделал следующее:

1.  <WEBLOGIC_HOME>\user_projects\domains\<USER_DOMAIN >\domain_name.bat

Добавил

    SETLOCAL
    chcp 1251

2. Поменял шрифт в консоли на Cucida Console

3.

В файле

    <WEBLOGIC_HOME>\user_projects\domains\<USER_DOMAIN >\bin\startWebLogic.cmd

Добавил ключ -Duser.country=US -Duser.language=en

<!--

    Так было написано у меня ранее

    set JAVA_OPTIONS=%SAVE_JAVA_OPTIONS% -Duser.country=US -Duser.language=en
    set SAVE_JAVA_OPTIONS=

-->

    set JAVA_OPTIONS=%JAVA_OPTIONS% -Duser.country=US -Duser.language=en
    set SAVE_JAVA_OPTIONS=%JAVA_OPTIONS%


    echo ***************************************************
    echo ** JAVA_OPTIONS=%JAVA_OPTIONS%
    echo ***************************************************

Под Linux, редактировать нужно файл setWLSEnv.sh
