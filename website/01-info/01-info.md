---
layout: page
title: Для информации
description: Для информации
keywords: Oracle ADF, Для информации
permalink: /info/
---

# Для информации

<ul>
    <li><a href="/foreword/"><strong>Немного информации</strong></a></li>
</ul>

<br/>

### Бесплатная версия Oracle ADF (Бесполезная игрушка)

<ul>
    <li><a href="/oracle-adf-free-version/"><strong>Бесплатная версия Oracle ADF (Glassfish)</strong></a></li>
</ul>

<br/>

### Сокращения и термины:

<ul>
    <li><a href="/terms/"><strong>Сокращения и термины</strong></a></li>
</ul>

<br/>

### Подготовка окружения:

<ul>
	<li><a href="/prepare-environment/environment/">Подготовка окружения для программирования с использованием Oracle ADF</a></li>
    <li><a href="/prepare-environment/how-to-setup-default-english-language-in-the-weblogic-console/">Установка английского языка по умолчанию для консоли weblogic</a></li>
	<li><a href="/prepare-environment/how-to-configure-jdeveloper-to-support-utf8-encoding/">Настройка JDeveloper для работы с кодировкой UTF-8</a></li>
	<li><a href="/apps/sample-adf-application/">Пример развертывания демонстрационного приложения</a></li>
	<li><a href="/prepare-environment/stub/">Создание "заглушки" для разработки приложения</a> (Скорее всего устарело для 12 версии)</li>
	<li><a href="/prepare-environment/how-to-create-jsf-page-and-managed-bean/">Создание странички JSF и связанного с ней Бина</a></li>

    <li><a href="https://blogs.oracle.com/shay/entry/shorter_url_for_your_adf">Shorter URL for your ADF application</a></li>

</ul>

<br/>

### Основы:

<ul>
	<li><a href="/basics/datacontrol-databindings-and-pagedef/">DataControl.dcx, DataBindings.cpx и PageDef.xml</a></li>
	<li><a href="/basics/bindings/">Привязки к данным (Data Bindings)</a></li>
	<li><a href="/basics/iterator/">Итератор (Iterator)</a></li>
</ul>
