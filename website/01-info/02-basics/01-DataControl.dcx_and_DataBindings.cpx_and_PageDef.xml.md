---
layout: page
title: DataControl.dcx, DataBindings.cpx и PageDef.xml
description: DataControl.dcx, DataBindings.cpx и PageDef.xml
keywords: Oracle ADF, DataControl.dcx, DataBindings.cpx, PageDef.xml
permalink: /basics/datacontrol-databindings-and-pagedef/
---

# DataControl.dcx, DataBindings.cpx и PageDef.xml

<strong>DataControl.dcx</strong> - В этом файле определяются все компоненты data control используемые в данном проекте, которые реализованы не через ADF Business Components.

<strong>&lt;pagename&gt;PageDef.xml</strong> – определяет привязку к данным для конкретной страницы проекта. Для каждой страницы существует только одни файл PageDef.xml. Если никаких операций с привязками не определено, этого файла может и не быть. Для создания такого файла, достаточно выбрать страницу (jspx, jsf), нажать правой кнопкой мыши и выбрать "go to the definition file".

<strong>DataBindings.cpx </strong> (Bindings Master File) - файл в котором определяется, какой странице, какой файл PageDef.xml соответствует. Все страницы проекта, которые работают с файлами PageDef.xml отражаются в этом файле. Он также определяет компоненты data control используемы в проекте.
<br/><br/>

<br/>
<strong>Структура файла PageDef</strong>

Все, что определено в секции executables, будет исполняться при загрузке страницы, причем последовательно сверху вниз в том порядке, в котором они записаны в файле.

Секция executables (исполняемые элементы) файла PageDef может содержать элементы: parameters (параметры), iterators (итераторы) или invokeAction (вызов действия).

<ul>
	<li>parameters - используются, если нужно задать значение какого-нибудь параметра, который потом можно считать, например следующей командой #{bindings.parameterName}.</li>
	<li>iterators - по сути тоже, что и итератор в цикле, только в данном случае он указывает на элемент в коллекции данных.</li>
	<li>invokeAction - вызывает существующую привязку действия или метода.</li>
</ul>

<br/>
<strong>Присвоение значения параметрам </strong>

Пример параметра: <br/>
<br/>

    <parameter id="status"
               value="${param.status}" />

Если страница вызывается с помощью URL mypage.jspx?status=open, параметру status будет присвоено значение open.

bindings - всевозможные привязки элементов<br/>
view instances - ссылки на эклемпляры коллекций объектов, построенных на (Entity Objects, View Objects).
