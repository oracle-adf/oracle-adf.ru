---
layout: page
title: Итератор (Iterator)
description: Итератор (Iterator)
keywords: Oracle ADF, Iterator
permalink: /basics/iterator/
---

# Итератор (Iterator)

Итератор – нужен для того, чтобы всевозможные привязки получили данные из набора данных, а итератор указывает, какой текущий элемент выбран.

Для одного и того же набора данных, можно создать несколько итераторов.

Итераторы определяются в файле PageDef в разделе executables (исполняемые элементы) перед разделом описывающим привязки.

    <executables>
     <iterator
        id="voMy1Iterator"
        Binds="voMy1"
        DataControl="AppModuleDataControl"/>
    </executables>
