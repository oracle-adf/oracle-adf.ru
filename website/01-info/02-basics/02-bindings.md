---
layout: page
title: Привязки к данным (Data Bindings)
description: Привязки к данным (Data Bindings)
keywords: Oracle ADF, bindings
permalink: /basics/bindings/
---

# Привязки к данным (Data Bindings)

Вся необходимя информация для привязки данных к компонентам на странице содержится в файле PageDef.

Для просмотра привязок достаточно открыть страницу на редактирование и переключиться на вкладку bindings.

Или можно выбрать страницу, нажать на ней правой кнопкой мыши и выбрать “Go To Page Definition”.

<br/>

**Типы привязок:**

<ul>
    <li>Привязки атрибутов – используются для привязывания отдельного атрибута в наборе данных.</li>
    <li>Привязки таблиц – используются для определения содержимого таблиц. Связанных с наборами данных.</li>
    <li>Привязи действий – используются для определния привязки стандартных операций, таких как Commit.</li>

    <li>Привязки методов – подоны привязкам действий, за исключением того, что используются для пользовательских методов, а не для стандартных операций.</li>
    <li>Привязки списков – используются для определения содеримого списочных компонентов, связанных с данными, таких как группы кнопок-переключателей (radio group) или ниспадающие списки.</li>
    <li>Привязки навигационных списков – используются для манипулировании и идентификации теущей строки в наборе строк.</li>
    <li>Булевы привязки – используются для определения набора улевых опций. Такие привязки используюются на страницах JSF для флагов.</li>
    <li>Привязки деревьев – определяют набор данных типа “master-detail” для заполнения иерархических компонентов типа деревьев или табличных деревьев.</li>

</ul>

<br/>

### Привязка таблиц:

Привязка таблиц похожа на привязку атрибутов, за исключением того, что каждая отдельная привязка инкапсилирует в себе несколько атрибутов для отображения, а не один.

    <table id="EmployeeUpdateable1"
              IterBinding="employeeUpdateable1Iterator">

        <AttrNames>
          <Item Value="EmplyeeId"/>
          <Item Value="FirstName"/>
          <Item Value="LastName"/>
          <Item Value="Email"/>
          <Item Value="PhoneNumber"/>
          <Item Value="HireDate"/>
          <Item Value="JobId"/>
          <Item Value="ManagerId"/>
          <Item Value="DepartmentId"/>

        </AttrNames>
    </table>

<br/>

Следующий фрагмент кода интерфейса пользователя показыает часть табличного копонента, основанного на наборе данных EmployeeUpdateable1.

<br/>

Для обращения к атриутам используется псевдоним row, определяемый атрибом var тега таблицы, который по умолчанию означает текущую строку. Например, выражение #{row.FirstName}, используемое в контексе таблицы, ссылается на атрибут FirstName текущей строки таблицы.

    <af:table value="#{bindings.EmployeeUpdateable1.collectionModel}">
              var="row"
              rows="#{bindings.EmployeeUpdateable.rangeStart}"
              emptyText="#{bindings.EmployeeUpdateable.viewable?, No row yet':'Accesss Dinied'}"
              selectionState="#{bindings.EmployeeUpdateable.collectionModel.selectedRow}"
              selectionListener="#{bindings.EmployeeUpdateable.collectionModel.makeCurrent}">

    <af:column sortProperty="FirstName"
              headerText="#{bindings.EmployeeUpdateable.labels.FirstName}">

    <af:inputText
        value="#{row.FirstName}"
        simple="true"
        required="#{bindings.EmployeeUpdateable.attrDefs.FirstName.mandatory}"
        columns="#{bindings.EmployeeUpdateable.attrHints.FirstName.displayWidth}"/>

    </af:column>

<strong>Атрибуты привязки таблицы доступные в языке выражений</strong>

<table>

<tr>
    <td><strong>Атрибут привязки таблицы</strong></td>
    <td><strong>Использование атрибута</strong></td>
</tr>

<tr>
    <td>collectionModel</td>
    <td>приводит привязку таблицы кформе, которую может использовать любая JSF-таблица. Объект collectionModel содержит дополнительные свойства и функции, например, свойство collectionModel.selectdRow, которое помогает таблице выделить текущую строку, и функцию collection.makeCurrent, которая синхонизирует итератор и строку, выделенную в таблице пользователем.</td>
</tr>

<tr>
    <td>rangeSize</td>
    <td>сколько строк должно отображаться в таблице за одни раз. Он напрямую связат с атрибутом RangeSize итератора, который используется привязкой.</td>
</tr>

<tr>
    <td>rangeStart</td>
    <td>основываясь на информации, предоставленной итератором, этот атрибут указыает талице, какая запись набора данных сейчас отображается в верхней строке таблицы.</td>
</tr>

<tr>
    <td>viewable</td>
    <td>если к набору данных применена система безопасности, это свойство указывает может ли данных пользователь видеть данные из набора.</td>
</tr>

<tr>
    <td>labels</td>
    <td>большинство управляющих подсказок, определенных в объекте представления или объекте сущности, доступны в языке выражений. Примером этого является атрибут labesl. Он представляет собой Map (список, значения которого индексируется с помощью имени атрибута) меток для атриутов данных. выражение  #{bindings.EmployeeUpdateable.labels.FirstName} означает метку атрибута FirstName. Эта метка будуте взята из управляющей подсказкп LabelText, если подсказка не установлена, будет взято имя атрибута. Привязки атрибутов также поддерживают свойство label (единичное), которое предоставляет ту же информацию в виде одного значения (в отличие от списка меток).</td>
</tr>

<tr>
    <td>attrDefs</td>
    <td>предоставляет более общий доступ к управлеяющим подсказкам интерфейса пользователя. Подобно атрибуту labels он указыает на список типа Map, индексированный по имени столбца. Объект attrDefs может быть использован дл получения подсказок интерфейса пользователя, таких как displayWidth и mandatory.</td>
</tr>

<tr>
    <td>rangeSet</td>
    <td>похож на атрибут collectionModel, но в отличие от послденего, не приоводит привязку таблицы к форме, приемлемой для JSF-талицы, а предоставляет строки связываемого набора данных в виде массива, который может быть использован в циклах af:forEach.</td>
</tr>
</table>

<br/>

### Привязка действий (Action):

<strong>Операции уровня data control</strong>

Операции уровня data control доступны для компонентов ADF BC или других компонентов, у который значение свойства "supportsTransactions" в файле DataControls.dcx равно "true".

Пример привязки (в файле PageDef) действия уровня data control для операции Commit:

    <action id="commit"
        InstanceName = "HRServiceDataControl"
        DataControl = "HRServiceDataControl"
        RequiresUpdateModel="true"
        Action="100" />

id - идентификатор. Используется при привязке операции к элементу интерфейса.<br/>
InstanceName - значение этого свойства должно совпадать с именем компонента data control.<br/>
DataControl - определяет компонент data control, к которому применяется данная операция.<br/>
RequiresUpdateModel - булево значение, указывает должны ли какие-лио изменения на странице применяться к уровню Model до выполнения операции.<br/>
Action - числовой код, указыающий, что делать. Каждая операция имеет уникальный код. 100 - Commit; 101 - Rollback.<br/>

<br/>
<table>

<tr>
    <td><strong>Операция</strong></td>
    <td><strong>Описание</strong></td>
</tr>

<tr>
    <td>Commit</td>
    <td>указывает компоненту data control на необходимость выполнить операцию COMMIT в базе данных для сохраненния данных транзакции</td>
</tr>

<tr>
    <td>Rollback</td>
    <td>выполняет операцию ROLLBACK в базе данных для отмены всех изменений, сделанных после последней операции COMMIT. Эта операция также очищает буферы записей в ADF Business Components и сбрасывает итераторы на первую запись.</td>
</tr>

</table>

<br/>
<strong>Операции с наборами данных</strong>

Большинство операций связано с наборами данных или, строго гоовря, с итераторами.

    <action id="First"
        RequiresUpdateModel="true"
        Action="12"
        IterBinding="EmployeesUpdateableIterator" />

<br/><br/>

<table>

<tr>
<td><strong>Операция</strong></td>
<td><strong>Описание</strong></td>
</tr>

<tr>
<td>Create (41)</td>
<td>Эта операция открывает слот для новой записи в наборе данных. Новая запись добавляется в набор данных при отсылке их со страницы на сервер (submit). Следовательно, если пользователь отменит ввод информации, просто покинув страницы, пустая запись не останется "висящей в воздухе". Новая запись, открываемя операцией Create, вставляется перед текущей записью итератора.</td>
</tr>

<tr>
<td>CreateInsert (40)</td>
<td>Эта операция подобна Create, за исключением того, что при ее выполнении новая пустая запись вставляется в набор данных. Это означает, что даже если пользователь никогда не подтвердит отсылку данных со страницы, строка все же будет создана в наборе данных. В отличие от create и других опреаций, операция CreateInsert не показана в узле Operations палитры Data Control Palette. Привязка этой операции может ыть осущствлена только вручную в файле PageDef.</td>
</tr>

<tr>
<td>Delete (30)</td>
<td>удаляет текущую строку из набора данных. Она подобна операции removeRowWithKey.</td>
</tr>

<tr>
<td>Execute (2 или 0 )</td>
<td>обновляет набор данных. Она подобна повторному исполненнию запроса к базе данных, хотя и работает с данными буфера промежуточного уровня, а не с самой базой.</td>
</tr>

<tr>
<td>ExecuteWithParams (95)</td>
<td>Эта операция доступна, если исходный объект представления содержит ссылки на переменные связывания. Операция ExeucteWithParams объединяет в себе выполенение задач присвоения значения параметров и оновления набора данных.</td>
</tr>

<tr>
<td>Find (3 или 1)</td>
<td>переводит набор данных в режим Query-By-Example (QBE). Операции Create и Delete в режиме поиска добавляют и удаляют элементы из списка критериев запроса, а не добавляют и удаляют строки.</td>
</tr>

<tr>
<td>First (12)</td>
<td>устанавливает текущую строку, как она опредлеена итератором, в начало набора данных.</td>
</tr>

<tr>
<td>Last (13)</td>
<td>устанавливает текущую строку в конец набора данных.</td>
</tr>

<tr>
<td>Next (10)</td>
<td>перемещает текущую строку на одну позицию вниз.</td>
</tr>

<tr>
<td>Next Set (14)</td>
<td>перемещает текущую строку вниз на число строк, указанное в свойстве итератора RangeSize.</td>
</tr>

<tr>
<td>Previous (11)</td>
<td>перемещает текущую строку на одну позицию вверх.</td>
</tr>

<tr>
<td>Previous Set (15)</td>
<td>перемещает текущую строку вверх на число строк, указанное в свойстве итератора RangeSize.</td>
</tr>

<tr>
<td>removeRowWithKey (99)</td>
<td>Эта операция удаляет указанную строку. Параметром этой операции является идентификатор строки, генерируемый ADFm и доступный через атриут привязки rowKeyStr. Этот атриут всегда доступен при навигации по набору данных, подобно ROWNUM в запросе SQL.</td>
</tr>

<tr>
<td>setCurrentRowWithKey (96)</td>
<td>Подобно removeRowWithKey принимает в качестве параметра идентификатор строки и устанавливает фокус итератора на указанную строку. Удаления строки не происходит. </td>
</tr>

<tr>
<td>setCurrentRowWithKeyValue (98)</td>
<td>Эта операция является версией setCurrentRowWithKey, в которой в качестве параметра используется первичный ключ, а не генерируемый ADFm идентификатор. </td>
</tr>

<tr>
<td>&lt;custom methods&gt; (999)</td>
<td>создваемые вами в модуле приложения или классе Impl методы отображаются подобно операциям.</td>
</tr>

</table>

<br/><br/>
<strong>Привязка действий в интерфейсе пользователя.</strong>

    <af:commandButton actionListener="#{bindings.First.execute}"
        text="First"
        disabled="#{bindings.First.enabled}"

actionListener - код, который выполянется до основного действия, заданного в атрибуте action элемента commandItem.

    <af:commandButton actionListener="#{bindings.Commit.execute}"
        action="Home"
        text="Save and return" />

Сначала выполняется операция commit, затем осуществляется навигационное действие action.

<br/>
<strong>Операции с параметрами (execute with params)</strong>

Некоторые операции, наиболее важная из которых - ExecuteWithParams, принимают аргументы. Операции ExecuteWithParams принимают столько аргументов, сколько определено переменных связывания в SQL-запросе соответствующего объекта представления.

Создание формы с параметрами на странице создает привязку к операции в файле PageDef, при этом на странице также создается по полю ввода для каждого аргумента операции и кнопка для выполеннеия этой операции.

Для приимера возьмем операцию ExecuteWithParams для набора даных DepartmentList1. Объект представления имеет единственную переменную связыания :location. JSF- тери выглядят следующим образом.

    <af:inputText value="#{bindings.location.inputValue}"
        value="#{bindings.location.label}"
        required="#{bindings.location.mandatory}"
        columns="#{bindings.location.displayWidth}">

    <af:validator binding="#{bindings.location.validator}" />
    <f:convertNumber groupingUsed="false"
        pattern="#{bindings.location.format}" />

    </af:inputText>


    <af:commandButton actionListener="#{bindings.ExecuteWithParams.execute}"
        text="ExecuteWithParams"
        disabled="#{!bindings.ExecuteWithParams.enabled}" />

<br/>
Определение привязки в файле PageDef тега af:commandButton выглядит следующим образом:

    <action id="ExecuteWithParams"
       IterBinding="DepartmentList1Iterator"
       InstanceName="HRServiceDataControl.DepartmentList1"
       DataControl="HRServiceDataControl"
       RequiresUpdateModel="true"
       Action="95">

       <NamedData NDName="location"
                  NDType="oracle.jbo.domain.number"
                  NDValue="${bindings.DepartmentList1_location}" />

    </action>

Вы видите, что операция ExecuteWithParams имеет код операции 95, и что она привязана к итератору DepartmentList1Iterator. Кроме того, вы видите дочерний элемент тега action - элемент NamedData.

NamedData используется для определения аргументов, которые система framework должна передать операциям и методам, имеющим параметры. У этого элемента есть следующие атрибуты:

<ul>
    <li>NDName - этот атрибут означает имя аргумента. Присвоение имени важно в случае, если вы соираетесь программно задать значение аргумента до вызова связанного метода.</li>
    <li>NDType  - этот атрибут определяет тип данных аргумента.</li>
    <li>NDValue - это занчение, которое надо передать параметру. Оно может ыть задано литералом или, как в данном примере, выражением.</li>
</ul>

<br/>

Выражение ${bindings.DepartmentList1_location} привязывает значенине локальной переенной DepartmentList1_location к аргументу вызываемой операции. Таким образом мы видим связь между полем ввода в форме параметров и вызовом операции. Поле ввода устанавливает значение локальной переменной, а операция передает это значение на ровень модели.

<br/>

### Привязка методов:

Привязка методов почти идентична привязке действий, используемой для доступа к операциям. Единственное отличие между пользовательскими методами и операциями - методы могут возвращать результат. Этот результат может быть затем привязан к странице и обработан так же как оычный атрибут.

Пример XML кода в файле PageDef для привязки методов:

    <methodAction id="searchByLocationId"
        InstanceName="HRServiceDataControl.DepartmentList1"
        DataControl="HRServiceDataControl"
        MethodName="searchByLocationId"
        RequiresUpdateModel="true"
        Action="999"
        IsViewObjectMethod="true"
        IterBinding="DepartmentsList1Iterator">

      <NamedData NDName="locationId"
          NDValue="10"
          NDType="oracle.jbo.domain.number" />
    </methodAction>
