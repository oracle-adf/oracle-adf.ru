---
layout: page
title: Бесплатная версия Oracle ADF (Glassfish)
description: Бесплатная версия Oracle ADF (Glassfish)
keywords: Oracle ADF, Glassfish, Бесплатная версия
permalink: /oracle-adf-free-version/
---

# Бесплатная версия Oracle ADF (Glassfish)

Не так давно появилась бесплатная версия ADF, которая называется Essentials.
<br/>Она поддерживается сервером приложений Glassfish.<br/>
Подробности, можно попытаться найти на следующих ресурсах:<br/><br/>

<strong>Основная страница ADF Essentials:</strong><br/>
http://www.oracle.com/technetwork/developer-tools/adf/overview/adfessentials-1719844.html
<br/><br/>

<strong>FAQ на тему "что вошло, а что не вошло в Essentials":</strong>
<br/>
http://www.oracle.com/technetwork/developer-tools/adf/overview/adfessentialsfaq-1837249.pdf

The following functionality is not included in Oracle ADF Essentials, and requires the full Oracle ADF version – Oracle
ADF Mobile, Oracle ADF Desktop Integration, Oracle ADF Security, The Oracle ADF Web service data control, Oracle ADF remote taskflows, Oracle ADF Business Component’s Service Interfaces, Oracle ADF Data Controls for BI, Essbase and BAM, Integration with Oracle Fusion Middleware features such as MDS, OPSS, OWSM, Enterprise Manager and MBeans, High Availability and Clustering.

<a href="/how-to-install-oracle-adf-essentials-on-glassfish-server/"><strong>Установка Oracle ADF Essentials на сервер приложений GlassFish</strong></a>
