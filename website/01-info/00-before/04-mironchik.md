---
layout: page
title: Oracle ADF Мирончик Игорь о технологии Oracle ADF
description: Oracle ADF Мирончик Игорь о технологии Oracle ADF
keywords: Oracle ADF, Мирончик Игорь, видео, русский язык
permalink: /mironchik/
---

# [Oracle ADF] Мирончик Игорь о технологии Oracle ADF

Лично я познакомился с данной технологие именно из курсов Игоря. Как оказалось, позднее я устроился работать в компанию,
сотрудники которой посмотрели эти же курсы и выбрали данную технологию для реализации собственного проекта.

<br/><br/>

<div align="center">
	<iframe width="640" height="480" src="http://www.youtube.com/embed/0Gx17oi_CfE#t=4h44m28s" frameborder="0" allowfullscreen></iframe>
</div>

Для изучения ADF, рекомендуется смотреть курс, прочитанный Игорем в Перми.<br/>
Ссылку на курс вы сможете найти на сайте автора.
