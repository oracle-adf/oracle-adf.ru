---
layout: page
title: Сокращения и термины
description: Сокращения и термины
keywords: Oracle ADF, Сокращения и термины
permalink: /terms/
---

# Сокращения и термины

View Object (VO) - объект на основе SQL запроса к базе<br/>
Entity Object (EO) - объект на основе java класса сущности (entity), который маппируется на данные в базе данных.<br/>
List of Values (LOV)<br/>
Application Modules (AM)<br/>

Associations - определяют отношения между Entity Object.<br/>
View links - используются для создания отношений master --> multiple detail и отношения cascading master-detail между View Objects.<br/>

Expression language (EL)<br/>

Query-By-Example (QBE)

<br/><br/>
<strong>binding container</strong> - это формальное название набора привязок на данной странице. Все привязки, определенные в одном файле PageDef, находятся в одном контейнере привязок. При построении интерфейса пользователя в JSF, как правило, существует однозначное соответствие между страницей и контейнером привязок.

ExternalContext<br/>
FacesContext <br/>

<!-- View Accessor - пока не знаю да и не особо актуально это для меня -->
